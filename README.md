Tutorial for Quantum-Inspired Computing
=======================================

This tutorial introduces you to combinatorial optimization problems, how to describe them as a formula and finally solve them,
especially with Fujitsu's Digital Annealer.

It is based on the software development kit `dadk`. This is a python library that contains classes for creation and manipulation
of binary polynomials, access to Digital Annealer services, an emulation of the Digital Annealer annealing algorithm and a framework
for structured Jupyter notebooks for solving optimization tasks.


Library Installation
--------------------

We recommend using Python 3.10 and setting up a fresh virtual environment, e.g. using venv.

```bash
python -m venv dadk-310
```

Active the new virtual environment according to your operating system. For help on venv see [here](https://docs.python.org/3/tutorial/venv.html).

The library can be found in the folder `Software`, and can be installed using pip.

```bash
pip install -U Software\dadk-light.tar.bz2
```

Getting Started
---------------

The [User Guide](./Development_KIT_Documentation/user_guide/index.html) provides a quick but comprehensive overview
of dadk and its capabilities.

The extensive tutorial material (contained in the `Tutorial` folder) is best accessed using `jupyter notebooks`. 


Questions or feedback?
----------------------

Thank you very much for your interest in our tutorials. Please, don't hesitate to share your feedback and ideas with us: <digital.incubation@fujitsu.com>.
