Search.setIndex({
    "alltitles": {
        "API": [
            [12, "api"]
        ],
        "Advanced Usage": [
            [22, null]
        ],
        "Analyzing and Interpreting the Solution": [
            [16, "analyzing-and-interpreting-the-solution"]
        ],
        "Analyzing the Solution": [
            [19, "analyzing-the-solution"]
        ],
        "AzureBlobMixin": [
            [2, "azureblobmixin"]
        ],
        "AzureBlobMixin.Blob": [
            [2, "azureblobmixin-blob"]
        ],
        "AzureBlobMixin.ProgressBar": [
            [2, "azureblobmixin-progressbar"]
        ],
        "BQP": [
            [3, "bqp"]
        ],
        "Basic Usage": [
            [22, null]
        ],
        "BinPol - Binary polynomials": [
            [3, "binpol-binary-polynomials"]
        ],
        "BinPolGen": [
            [4, "binpolgen"]
        ],
        "Binary Polynomials": [
            [20, "binary-polynomials"]
        ],
        "BitArray": [
            [7, "bitarray"]
        ],
        "BitArrayShape": [
            [3, "bitarrayshape"]
        ],
        "Blob": [
            [2, "blob"],
            [2, "id10"],
            [2, "id26"]
        ],
        "BoltzmannSampling(SamplingBasis)": [
            [2, "boltzmannsampling-samplingbasis"]
        ],
        "Category(BitArrayShape)": [
            [3, "category-bitarrayshape"]
        ],
        "Choosing the Solver": [
            [16, "choosing-the-solver"],
            [19, "choosing-the-solver"]
        ],
        "Classmethods": [
            [0, "classmethods"],
            [0, "id2"],
            [0, "id4"],
            [0, "id6"],
            [0, "id8"],
            [1, "classmethods"],
            [1, "id2"],
            [1, "id6"],
            [1, "id10"],
            [1, "id14"],
            [1, "id18"],
            [1, "id25"],
            [1, "id29"],
            [1, "id33"],
            [2, "classmethods"],
            [2, "id1"],
            [2, "id17"],
            [3, "classmethods"],
            [3, "id61"],
            [7, "classmethods"],
            [7, "id4"]
        ],
        "CleanupAzureBlobStorage": [
            [5, "cleanupazureblobstorage"]
        ],
        "CleanupJobs": [
            [5, "cleanupjobs"]
        ],
        "Comparison of Annealing with and without Guidance Configuration": [
            [14, "comparison-of-annealing-with-and-without-guidance-configuration"]
        ],
        "ConnectionParameter": [
            [1, "connectionparameter"],
            [1, "dadk-internal-connectionparameter-connectionparameter"]
        ],
        "ConnectionParameterWithAzureBlob(ConnectionParameter)": [
            [1, "connectionparameterwithazureblob-connectionparameter"]
        ],
        "Constant Bits": [
            [17, "constant-bits"]
        ],
        "Constraints": [
            [13, null]
        ],
        "Contact": [
            [12, "contact"]
        ],
        "Container": [
            [2, "container"],
            [2, "id9"],
            [2, "id25"]
        ],
        "Content": [
            [11, "content"]
        ],
        "CoordinatesUtils": [
            [0, "coordinatesutils"],
            [0, "dadk-utils-coordinatesutils-coordinatesutils"]
        ],
        "DADK": [
            [23, "dadk"]
        ],
        "DADK and Fujitsu\u2019s Digital Annealer": [
            [21, null]
        ],
        "DAv2Parameter": [
            [1, "dav2parameter"],
            [1, "dadk-internal-dav2parameter-dav2parameter"]
        ],
        "DAv3Parameter": [
            [1, "dav3parameter"],
            [1, "dadk-internal-dav3parameter-dav3parameter"]
        ],
        "DAv3cParameter": [
            [1, "dav3cparameter"],
            [1, "dadk-internal-dav3cparameter-dav3cparameter"]
        ],
        "DAv4Parameter": [
            [1, "dav4parameter"],
            [1, "dadk-internal-dav4parameter-dav4parameter"]
        ],
        "DaoDecoder": [
            [0, "daodecoder"]
        ],
        "DaoEncoder(JSONEncoder)": [
            [0, "daoencoder-jsonencoder"]
        ],
        "DatetimePicker(WidgetBoundedIntArray)": [
            [5, "datetimepicker-widgetboundedintarray"]
        ],
        "Define polynomial terms": [
            [3, "define-polynomial-terms"]
        ],
        "Deprecated": [
            [3, "deprecated"]
        ],
        "Determine Solver Settings": [
            [14, "determine-solver-settings"]
        ],
        "Determining a Guidance Configuration using a Greedy Approach": [
            [14, "determining-a-guidance-configuration-using-a-greedy-approach"]
        ],
        "Digital Annealer Development Kit": [
            [11, null],
            [12, null]
        ],
        "Digital Annealer V2": [
            [21, "digital-annealer-v2"]
        ],
        "Digital Annealer V3": [
            [21, "digital-annealer-v3"]
        ],
        "DirectoryManager": [
            [6, "directorymanager"]
        ],
        "DirectorySelector(Dropdown)": [
            [5, "directoryselector-dropdown"]
        ],
        "EXPERIMENTAL": [
            [3, "experimental"]
        ],
        "Enums": [
            [3, "enums"]
        ],
        "Exactly 1-bit-on constraints": [
            [15, "exactly-1-bit-on-constraints"]
        ],
        "Exactly n-bits-on constraints": [
            [15, "exactly-n-bits-on-constraints"]
        ],
        "Example": [
            [4, "example"]
        ],
        "Example Problem": [
            [13, "example-problem"],
            [17, "example-problem"],
            [18, "example-problem"]
        ],
        "Exceptions": [
            [3, "exceptions"]
        ],
        "FileSelector": [
            [5, "fileselector"]
        ],
        "Final QUBO": [
            [15, "final-qubo"]
        ],
        "Functions": [
            [0, "functions"],
            [1, "functions"],
            [2, "functions"],
            [2, "id63"],
            [3, "functions"],
            [5, "functions"],
            [8, "functions"],
            [9, "functions"],
            [10, "functions"]
        ],
        "Generate Polynomials and Description": [
            [4, null]
        ],
        "Get started": [
            [22, null]
        ],
        "Grammar in lark syntax with start element polynomial:": [
            [4, "grammar-in-lark-syntax-with-start-element-polynomial"]
        ],
        "GraphicsData": [
            [1, "graphicsdata"],
            [1, "dadk-internal-graphicsdata-graphicsdata"]
        ],
        "GraphicsDetail(Enum)": [
            [1, "graphicsdetail-enum"]
        ],
        "Guidance Configuration": [
            [14, null]
        ],
        "Index": [
            [12, "index"]
        ],
        "Inequalities for Workers\u2019 Shifts per Week": [
            [13, "inequalities-for-workers-shifts-per-week"]
        ],
        "Inequality": [
            [3, "inequality"]
        ],
        "InequalitySign(Enum)": [
            [3, "inequalitysign-enum"]
        ],
        "Installation": [
            [23, null]
        ],
        "Internal": [
            [1, null]
        ],
        "Interpreting the Solution": [
            [19, "interpreting-the-solution"]
        ],
        "InterruptKey": [
            [6, "interruptkey"]
        ],
        "IsingPol": [
            [3, "isingpol"]
        ],
        "JSONTools": [
            [0, "jsontools"]
        ],
        "JobStatus": [
            [2, "jobstatus"]
        ],
        "JupyterTools": [
            [5, null]
        ],
        "LocalMinimaSampling": [
            [2, "localminimasampling"]
        ],
        "MakeQuboError(Exception)": [
            [3, "makequboerror-exception"]
        ],
        "MakeQuboNotUsedBitWarning(MakeQuboWarning)": [
            [3, "makequbonotusedbitwarning-makequbowarning"]
        ],
        "MakeQuboRoundedToZeroWarning(MakeQuboWarning)": [
            [3, "makequboroundedtozerowarning-makequbowarning"]
        ],
        "MakeQuboWarning(ABC)": [
            [3, "makequbowarning-abc"]
        ],
        "Methods": [
            [0, "methods"],
            [1, "methods"],
            [1, "id3"],
            [1, "id7"],
            [1, "id11"],
            [1, "id15"],
            [1, "id19"],
            [1, "id22"],
            [1, "id26"],
            [1, "id30"],
            [2, "methods"],
            [2, "id4"],
            [2, "id20"],
            [2, "id33"],
            [2, "id35"],
            [2, "id37"],
            [2, "id48"],
            [2, "id56"],
            [2, "id59"],
            [3, "methods"],
            [3, "id3"],
            [3, "id11"],
            [3, "id16"],
            [3, "id23"],
            [3, "id25"],
            [3, "id29"],
            [3, "id52"],
            [3, "id55"],
            [3, "id62"],
            [4, "methods"],
            [5, "methods"],
            [5, "id2"],
            [5, "id4"],
            [5, "id5"],
            [5, "id6"],
            [5, "id8"],
            [5, "id11"],
            [5, "id16"],
            [5, "id18"],
            [5, "id19"],
            [5, "id22"],
            [5, "id37"],
            [5, "id42"],
            [6, "methods"],
            [6, "id1"],
            [6, "id14"],
            [6, "id16"],
            [6, "id18"],
            [7, "methods"],
            [7, "id1"],
            [7, "id7"],
            [7, "id9"]
        ],
        "Methods (AzureBlobMixin)": [
            [2, "methods-azureblobmixin"],
            [2, "id8"],
            [2, "id24"]
        ],
        "Methods (BitArrayShape)": [
            [3, "methods-bitarrayshape"],
            [3, "id35"],
            [3, "id37"],
            [3, "id40"],
            [3, "id43"],
            [3, "id46"],
            [3, "id49"]
        ],
        "Methods (Inequality)": [
            [3, "methods-inequality"]
        ],
        "Methods (OptimizerModelComplex)": [
            [6, "methods-optimizermodelcomplex"]
        ],
        "Methods (QUBOSolverBase)": [
            [2, "methods-qubosolverbase"],
            [2, "id12"],
            [2, "id28"],
            [2, "id40"],
            [2, "id44"],
            [2, "id51"]
        ],
        "Methods (RestMixin)": [
            [2, "methods-restmixin"],
            [2, "id7"],
            [2, "id23"]
        ],
        "Methods (SamplingBasis)": [
            [2, "methods-samplingbasis"]
        ],
        "Miscellaneous": [
            [3, "miscellaneous"]
        ],
        "Most 1-bit-on constraints": [
            [15, "most-1-bit-on-constraints"]
        ],
        "NumberPlot": [
            [5, "numberplot"]
        ],
        "NumberTable": [
            [5, "numbertable"]
        ],
        "One Hot Groups": [
            [17, "one-hot-groups"]
        ],
        "OneHot(Enum)": [
            [3, "onehot-enum"]
        ],
        "OneHotGroup": [
            [3, "onehotgroup"]
        ],
        "Operators": [
            [3, "operators"],
            [3, "id68"]
        ],
        "Optimization Objectives": [
            [13, "optimization-objectives"],
            [15, "optimization-objectives"]
        ],
        "Optimization Solutions": [
            [7, null]
        ],
        "OptimizationMethod(Enum)": [
            [1, "optimizationmethod-enum"]
        ],
        "Optimizer": [
            [6, null]
        ],
        "Optimizer(Tab)": [
            [6, "optimizer-tab"]
        ],
        "OptimizerModel(OptimizerModelComplex)": [
            [6, "optimizermodel-optimizermodelcomplex"]
        ],
        "OptimizerModelComplex": [
            [6, "optimizermodelcomplex"]
        ],
        "OptimizerSettings(_Settings)": [
            [6, "optimizersettings-settings"]
        ],
        "OptimizerSettings.AnnealingLogSettings(_Settings)": [
            [6, "optimizersettings-annealinglogsettings-settings"]
        ],
        "OptimizerSettings.BatchSettings(_Settings)": [
            [6, "optimizersettings-batchsettings-settings"]
        ],
        "OptimizerSettings.ComposeLogSettings(_Settings)": [
            [6, "optimizersettings-composelogsettings-settings"]
        ],
        "OptimizerSettings.ComposeSettings(_Settings)": [
            [6, "optimizersettings-composesettings-settings"]
        ],
        "OptimizerSettings.OptunaSettings(_Settings)": [
            [6, "optimizersettings-optunasettings-settings"]
        ],
        "OptimizerSettings.ParetoSettings(_Settings)": [
            [6, "optimizersettings-paretosettings-settings"]
        ],
        "OptimizerSettings.SolveDAv2Settings(_Settings)": [
            [6, "optimizersettings-solvedav2settings-settings"]
        ],
        "OptimizerSettings.SolveGUISettings(_Settings)": [
            [6, "optimizersettings-solveguisettings-settings"]
        ],
        "OptimizerSettings.TimeseriesSettings(_Settings)": [
            [6, "optimizersettings-timeseriessettings-settings"]
        ],
        "OptimizerTab(HBox)": [
            [6, "optimizertab-hbox"]
        ],
        "OptimizerTabRequirement(Enum)": [
            [6, "optimizertabrequirement-enum"]
        ],
        "Other": [
            [2, "other"],
            [2, "id11"],
            [2, "id27"]
        ],
        "Other constraints": [
            [15, "other-constraints"]
        ],
        "OutputTab": [
            [5, "outputtab"]
        ],
        "Overview": [
            [2, "overview"]
        ],
        "ParallelTemperingManagement": [
            [2, "paralleltemperingmanagement"]
        ],
        "ParameterLogging(Enum)": [
            [5, "parameterlogging-enum"]
        ],
        "ParetoPlot": [
            [5, "paretoplot"]
        ],
        "PartialConfig": [
            [3, "partialconfig"]
        ],
        "Performance": [
            [24, "performance"]
        ],
        "Polynomial evaluation": [
            [3, "polynomial-evaluation"]
        ],
        "Polynomial operations": [
            [3, "polynomial-operations"]
        ],
        "Polynomials": [
            [3, null]
        ],
        "ProbabilityModel(Enum)": [
            [1, "probabilitymodel-enum"]
        ],
        "Problem Formulation": [
            [16, "problem-formulation"],
            [19, "problem-formulation"]
        ],
        "ProfileUtils": [
            [0, "profileutils"],
            [0, "dadk-profileutils-profileutils"]
        ],
        "Progress(list)": [
            [7, "progress-list"]
        ],
        "Progress.Entry": [
            [7, "progress-entry"]
        ],
        "Properties": [
            [1, "properties"],
            [1, "id4"],
            [1, "id8"],
            [1, "id12"],
            [1, "id16"],
            [1, "id20"],
            [1, "id23"],
            [1, "id27"],
            [1, "id31"],
            [1, "id34"],
            [2, "properties"],
            [2, "id13"],
            [2, "id29"],
            [2, "id34"],
            [2, "id36"],
            [2, "id38"],
            [2, "id41"],
            [2, "id49"],
            [2, "id50"],
            [2, "id54"],
            [2, "id55"],
            [2, "id57"],
            [2, "id58"],
            [2, "id60"],
            [3, "properties"],
            [3, "id10"],
            [3, "id14"],
            [3, "id22"],
            [3, "id24"],
            [3, "id28"],
            [3, "id32"],
            [3, "id33"],
            [3, "id54"],
            [3, "id59"],
            [3, "id60"],
            [3, "id63"],
            [3, "id64"],
            [3, "id65"],
            [3, "id67"],
            [4, "properties"],
            [5, "properties"],
            [5, "id1"],
            [5, "id3"],
            [5, "id7"],
            [5, "id12"],
            [5, "id13"],
            [5, "id17"],
            [5, "id33"],
            [5, "id41"],
            [6, "properties"],
            [6, "id3"],
            [6, "id4"],
            [6, "id5"],
            [6, "id6"],
            [6, "id7"],
            [6, "id8"],
            [6, "id9"],
            [6, "id10"],
            [6, "id11"],
            [6, "id12"],
            [6, "id13"],
            [6, "id17"],
            [6, "id19"],
            [7, "properties"],
            [7, "id3"],
            [7, "id8"],
            [7, "id11"]
        ],
        "Properties (BitArrayShape)": [
            [3, "properties-bitarrayshape"],
            [3, "id34"],
            [3, "id36"],
            [3, "id39"],
            [3, "id42"],
            [3, "id45"],
            [3, "id48"],
            [3, "id51"]
        ],
        "Properties (ConnectionParameter)": [
            [1, "properties-connectionparameter"]
        ],
        "Properties (Inequality)": [
            [3, "properties-inequality"]
        ],
        "Properties (MakeQuboWarning)": [
            [3, "properties-makequbowarning"],
            [3, "id66"]
        ],
        "Properties (OptimizerModelComplex)": [
            [6, "properties-optimizermodelcomplex"]
        ],
        "Properties (QUBOSolverBase)": [
            [2, "properties-qubosolverbase"],
            [2, "id15"],
            [2, "id31"],
            [2, "id42"],
            [2, "id46"],
            [2, "id52"]
        ],
        "Properties (QUBOSolverBaseV2)": [
            [2, "properties-qubosolverbasev2"],
            [2, "id45"]
        ],
        "Properties (ReplicaExchangeModel)": [
            [2, "properties-replicaexchangemodel"]
        ],
        "Properties (RestMixin)": [
            [2, "properties-restmixin"],
            [2, "id14"],
            [2, "id30"]
        ],
        "Properties (SolverBase)": [
            [2, "properties-solverbase"],
            [2, "id16"],
            [2, "id32"],
            [2, "id39"],
            [2, "id43"],
            [2, "id47"],
            [2, "id53"]
        ],
        "Properties (TemperatureModel)": [
            [2, "properties-temperaturemodel"],
            [2, "id61"],
            [2, "id62"]
        ],
        "Properties (Variable)": [
            [3, "properties-variable"],
            [3, "id38"],
            [3, "id41"],
            [3, "id44"],
            [3, "id47"],
            [3, "id50"]
        ],
        "Properties (WidgetBoundedNumberArray)": [
            [5, "properties-widgetboundednumberarray"],
            [5, "id14"],
            [5, "id25"],
            [5, "id27"],
            [5, "id29"],
            [5, "id31"]
        ],
        "Properties (WidgetNumberArray)": [
            [5, "properties-widgetnumberarray"],
            [5, "id15"],
            [5, "id26"],
            [5, "id28"],
            [5, "id30"],
            [5, "id32"],
            [5, "id34"],
            [5, "id35"],
            [5, "id36"],
            [5, "id39"],
            [5, "id40"]
        ],
        "Python": [
            [23, "python"]
        ],
        "QUBOServiceCPU": [
            [8, null]
        ],
        "QUBOServiceDAv3c": [
            [9, null]
        ],
        "QUBOServiceDAv4": [
            [10, null]
        ],
        "QUBOSizeExceedsSolverMaxBits(Exception)": [
            [3, "qubosizeexceedssolvermaxbits-exception"]
        ],
        "QUBOSolverBase": [
            [2, "qubosolverbase"]
        ],
        "QUBOSolverBase(SolverBase)": [
            [2, "qubosolverbase-solverbase"]
        ],
        "QUBOSolverBaseV2(QUBOSolverBase)": [
            [2, "qubosolverbasev2-qubosolverbase"]
        ],
        "QUBOSolverBaseV3(QUBOSolverBase)": [
            [2, "qubosolverbasev3-qubosolverbase"]
        ],
        "QUBOSolverCPU(RestMixin, AzureBlobMixin, QUBOSolverEmulator)": [
            [2, "qubosolvercpu-restmixin-azureblobmixin-qubosolveremulator"]
        ],
        "QUBOSolverDAv3c(RestMixin, AzureBlobMixin, QUBOSolverBaseV3)": [
            [2, "qubosolverdav3c-restmixin-azureblobmixin-qubosolverbasev3"]
        ],
        "QUBOSolverDAv4(RestMixin, AzureBlobMixin, QUBOSolverBaseV3)": [
            [2, "qubosolverdav4-restmixin-azureblobmixin-qubosolverbasev3"]
        ],
        "QUBOSolverEmulator(QUBOSolverBaseV2)": [
            [2, "qubosolveremulator-qubosolverbasev2"]
        ],
        "Quadratic Unconstrained Optimization": [
            [15, null],
            [18, null]
        ],
        "QuboEditor": [
            [5, "quboeditor"]
        ],
        "ReplicaExchangeModel": [
            [2, "replicaexchangemodel"]
        ],
        "ReplicaExchangeModelFarJump(ReplicaExchangeModel)": [
            [2, "replicaexchangemodelfarjump-replicaexchangemodel"]
        ],
        "RequestMode(Enum)": [
            [1, "requestmode-enum"]
        ],
        "RestMixin": [
            [2, "restmixin"]
        ],
        "SampleFilter": [
            [3, "samplefilter"]
        ],
        "SamplingBasis(ABC)": [
            [2, "samplingbasis-abc"]
        ],
        "SamplingParameter": [
            [1, "samplingparameter"],
            [1, "dadk-internal-samplingparameter-samplingparameter"]
        ],
        "ScalingAction(Enum)": [
            [1, "scalingaction-enum"]
        ],
        "ScalingParameter": [
            [1, "scalingparameter"],
            [1, "dadk-internal-scalingparameter-scalingparameter"]
        ],
        "SetAnnealerProfile": [
            [5, "setannealerprofile"]
        ],
        "ShowSolverParameter": [
            [5, "showsolverparameter"]
        ],
        "SlackType(Enum)": [
            [3, "slacktype-enum"]
        ],
        "Solution": [
            [7, "solution"]
        ],
        "SolutionList": [
            [7, "solutionlist"]
        ],
        "SolutionMode(Enum)": [
            [1, "solutionmode-enum"]
        ],
        "SolverBase(ABC)": [
            [2, "solverbase-abc"]
        ],
        "SolverFactory": [
            [2, "solverfactory"]
        ],
        "SolverTimes": [
            [1, "solvertimes"],
            [1, "dadk-internal-solvertimes-solvertimes"]
        ],
        "Solvers": [
            [2, null]
        ],
        "Solving Constrained QUBOs": [
            [16, null]
        ],
        "Solving QUBOs": [
            [19, null]
        ],
        "Solving the Problem with Guidance Configuration": [
            [14, "solving-the-problem-with-guidance-configuration"]
        ],
        "Staticmethod": [
            [5, "staticmethod"]
        ],
        "StorageAccountType(Enum)": [
            [1, "storageaccounttype-enum"]
        ],
        "Structured Bit Variables": [
            [3, "structured-bit-variables"]
        ],
        "Structured Variables": [
            [17, null],
            [20, null]
        ],
        "Supported Systems": [
            [23, "supported-systems"]
        ],
        "TSPUtils": [
            [0, "tsputils"],
            [0, "dadk-utils-tsputils-tsputils"]
        ],
        "TaggedGuiControl": [
            [5, "taggedguicontrol"]
        ],
        "TemperatureModel": [
            [2, "temperaturemodel"]
        ],
        "TemperatureModelExponential(TemperatureModel)": [
            [2, "temperaturemodelexponential-temperaturemodel"]
        ],
        "TemperatureModelHukushima(TemperatureModelExponential)": [
            [2, "temperaturemodelhukushima-temperaturemodelexponential"]
        ],
        "TemperatureModelLinear(TemperatureModel)": [
            [2, "temperaturemodellinear-temperaturemodel"]
        ],
        "Term - Products of binary variables": [
            [3, "term-products-of-binary-variables"]
        ],
        "The Binary Search Space": [
            [3, "the-binary-search-space"]
        ],
        "TimePicker(WidgetBoundedIntArray)": [
            [5, "timepicker-widgetboundedintarray"]
        ],
        "TimeReporter": [
            [5, "timereporter"]
        ],
        "TimeTracker": [
            [5, "timetracker"]
        ],
        "Traverse2Plot": [
            [5, "traverse2plot"]
        ],
        "TraversePlot": [
            [5, "traverseplot"]
        ],
        "Tutorials": [
            [23, "tutorials"]
        ],
        "TwoSideInequality(Inequality)": [
            [3, "twosideinequality-inequality"]
        ],
        "User Guide": [
            [12, "user-guide"],
            [22, null]
        ],
        "Utils": [
            [0, null],
            [0, "id1"]
        ],
        "VarSet": [
            [7, "varset"]
        ],
        "VarShapeSet": [
            [3, "varshapeset"],
            [17, "varshapeset"]
        ],
        "VarSlack": [
            [3, "varslack"]
        ],
        "Variable(BitArrayShape, ABC)": [
            [3, "variable-bitarrayshape-abc"]
        ],
        "VariableBase10(Variable)": [
            [3, "variablebase10-variable"]
        ],
        "VariableBinary(Variable)": [
            [3, "variablebinary-variable"]
        ],
        "VariableFibonacci(Variable)": [
            [3, "variablefibonacci-variable"]
        ],
        "VariableLittlegauss(Variable)": [
            [3, "variablelittlegauss-variable"]
        ],
        "VariableSequential(Variable)": [
            [3, "variablesequential-variable"]
        ],
        "VariableUnary(Variable)": [
            [3, "variableunary-variable"]
        ],
        "Variables": [
            [20, "variables"]
        ],
        "Variables and QUBO": [
            [18, "variables-and-qubo"]
        ],
        "Warnings": [
            [3, "warnings"]
        ],
        "Why DADK": [
            [24, null]
        ],
        "WidgetBoundedFloatArray(WidgetBoundedNumberArray)": [
            [5, "widgetboundedfloatarray-widgetboundednumberarray"]
        ],
        "WidgetBoundedFloatRange(WidgetBoundedNumberArray)": [
            [5, "widgetboundedfloatrange-widgetboundednumberarray"]
        ],
        "WidgetBoundedIntArray(WidgetBoundedNumberArray)": [
            [5, "widgetboundedintarray-widgetboundednumberarray"]
        ],
        "WidgetBoundedIntRange(WidgetBoundedNumberArray)": [
            [5, "widgetboundedintrange-widgetboundednumberarray"]
        ],
        "WidgetBoundedNumberArray(WidgetNumberArray)": [
            [5, "widgetboundednumberarray-widgetnumberarray"]
        ],
        "WidgetDefinition(dict)": [
            [5, "widgetdefinition-dict"]
        ],
        "WidgetFloatArray(WidgetNumberArray)": [
            [5, "widgetfloatarray-widgetnumberarray"]
        ],
        "WidgetFloatRange(WidgetNumberArray)": [
            [5, "widgetfloatrange-widgetnumberarray"]
        ],
        "WidgetGui(HBox)": [
            [5, "widgetgui-hbox"]
        ],
        "WidgetIntArray(WidgetNumberArray)": [
            [5, "widgetintarray-widgetnumberarray"]
        ],
        "WidgetIntRange(WidgetNumberArray)": [
            [5, "widgetintrange-widgetnumberarray"]
        ],
        "WidgetNumberArray(VBox)": [
            [5, "widgetnumberarray-vbox"]
        ],
        "WidgetTab(Tab)": [
            [5, "widgettab-tab"]
        ],
        "activate_output": [
            [5, "activate-output"]
        ],
        "add": [
            [3, "add"],
            [5, "add"]
        ],
        "add_exactly_1_bit_on": [
            [3, "add-exactly-1-bit-on"]
        ],
        "add_exactly_n_bits_on": [
            [3, "add-exactly-n-bits-on"]
        ],
        "add_figure": [
            [5, "add-figure"],
            [5, "dadk-jupytertools-paretoplot-add-figure"],
            [5, "dadk-jupytertools-traverse2plot-add-figure"],
            [5, "dadk-jupytertools-traverseplot-add-figure"]
        ],
        "add_json_lines": [
            [3, "add-json-lines"]
        ],
        "add_most_1_bit_on": [
            [3, "add-most-1-bit-on"]
        ],
        "add_slack_penalty": [
            [3, "add-slack-penalty"]
        ],
        "add_slack_variable": [
            [3, "add-slack-variable"]
        ],
        "add_tab": [
            [5, "add-tab"]
        ],
        "add_term": [
            [3, "add-term"],
            [3, "dadk-binpol-isingpol-add-term"]
        ],
        "add_variable": [
            [3, "add-variable"]
        ],
        "add_variable_penalty": [
            [3, "add-variable-penalty"]
        ],
        "as_bqm": [
            [3, "as-bqm"]
        ],
        "as_hbsolv": [
            [3, "as-hbsolv"]
        ],
        "as_json": [
            [3, "as-json"],
            [3, "dadk-binpol-partialconfig-as-json"]
        ],
        "as_latex": [
            [3, "as-latex"],
            [3, "dadk-binpol-inequality-as-latex"],
            [3, "dadk-binpol-twosideinequality-as-latex"],
            [3, "dadk-binpol-isingpol-as-latex"]
        ],
        "as_qbsolv": [
            [3, "as-qbsolv"]
        ],
        "as_text": [
            [3, "as-text"],
            [3, "dadk-binpol-inequality-as-text"],
            [3, "dadk-binpol-twosideinequality-as-text"],
            [3, "dadk-binpol-isingpol-as-text"]
        ],
        "build_and_solve": [
            [6, "build-and-solve"]
        ],
        "build_qubo": [
            [6, "build-qubo"]
        ],
        "calculate_hash": [
            [3, "calculate-hash"]
        ],
        "call_function": [
            [5, "call-function"]
        ],
        "check_da_parameter": [
            [2, "check-da-parameter"],
            [2, "dadk-qubosolverdav4-qubosolverdav4-check-da-parameter"],
            [2, "dadk-qubosolvercpu-qubosolvercpu-check-da-parameter"]
        ],
        "clone": [
            [3, "clone"],
            [3, "dadk-binpol-inequality-clone"],
            [3, "dadk-binpol-samplefilter-clone"]
        ],
        "close": [
            [5, "close"]
        ],
        "compute": [
            [3, "compute"],
            [3, "dadk-binpol-term-compute"],
            [3, "dadk-binpol-inequality-compute"]
        ],
        "configuration_2_string": [
            [0, "configuration-2-string"]
        ],
        "convert_to_binary": [
            [3, "convert-to-binary"]
        ],
        "copy_to": [
            [6, "copy-to"]
        ],
        "count_files": [
            [6, "count-files"]
        ],
        "create": [
            [6, "create"]
        ],
        "create_batch": [
            [6, "create-batch"]
        ],
        "create_random_state": [
            [3, "create-random-state"]
        ],
        "create_rpoly_hd5": [
            [3, "create-rpoly-hd5"]
        ],
        "create_rpoly_script": [
            [3, "create-rpoly-script"]
        ],
        "create_solver": [
            [2, "create-solver"]
        ],
        "crs2Inequalities": [
            [3, "crs2inequalities"]
        ],
        "crs2QUBO": [
            [3, "crs2qubo"]
        ],
        "csv_content": [
            [5, "csv-content"]
        ],
        "dadk_version_check": [
            [0, "dadk-version-check"]
        ],
        "deactivate_output": [
            [5, "deactivate-output"]
        ],
        "debug_exception": [
            [5, "debug-exception"]
        ],
        "decode": [
            [7, "decode"],
            [7, "dadk-solution-solutionlist-solutionlist-decode"]
        ],
        "delete_job": [
            [8, "delete-job"],
            [9, "delete-job"],
            [10, "delete-job"]
        ],
        "display": [
            [5, "display"]
        ],
        "display_applier": [
            [5, "display-applier"]
        ],
        "display_content": [
            [5, "display-content"]
        ],
        "display_control": [
            [5, "display-control"],
            [5, "dadk-jupytertools-paretoplot-display-control"],
            [5, "dadk-jupytertools-traverse2plot-display-control"],
            [5, "dadk-jupytertools-traverseplot-display-control"]
        ],
        "display_creator": [
            [5, "display-creator"]
        ],
        "display_graphs": [
            [7, "display-graphs"]
        ],
        "draw": [
            [7, "draw"],
            [7, "dadk-solution-solutionlist-solution-draw"]
        ],
        "encode": [
            [7, "encode"],
            [7, "dadk-solution-solutionlist-solutionlist-encode"]
        ],
        "evaluate": [
            [7, "evaluate"]
        ],
        "exactly_1_bit_on": [
            [3, "exactly-1-bit-on"]
        ],
        "exactly_n_bits_on": [
            [3, "exactly-n-bits-on"]
        ],
        "extract_bit_array": [
            [7, "extract-bit-array"],
            [7, "dadk-solution-solutionlist-varset-extract-bit-array"]
        ],
        "file_size_info": [
            [2, "file-size-info"]
        ],
        "freeze_var_shape_set": [
            [3, "freeze-var-shape-set"],
            [3, "dadk-binpol-isingpol-freeze-var-shape-set"]
        ],
        "from_QPoly": [
            [3, "from-qpoly"]
        ],
        "gen_latex": [
            [4, "gen-latex"]
        ],
        "gen_pretty_string": [
            [4, "gen-pretty-string"]
        ],
        "gen_python": [
            [4, "gen-python"]
        ],
        "generate_penalty_polynomial": [
            [3, "generate-penalty-polynomial"]
        ],
        "generate_slack_polynomial": [
            [3, "generate-slack-polynomial"]
        ],
        "get": [
            [3, "get"],
            [3, "dadk-binpol-isingpol-get"]
        ],
        "get_active_parameters": [
            [5, "get-active-parameters"]
        ],
        "get_bits": [
            [3, "get-bits"]
        ],
        "get_bits_of_1w1h_group": [
            [3, "get-bits-of-1w1h-group"]
        ],
        "get_bits_of_2w1h_group": [
            [3, "get-bits-of-2w1h-group"]
        ],
        "get_default_setting": [
            [5, "get-default-setting"]
        ],
        "get_false_bit_indices": [
            [3, "get-false-bit-indices"]
        ],
        "get_free_bit_indices": [
            [3, "get-free-bit-indices"]
        ],
        "get_index": [
            [3, "get-index"],
            [3, "dadk-binpol-bitarrayshape-get-index"],
            [3, "dadk-binpol-category-get-index"]
        ],
        "get_job": [
            [8, "get-job"],
            [9, "get-job"],
            [10, "get-job"]
        ],
        "get_json": [
            [3, "get-json"]
        ],
        "get_json_generator": [
            [3, "get-json-generator"]
        ],
        "get_max_abs_coefficient": [
            [3, "get-max-abs-coefficient"]
        ],
        "get_minimum_energy_solution": [
            [7, "get-minimum-energy-solution"]
        ],
        "get_normalize_factor": [
            [3, "get-normalize-factor"]
        ],
        "get_not_false_bit_indices": [
            [3, "get-not-false-bit-indices"]
        ],
        "get_parameter_definitions": [
            [2, "get-parameter-definitions"],
            [2, "dadk-qubosolverdav4-qubosolverdav4-get-parameter-definitions"],
            [2, "dadk-qubosolvercpu-qubosolvercpu-get-parameter-definitions"]
        ],
        "get_persistent_setting": [
            [5, "get-persistent-setting"]
        ],
        "get_results": [
            [6, "get-results"]
        ],
        "get_scale_factor": [
            [3, "get-scale-factor"]
        ],
        "get_solution_list": [
            [7, "get-solution-list"]
        ],
        "get_solver_IDs": [
            [2, "get-solver-ids"]
        ],
        "get_sorted_solution_list": [
            [7, "get-sorted-solution-list"]
        ],
        "get_summary": [
            [5, "get-summary"]
        ],
        "get_symbolic": [
            [3, "get-symbolic"],
            [3, "dadk-binpol-bitarrayshape-get-symbolic"],
            [3, "dadk-binpol-category-get-symbolic"]
        ],
        "get_time": [
            [6, "get-time"],
            [7, "get-time"]
        ],
        "get_times": [
            [7, "get-times"]
        ],
        "get_true_bit_indices": [
            [3, "get-true-bit-indices"]
        ],
        "get_value": [
            [5, "get-value"]
        ],
        "get_weights": [
            [3, "get-weights"]
        ],
        "get_widget": [
            [5, "get-widget"],
            [6, "get-widget"],
            [6, "dadk-optimizer-optimizertab-get-widget"]
        ],
        "gui_build": [
            [6, "gui-build"]
        ],
        "gui_default_values": [
            [5, "gui-default-values"]
        ],
        "gui_reset_values": [
            [5, "gui-reset-values"]
        ],
        "gui_save_values": [
            [5, "gui-save-values"]
        ],
        "gui_solve": [
            [6, "gui-solve"]
        ],
        "has_solver": [
            [2, "has-solver"]
        ],
        "healthcheck": [
            [8, "healthcheck"],
            [9, "healthcheck"],
            [10, "healthcheck"]
        ],
        "hobo2qubo": [
            [8, "hobo2qubo"],
            [9, "hobo2qubo"],
            [10, "hobo2qubo"]
        ],
        "i18n_get": [
            [0, "i18n-get"]
        ],
        "inequalities2CRS": [
            [3, "inequalities2crs"]
        ],
        "inflate_clamped_configuration": [
            [3, "inflate-clamped-configuration"]
        ],
        "is_empty": [
            [3, "is-empty"]
        ],
        "list_jobs": [
            [8, "list-jobs"],
            [9, "list-jobs"],
            [10, "list-jobs"]
        ],
        "load": [
            [6, "load"],
            [6, "dadk-optimizer-optimizermodelcomplex-load"]
        ],
        "load_connection_parameter": [
            [1, "load-connection-parameter"]
        ],
        "load_hdf5": [
            [3, "load-hdf5"]
        ],
        "load_json": [
            [3, "load-json"]
        ],
        "load_library": [
            [0, "load-library"]
        ],
        "load_qbsolv": [
            [3, "load-qbsolv"]
        ],
        "log": [
            [6, "log"]
        ],
        "log_data": [
            [6, "log-data"]
        ],
        "log_msg": [
            [5, "log-msg"]
        ],
        "make_clamped": [
            [3, "make-clamped"]
        ],
        "make_qubo": [
            [3, "make-qubo"],
            [3, "dadk-binpol-inequality-make-qubo"],
            [3, "dadk-binpol-samplefilter-make-qubo"]
        ],
        "math_prod": [
            [0, "math-prod"]
        ],
        "max_number_of_bits": [
            [2, "max-number-of-bits"],
            [2, "dadk-qubosolverdav4-qubosolverdav4-max-number-of-bits"],
            [2, "dadk-qubosolvercpu-qubosolvercpu-max-number-of-bits"]
        ],
        "merge_dicts": [
            [0, "merge-dicts"]
        ],
        "minimize": [
            [2, "minimize"],
            [2, "dadk-qubosolverdav4-qubosolverdav4-minimize"],
            [2, "dadk-qubosolvercpu-qubosolvercpu-minimize"],
            [8, "minimize"],
            [9, "minimize"],
            [10, "minimize"]
        ],
        "most_1_bit_on": [
            [3, "most-1-bit-on"]
        ],
        "move_to": [
            [6, "move-to"]
        ],
        "multiply": [
            [3, "multiply"]
        ],
        "multiply_scalar": [
            [3, "multiply-scalar"],
            [3, "dadk-binpol-inequality-multiply-scalar"],
            [3, "dadk-binpol-samplefilter-multiply-scalar"]
        ],
        "normalize": [
            [3, "normalize"]
        ],
        "optuna": [
            [6, "optuna"]
        ],
        "pareto": [
            [6, "pareto"]
        ],
        "parse_poly": [
            [4, "parse-poly"]
        ],
        "plot_histogram": [
            [3, "plot-histogram"]
        ],
        "power": [
            [3, "power"]
        ],
        "power2": [
            [3, "power2"]
        ],
        "prep_result": [
            [6, "prep-result"]
        ],
        "prepare_content": [
            [5, "prepare-content"]
        ],
        "print_progress": [
            [7, "print-progress"]
        ],
        "print_stats": [
            [7, "print-stats"]
        ],
        "print_to_output": [
            [5, "print-to-output"]
        ],
        "prolog": [
            [5, "prolog"]
        ],
        "qubo2CRS": [
            [3, "qubo2crs"]
        ],
        "read_json_file": [
            [6, "read-json-file"]
        ],
        "read_text_file": [
            [6, "read-text-file"]
        ],
        "reduce_higher_degree": [
            [3, "reduce-higher-degree"]
        ],
        "reduce_higher_degree_to_qubo": [
            [3, "reduce-higher-degree-to-qubo"]
        ],
        "register_logging_output": [
            [5, "register-logging-output"]
        ],
        "register_solver": [
            [2, "register-solver"]
        ],
        "register_widget_class": [
            [5, "register-widget-class"]
        ],
        "remove": [
            [6, "remove"]
        ],
        "remove_all_tabs": [
            [5, "remove-all-tabs"]
        ],
        "remove_files": [
            [6, "remove-files"]
        ],
        "report_fact": [
            [5, "report-fact"]
        ],
        "restore": [
            [3, "restore"]
        ],
        "round_to_n": [
            [0, "round-to-n"]
        ],
        "run": [
            [6, "run"]
        ],
        "save_as_hdf5": [
            [3, "save-as-hdf5"]
        ],
        "scale": [
            [3, "scale"]
        ],
        "search_stats_info": [
            [7, "search-stats-info"]
        ],
        "select_instance": [
            [6, "select-instance"]
        ],
        "select_tab": [
            [5, "select-tab"]
        ],
        "set_CLONE_WARNING_COUNTER": [
            [3, "set-clone-warning-counter"]
        ],
        "set_bit": [
            [3, "set-bit"]
        ],
        "set_build_qubo_details_reference": [
            [6, "set-build-qubo-details-reference"]
        ],
        "set_calculated_build_qubo_parameter": [
            [6, "set-calculated-build-qubo-parameter"]
        ],
        "set_calculated_solver_parameter": [
            [6, "set-calculated-solver-parameter"]
        ],
        "set_compose_model_details_reference": [
            [6, "set-compose-model-details-reference"]
        ],
        "set_directory": [
            [5, "set-directory"]
        ],
        "set_disabled": [
            [5, "set-disabled"],
            [6, "set-disabled"]
        ],
        "set_fixed_solver_parameter": [
            [6, "set-fixed-solver-parameter"]
        ],
        "set_load_details_reference": [
            [6, "set-load-details-reference"]
        ],
        "set_persistent_setting": [
            [5, "set-persistent-setting"]
        ],
        "set_prep_result_details_reference": [
            [6, "set-prep-result-details-reference"]
        ],
        "set_solver_parameter": [
            [6, "set-solver-parameter"]
        ],
        "set_term": [
            [3, "set-term"],
            [3, "dadk-binpol-isingpol-set-term"]
        ],
        "set_title": [
            [5, "set-title"]
        ],
        "set_value": [
            [5, "set-value"],
            [5, "dadk-jupytertools-widgetgui-set-value"]
        ],
        "set_variable": [
            [3, "set-variable"]
        ],
        "set_visibility": [
            [5, "set-visibility"]
        ],
        "sort": [
            [5, "sort"]
        ],
        "stop": [
            [6, "stop"]
        ],
        "string_2_configuration": [
            [0, "string-2-configuration"]
        ],
        "sum": [
            [3, "sum"]
        ],
        "tab_report": [
            [6, "tab-report"]
        ],
        "take_time": [
            [5, "take-time"]
        ],
        "text_content": [
            [5, "text-content"]
        ],
        "timeseries": [
            [6, "timeseries"]
        ],
        "to_QPoly": [
            [3, "to-qpoly"]
        ],
        "trigger_change_handlers": [
            [5, "trigger-change-handlers"]
        ],
        "update_content": [
            [5, "update-content"]
        ],
        "update_last_setting": [
            [5, "update-last-setting"]
        ],
        "write_json_file": [
            [6, "write-json-file"]
        ],
        "write_text_file": [
            [6, "write-text-file"]
        ]
    },
    "docnames": ["api/Additional", "api/Internal", "api/Solvers", "api/dadk/BinPol", "api/dadk/BinPolGen", "api/dadk/JupyterTools", "api/dadk/Optimizer", "api/dadk/Solution_SolutionList", "api/dadk/flask/QUBOServiceCPU", "api/dadk/flask/QUBOServiceDAv3c", "api/dadk/flask/QUBOServiceDAv4", "api/index", "index", "user_guide/a_constraints", "user_guide/a_guidance", "user_guide/a_qubo", "user_guide/a_solving", "user_guide/a_varshape", "user_guide/b_qubo", "user_guide/b_solving", "user_guide/b_varshape", "user_guide/dadk_da", "user_guide/index", "user_guide/install", "user_guide/why"],
    "envversion": {
        "sphinx": 65,
        "sphinx.domains.c": 3,
        "sphinx.domains.changeset": 1,
        "sphinx.domains.citation": 1,
        "sphinx.domains.cpp": 9,
        "sphinx.domains.index": 1,
        "sphinx.domains.javascript": 3,
        "sphinx.domains.math": 2,
        "sphinx.domains.python": 4,
        "sphinx.domains.rst": 2,
        "sphinx.domains.std": 2
    },
    "filenames": ["api/Additional.rst", "api/Internal.rst", "api/Solvers.rst", "api/dadk/BinPol.rst", "api/dadk/BinPolGen.rst", "api/dadk/JupyterTools.rst", "api/dadk/Optimizer.rst", "api/dadk/Solution_SolutionList.rst", "api/dadk/flask/QUBOServiceCPU.rst", "api/dadk/flask/QUBOServiceDAv3c.rst", "api/dadk/flask/QUBOServiceDAv4.rst", "api/index.rst", "index.rst", "user_guide/a_constraints.rst", "user_guide/a_guidance.rst", "user_guide/a_qubo.rst", "user_guide/a_solving.rst", "user_guide/a_varshape.rst", "user_guide/b_qubo.rst", "user_guide/b_solving.rst", "user_guide/b_varshape.rst", "user_guide/dadk_da.rst", "user_guide/index.rst", "user_guide/install.rst", "user_guide/why.rst"],
    "indexentries": {
        "activate_output() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.activate_output", false]
        ],
        "add() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add", false]
        ],
        "add() (dadk.jupytertools.timetracker method)": [
            [5, "dadk.JupyterTools.TimeTracker.add", false]
        ],
        "add_exactly_1_bit_on() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_exactly_1_bit_on", false]
        ],
        "add_exactly_n_bits_on() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_exactly_n_bits_on", false]
        ],
        "add_figure() (dadk.jupytertools.numberplot method)": [
            [5, "dadk.JupyterTools.NumberPlot.add_figure", false]
        ],
        "add_figure() (dadk.jupytertools.paretoplot method)": [
            [5, "dadk.JupyterTools.ParetoPlot.add_figure", false]
        ],
        "add_figure() (dadk.jupytertools.traverse2plot method)": [
            [5, "dadk.JupyterTools.Traverse2Plot.add_figure", false]
        ],
        "add_figure() (dadk.jupytertools.traverseplot method)": [
            [5, "dadk.JupyterTools.TraversePlot.add_figure", false]
        ],
        "add_json_lines() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_json_lines", false]
        ],
        "add_most_1_bit_on() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_most_1_bit_on", false]
        ],
        "add_slack_penalty() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_slack_penalty", false]
        ],
        "add_slack_variable() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_slack_variable", false]
        ],
        "add_tab() (dadk.jupytertools.widgettab method)": [
            [5, "dadk.JupyterTools.WidgetTab.add_tab", false]
        ],
        "add_term() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_term", false]
        ],
        "add_term() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.add_term", false]
        ],
        "add_variable() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_variable", false]
        ],
        "add_variable_penalty() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.add_variable_penalty", false]
        ],
        "annealer_address (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.annealer_address", false]
        ],
        "annealer_path (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.annealer_path", false]
        ],
        "annealer_port (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.annealer_port", false]
        ],
        "annealer_protocol (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.annealer_protocol", false]
        ],
        "annealer_queue_size (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.annealer_queue_size", false]
        ],
        "annealing_log (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.annealing_log", false]
        ],
        "annealing_steps (dadk.internal.samplingparameter.samplingparameter property)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.annealing_steps", false]
        ],
        "annealinglogsettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings", false]
        ],
        "api_key (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.api_key", false]
        ],
        "applier (dadk.jupytertools.taggedguicontrol property)": [
            [5, "dadk.JupyterTools.TaggedGuiControl.applier", false]
        ],
        "as_bqm() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_bqm", false]
        ],
        "as_hbsolv() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_hbsolv", false]
        ],
        "as_json() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_json", false]
        ],
        "as_json() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.as_json", false]
        ],
        "as_latex() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_latex", false]
        ],
        "as_latex() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.as_latex", false]
        ],
        "as_latex() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.as_latex", false]
        ],
        "as_latex() (dadk.binpol.twosideinequality method)": [
            [3, "dadk.BinPol.TwoSideInequality.as_latex", false]
        ],
        "as_qbsolv() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_qbsolv", false]
        ],
        "as_text() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.as_text", false]
        ],
        "as_text() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.as_text", false]
        ],
        "as_text() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.as_text", false]
        ],
        "as_text() (dadk.binpol.twosideinequality method)": [
            [3, "dadk.BinPol.TwoSideInequality.as_text", false]
        ],
        "as_text() (dadk.internal.connectionparameter.connectionparameter method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.as_text", false]
        ],
        "as_text() (dadk.internal.connectionparameter.connectionparameterwithazureblob method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.as_text", false]
        ],
        "as_text() (dadk.internal.dav2parameter.dav2parameter method)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.as_text", false]
        ],
        "as_text() (dadk.internal.dav3cparameter.dav3cparameter method)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.as_text", false]
        ],
        "as_text() (dadk.internal.dav3parameter.dav3parameter method)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.as_text", false]
        ],
        "as_text() (dadk.internal.dav4parameter.dav4parameter method)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.as_text", false]
        ],
        "as_text() (dadk.internal.samplingparameter.samplingparameter method)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.as_text", false]
        ],
        "as_text() (dadk.internal.scalingparameter.scalingparameter method)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.as_text", false]
        ],
        "azureblobmixin (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin", false]
        ],
        "batch (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.batch", false]
        ],
        "batchsettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.BatchSettings", false]
        ],
        "binpol (class in dadk.binpol)": [
            [3, "dadk.BinPol.BinPol", false]
        ],
        "binpolgen (class in dadk.binpolgen)": [
            [4, "dadk.BinPolGen.BinPolGen", false]
        ],
        "bit_flips (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.bit_flips", false]
        ],
        "bit_no (dadk.binpol.makequbowarning property)": [
            [3, "dadk.BinPol.MakeQuboWarning.bit_no", false]
        ],
        "bitarray (class in dadk.solution_solutionlist)": [
            [7, "dadk.Solution_SolutionList.BitArray", false]
        ],
        "bitarrayshape (class in dadk.binpol)": [
            [3, "dadk.BinPol.BitArrayShape", false]
        ],
        "bits (dadk.binpol.qubosizeexceedssolvermaxbits property)": [
            [3, "dadk.BinPol.QUBOSizeExceedsSolverMaxBits.bits", false]
        ],
        "blob (class in dadk.qubosolverbase.azureblobmixin)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.Blob", false]
        ],
        "blob_name (dadk.qubosolverbase.azureblobmixin.blob property)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.Blob.blob_name", false]
        ],
        "boltzmannsampling (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.BoltzmannSampling", false]
        ],
        "bqp (class in dadk.binpol)": [
            [3, "dadk.BinPol.BQP", false]
        ],
        "build_and_solve() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.build_and_solve", false]
        ],
        "build_qubo() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.build_qubo", false]
        ],
        "button_width (dadk.optimizer.optimizersettings.solveguisettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.SolveGUISettings.button_width", false]
        ],
        "calculate_hash() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.calculate_hash", false]
        ],
        "call_back (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.call_back", false]
        ],
        "call_function() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.call_function", false]
        ],
        "cancel_job() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.cancel_job", false]
        ],
        "category (class in dadk.binpol)": [
            [3, "dadk.BinPol.Category", false]
        ],
        "check_da_parameter() (dadk.qubosolvercpu.qubosolvercpu method)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU.check_da_parameter", false]
        ],
        "check_da_parameter() (dadk.qubosolverdav3c.qubosolverdav3c method)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.check_da_parameter", false]
        ],
        "check_da_parameter() (dadk.qubosolverdav4.qubosolverdav4 method)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.check_da_parameter", false]
        ],
        "cleanup_jobs() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.cleanup_jobs", false]
        ],
        "cleanupazureblobstorage (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.CleanupAzureBlobStorage", false]
        ],
        "cleanupjobs (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.CleanupJobs", false]
        ],
        "clone() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.clone", false]
        ],
        "clone() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.clone", false]
        ],
        "clone() (dadk.binpol.samplefilter method)": [
            [3, "dadk.BinPol.SampleFilter.clone", false]
        ],
        "close() (dadk.jupytertools.timetracker method)": [
            [5, "dadk.JupyterTools.TimeTracker.close", false]
        ],
        "close() (dadk.qubosolverbase.azureblobmixin.progressbar method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.ProgressBar.close", false]
        ],
        "coefficient (dadk.binpol.makequboroundedtozerowarning property)": [
            [3, "dadk.BinPol.MakeQuboRoundedToZeroWarning.coefficient", false]
        ],
        "columns (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.columns", false]
        ],
        "compose (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.compose", false]
        ],
        "compose_log (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.compose_log", false]
        ],
        "composelogsettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeLogSettings", false]
        ],
        "composesettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings", false]
        ],
        "compute() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.compute", false]
        ],
        "compute() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.compute", false]
        ],
        "compute() (dadk.binpol.term method)": [
            [3, "dadk.BinPol.Term.compute", false]
        ],
        "configuration (dadk.solution_solutionlist.solution property)": [
            [7, "dadk.Solution_SolutionList.Solution.configuration", false]
        ],
        "configuration_2_string() (in module dadk.utils)": [
            [0, "dadk.Utils.configuration_2_string", false]
        ],
        "connection_parameter (dadk.qubosolverbase.restmixin property)": [
            [2, "dadk.QUBOSolverBase.RestMixin.connection_parameter", false]
        ],
        "connectionparameter (class in dadk.internal.connectionparameter)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter", false]
        ],
        "connectionparameterwithazureblob (class in dadk.internal.connectionparameter)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob", false]
        ],
        "container_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.container_name", false]
        ],
        "container_name (dadk.qubosolverbase.azureblobmixin.blob property)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.Blob.container_name", false]
        ],
        "convert_to_binary() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.convert_to_binary", false]
        ],
        "coordinatesutils (class in dadk.utils.coordinatesutils)": [
            [0, "dadk.utils.CoordinatesUtils.CoordinatesUtils", false]
        ],
        "copy_to() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.copy_to", false]
        ],
        "count_files() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.count_files", false]
        ],
        "create() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.create", false]
        ],
        "create_batch() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.create_batch", false]
        ],
        "create_container() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.create_container", false]
        ],
        "create_qubos() (dadk.utils.tsputils.tsputils class method)": [
            [0, "dadk.utils.TSPUtils.TSPUtils.create_QUBOS", false]
        ],
        "create_random_state() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.create_random_state", false]
        ],
        "create_rpoly_hd5() (in module dadk.binpol)": [
            [3, "dadk.BinPol.create_rpoly_hd5", false]
        ],
        "create_rpoly_script() (in module dadk.binpol)": [
            [3, "dadk.BinPol.create_rpoly_script", false]
        ],
        "create_sas_token() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.create_sas_token", false]
        ],
        "create_solver() (in module dadk.solverfactory)": [
            [2, "dadk.SolverFactory.create_solver", false]
        ],
        "create_summary_excel (dadk.optimizer.optimizersettings.composesettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings.create_summary_excel", false]
        ],
        "creator (dadk.jupytertools.taggedguicontrol property)": [
            [5, "dadk.JupyterTools.TaggedGuiControl.creator", false]
        ],
        "crs2inequalities() (in module dadk.binpol)": [
            [3, "dadk.BinPol.crs2Inequalities", false]
        ],
        "crs2qubo() (in module dadk.binpol)": [
            [3, "dadk.BinPol.crs2QUBO", false]
        ],
        "csv_content() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.csv_content", false]
        ],
        "da_parameter (dadk.qubosolverbase.qubosolverbasev2 property)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBaseV2.da_parameter", false]
        ],
        "da_parameter (dadk.qubosolverdav3c.qubosolverdav3c property)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.da_parameter", false]
        ],
        "da_parameter (dadk.qubosolverdav4.qubosolverdav4 property)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.da_parameter", false]
        ],
        "da_parameter (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.da_parameter", false]
        ],
        "dadk_version_check() (in module dadk.utils)": [
            [0, "dadk.Utils.dadk_version_check", false]
        ],
        "daodecoder (class in dadk.jsontools)": [
            [0, "dadk.JSONTools.DaoDecoder", false]
        ],
        "daoencoder (class in dadk.jsontools)": [
            [0, "dadk.JSONTools.DaoEncoder", false]
        ],
        "datetimepicker (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.DatetimePicker", false]
        ],
        "dav2parameter (class in dadk.internal.dav2parameter)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter", false]
        ],
        "dav3cparameter (class in dadk.internal.dav3cparameter)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter", false]
        ],
        "dav3parameter (class in dadk.internal.dav3parameter)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter", false]
        ],
        "dav4parameter (class in dadk.internal.dav4parameter)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter", false]
        ],
        "deactivate_output() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.deactivate_output", false]
        ],
        "debug_exception() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.debug_exception", false]
        ],
        "decode() (dadk.internal.connectionparameter.connectionparameter class method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.decode", false]
        ],
        "decode() (dadk.internal.connectionparameter.connectionparameterwithazureblob class method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.decode", false]
        ],
        "decode() (dadk.internal.dav2parameter.dav2parameter class method)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.decode", false]
        ],
        "decode() (dadk.internal.dav3cparameter.dav3cparameter class method)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.decode", false]
        ],
        "decode() (dadk.internal.dav3parameter.dav3parameter class method)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.decode", false]
        ],
        "decode() (dadk.internal.dav4parameter.dav4parameter class method)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.decode", false]
        ],
        "decode() (dadk.internal.samplingparameter.samplingparameter class method)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.decode", false]
        ],
        "decode() (dadk.internal.scalingparameter.scalingparameter class method)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.decode", false]
        ],
        "decode() (dadk.internal.solvertimes.solvertimes class method)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.decode", false]
        ],
        "decode() (dadk.solution_solutionlist.solution class method)": [
            [7, "dadk.Solution_SolutionList.Solution.decode", false]
        ],
        "decode() (dadk.solution_solutionlist.solutionlist class method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.decode", false]
        ],
        "default() (dadk.jsontools.daoencoder method)": [
            [0, "dadk.JSONTools.DaoEncoder.default", false]
        ],
        "degree (dadk.binpol.binpol property)": [
            [3, "dadk.BinPol.BinPol.degree", false]
        ],
        "degree (dadk.binpol.isingpol property)": [
            [3, "dadk.BinPol.IsingPol.degree", false]
        ],
        "delete_annealer_access_profile() (dadk.profileutils.profileutils class method)": [
            [0, "dadk.ProfileUtils.ProfileUtils.delete_annealer_access_profile", false]
        ],
        "delete_blob() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.delete_blob", false]
        ],
        "delete_container() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.delete_container", false]
        ],
        "delete_files (dadk.optimizer.optimizersettings.optunasettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.OptunaSettings.delete_files", false]
        ],
        "delete_files (dadk.optimizer.optimizersettings.paretosettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ParetoSettings.delete_files", false]
        ],
        "delete_files (dadk.optimizer.optimizersettings.timeseriessettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.TimeseriesSettings.delete_files", false]
        ],
        "delete_job() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.delete_job", false]
        ],
        "delete_job() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.delete_job", false]
        ],
        "delete_job() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.delete_job", false]
        ],
        "delete_job() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.delete_job", false]
        ],
        "description (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.description", false]
        ],
        "description_width (dadk.optimizer.optimizersettings.solveguisettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.SolveGUISettings.description_width", false]
        ],
        "dict (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.dict", false]
        ],
        "dict (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.dict", false]
        ],
        "dict (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.dict", false]
        ],
        "dict (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.dict", false]
        ],
        "dict (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.dict", false]
        ],
        "dict (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.dict", false]
        ],
        "dict (dadk.internal.samplingparameter.samplingparameter property)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.dict", false]
        ],
        "dict (dadk.internal.scalingparameter.scalingparameter property)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.dict", false]
        ],
        "dimensions (dadk.binpol.bitarrayshape property)": [
            [3, "dadk.BinPol.BitArrayShape.dimensions", false]
        ],
        "directory (dadk.jupytertools.directoryselector property)": [
            [5, "dadk.JupyterTools.DirectorySelector.directory", false]
        ],
        "directorymanager (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.DirectoryManager", false]
        ],
        "directoryselector (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.DirectorySelector", false]
        ],
        "disabled (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.disabled", false]
        ],
        "display() (dadk.jupytertools.outputtab method)": [
            [5, "dadk.JupyterTools.OutputTab.display", false]
        ],
        "display_applier() (dadk.jupytertools.taggedguicontrol method)": [
            [5, "dadk.JupyterTools.TaggedGuiControl.display_applier", false]
        ],
        "display_blobs() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.display_blobs", false]
        ],
        "display_content() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.display_content", false]
        ],
        "display_control() (dadk.jupytertools.numberplot method)": [
            [5, "dadk.JupyterTools.NumberPlot.display_control", false]
        ],
        "display_control() (dadk.jupytertools.paretoplot method)": [
            [5, "dadk.JupyterTools.ParetoPlot.display_control", false]
        ],
        "display_control() (dadk.jupytertools.traverse2plot method)": [
            [5, "dadk.JupyterTools.Traverse2Plot.display_control", false]
        ],
        "display_control() (dadk.jupytertools.traverseplot method)": [
            [5, "dadk.JupyterTools.TraversePlot.display_control", false]
        ],
        "display_creator() (dadk.jupytertools.taggedguicontrol method)": [
            [5, "dadk.JupyterTools.TaggedGuiControl.display_creator", false]
        ],
        "display_graphs() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.display_graphs", false]
        ],
        "display_jobs() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.display_jobs", false]
        ],
        "do_sampling() (dadk.qubosolverbase.samplingbasis method)": [
            [2, "dadk.QUBOSolverBase.SamplingBasis.do_sampling", false]
        ],
        "download_blob() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.download_blob", false]
        ],
        "download_job() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.download_job", false]
        ],
        "draw() (dadk.solution_solutionlist.bitarray method)": [
            [7, "dadk.Solution_SolutionList.BitArray.draw", false]
        ],
        "draw() (dadk.solution_solutionlist.solution method)": [
            [7, "dadk.Solution_SolutionList.Solution.draw", false]
        ],
        "duration_account_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_account_occupied", false]
        ],
        "duration_anneal (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_anneal", false]
        ],
        "duration_cpu (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_cpu", false]
        ],
        "duration_dau_service (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_dau_service", false]
        ],
        "duration_elapsed (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_elapsed", false]
        ],
        "duration_execution (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_execution", false]
        ],
        "duration_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_occupied", false]
        ],
        "duration_parse_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_parse_response", false]
        ],
        "duration_prepare_qubo (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_prepare_qubo", false]
        ],
        "duration_prepare_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_prepare_request", false]
        ],
        "duration_queue (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_queue", false]
        ],
        "duration_receive_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_receive_response", false]
        ],
        "duration_reduce_degree (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_reduce_degree", false]
        ],
        "duration_sampling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_sampling", false]
        ],
        "duration_save_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_save_request", false]
        ],
        "duration_save_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_save_response", false]
        ],
        "duration_scaling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_scaling", false]
        ],
        "duration_send_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_send_request", false]
        ],
        "duration_solve (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_solve", false]
        ],
        "duration_waiting (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_waiting", false]
        ],
        "duration_waiting_2_finish (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_waiting_2_finish", false]
        ],
        "duration_waiting_2_start (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.duration_waiting_2_start", false]
        ],
        "encode() (dadk.internal.connectionparameter.connectionparameter class method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.encode", false]
        ],
        "encode() (dadk.internal.connectionparameter.connectionparameterwithazureblob class method)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.encode", false]
        ],
        "encode() (dadk.internal.dav2parameter.dav2parameter class method)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.encode", false]
        ],
        "encode() (dadk.internal.dav3cparameter.dav3cparameter class method)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.encode", false]
        ],
        "encode() (dadk.internal.dav3parameter.dav3parameter class method)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.encode", false]
        ],
        "encode() (dadk.internal.dav4parameter.dav4parameter class method)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.encode", false]
        ],
        "encode() (dadk.internal.samplingparameter.samplingparameter class method)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.encode", false]
        ],
        "encode() (dadk.internal.scalingparameter.scalingparameter class method)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.encode", false]
        ],
        "encode() (dadk.internal.solvertimes.solvertimes class method)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.encode", false]
        ],
        "encode() (dadk.solution_solutionlist.solution class method)": [
            [7, "dadk.Solution_SolutionList.Solution.encode", false]
        ],
        "encode() (dadk.solution_solutionlist.solutionlist class method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.encode", false]
        ],
        "end_account_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_account_occupied", false]
        ],
        "end_anneal (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_anneal", false]
        ],
        "end_cpu (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_cpu", false]
        ],
        "end_dau_service (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_dau_service", false]
        ],
        "end_elapsed (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_elapsed", false]
        ],
        "end_execution (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_execution", false]
        ],
        "end_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_occupied", false]
        ],
        "end_parse_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_parse_response", false]
        ],
        "end_prepare_qubo (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_prepare_qubo", false]
        ],
        "end_prepare_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_prepare_request", false]
        ],
        "end_queue (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_queue", false]
        ],
        "end_receive_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_receive_response", false]
        ],
        "end_reduce_degree (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_reduce_degree", false]
        ],
        "end_sampling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_sampling", false]
        ],
        "end_save_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_save_request", false]
        ],
        "end_save_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_save_response", false]
        ],
        "end_scaling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_scaling", false]
        ],
        "end_send_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_send_request", false]
        ],
        "end_solve (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_solve", false]
        ],
        "end_waiting (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_waiting", false]
        ],
        "end_waiting_2_finish (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_waiting_2_finish", false]
        ],
        "end_waiting_2_start (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.end_waiting_2_start", false]
        ],
        "energies (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.energies", false]
        ],
        "energy (dadk.solution_solutionlist.progress.entry property)": [
            [7, "dadk.Solution_SolutionList.Progress.Entry.energy", false]
        ],
        "energy (dadk.solution_solutionlist.solution property)": [
            [7, "dadk.Solution_SolutionList.Solution.energy", false]
        ],
        "entries (dadk.binpol.onehotgroup property)": [
            [3, "dadk.BinPol.OneHotGroup.entries", false]
        ],
        "entry (class in dadk.solution_solutionlist.progress)": [
            [7, "dadk.Solution_SolutionList.Progress.Entry", false]
        ],
        "evaluate() (dadk.solution_solutionlist.bitarray method)": [
            [7, "dadk.Solution_SolutionList.BitArray.evaluate", false]
        ],
        "exactly_1_bit_on() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.exactly_1_bit_on", false]
        ],
        "exactly_n_bits_on() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.exactly_n_bits_on", false]
        ],
        "excel_summary_columns (dadk.optimizer.optimizersettings.composesettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings.excel_summary_columns", false]
        ],
        "extract_bit_array() (dadk.solution_solutionlist.solution method)": [
            [7, "dadk.Solution_SolutionList.Solution.extract_bit_array", false]
        ],
        "extract_bit_array() (dadk.solution_solutionlist.varset method)": [
            [7, "dadk.Solution_SolutionList.VarSet.extract_bit_array", false]
        ],
        "figsize (dadk.optimizer.optimizersettings.composelogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeLogSettings.figsize", false]
        ],
        "file (dadk.jupytertools.fileselector property)": [
            [5, "dadk.JupyterTools.FileSelector.file", false]
        ],
        "file_size_info() (in module dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.file_size_info", false]
        ],
        "fileselector (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.FileSelector", false]
        ],
        "flip_probabilities (dadk.internal.samplingparameter.samplingparameter property)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.flip_probabilities", false]
        ],
        "freeze_var_shape_set() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.freeze_var_shape_set", false]
        ],
        "freeze_var_shape_set() (dadk.binpol.isingpol class method)": [
            [3, "dadk.BinPol.IsingPol.freeze_var_shape_set", false]
        ],
        "from_qpoly() (dadk.binpol.bqp class method)": [
            [3, "dadk.BinPol.BQP.from_QPoly", false]
        ],
        "furnace_mean_energy_list (dadk.qubosolverbase.boltzmannsampling property)": [
            [2, "dadk.QUBOSolverBase.BoltzmannSampling.furnace_mean_energy_list", false]
        ],
        "furnace_process_length (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.furnace_process_length", false]
        ],
        "furnace_replica_list (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.furnace_replica_list", false]
        ],
        "furnaces_replica_history (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.furnaces_replica_history", false]
        ],
        "gen_latex() (dadk.binpolgen.binpolgen method)": [
            [4, "dadk.BinPolGen.BinPolGen.gen_latex", false]
        ],
        "gen_pretty_string() (dadk.binpolgen.binpolgen method)": [
            [4, "dadk.BinPolGen.BinPolGen.gen_pretty_string", false]
        ],
        "gen_python() (dadk.binpolgen.binpolgen method)": [
            [4, "dadk.BinPolGen.BinPolGen.gen_python", false]
        ],
        "generate_coordinates() (dadk.utils.coordinatesutils.coordinatesutils class method)": [
            [0, "dadk.utils.CoordinatesUtils.CoordinatesUtils.generate_coordinates", false]
        ],
        "generate_penalty_polynomial() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.generate_penalty_polynomial", false]
        ],
        "generate_replica_furnace_exchange_step() (dadk.qubosolverbase.paralleltemperingmanagement method)": [
            [2, "dadk.QUBOSolverBase.ParallelTemperingManagement.generate_replica_furnace_exchange_step", false]
        ],
        "generate_slack_polynomial() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.generate_slack_polynomial", false]
        ],
        "get() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get", false]
        ],
        "get() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.get", false]
        ],
        "get_active_parameters() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.get_active_parameters", false]
        ],
        "get_annealer_access_profile() (dadk.profileutils.profileutils class method)": [
            [0, "dadk.ProfileUtils.ProfileUtils.get_annealer_access_profile", false]
        ],
        "get_bit_flip() (dadk.internal.graphicsdata.graphicsdata method)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.get_bit_flip", false]
        ],
        "get_bits() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.get_bits", false]
        ],
        "get_bits_of_1w1h_group() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_bits_of_1w1h_group", false]
        ],
        "get_bits_of_2w1h_group() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_bits_of_2w1h_group", false]
        ],
        "get_default_setting() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.get_default_setting", false]
        ],
        "get_energy() (dadk.internal.graphicsdata.graphicsdata method)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.get_energy", false]
        ],
        "get_false_bit_indices() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_false_bit_indices", false]
        ],
        "get_free_bit_indices() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_free_bit_indices", false]
        ],
        "get_index() (dadk.binpol.bitarrayshape method)": [
            [3, "dadk.BinPol.BitArrayShape.get_index", false]
        ],
        "get_index() (dadk.binpol.category method)": [
            [3, "dadk.BinPol.Category.get_index", false]
        ],
        "get_index() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_index", false]
        ],
        "get_job() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.get_job", false]
        ],
        "get_job() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.get_job", false]
        ],
        "get_job() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.get_job", false]
        ],
        "get_json() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_json", false]
        ],
        "get_json_generator() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_json_generator", false]
        ],
        "get_max_abs_coefficient() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_max_abs_coefficient", false]
        ],
        "get_minimum_energy_solution() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.get_minimum_energy_solution", false]
        ],
        "get_normalize_factor() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_normalize_factor", false]
        ],
        "get_not_false_bit_indices() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_not_false_bit_indices", false]
        ],
        "get_parameter_definitions() (dadk.qubosolvercpu.qubosolvercpu class method)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU.get_parameter_definitions", false]
        ],
        "get_parameter_definitions() (dadk.qubosolverdav3c.qubosolverdav3c class method)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.get_parameter_definitions", false]
        ],
        "get_parameter_definitions() (dadk.qubosolverdav4.qubosolverdav4 class method)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.get_parameter_definitions", false]
        ],
        "get_persistent_setting() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.get_persistent_setting", false]
        ],
        "get_results() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.get_results", false]
        ],
        "get_scale_factor() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_scale_factor", false]
        ],
        "get_solution_list() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.get_solution_list", false]
        ],
        "get_solver_ids() (in module dadk.solverfactory)": [
            [2, "dadk.SolverFactory.get_solver_IDs", false]
        ],
        "get_sorted_solution_list() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.get_sorted_solution_list", false]
        ],
        "get_state() (dadk.internal.graphicsdata.graphicsdata method)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.get_state", false]
        ],
        "get_summary() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.get_summary", false]
        ],
        "get_symbolic() (dadk.binpol.bitarrayshape method)": [
            [3, "dadk.BinPol.BitArrayShape.get_symbolic", false]
        ],
        "get_symbolic() (dadk.binpol.category method)": [
            [3, "dadk.BinPol.Category.get_symbolic", false]
        ],
        "get_symbolic() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_symbolic", false]
        ],
        "get_temperature() (dadk.internal.graphicsdata.graphicsdata method)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.get_temperature", false]
        ],
        "get_time() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.get_time", false]
        ],
        "get_time() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.get_time", false]
        ],
        "get_times() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.get_times", false]
        ],
        "get_true_bit_indices() (dadk.binpol.varshapeset method)": [
            [3, "dadk.BinPol.VarShapeSet.get_true_bit_indices", false]
        ],
        "get_value() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.get_value", false]
        ],
        "get_weights() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.get_weights", false]
        ],
        "get_widget() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.get_widget", false]
        ],
        "get_widget() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.get_widget", false]
        ],
        "get_widget() (dadk.optimizer.optimizertab method)": [
            [6, "dadk.Optimizer.OptimizerTab.get_widget", false]
        ],
        "graphics (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.graphics", false]
        ],
        "graphics (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.graphics", false]
        ],
        "graphicsdata (class in dadk.internal.graphicsdata)": [
            [1, "dadk.internal.GraphicsData.GraphicsData", false]
        ],
        "graphicsdetail (class in dadk.internal.dav2parameter)": [
            [1, "dadk.internal.DAv2Parameter.GraphicsDetail", false]
        ],
        "graphs_figsize (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.graphs_figsize", false]
        ],
        "gs_max_penalty_coef (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_max_penalty_coef", false]
        ],
        "gs_max_penalty_coef (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_max_penalty_coef", false]
        ],
        "gs_max_penalty_coef (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_max_penalty_coef", false]
        ],
        "gs_num_iteration_cl (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_num_iteration_cl", false]
        ],
        "gs_num_iteration_cl (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_num_iteration_cl", false]
        ],
        "gs_num_iteration_cl (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_num_iteration_cl", false]
        ],
        "gs_num_iteration_factor (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_num_iteration_factor", false]
        ],
        "gs_num_iteration_factor (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_num_iteration_factor", false]
        ],
        "gs_num_iteration_factor (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_num_iteration_factor", false]
        ],
        "gs_ohs_xw1h_num_iteration_cl (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_ohs_xw1h_num_iteration_cl", false]
        ],
        "gs_ohs_xw1h_num_iteration_cl (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_ohs_xw1h_num_iteration_cl", false]
        ],
        "gs_ohs_xw1h_num_iteration_factor (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_ohs_xw1h_num_iteration_factor", false]
        ],
        "gs_ohs_xw1h_num_iteration_factor (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_ohs_xw1h_num_iteration_factor", false]
        ],
        "gs_penalty_auto_mode (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_penalty_auto_mode", false]
        ],
        "gs_penalty_auto_mode (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_penalty_auto_mode", false]
        ],
        "gs_penalty_auto_mode (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_penalty_auto_mode", false]
        ],
        "gs_penalty_coef (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_penalty_coef", false]
        ],
        "gs_penalty_coef (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_penalty_coef", false]
        ],
        "gs_penalty_coef (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_penalty_coef", false]
        ],
        "gs_penalty_inc_rate (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.gs_penalty_inc_rate", false]
        ],
        "gs_penalty_inc_rate (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.gs_penalty_inc_rate", false]
        ],
        "gs_penalty_inc_rate (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.gs_penalty_inc_rate", false]
        ],
        "gui_build() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.gui_build", false]
        ],
        "gui_compose (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.gui_compose", false]
        ],
        "gui_default_values() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.gui_default_values", false]
        ],
        "gui_load (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.gui_load", false]
        ],
        "gui_reset_values() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.gui_reset_values", false]
        ],
        "gui_save_values() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.gui_save_values", false]
        ],
        "gui_solve() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.gui_solve", false]
        ],
        "guidance_config (dadk.qubosolverbase.qubosolverbase property)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBase.guidance_config", false]
        ],
        "has_solver() (in module dadk.solverfactory)": [
            [2, "dadk.SolverFactory.has_solver", false]
        ],
        "headers (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.headers", false]
        ],
        "healthcheck() (dadk.qubosolverbase.qubosolverbase method)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBase.healthcheck", false]
        ],
        "healthcheck() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.healthcheck", false]
        ],
        "healthcheck() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.healthcheck", false]
        ],
        "healthcheck() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.healthcheck", false]
        ],
        "hill_energies (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.hill_energies", false]
        ],
        "hill_steps (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.hill_steps", false]
        ],
        "hobo2qubo() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.hobo2qubo", false]
        ],
        "hobo2qubo() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.hobo2qubo", false]
        ],
        "hobo2qubo() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.hobo2qubo", false]
        ],
        "i18n_get() (in module dadk.utils)": [
            [0, "dadk.Utils.i18n_get", false]
        ],
        "ignore_proxy (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.ignore_proxy", false]
        ],
        "index (dadk.binpol.makequbowarning property)": [
            [3, "dadk.BinPol.MakeQuboWarning.index", false]
        ],
        "inequalities (dadk.binpol.bqp property)": [
            [3, "dadk.BinPol.BQP.inequalities", false]
        ],
        "inequalities (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.inequalities", false]
        ],
        "inequalities2crs() (in module dadk.binpol)": [
            [3, "dadk.BinPol.inequalities2CRS", false]
        ],
        "inequalities_blob_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.inequalities_blob_name", false]
        ],
        "inequalities_filename (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.inequalities_filename", false]
        ],
        "inequality (class in dadk.binpol)": [
            [3, "dadk.BinPol.Inequality", false]
        ],
        "inequalitysign (class in dadk.binpol)": [
            [3, "dadk.BinPol.InequalitySign", false]
        ],
        "inflate_clamped_configuration() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.inflate_clamped_configuration", false]
        ],
        "interrupt (dadk.optimizer.interruptkey property)": [
            [6, "dadk.Optimizer.InterruptKey.interrupt", false]
        ],
        "interruptkey (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.InterruptKey", false]
        ],
        "is_empty() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.is_empty", false]
        ],
        "isingpol (class in dadk.binpol)": [
            [3, "dadk.BinPol.IsingPol", false]
        ],
        "job_id (dadk.qubosolverbase.jobstatus property)": [
            [2, "dadk.QUBOSolverBase.JobStatus.job_id", false]
        ],
        "jobstatus (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.JobStatus", false]
        ],
        "label (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.label", false]
        ],
        "lambda_value (dadk.binpol.inequality property)": [
            [3, "dadk.BinPol.Inequality.lambda_value", false]
        ],
        "last_modified (dadk.qubosolverbase.azureblobmixin.blob property)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.Blob.last_modified", false]
        ],
        "left_qubo (dadk.binpol.twosideinequality property)": [
            [3, "dadk.BinPol.TwoSideInequality.left_qubo", false]
        ],
        "length (dadk.binpol.bitarrayshape property)": [
            [3, "dadk.BinPol.BitArrayShape.length", false]
        ],
        "length (dadk.binpol.varshapeset property)": [
            [3, "dadk.BinPol.VarShapeSet.length", false]
        ],
        "list_annealer_access_profiles() (dadk.profileutils.profileutils class method)": [
            [0, "dadk.ProfileUtils.ProfileUtils.list_annealer_access_profiles", false]
        ],
        "list_blobs() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.list_blobs", false]
        ],
        "list_containers() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.list_containers", false]
        ],
        "list_jobs() (dadk.qubosolverbase.restmixin method)": [
            [2, "dadk.QUBOSolverBase.RestMixin.list_jobs", false]
        ],
        "list_jobs() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.list_jobs", false]
        ],
        "list_jobs() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.list_jobs", false]
        ],
        "list_jobs() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.list_jobs", false]
        ],
        "load() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.load", false]
        ],
        "load() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.load", false]
        ],
        "load_connection_parameter() (in module dadk.internal.connectionparameter)": [
            [1, "dadk.internal.ConnectionParameter.load_connection_parameter", false]
        ],
        "load_hdf5() (dadk.binpol.bqp class method)": [
            [3, "dadk.BinPol.BQP.load_hdf5", false]
        ],
        "load_json() (dadk.binpol.bqp class method)": [
            [3, "dadk.BinPol.BQP.load_json", false]
        ],
        "load_library() (in module dadk.utils)": [
            [0, "dadk.Utils.load_library", false]
        ],
        "load_qbsolv() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.load_qbsolv", false]
        ],
        "localminimasampling (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.LocalMinimaSampling", false]
        ],
        "log() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.log", false]
        ],
        "log_data() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.log_data", false]
        ],
        "log_memory_size (dadk.optimizer.optimizersettings.composesettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings.log_memory_size", false]
        ],
        "log_msg() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.log_msg", false]
        ],
        "logger (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.logger", false]
        ],
        "logger (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.logger", false]
        ],
        "logger (dadk.qubosolverbase.solverbase property)": [
            [2, "dadk.QUBOSolverBase.SolverBase.logger", false]
        ],
        "logger (dadk.qubosolverbase.temperaturemodel property)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel.logger", false]
        ],
        "make_clamped() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.make_clamped", false]
        ],
        "make_qubo() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.make_qubo", false]
        ],
        "make_qubo() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.make_qubo", false]
        ],
        "make_qubo() (dadk.binpol.samplefilter method)": [
            [3, "dadk.BinPol.SampleFilter.make_qubo", false]
        ],
        "makequbo_precision (dadk.binpol.samplefilter property)": [
            [3, "dadk.BinPol.SampleFilter.makeQUBO_precision", false]
        ],
        "makequboerror (class in dadk.binpol)": [
            [3, "dadk.BinPol.MakeQuboError", false]
        ],
        "makequbonotusedbitwarning (class in dadk.binpol)": [
            [3, "dadk.BinPol.MakeQuboNotUsedBitWarning", false]
        ],
        "makequboroundedtozerowarning (class in dadk.binpol)": [
            [3, "dadk.BinPol.MakeQuboRoundedToZeroWarning", false]
        ],
        "makequbowarning (class in dadk.binpol)": [
            [3, "dadk.BinPol.MakeQuboWarning", false]
        ],
        "math_prod() (in module dadk.utils)": [
            [0, "dadk.Utils.math_prod", false]
        ],
        "max (dadk.jupytertools.widgetboundednumberarray property)": [
            [5, "dadk.JupyterTools.WidgetBoundedNumberArray.max", false]
        ],
        "max_bits (dadk.binpol.qubosizeexceedssolvermaxbits property)": [
            [3, "dadk.BinPol.QUBOSizeExceedsSolverMaxBits.max_bits", false]
        ],
        "max_job_list_retry (dadk.qubosolverbase.restmixin property)": [
            [2, "dadk.QUBOSolverBase.RestMixin.max_job_list_retry", false]
        ],
        "max_number_of_bits() (dadk.qubosolvercpu.qubosolvercpu class method)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU.max_number_of_bits", false]
        ],
        "max_number_of_bits() (dadk.qubosolverdav3c.qubosolverdav3c class method)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.max_number_of_bits", false]
        ],
        "max_number_of_bits() (dadk.qubosolverdav4.qubosolverdav4 class method)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.max_number_of_bits", false]
        ],
        "merge_dicts() (in module dadk.utils)": [
            [0, "dadk.Utils.merge_dicts", false]
        ],
        "min (dadk.jupytertools.widgetboundednumberarray property)": [
            [5, "dadk.JupyterTools.WidgetBoundedNumberArray.min", false]
        ],
        "min_energy (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.min_energy", false]
        ],
        "min_solution (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.min_solution", false]
        ],
        "min_step (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.min_step", false]
        ],
        "minimize() (dadk.qubosolvercpu.qubosolvercpu method)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU.minimize", false]
        ],
        "minimize() (dadk.qubosolverdav3c.qubosolverdav3c method)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.minimize", false]
        ],
        "minimize() (dadk.qubosolverdav4.qubosolverdav4 method)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.minimize", false]
        ],
        "minimize() (in module dadk.flask.quboservicecpu)": [
            [8, "dadk.flask.QUBOServiceCPU.minimize", false]
        ],
        "minimize() (in module dadk.flask.quboservicedav3c)": [
            [9, "dadk.flask.QUBOServiceDAv3c.minimize", false]
        ],
        "minimize() (in module dadk.flask.quboservicedav4)": [
            [10, "dadk.flask.QUBOServiceDAv4.minimize", false]
        ],
        "most_1_bit_on() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.most_1_bit_on", false]
        ],
        "move_to() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.move_to", false]
        ],
        "multiply() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.multiply", false]
        ],
        "multiply_scalar() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.multiply_scalar", false]
        ],
        "multiply_scalar() (dadk.binpol.inequality method)": [
            [3, "dadk.BinPol.Inequality.multiply_scalar", false]
        ],
        "multiply_scalar() (dadk.binpol.samplefilter method)": [
            [3, "dadk.BinPol.SampleFilter.multiply_scalar", false]
        ],
        "n (dadk.binpol.binpol property)": [
            [3, "dadk.BinPol.BinPol.N", false]
        ],
        "n (dadk.binpol.isingpol property)": [
            [3, "dadk.BinPol.IsingPol.N", false]
        ],
        "name (dadk.binpol.bitarrayshape property)": [
            [3, "dadk.BinPol.BitArrayShape.name", false]
        ],
        "name (dadk.binpol.qubosizeexceedssolvermaxbits property)": [
            [3, "dadk.BinPol.QUBOSizeExceedsSolverMaxBits.name", false]
        ],
        "name (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.name", false]
        ],
        "normalize() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.normalize", false]
        ],
        "num_group (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.num_group", false]
        ],
        "num_group (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.num_group", false]
        ],
        "num_group (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.num_group", false]
        ],
        "num_output_solution (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.num_output_solution", false]
        ],
        "num_output_solution (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.num_output_solution", false]
        ],
        "num_output_solution (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.num_output_solution", false]
        ],
        "num_solution (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.num_solution", false]
        ],
        "num_solution (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.num_solution", false]
        ],
        "num_solution (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.num_solution", false]
        ],
        "number_iterations (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.number_iterations", false]
        ],
        "number_of_bits (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.number_of_bits", false]
        ],
        "number_of_hills (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.number_of_hills", false]
        ],
        "number_of_iterations (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.number_of_iterations", false]
        ],
        "number_of_valleys (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.number_of_valleys", false]
        ],
        "number_of_walks (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.number_of_walks", false]
        ],
        "number_replicas (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.number_replicas", false]
        ],
        "number_runs (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.number_runs", false]
        ],
        "numberplot (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.NumberPlot", false]
        ],
        "numbertable (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.NumberTable", false]
        ],
        "object_hook() (dadk.jsontools.daodecoder class method)": [
            [0, "dadk.JSONTools.DaoDecoder.object_hook", false]
        ],
        "offset_energy (dadk.qubosolverbase.localminimasampling property)": [
            [2, "dadk.QUBOSolverBase.LocalMinimaSampling.offset_energy", false]
        ],
        "offset_increase_rate (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.offset_increase_rate", false]
        ],
        "ohs_xw1h_internal_penalty (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.ohs_xw1h_internal_penalty", false]
        ],
        "ohs_xw1h_internal_penalty (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.ohs_xw1h_internal_penalty", false]
        ],
        "on_change_code (dadk.jupytertools.fileselector property)": [
            [5, "dadk.JupyterTools.FileSelector.on_change_code", false]
        ],
        "one_hot (dadk.binpol.bitarrayshape property)": [
            [3, "dadk.BinPol.BitArrayShape.one_hot", false]
        ],
        "one_hot (dadk.binpol.varshapeset property)": [
            [3, "dadk.BinPol.VarShapeSet.one_hot", false]
        ],
        "onehot (class in dadk.binpol)": [
            [3, "dadk.BinPol.OneHot", false]
        ],
        "onehotgroup (class in dadk.binpol)": [
            [3, "dadk.BinPol.OneHotGroup", false]
        ],
        "optimization_method (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.optimization_method", false]
        ],
        "optimization_method (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.optimization_method", false]
        ],
        "optimizationmethod (class in dadk.internal.dav2parameter)": [
            [1, "dadk.internal.DAv2Parameter.OptimizationMethod", false]
        ],
        "optimizer (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.Optimizer", false]
        ],
        "optimizer (dadk.optimizer.optimizermodelcomplex property)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.Optimizer", false]
        ],
        "optimizermodel (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.OptimizerModel", false]
        ],
        "optimizermodelcomplex (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.OptimizerModelComplex", false]
        ],
        "optimizersettings (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.OptimizerSettings", false]
        ],
        "optimizertab (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.OptimizerTab", false]
        ],
        "optimizertabrequirement (class in dadk.optimizer)": [
            [6, "dadk.Optimizer.OptimizerTabRequirement", false]
        ],
        "optuna (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.optuna", false]
        ],
        "optuna() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.optuna", false]
        ],
        "optuna_dirname (dadk.optimizer.optimizermodelcomplex property)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.optuna_dirname", false]
        ],
        "optunasettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.OptunaSettings", false]
        ],
        "outputtab (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.OutputTab", false]
        ],
        "p (dadk.binpol.binpol property)": [
            [3, "dadk.BinPol.BinPol.p", false]
        ],
        "p (dadk.binpol.isingpol property)": [
            [3, "dadk.BinPol.IsingPol.p", false]
        ],
        "paralleltemperingmanagement (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.ParallelTemperingManagement", false]
        ],
        "parameter_logging (dadk.optimizer.optimizersettings.batchsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.BatchSettings.parameter_logging", false]
        ],
        "parameterlogging (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.ParameterLogging", false]
        ],
        "pareto (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.pareto", false]
        ],
        "pareto() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.pareto", false]
        ],
        "pareto_dirname (dadk.optimizer.optimizermodelcomplex property)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.pareto_dirname", false]
        ],
        "paretoplot (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.ParetoPlot", false]
        ],
        "paretosettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.ParetoSettings", false]
        ],
        "parse_poly() (dadk.binpolgen.binpolgen method)": [
            [4, "dadk.BinPolGen.BinPolGen.parse_poly", false]
        ],
        "partialconfig (class in dadk.binpol)": [
            [3, "dadk.BinPol.PartialConfig", false]
        ],
        "path (dadk.jupytertools.fileselector property)": [
            [5, "dadk.JupyterTools.FileSelector.path", false]
        ],
        "path (dadk.optimizer.directorymanager property)": [
            [6, "dadk.Optimizer.DirectoryManager.path", false]
        ],
        "penalty_energy (dadk.solution_solutionlist.progress.entry property)": [
            [7, "dadk.Solution_SolutionList.Progress.Entry.penalty_energy", false]
        ],
        "penalty_energy (dadk.solution_solutionlist.solution property)": [
            [7, "dadk.Solution_SolutionList.Solution.penalty_energy", false]
        ],
        "penalty_qubo (dadk.binpol.bqp property)": [
            [3, "dadk.BinPol.BQP.penalty_qubo", false]
        ],
        "penalty_qubo (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.penalty_qubo", false]
        ],
        "penalty_qubo_blob_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.penalty_qubo_blob_name", false]
        ],
        "penalty_qubo_filename (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.penalty_qubo_filename", false]
        ],
        "plot_coordinates() (dadk.utils.coordinatesutils.coordinatesutils class method)": [
            [0, "dadk.utils.CoordinatesUtils.CoordinatesUtils.plot_coordinates", false]
        ],
        "plot_graphs() (dadk.internal.graphicsdata.graphicsdata method)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.plot_graphs", false]
        ],
        "plot_histogram() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.plot_histogram", false]
        ],
        "plot_route() (dadk.utils.tsputils.tsputils class method)": [
            [0, "dadk.utils.TSPUtils.TSPUtils.plot_route", false]
        ],
        "plot_temperature_curve() (dadk.internal.dav2parameter.dav2parameter method)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.plot_temperature_curve", false]
        ],
        "power() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.power", false]
        ],
        "power2() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.power2", false]
        ],
        "prep_result() (dadk.optimizer.optimizermodelcomplex method)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.prep_result", false]
        ],
        "prepare_content() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.prepare_content", false]
        ],
        "print_progress() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.print_progress", false]
        ],
        "print_stats() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.print_stats", false]
        ],
        "print_to_output() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.print_to_output", false]
        ],
        "probability_model (dadk.internal.samplingparameter.samplingparameter property)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.probability_model", false]
        ],
        "probabilitymodel (class in dadk.internal.samplingparameter)": [
            [1, "dadk.internal.SamplingParameter.ProbabilityModel", false]
        ],
        "profileutils (class in dadk.profileutils)": [
            [0, "dadk.ProfileUtils.ProfileUtils", false]
        ],
        "progress (class in dadk.solution_solutionlist)": [
            [7, "dadk.Solution_SolutionList.Progress", false]
        ],
        "progress (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.progress", false]
        ],
        "progressbar (class in dadk.qubosolverbase.azureblobmixin)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.ProgressBar", false]
        ],
        "prolog() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.prolog", false]
        ],
        "prolog_blob_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.prolog_blob_name", false]
        ],
        "prolog_filename (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.prolog_filename", false]
        ],
        "proxy (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.proxy", false]
        ],
        "proxy_password (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.proxy_password", false]
        ],
        "proxy_port (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.proxy_port", false]
        ],
        "proxy_user (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.proxy_user", false]
        ],
        "pt_replica_exchange_model (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.pt_replica_exchange_model", false]
        ],
        "pt_temperature_model (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.pt_temperature_model", false]
        ],
        "qubo (dadk.binpol.bqp property)": [
            [3, "dadk.BinPol.BQP.qubo", false]
        ],
        "qubo (dadk.binpol.inequality property)": [
            [3, "dadk.BinPol.Inequality.qubo", false]
        ],
        "qubo (dadk.binpol.samplefilter property)": [
            [3, "dadk.BinPol.SampleFilter.qubo", false]
        ],
        "qubo (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.qubo", false]
        ],
        "qubo2crs() (in module dadk.binpol)": [
            [3, "dadk.BinPol.qubo2CRS", false]
        ],
        "qubo_blob_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.qubo_blob_name", false]
        ],
        "qubo_filename (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.qubo_filename", false]
        ],
        "quboeditor (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.QuboEditor", false]
        ],
        "qubosizeexceedssolvermaxbits (class in dadk.binpol)": [
            [3, "dadk.BinPol.QUBOSizeExceedsSolverMaxBits", false]
        ],
        "qubosolverbase (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBase", false]
        ],
        "qubosolverbasev2 (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBaseV2", false]
        ],
        "qubosolverbasev3 (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBaseV3", false]
        ],
        "qubosolvercpu (class in dadk.qubosolvercpu)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU", false]
        ],
        "qubosolverdav3c (class in dadk.qubosolverdav3c)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c", false]
        ],
        "qubosolverdav4 (class in dadk.qubosolverdav4)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4", false]
        ],
        "qubosolveremulator (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverEmulator", false]
        ],
        "random_seed (dadk.qubosolverbase.solverbase property)": [
            [2, "dadk.QUBOSolverBase.SolverBase.random_seed", false]
        ],
        "randomwalk_count (dadk.qubosolverbase.boltzmannsampling property)": [
            [2, "dadk.QUBOSolverBase.BoltzmannSampling.randomwalk_count", false]
        ],
        "randomwalk_length (dadk.qubosolverbase.boltzmannsampling property)": [
            [2, "dadk.QUBOSolverBase.BoltzmannSampling.randomwalk_length", false]
        ],
        "read_json_file() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.read_json_file", false]
        ],
        "read_text_file() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.read_text_file", false]
        ],
        "reduce_higher_degree() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.reduce_higher_degree", false]
        ],
        "reduce_higher_degree_to_qubo() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.reduce_higher_degree_to_qubo", false]
        ],
        "register_decoder() (dadk.jsontools.daodecoder class method)": [
            [0, "dadk.JSONTools.DaoDecoder.register_decoder", false]
        ],
        "register_encoder() (dadk.jsontools.daoencoder class method)": [
            [0, "dadk.JSONTools.DaoEncoder.register_encoder", false]
        ],
        "register_function() (dadk.jsontools.daodecoder class method)": [
            [0, "dadk.JSONTools.DaoDecoder.register_function", false]
        ],
        "register_function() (dadk.jsontools.daoencoder class method)": [
            [0, "dadk.JSONTools.DaoEncoder.register_function", false]
        ],
        "register_logging_output() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.register_logging_output", false]
        ],
        "register_solver() (in module dadk.solverfactory)": [
            [2, "dadk.SolverFactory.register_solver", false]
        ],
        "register_widget_class() (dadk.jupytertools.widgetgui static method)": [
            [5, "dadk.JupyterTools.WidgetGui.register_widget_class", false]
        ],
        "relative_directory (dadk.jupytertools.directoryselector property)": [
            [5, "dadk.JupyterTools.DirectorySelector.relative_directory", false]
        ],
        "remove() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.remove", false]
        ],
        "remove_all_tabs() (dadk.jupytertools.widgettab method)": [
            [5, "dadk.JupyterTools.WidgetTab.remove_all_tabs", false]
        ],
        "remove_files() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.remove_files", false]
        ],
        "replica_count (dadk.qubosolverbase.temperaturemodel property)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel.replica_count", false]
        ],
        "replica_exchange_count (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.replica_exchange_count", false]
        ],
        "replica_furnace_history (dadk.qubosolverbase.paralleltemperingmanagement property)": [
            [2, "dadk.QUBOSolverBase.ParallelTemperingManagement.replica_furnace_history", false]
        ],
        "replica_furnace_history (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.replica_furnace_history", false]
        ],
        "replica_furnace_list (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.replica_furnace_list", false]
        ],
        "replica_history (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.replica_history", false]
        ],
        "replica_temperature_list (dadk.qubosolverbase.paralleltemperingmanagement property)": [
            [2, "dadk.QUBOSolverBase.ParallelTemperingManagement.replica_temperature_list", false]
        ],
        "replica_temperature_list (dadk.qubosolverbase.replicaexchangemodel property)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel.replica_temperature_list", false]
        ],
        "replicaexchangemodel (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModel", false]
        ],
        "replicaexchangemodelfarjump (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.ReplicaExchangeModelFarJump", false]
        ],
        "report_fact() (dadk.jupytertools.timereporter method)": [
            [5, "dadk.JupyterTools.TimeReporter.report_fact", false]
        ],
        "request_mode (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.request_mode", false]
        ],
        "requestmode (class in dadk.internal.connectionparameter)": [
            [1, "dadk.internal.ConnectionParameter.RequestMode", false]
        ],
        "restmixin (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.RestMixin", false]
        ],
        "restore() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.restore", false]
        ],
        "right_qubo (dadk.binpol.twosideinequality property)": [
            [3, "dadk.BinPol.TwoSideInequality.right_qubo", false]
        ],
        "round_to_n() (in module dadk.utils)": [
            [0, "dadk.Utils.round_to_n", false]
        ],
        "row_labels (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.row_labels", false]
        ],
        "rows (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.rows", false]
        ],
        "run() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.run", false]
        ],
        "samplefilter (class in dadk.binpol)": [
            [3, "dadk.BinPol.SampleFilter", false]
        ],
        "sampling_parameter (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.sampling_parameter", false]
        ],
        "sampling_runs (dadk.internal.samplingparameter.samplingparameter property)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter.sampling_runs", false]
        ],
        "samplingbasis (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.SamplingBasis", false]
        ],
        "samplingparameter (class in dadk.internal.samplingparameter)": [
            [1, "dadk.internal.SamplingParameter.SamplingParameter", false]
        ],
        "sas_token (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.sas_token", false]
        ],
        "save_as_hdf5() (dadk.binpol.bqp method)": [
            [3, "dadk.BinPol.BQP.save_as_hdf5", false]
        ],
        "save_graphs (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.save_graphs", false]
        ],
        "save_graphs (dadk.optimizer.optimizersettings.composelogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeLogSettings.save_graphs", false]
        ],
        "save_graphs_figsize (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.save_graphs_figsize", false]
        ],
        "save_graphs_figsize (dadk.optimizer.optimizersettings.composelogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeLogSettings.save_graphs_figsize", false]
        ],
        "save_times (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.save_times", false]
        ],
        "save_times_figsize (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.save_times_figsize", false]
        ],
        "scale() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.scale", false]
        ],
        "scaling_action (dadk.internal.scalingparameter.scalingparameter property)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.scaling_action", false]
        ],
        "scaling_bit_precision (dadk.internal.scalingparameter.scalingparameter property)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.scaling_bit_precision", false]
        ],
        "scaling_factor (dadk.internal.scalingparameter.scalingparameter property)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter.scaling_factor", false]
        ],
        "scaling_parameter (dadk.qubosolverbase.qubosolverbase property)": [
            [2, "dadk.QUBOSolverBase.QUBOSolverBase.scaling_parameter", false]
        ],
        "scaling_parameter (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.scaling_parameter", false]
        ],
        "scalingaction (class in dadk.internal.scalingparameter)": [
            [1, "dadk.internal.ScalingParameter.ScalingAction", false]
        ],
        "scalingparameter (class in dadk.internal.scalingparameter)": [
            [1, "dadk.internal.ScalingParameter.ScalingParameter", false]
        ],
        "search_stats_info() (dadk.solution_solutionlist.solutionlist method)": [
            [7, "dadk.Solution_SolutionList.SolutionList.search_stats_info", false]
        ],
        "select_instance() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.select_instance", false]
        ],
        "select_tab() (dadk.jupytertools.widgettab method)": [
            [5, "dadk.JupyterTools.WidgetTab.select_tab", false]
        ],
        "set() (dadk.qubosolverbase.azureblobmixin.progressbar method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.ProgressBar.set", false]
        ],
        "set_bit() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.set_bit", false]
        ],
        "set_build_qubo_details_reference() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_build_qubo_details_reference", false]
        ],
        "set_calculated_build_qubo_parameter() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_calculated_build_qubo_parameter", false]
        ],
        "set_calculated_solver_parameter() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_calculated_solver_parameter", false]
        ],
        "set_clone_warning_counter() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.set_CLONE_WARNING_COUNTER", false]
        ],
        "set_compose_model_details_reference() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_compose_model_details_reference", false]
        ],
        "set_directory() (dadk.jupytertools.directoryselector method)": [
            [5, "dadk.JupyterTools.DirectorySelector.set_directory", false]
        ],
        "set_disabled() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.set_disabled", false]
        ],
        "set_disabled() (dadk.optimizer.optimizertab method)": [
            [6, "dadk.Optimizer.OptimizerTab.set_disabled", false]
        ],
        "set_fixed_solver_parameter() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_fixed_solver_parameter", false]
        ],
        "set_load_details_reference() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_load_details_reference", false]
        ],
        "set_persistent_setting() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.set_persistent_setting", false]
        ],
        "set_prep_result_details_reference() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_prep_result_details_reference", false]
        ],
        "set_solver_parameter() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.set_solver_parameter", false]
        ],
        "set_term() (dadk.binpol.binpol method)": [
            [3, "dadk.BinPol.BinPol.set_term", false]
        ],
        "set_term() (dadk.binpol.isingpol method)": [
            [3, "dadk.BinPol.IsingPol.set_term", false]
        ],
        "set_title() (dadk.jupytertools.widgettab method)": [
            [5, "dadk.JupyterTools.WidgetTab.set_title", false]
        ],
        "set_value() (dadk.jupytertools.fileselector method)": [
            [5, "dadk.JupyterTools.FileSelector.set_value", false]
        ],
        "set_value() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.set_value", false]
        ],
        "set_variable() (dadk.binpol.partialconfig method)": [
            [3, "dadk.BinPol.PartialConfig.set_variable", false]
        ],
        "set_visibility() (dadk.jupytertools.outputtab method)": [
            [5, "dadk.JupyterTools.OutputTab.set_visibility", false]
        ],
        "setannealerprofile (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.SetAnnealerProfile", false]
        ],
        "shape (dadk.binpol.bitarrayshape property)": [
            [3, "dadk.BinPol.BitArrayShape.shape", false]
        ],
        "shape (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.shape", false]
        ],
        "show_summary (dadk.optimizer.optimizersettings.composesettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings.show_summary", false]
        ],
        "showsolverparameter() (in module dadk.jupytertools)": [
            [5, "dadk.JupyterTools.ShowSolverParameter", false]
        ],
        "sign (dadk.binpol.twosideinequality property)": [
            [3, "dadk.BinPol.TwoSideInequality.sign", false]
        ],
        "silent (dadk.jupytertools.timereporter property)": [
            [5, "dadk.JupyterTools.TimeReporter.silent", false]
        ],
        "size (dadk.binpol.binpol property)": [
            [3, "dadk.BinPol.BinPol.size", false]
        ],
        "size (dadk.binpol.isingpol property)": [
            [3, "dadk.BinPol.IsingPol.size", false]
        ],
        "size (dadk.binpol.partialconfig property)": [
            [3, "dadk.BinPol.PartialConfig.size", false]
        ],
        "size (dadk.qubosolverbase.azureblobmixin.blob property)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.Blob.size", false]
        ],
        "slacktype (class in dadk.binpol)": [
            [3, "dadk.BinPol.SlackType", false]
        ],
        "sleep_duration (dadk.qubosolverbase.restmixin property)": [
            [2, "dadk.QUBOSolverBase.RestMixin.sleep_duration", false]
        ],
        "sleep_function (dadk.qubosolverbase.restmixin property)": [
            [2, "dadk.QUBOSolverBase.RestMixin.sleep_function", false]
        ],
        "solution (class in dadk.solution_solutionlist)": [
            [7, "dadk.Solution_SolutionList.Solution", false]
        ],
        "solution_mode (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.solution_mode", false]
        ],
        "solutionlist (class in dadk.solution_solutionlist)": [
            [7, "dadk.Solution_SolutionList.SolutionList", false]
        ],
        "solutionmode (class in dadk.internal.dav2parameter)": [
            [1, "dadk.internal.DAv2Parameter.SolutionMode", false]
        ],
        "solve_dav2 (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.solve_dav2", false]
        ],
        "solve_gui (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.solve_gui", false]
        ],
        "solvedav2settings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.SolveDAv2Settings", false]
        ],
        "solveguisettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.SolveGUISettings", false]
        ],
        "solver_times (dadk.solution_solutionlist.solutionlist property)": [
            [7, "dadk.Solution_SolutionList.SolutionList.solver_times", false]
        ],
        "solverbase (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.SolverBase", false]
        ],
        "solvertimes (class in dadk.internal.solvertimes)": [
            [1, "dadk.internal.SolverTimes.SolverTimes", false]
        ],
        "sort() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.sort", false]
        ],
        "source (dadk.binpolgen.binpolgen property)": [
            [4, "dadk.BinPolGen.BinPolGen.source", false]
        ],
        "ssl_disable_warnings (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.ssl_disable_warnings", false]
        ],
        "ssl_verify (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.ssl_verify", false]
        ],
        "start (dadk.binpol.variable property)": [
            [3, "dadk.BinPol.Variable.start", false]
        ],
        "start (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.start", false]
        ],
        "start_account_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_account_occupied", false]
        ],
        "start_anneal (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_anneal", false]
        ],
        "start_cpu (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_cpu", false]
        ],
        "start_dau_service (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_dau_service", false]
        ],
        "start_elapsed (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_elapsed", false]
        ],
        "start_execution (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_execution", false]
        ],
        "start_occupied (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_occupied", false]
        ],
        "start_parse_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_parse_response", false]
        ],
        "start_prepare_qubo (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_prepare_qubo", false]
        ],
        "start_prepare_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_prepare_request", false]
        ],
        "start_queue (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_queue", false]
        ],
        "start_receive_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_receive_response", false]
        ],
        "start_reduce_degree (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_reduce_degree", false]
        ],
        "start_sampling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_sampling", false]
        ],
        "start_save_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_save_request", false]
        ],
        "start_save_response (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_save_response", false]
        ],
        "start_scaling (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_scaling", false]
        ],
        "start_send_request (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_send_request", false]
        ],
        "start_solve (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_solve", false]
        ],
        "start_states (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.start_states", false]
        ],
        "start_time (dadk.qubosolverbase.jobstatus property)": [
            [2, "dadk.QUBOSolverBase.JobStatus.start_time", false]
        ],
        "start_waiting (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_waiting", false]
        ],
        "start_waiting_2_finish (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_waiting_2_finish", false]
        ],
        "start_waiting_2_start (dadk.internal.solvertimes.solvertimes property)": [
            [1, "dadk.internal.SolverTimes.SolverTimes.start_waiting_2_start", false]
        ],
        "status (dadk.qubosolverbase.jobstatus property)": [
            [2, "dadk.QUBOSolverBase.JobStatus.status", false]
        ],
        "step (dadk.binpol.variable property)": [
            [3, "dadk.BinPol.Variable.step", false]
        ],
        "step (dadk.jupytertools.widgetboundednumberarray property)": [
            [5, "dadk.JupyterTools.WidgetBoundedNumberArray.step", false]
        ],
        "step (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.step", false]
        ],
        "stop (dadk.binpol.variable property)": [
            [3, "dadk.BinPol.Variable.stop", false]
        ],
        "stop (dadk.solution_solutionlist.bitarray property)": [
            [7, "dadk.Solution_SolutionList.BitArray.stop", false]
        ],
        "stop() (dadk.optimizer.interruptkey method)": [
            [6, "dadk.Optimizer.InterruptKey.stop", false]
        ],
        "storage_account_name (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.storage_account_name", false]
        ],
        "storage_account_type (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.storage_account_type", false]
        ],
        "storage_connection_string (dadk.internal.connectionparameter.connectionparameterwithazureblob property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob.storage_connection_string", false]
        ],
        "storageaccounttype (class in dadk.internal.connectionparameter)": [
            [1, "dadk.internal.ConnectionParameter.StorageAccountType", false]
        ],
        "store_annealer_access_profile() (dadk.profileutils.profileutils class method)": [
            [0, "dadk.ProfileUtils.ProfileUtils.store_annealer_access_profile", false]
        ],
        "store_graphics (dadk.optimizer.optimizersettings.solvedav2settings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.SolveDAv2Settings.store_graphics", false]
        ],
        "string_2_configuration() (in module dadk.utils)": [
            [0, "dadk.Utils.string_2_configuration", false]
        ],
        "sum() (dadk.binpol.binpol class method)": [
            [3, "dadk.BinPol.BinPol.sum", false]
        ],
        "summary_columns (dadk.optimizer.optimizersettings.composesettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeSettings.summary_columns", false]
        ],
        "tab_build (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_build", false]
        ],
        "tab_compose (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_compose", false]
        ],
        "tab_load (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_load", false]
        ],
        "tab_optuna (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_optuna", false]
        ],
        "tab_out (dadk.jupytertools.outputtab property)": [
            [5, "dadk.JupyterTools.OutputTab.tab_out", false]
        ],
        "tab_pareto (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_pareto", false]
        ],
        "tab_report() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.tab_report", false]
        ],
        "tab_solve (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_solve", false]
        ],
        "tab_timeseries (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_timeseries", false]
        ],
        "tab_visualization (dadk.optimizer.optimizer property)": [
            [6, "dadk.Optimizer.Optimizer.tab_visualization", false]
        ],
        "taggedguicontrol (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.TaggedGuiControl", false]
        ],
        "take_time() (dadk.jupytertools.timereporter method)": [
            [5, "dadk.JupyterTools.TimeReporter.take_time", false]
        ],
        "target_energy (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.target_energy", false]
        ],
        "target_energy (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.target_energy", false]
        ],
        "target_energy (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.target_energy", false]
        ],
        "temperature_decay (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_decay", false]
        ],
        "temperature_end (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_end", false]
        ],
        "temperature_end (dadk.qubosolverbase.localminimasampling property)": [
            [2, "dadk.QUBOSolverBase.LocalMinimaSampling.temperature_end", false]
        ],
        "temperature_high (dadk.qubosolverbase.temperaturemodel property)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel.temperature_high", false]
        ],
        "temperature_interval (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_interval", false]
        ],
        "temperature_list (dadk.qubosolverbase.temperaturemodel property)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel.temperature_list", false]
        ],
        "temperature_low (dadk.qubosolverbase.temperaturemodel property)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel.temperature_low", false]
        ],
        "temperature_mode (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_mode", false]
        ],
        "temperature_sampling (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_sampling", false]
        ],
        "temperature_start (dadk.internal.dav2parameter.dav2parameter property)": [
            [1, "dadk.internal.DAv2Parameter.DAv2Parameter.temperature_start", false]
        ],
        "temperature_start (dadk.qubosolverbase.localminimasampling property)": [
            [2, "dadk.QUBOSolverBase.LocalMinimaSampling.temperature_start", false]
        ],
        "temperaturemodel (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.TemperatureModel", false]
        ],
        "temperaturemodelexponential (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.TemperatureModelExponential", false]
        ],
        "temperaturemodelhukushima (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.TemperatureModelHukushima", false]
        ],
        "temperaturemodellinear (class in dadk.qubosolverbase)": [
            [2, "dadk.QUBOSolverBase.TemperatureModelLinear", false]
        ],
        "temperatures (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.temperatures", false]
        ],
        "term (class in dadk.binpol)": [
            [3, "dadk.BinPol.Term", false]
        ],
        "text_content() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.text_content", false]
        ],
        "time (dadk.solution_solutionlist.progress.entry property)": [
            [7, "dadk.Solution_SolutionList.Progress.Entry.time", false]
        ],
        "time_limit_sec (dadk.internal.dav3cparameter.dav3cparameter property)": [
            [1, "dadk.internal.DAv3cParameter.DAv3cParameter.time_limit_sec", false]
        ],
        "time_limit_sec (dadk.internal.dav3parameter.dav3parameter property)": [
            [1, "dadk.internal.DAv3Parameter.DAv3Parameter.time_limit_sec", false]
        ],
        "time_limit_sec (dadk.internal.dav4parameter.dav4parameter property)": [
            [1, "dadk.internal.DAv4Parameter.DAv4Parameter.time_limit_sec", false]
        ],
        "timeout (dadk.internal.connectionparameter.connectionparameter property)": [
            [1, "dadk.internal.ConnectionParameter.ConnectionParameter.timeout", false]
        ],
        "timepicker (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.TimePicker", false]
        ],
        "timereporter (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.TimeReporter", false]
        ],
        "times_figsize (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.times_figsize", false]
        ],
        "timeseries (dadk.optimizer.optimizersettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.timeseries", false]
        ],
        "timeseries() (dadk.optimizer.optimizer method)": [
            [6, "dadk.Optimizer.Optimizer.timeseries", false]
        ],
        "timeseries_dirname (dadk.optimizer.optimizermodelcomplex property)": [
            [6, "dadk.Optimizer.OptimizerModelComplex.timeseries_dirname", false]
        ],
        "timeseriessettings (class in dadk.optimizer.optimizersettings)": [
            [6, "dadk.Optimizer.OptimizerSettings.TimeseriesSettings", false]
        ],
        "timetracker (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.TimeTracker", false]
        ],
        "to_qpoly() (dadk.binpol.bqp method)": [
            [3, "dadk.BinPol.BQP.to_QPoly", false]
        ],
        "traverse2plot (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.Traverse2Plot", false]
        ],
        "traverseplot (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.TraversePlot", false]
        ],
        "tree (dadk.binpolgen.binpolgen property)": [
            [4, "dadk.BinPolGen.BinPolGen.tree", false]
        ],
        "trigger_change_handlers() (dadk.jupytertools.widgetgui method)": [
            [5, "dadk.JupyterTools.WidgetGui.trigger_change_handlers", false]
        ],
        "tsputils (class in dadk.utils.tsputils)": [
            [0, "dadk.utils.TSPUtils.TSPUtils", false]
        ],
        "twosideinequality (class in dadk.binpol)": [
            [3, "dadk.BinPol.TwoSideInequality", false]
        ],
        "update_annealer_access_profiles() (dadk.profileutils.profileutils class method)": [
            [0, "dadk.ProfileUtils.ProfileUtils.update_annealer_access_profiles", false]
        ],
        "update_content() (dadk.jupytertools.numbertable method)": [
            [5, "dadk.JupyterTools.NumberTable.update_content", false]
        ],
        "update_last_setting() (dadk.jupytertools.taggedguicontrol method)": [
            [5, "dadk.JupyterTools.TaggedGuiControl.update_last_setting", false]
        ],
        "upload_blob() (dadk.qubosolverbase.azureblobmixin method)": [
            [2, "dadk.QUBOSolverBase.AzureBlobMixin.upload_blob", false]
        ],
        "upper_limit (dadk.binpol.samplefilter property)": [
            [3, "dadk.BinPol.SampleFilter.upper_limit", false]
        ],
        "url (dadk.qubosolvercpu.qubosolvercpu property)": [
            [2, "dadk.QUBOSolverCPU.QUBOSolverCPU.url", false]
        ],
        "url (dadk.qubosolverdav3c.qubosolverdav3c property)": [
            [2, "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c.url", false]
        ],
        "url (dadk.qubosolverdav4.qubosolverdav4 property)": [
            [2, "dadk.QUBOSolverDAv4.QUBOSolverDAv4.url", false]
        ],
        "valley_energies (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.valley_energies", false]
        ],
        "valley_steps (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.valley_steps", false]
        ],
        "value (dadk.jupytertools.datetimepicker property)": [
            [5, "dadk.JupyterTools.DatetimePicker.value", false]
        ],
        "value (dadk.jupytertools.fileselector property)": [
            [5, "dadk.JupyterTools.FileSelector.value", false]
        ],
        "value (dadk.jupytertools.timepicker property)": [
            [5, "dadk.JupyterTools.TimePicker.value", false]
        ],
        "value (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.value", false]
        ],
        "values (dadk.binpol.category property)": [
            [3, "dadk.BinPol.Category.values", false]
        ],
        "values_per_row (dadk.optimizer.optimizersettings.annealinglogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings.values_per_row", false]
        ],
        "values_per_row (dadk.optimizer.optimizersettings.composelogsettings property)": [
            [6, "dadk.Optimizer.OptimizerSettings.ComposeLogSettings.values_per_row", false]
        ],
        "var_shape_set (dadk.binpol.binpol property)": [
            [3, "dadk.BinPol.BinPol.var_shape_set", false]
        ],
        "var_shape_set (dadk.binpol.isingpol property)": [
            [3, "dadk.BinPol.IsingPol.var_shape_set", false]
        ],
        "var_shape_set (dadk.binpol.samplefilter property)": [
            [3, "dadk.BinPol.SampleFilter.var_shape_set", false]
        ],
        "var_shape_set (dadk.solution_solutionlist.solution property)": [
            [7, "dadk.Solution_SolutionList.Solution.var_shape_set", false]
        ],
        "variable (class in dadk.binpol)": [
            [3, "dadk.BinPol.Variable", false]
        ],
        "variablebase10 (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableBase10", false]
        ],
        "variablebinary (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableBinary", false]
        ],
        "variablefibonacci (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableFibonacci", false]
        ],
        "variablelittlegauss (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableLittlegauss", false]
        ],
        "variablesequential (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableSequential", false]
        ],
        "variableunary (class in dadk.binpol)": [
            [3, "dadk.BinPol.VariableUnary", false]
        ],
        "varset (class in dadk.solution_solutionlist)": [
            [7, "dadk.Solution_SolutionList.VarSet", false]
        ],
        "varshapeset (class in dadk.binpol)": [
            [3, "dadk.BinPol.VarShapeSet", false]
        ],
        "varslack() (in module dadk.binpol)": [
            [3, "dadk.BinPol.VarSlack", false]
        ],
        "visible (dadk.jupytertools.widgetnumberarray property)": [
            [5, "dadk.JupyterTools.WidgetNumberArray.visible", false]
        ],
        "wait_cycles (dadk.internal.graphicsdata.graphicsdata property)": [
            [1, "dadk.internal.GraphicsData.GraphicsData.wait_cycles", false]
        ],
        "widgetboundedfloatarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetBoundedFloatArray", false]
        ],
        "widgetboundedfloatrange (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetBoundedFloatRange", false]
        ],
        "widgetboundedintarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetBoundedIntArray", false]
        ],
        "widgetboundedintrange (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetBoundedIntRange", false]
        ],
        "widgetboundednumberarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetBoundedNumberArray", false]
        ],
        "widgetdefinition (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetDefinition", false]
        ],
        "widgetfloatarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetFloatArray", false]
        ],
        "widgetfloatrange (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetFloatRange", false]
        ],
        "widgetgui (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetGui", false]
        ],
        "widgetintarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetIntArray", false]
        ],
        "widgetintrange (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetIntRange", false]
        ],
        "widgetnumberarray (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetNumberArray", false]
        ],
        "widgettab (class in dadk.jupytertools)": [
            [5, "dadk.JupyterTools.WidgetTab", false]
        ],
        "write_json_file() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.write_json_file", false]
        ],
        "write_text_file() (dadk.optimizer.directorymanager method)": [
            [6, "dadk.Optimizer.DirectoryManager.write_text_file", false]
        ]
    },
    "objects": {
        "dadk.BinPol": [
            [3, 0, 1, "", "BQP"],
            [3, 0, 1, "", "BinPol"],
            [3, 0, 1, "", "BitArrayShape"],
            [3, 0, 1, "", "Category"],
            [3, 0, 1, "", "Inequality"],
            [3, 0, 1, "", "InequalitySign"],
            [3, 0, 1, "", "IsingPol"],
            [3, 0, 1, "", "MakeQuboError"],
            [3, 0, 1, "", "MakeQuboNotUsedBitWarning"],
            [3, 0, 1, "", "MakeQuboRoundedToZeroWarning"],
            [3, 0, 1, "", "MakeQuboWarning"],
            [3, 0, 1, "", "OneHot"],
            [3, 0, 1, "", "OneHotGroup"],
            [3, 0, 1, "", "PartialConfig"],
            [3, 0, 1, "", "QUBOSizeExceedsSolverMaxBits"],
            [3, 0, 1, "", "SampleFilter"],
            [3, 0, 1, "", "SlackType"],
            [3, 0, 1, "", "Term"],
            [3, 0, 1, "", "TwoSideInequality"],
            [3, 0, 1, "", "VarShapeSet"],
            [3, 3, 1, "", "VarSlack"],
            [3, 0, 1, "", "Variable"],
            [3, 0, 1, "", "VariableBase10"],
            [3, 0, 1, "", "VariableBinary"],
            [3, 0, 1, "", "VariableFibonacci"],
            [3, 0, 1, "", "VariableLittlegauss"],
            [3, 0, 1, "", "VariableSequential"],
            [3, 0, 1, "", "VariableUnary"],
            [3, 3, 1, "", "create_rpoly_hd5"],
            [3, 3, 1, "", "create_rpoly_script"],
            [3, 3, 1, "", "crs2Inequalities"],
            [3, 3, 1, "", "crs2QUBO"],
            [3, 3, 1, "", "inequalities2CRS"],
            [3, 3, 1, "", "qubo2CRS"]
        ],
        "dadk.BinPol.BQP": [
            [3, 1, 1, "", "from_QPoly"],
            [3, 2, 1, "", "inequalities"],
            [3, 1, 1, "", "load_hdf5"],
            [3, 1, 1, "", "load_json"],
            [3, 2, 1, "", "penalty_qubo"],
            [3, 2, 1, "", "qubo"],
            [3, 1, 1, "", "save_as_hdf5"],
            [3, 1, 1, "", "to_QPoly"]
        ],
        "dadk.BinPol.BinPol": [
            [3, 2, 1, "", "N"],
            [3, 1, 1, "", "add"],
            [3, 1, 1, "", "add_exactly_1_bit_on"],
            [3, 1, 1, "", "add_exactly_n_bits_on"],
            [3, 1, 1, "", "add_json_lines"],
            [3, 1, 1, "", "add_most_1_bit_on"],
            [3, 1, 1, "", "add_slack_penalty"],
            [3, 1, 1, "", "add_slack_variable"],
            [3, 1, 1, "", "add_term"],
            [3, 1, 1, "", "add_variable"],
            [3, 1, 1, "", "add_variable_penalty"],
            [3, 1, 1, "", "as_bqm"],
            [3, 1, 1, "", "as_hbsolv"],
            [3, 1, 1, "", "as_json"],
            [3, 1, 1, "", "as_latex"],
            [3, 1, 1, "", "as_qbsolv"],
            [3, 1, 1, "", "as_text"],
            [3, 1, 1, "", "calculate_hash"],
            [3, 1, 1, "", "clone"],
            [3, 1, 1, "", "compute"],
            [3, 2, 1, "", "degree"],
            [3, 1, 1, "", "exactly_1_bit_on"],
            [3, 1, 1, "", "exactly_n_bits_on"],
            [3, 1, 1, "", "freeze_var_shape_set"],
            [3, 1, 1, "", "generate_slack_polynomial"],
            [3, 1, 1, "", "get"],
            [3, 1, 1, "", "get_json"],
            [3, 1, 1, "", "get_json_generator"],
            [3, 1, 1, "", "get_max_abs_coefficient"],
            [3, 1, 1, "", "get_normalize_factor"],
            [3, 1, 1, "", "get_scale_factor"],
            [3, 1, 1, "", "get_weights"],
            [3, 1, 1, "", "inflate_clamped_configuration"],
            [3, 1, 1, "", "load_qbsolv"],
            [3, 1, 1, "", "make_clamped"],
            [3, 1, 1, "", "make_qubo"],
            [3, 1, 1, "", "most_1_bit_on"],
            [3, 1, 1, "", "multiply"],
            [3, 1, 1, "", "multiply_scalar"],
            [3, 1, 1, "", "normalize"],
            [3, 2, 1, "", "p"],
            [3, 1, 1, "", "plot_histogram"],
            [3, 1, 1, "", "power"],
            [3, 1, 1, "", "power2"],
            [3, 1, 1, "", "reduce_higher_degree"],
            [3, 1, 1, "", "reduce_higher_degree_to_qubo"],
            [3, 1, 1, "", "restore"],
            [3, 1, 1, "", "scale"],
            [3, 1, 1, "", "set_CLONE_WARNING_COUNTER"],
            [3, 1, 1, "", "set_term"],
            [3, 2, 1, "", "size"],
            [3, 1, 1, "", "sum"],
            [3, 2, 1, "", "var_shape_set"]
        ],
        "dadk.BinPol.BitArrayShape": [
            [3, 2, 1, "", "dimensions"],
            [3, 1, 1, "", "get_index"],
            [3, 1, 1, "", "get_symbolic"],
            [3, 2, 1, "", "length"],
            [3, 2, 1, "", "name"],
            [3, 2, 1, "", "one_hot"],
            [3, 2, 1, "", "shape"]
        ],
        "dadk.BinPol.Category": [
            [3, 1, 1, "", "get_index"],
            [3, 1, 1, "", "get_symbolic"],
            [3, 2, 1, "", "values"]
        ],
        "dadk.BinPol.Inequality": [
            [3, 1, 1, "", "as_latex"],
            [3, 1, 1, "", "as_text"],
            [3, 1, 1, "", "clone"],
            [3, 1, 1, "", "compute"],
            [3, 2, 1, "", "lambda_value"],
            [3, 1, 1, "", "make_qubo"],
            [3, 1, 1, "", "multiply_scalar"],
            [3, 2, 1, "", "qubo"]
        ],
        "dadk.BinPol.IsingPol": [
            [3, 2, 1, "", "N"],
            [3, 1, 1, "", "add_term"],
            [3, 1, 1, "", "as_latex"],
            [3, 1, 1, "", "as_text"],
            [3, 1, 1, "", "convert_to_binary"],
            [3, 2, 1, "", "degree"],
            [3, 1, 1, "", "freeze_var_shape_set"],
            [3, 1, 1, "", "get"],
            [3, 2, 1, "", "p"],
            [3, 1, 1, "", "set_term"],
            [3, 2, 1, "", "size"],
            [3, 2, 1, "", "var_shape_set"]
        ],
        "dadk.BinPol.MakeQuboRoundedToZeroWarning": [
            [3, 2, 1, "", "coefficient"]
        ],
        "dadk.BinPol.MakeQuboWarning": [
            [3, 2, 1, "", "bit_no"],
            [3, 2, 1, "", "index"]
        ],
        "dadk.BinPol.OneHotGroup": [
            [3, 2, 1, "", "entries"]
        ],
        "dadk.BinPol.PartialConfig": [
            [3, 1, 1, "", "as_json"],
            [3, 1, 1, "", "create_random_state"],
            [3, 1, 1, "", "get_bits"],
            [3, 1, 1, "", "is_empty"],
            [3, 1, 1, "", "set_bit"],
            [3, 1, 1, "", "set_variable"],
            [3, 2, 1, "", "size"]
        ],
        "dadk.BinPol.QUBOSizeExceedsSolverMaxBits": [
            [3, 2, 1, "", "bits"],
            [3, 2, 1, "", "max_bits"],
            [3, 2, 1, "", "name"]
        ],
        "dadk.BinPol.SampleFilter": [
            [3, 1, 1, "", "clone"],
            [3, 2, 1, "", "makeQUBO_precision"],
            [3, 1, 1, "", "make_qubo"],
            [3, 1, 1, "", "multiply_scalar"],
            [3, 2, 1, "", "qubo"],
            [3, 2, 1, "", "upper_limit"],
            [3, 2, 1, "", "var_shape_set"]
        ],
        "dadk.BinPol.Term": [
            [3, 1, 1, "", "compute"]
        ],
        "dadk.BinPol.TwoSideInequality": [
            [3, 1, 1, "", "as_latex"],
            [3, 1, 1, "", "as_text"],
            [3, 2, 1, "", "left_qubo"],
            [3, 2, 1, "", "right_qubo"],
            [3, 2, 1, "", "sign"]
        ],
        "dadk.BinPol.VarShapeSet": [
            [3, 1, 1, "", "generate_penalty_polynomial"],
            [3, 1, 1, "", "get_bits_of_1w1h_group"],
            [3, 1, 1, "", "get_bits_of_2w1h_group"],
            [3, 1, 1, "", "get_false_bit_indices"],
            [3, 1, 1, "", "get_free_bit_indices"],
            [3, 1, 1, "", "get_index"],
            [3, 1, 1, "", "get_not_false_bit_indices"],
            [3, 1, 1, "", "get_symbolic"],
            [3, 1, 1, "", "get_true_bit_indices"],
            [3, 2, 1, "", "length"],
            [3, 2, 1, "", "one_hot"]
        ],
        "dadk.BinPol.Variable": [
            [3, 2, 1, "", "start"],
            [3, 2, 1, "", "step"],
            [3, 2, 1, "", "stop"]
        ],
        "dadk.BinPolGen": [
            [4, 0, 1, "", "BinPolGen"]
        ],
        "dadk.BinPolGen.BinPolGen": [
            [4, 1, 1, "", "gen_latex"],
            [4, 1, 1, "", "gen_pretty_string"],
            [4, 1, 1, "", "gen_python"],
            [4, 1, 1, "", "parse_poly"],
            [4, 2, 1, "", "source"],
            [4, 2, 1, "", "tree"]
        ],
        "dadk.JSONTools": [
            [0, 0, 1, "", "DaoDecoder"],
            [0, 0, 1, "", "DaoEncoder"]
        ],
        "dadk.JSONTools.DaoDecoder": [
            [0, 1, 1, "", "object_hook"],
            [0, 1, 1, "", "register_decoder"],
            [0, 1, 1, "", "register_function"]
        ],
        "dadk.JSONTools.DaoEncoder": [
            [0, 1, 1, "", "default"],
            [0, 1, 1, "", "register_encoder"],
            [0, 1, 1, "", "register_function"]
        ],
        "dadk.JupyterTools": [
            [5, 0, 1, "", "CleanupAzureBlobStorage"],
            [5, 0, 1, "", "CleanupJobs"],
            [5, 0, 1, "", "DatetimePicker"],
            [5, 0, 1, "", "DirectorySelector"],
            [5, 0, 1, "", "FileSelector"],
            [5, 0, 1, "", "NumberPlot"],
            [5, 0, 1, "", "NumberTable"],
            [5, 0, 1, "", "OutputTab"],
            [5, 0, 1, "", "ParameterLogging"],
            [5, 0, 1, "", "ParetoPlot"],
            [5, 0, 1, "", "QuboEditor"],
            [5, 0, 1, "", "SetAnnealerProfile"],
            [5, 3, 1, "", "ShowSolverParameter"],
            [5, 0, 1, "", "TaggedGuiControl"],
            [5, 0, 1, "", "TimePicker"],
            [5, 0, 1, "", "TimeReporter"],
            [5, 0, 1, "", "TimeTracker"],
            [5, 0, 1, "", "Traverse2Plot"],
            [5, 0, 1, "", "TraversePlot"],
            [5, 0, 1, "", "WidgetBoundedFloatArray"],
            [5, 0, 1, "", "WidgetBoundedFloatRange"],
            [5, 0, 1, "", "WidgetBoundedIntArray"],
            [5, 0, 1, "", "WidgetBoundedIntRange"],
            [5, 0, 1, "", "WidgetBoundedNumberArray"],
            [5, 0, 1, "", "WidgetDefinition"],
            [5, 0, 1, "", "WidgetFloatArray"],
            [5, 0, 1, "", "WidgetFloatRange"],
            [5, 0, 1, "", "WidgetGui"],
            [5, 0, 1, "", "WidgetIntArray"],
            [5, 0, 1, "", "WidgetIntRange"],
            [5, 0, 1, "", "WidgetNumberArray"],
            [5, 0, 1, "", "WidgetTab"],
            [5, 3, 1, "", "activate_output"],
            [5, 3, 1, "", "deactivate_output"],
            [5, 3, 1, "", "debug_exception"],
            [5, 3, 1, "", "get_active_parameters"],
            [5, 3, 1, "", "log_msg"],
            [5, 3, 1, "", "print_to_output"],
            [5, 3, 1, "", "prolog"],
            [5, 3, 1, "", "register_logging_output"]
        ],
        "dadk.JupyterTools.DatetimePicker": [
            [5, 2, 1, "", "value"]
        ],
        "dadk.JupyterTools.DirectorySelector": [
            [5, 2, 1, "", "directory"],
            [5, 2, 1, "", "relative_directory"],
            [5, 1, 1, "", "set_directory"]
        ],
        "dadk.JupyterTools.FileSelector": [
            [5, 2, 1, "", "file"],
            [5, 2, 1, "", "on_change_code"],
            [5, 2, 1, "", "path"],
            [5, 1, 1, "", "set_value"],
            [5, 2, 1, "", "value"]
        ],
        "dadk.JupyterTools.NumberPlot": [
            [5, 1, 1, "", "add_figure"],
            [5, 1, 1, "", "display_control"]
        ],
        "dadk.JupyterTools.NumberTable": [
            [5, 1, 1, "", "csv_content"],
            [5, 1, 1, "", "display_content"],
            [5, 1, 1, "", "get_summary"],
            [5, 1, 1, "", "prepare_content"],
            [5, 1, 1, "", "sort"],
            [5, 1, 1, "", "text_content"],
            [5, 1, 1, "", "update_content"]
        ],
        "dadk.JupyterTools.OutputTab": [
            [5, 1, 1, "", "display"],
            [5, 1, 1, "", "set_visibility"],
            [5, 2, 1, "", "tab_out"]
        ],
        "dadk.JupyterTools.ParetoPlot": [
            [5, 1, 1, "", "add_figure"],
            [5, 1, 1, "", "display_control"]
        ],
        "dadk.JupyterTools.TaggedGuiControl": [
            [5, 2, 1, "", "applier"],
            [5, 2, 1, "", "creator"],
            [5, 1, 1, "", "display_applier"],
            [5, 1, 1, "", "display_creator"],
            [5, 1, 1, "", "update_last_setting"]
        ],
        "dadk.JupyterTools.TimePicker": [
            [5, 2, 1, "", "value"]
        ],
        "dadk.JupyterTools.TimeReporter": [
            [5, 1, 1, "", "report_fact"],
            [5, 2, 1, "", "silent"],
            [5, 1, 1, "", "take_time"]
        ],
        "dadk.JupyterTools.TimeTracker": [
            [5, 1, 1, "", "add"],
            [5, 1, 1, "", "close"]
        ],
        "dadk.JupyterTools.Traverse2Plot": [
            [5, 1, 1, "", "add_figure"],
            [5, 1, 1, "", "display_control"]
        ],
        "dadk.JupyterTools.TraversePlot": [
            [5, 1, 1, "", "add_figure"],
            [5, 1, 1, "", "display_control"]
        ],
        "dadk.JupyterTools.WidgetBoundedNumberArray": [
            [5, 2, 1, "", "max"],
            [5, 2, 1, "", "min"],
            [5, 2, 1, "", "step"]
        ],
        "dadk.JupyterTools.WidgetGui": [
            [5, 1, 1, "", "call_function"],
            [5, 1, 1, "", "get_default_setting"],
            [5, 1, 1, "", "get_persistent_setting"],
            [5, 1, 1, "", "get_value"],
            [5, 1, 1, "", "get_widget"],
            [5, 1, 1, "", "gui_default_values"],
            [5, 1, 1, "", "gui_reset_values"],
            [5, 1, 1, "", "gui_save_values"],
            [5, 1, 1, "", "register_widget_class"],
            [5, 1, 1, "", "set_disabled"],
            [5, 1, 1, "", "set_persistent_setting"],
            [5, 1, 1, "", "set_value"],
            [5, 1, 1, "", "trigger_change_handlers"]
        ],
        "dadk.JupyterTools.WidgetNumberArray": [
            [5, 2, 1, "", "columns"],
            [5, 2, 1, "", "description"],
            [5, 2, 1, "", "disabled"],
            [5, 2, 1, "", "headers"],
            [5, 2, 1, "", "label"],
            [5, 2, 1, "", "row_labels"],
            [5, 2, 1, "", "rows"],
            [5, 2, 1, "", "value"],
            [5, 2, 1, "", "visible"]
        ],
        "dadk.JupyterTools.WidgetTab": [
            [5, 1, 1, "", "add_tab"],
            [5, 1, 1, "", "remove_all_tabs"],
            [5, 1, 1, "", "select_tab"],
            [5, 1, 1, "", "set_title"]
        ],
        "dadk.Optimizer": [
            [6, 0, 1, "", "DirectoryManager"],
            [6, 0, 1, "", "InterruptKey"],
            [6, 0, 1, "", "Optimizer"],
            [6, 0, 1, "", "OptimizerModel"],
            [6, 0, 1, "", "OptimizerModelComplex"],
            [6, 0, 1, "", "OptimizerSettings"],
            [6, 0, 1, "", "OptimizerTab"],
            [6, 0, 1, "", "OptimizerTabRequirement"]
        ],
        "dadk.Optimizer.DirectoryManager": [
            [6, 1, 1, "", "copy_to"],
            [6, 1, 1, "", "count_files"],
            [6, 1, 1, "", "create"],
            [6, 1, 1, "", "move_to"],
            [6, 2, 1, "", "path"],
            [6, 1, 1, "", "read_json_file"],
            [6, 1, 1, "", "read_text_file"],
            [6, 1, 1, "", "remove"],
            [6, 1, 1, "", "remove_files"],
            [6, 1, 1, "", "write_json_file"],
            [6, 1, 1, "", "write_text_file"]
        ],
        "dadk.Optimizer.InterruptKey": [
            [6, 2, 1, "", "interrupt"],
            [6, 1, 1, "", "stop"]
        ],
        "dadk.Optimizer.Optimizer": [
            [6, 1, 1, "", "create_batch"],
            [6, 1, 1, "", "get_results"],
            [6, 1, 1, "", "get_widget"],
            [6, 1, 1, "", "gui_build"],
            [6, 2, 1, "", "gui_compose"],
            [6, 2, 1, "", "gui_load"],
            [6, 1, 1, "", "gui_solve"],
            [6, 1, 1, "", "load"],
            [6, 1, 1, "", "optuna"],
            [6, 1, 1, "", "pareto"],
            [6, 1, 1, "", "run"],
            [6, 1, 1, "", "select_instance"],
            [6, 1, 1, "", "set_build_qubo_details_reference"],
            [6, 1, 1, "", "set_calculated_build_qubo_parameter"],
            [6, 1, 1, "", "set_calculated_solver_parameter"],
            [6, 1, 1, "", "set_compose_model_details_reference"],
            [6, 1, 1, "", "set_fixed_solver_parameter"],
            [6, 1, 1, "", "set_load_details_reference"],
            [6, 1, 1, "", "set_prep_result_details_reference"],
            [6, 1, 1, "", "set_solver_parameter"],
            [6, 2, 1, "", "tab_build"],
            [6, 2, 1, "", "tab_compose"],
            [6, 2, 1, "", "tab_load"],
            [6, 2, 1, "", "tab_optuna"],
            [6, 2, 1, "", "tab_pareto"],
            [6, 1, 1, "", "tab_report"],
            [6, 2, 1, "", "tab_solve"],
            [6, 2, 1, "", "tab_timeseries"],
            [6, 2, 1, "", "tab_visualization"],
            [6, 1, 1, "", "timeseries"]
        ],
        "dadk.Optimizer.OptimizerModelComplex": [
            [6, 2, 1, "", "Optimizer"],
            [6, 1, 1, "", "build_and_solve"],
            [6, 1, 1, "", "build_qubo"],
            [6, 1, 1, "", "get_time"],
            [6, 1, 1, "", "load"],
            [6, 1, 1, "", "log"],
            [6, 1, 1, "", "log_data"],
            [6, 2, 1, "", "optuna_dirname"],
            [6, 2, 1, "", "pareto_dirname"],
            [6, 1, 1, "", "prep_result"],
            [6, 2, 1, "", "timeseries_dirname"]
        ],
        "dadk.Optimizer.OptimizerSettings": [
            [6, 0, 1, "", "AnnealingLogSettings"],
            [6, 0, 1, "", "BatchSettings"],
            [6, 0, 1, "", "ComposeLogSettings"],
            [6, 0, 1, "", "ComposeSettings"],
            [6, 0, 1, "", "OptunaSettings"],
            [6, 0, 1, "", "ParetoSettings"],
            [6, 0, 1, "", "SolveDAv2Settings"],
            [6, 0, 1, "", "SolveGUISettings"],
            [6, 0, 1, "", "TimeseriesSettings"],
            [6, 2, 1, "", "annealing_log"],
            [6, 2, 1, "", "batch"],
            [6, 2, 1, "", "compose"],
            [6, 2, 1, "", "compose_log"],
            [6, 2, 1, "", "optuna"],
            [6, 2, 1, "", "pareto"],
            [6, 2, 1, "", "solve_dav2"],
            [6, 2, 1, "", "solve_gui"],
            [6, 2, 1, "", "timeseries"]
        ],
        "dadk.Optimizer.OptimizerSettings.AnnealingLogSettings": [
            [6, 2, 1, "", "graphs_figsize"],
            [6, 2, 1, "", "save_graphs"],
            [6, 2, 1, "", "save_graphs_figsize"],
            [6, 2, 1, "", "save_times"],
            [6, 2, 1, "", "save_times_figsize"],
            [6, 2, 1, "", "times_figsize"],
            [6, 2, 1, "", "values_per_row"]
        ],
        "dadk.Optimizer.OptimizerSettings.BatchSettings": [
            [6, 2, 1, "", "parameter_logging"]
        ],
        "dadk.Optimizer.OptimizerSettings.ComposeLogSettings": [
            [6, 2, 1, "", "figsize"],
            [6, 2, 1, "", "save_graphs"],
            [6, 2, 1, "", "save_graphs_figsize"],
            [6, 2, 1, "", "values_per_row"]
        ],
        "dadk.Optimizer.OptimizerSettings.ComposeSettings": [
            [6, 2, 1, "", "create_summary_excel"],
            [6, 2, 1, "", "excel_summary_columns"],
            [6, 2, 1, "", "log_memory_size"],
            [6, 2, 1, "", "show_summary"],
            [6, 2, 1, "", "summary_columns"]
        ],
        "dadk.Optimizer.OptimizerSettings.OptunaSettings": [
            [6, 2, 1, "", "delete_files"]
        ],
        "dadk.Optimizer.OptimizerSettings.ParetoSettings": [
            [6, 2, 1, "", "delete_files"]
        ],
        "dadk.Optimizer.OptimizerSettings.SolveDAv2Settings": [
            [6, 2, 1, "", "store_graphics"]
        ],
        "dadk.Optimizer.OptimizerSettings.SolveGUISettings": [
            [6, 2, 1, "", "button_width"],
            [6, 2, 1, "", "description_width"]
        ],
        "dadk.Optimizer.OptimizerSettings.TimeseriesSettings": [
            [6, 2, 1, "", "delete_files"]
        ],
        "dadk.Optimizer.OptimizerTab": [
            [6, 1, 1, "", "get_widget"],
            [6, 1, 1, "", "set_disabled"]
        ],
        "dadk.ProfileUtils": [
            [0, 0, 1, "", "ProfileUtils"]
        ],
        "dadk.ProfileUtils.ProfileUtils": [
            [0, 1, 1, "", "delete_annealer_access_profile"],
            [0, 1, 1, "", "get_annealer_access_profile"],
            [0, 1, 1, "", "list_annealer_access_profiles"],
            [0, 1, 1, "", "store_annealer_access_profile"],
            [0, 1, 1, "", "update_annealer_access_profiles"]
        ],
        "dadk.QUBOSolverBase": [
            [2, 0, 1, "", "AzureBlobMixin"],
            [2, 0, 1, "", "BoltzmannSampling"],
            [2, 0, 1, "", "JobStatus"],
            [2, 0, 1, "", "LocalMinimaSampling"],
            [2, 0, 1, "", "ParallelTemperingManagement"],
            [2, 0, 1, "", "QUBOSolverBase"],
            [2, 0, 1, "", "QUBOSolverBaseV2"],
            [2, 0, 1, "", "QUBOSolverBaseV3"],
            [2, 0, 1, "", "QUBOSolverEmulator"],
            [2, 0, 1, "", "ReplicaExchangeModel"],
            [2, 0, 1, "", "ReplicaExchangeModelFarJump"],
            [2, 0, 1, "", "RestMixin"],
            [2, 0, 1, "", "SamplingBasis"],
            [2, 0, 1, "", "SolverBase"],
            [2, 0, 1, "", "TemperatureModel"],
            [2, 0, 1, "", "TemperatureModelExponential"],
            [2, 0, 1, "", "TemperatureModelHukushima"],
            [2, 0, 1, "", "TemperatureModelLinear"],
            [2, 3, 1, "", "file_size_info"]
        ],
        "dadk.QUBOSolverBase.AzureBlobMixin": [
            [2, 0, 1, "", "Blob"],
            [2, 0, 1, "", "ProgressBar"],
            [2, 1, 1, "", "create_container"],
            [2, 1, 1, "", "create_sas_token"],
            [2, 1, 1, "", "delete_blob"],
            [2, 1, 1, "", "delete_container"],
            [2, 1, 1, "", "display_blobs"],
            [2, 1, 1, "", "download_blob"],
            [2, 1, 1, "", "list_blobs"],
            [2, 1, 1, "", "list_containers"],
            [2, 1, 1, "", "upload_blob"]
        ],
        "dadk.QUBOSolverBase.AzureBlobMixin.Blob": [
            [2, 2, 1, "", "blob_name"],
            [2, 2, 1, "", "container_name"],
            [2, 2, 1, "", "last_modified"],
            [2, 2, 1, "", "size"]
        ],
        "dadk.QUBOSolverBase.AzureBlobMixin.ProgressBar": [
            [2, 1, 1, "", "close"],
            [2, 1, 1, "", "set"]
        ],
        "dadk.QUBOSolverBase.BoltzmannSampling": [
            [2, 2, 1, "", "furnace_mean_energy_list"],
            [2, 2, 1, "", "randomwalk_count"],
            [2, 2, 1, "", "randomwalk_length"]
        ],
        "dadk.QUBOSolverBase.JobStatus": [
            [2, 2, 1, "", "job_id"],
            [2, 2, 1, "", "start_time"],
            [2, 2, 1, "", "status"]
        ],
        "dadk.QUBOSolverBase.LocalMinimaSampling": [
            [2, 2, 1, "", "offset_energy"],
            [2, 2, 1, "", "temperature_end"],
            [2, 2, 1, "", "temperature_start"]
        ],
        "dadk.QUBOSolverBase.ParallelTemperingManagement": [
            [2, 1, 1, "", "generate_replica_furnace_exchange_step"],
            [2, 2, 1, "", "replica_furnace_history"],
            [2, 2, 1, "", "replica_temperature_list"]
        ],
        "dadk.QUBOSolverBase.QUBOSolverBase": [
            [2, 2, 1, "", "guidance_config"],
            [2, 1, 1, "", "healthcheck"],
            [2, 2, 1, "", "scaling_parameter"]
        ],
        "dadk.QUBOSolverBase.QUBOSolverBaseV2": [
            [2, 2, 1, "", "da_parameter"]
        ],
        "dadk.QUBOSolverBase.ReplicaExchangeModel": [
            [2, 2, 1, "", "furnace_replica_list"],
            [2, 2, 1, "", "furnaces_replica_history"],
            [2, 2, 1, "", "logger"],
            [2, 2, 1, "", "replica_furnace_history"],
            [2, 2, 1, "", "replica_furnace_list"],
            [2, 2, 1, "", "replica_temperature_list"]
        ],
        "dadk.QUBOSolverBase.RestMixin": [
            [2, 1, 1, "", "cancel_job"],
            [2, 1, 1, "", "cleanup_jobs"],
            [2, 2, 1, "", "connection_parameter"],
            [2, 1, 1, "", "delete_job"],
            [2, 1, 1, "", "display_jobs"],
            [2, 1, 1, "", "download_job"],
            [2, 1, 1, "", "list_jobs"],
            [2, 2, 1, "", "max_job_list_retry"],
            [2, 2, 1, "", "sleep_duration"],
            [2, 2, 1, "", "sleep_function"]
        ],
        "dadk.QUBOSolverBase.SamplingBasis": [
            [2, 1, 1, "", "do_sampling"]
        ],
        "dadk.QUBOSolverBase.SolverBase": [
            [2, 2, 1, "", "logger"],
            [2, 2, 1, "", "random_seed"]
        ],
        "dadk.QUBOSolverBase.TemperatureModel": [
            [2, 2, 1, "", "logger"],
            [2, 2, 1, "", "replica_count"],
            [2, 2, 1, "", "temperature_high"],
            [2, 2, 1, "", "temperature_list"],
            [2, 2, 1, "", "temperature_low"]
        ],
        "dadk.QUBOSolverCPU": [
            [2, 0, 1, "", "QUBOSolverCPU"]
        ],
        "dadk.QUBOSolverCPU.QUBOSolverCPU": [
            [2, 1, 1, "", "check_da_parameter"],
            [2, 1, 1, "", "get_parameter_definitions"],
            [2, 1, 1, "", "max_number_of_bits"],
            [2, 1, 1, "", "minimize"],
            [2, 2, 1, "", "url"]
        ],
        "dadk.QUBOSolverDAv3c": [
            [2, 0, 1, "", "QUBOSolverDAv3c"]
        ],
        "dadk.QUBOSolverDAv3c.QUBOSolverDAv3c": [
            [2, 1, 1, "", "check_da_parameter"],
            [2, 2, 1, "", "da_parameter"],
            [2, 1, 1, "", "get_parameter_definitions"],
            [2, 1, 1, "", "max_number_of_bits"],
            [2, 1, 1, "", "minimize"],
            [2, 2, 1, "", "url"]
        ],
        "dadk.QUBOSolverDAv4": [
            [2, 0, 1, "", "QUBOSolverDAv4"]
        ],
        "dadk.QUBOSolverDAv4.QUBOSolverDAv4": [
            [2, 1, 1, "", "check_da_parameter"],
            [2, 2, 1, "", "da_parameter"],
            [2, 1, 1, "", "get_parameter_definitions"],
            [2, 1, 1, "", "max_number_of_bits"],
            [2, 1, 1, "", "minimize"],
            [2, 2, 1, "", "url"]
        ],
        "dadk.Solution_SolutionList": [
            [7, 0, 1, "", "BitArray"],
            [7, 0, 1, "", "Progress"],
            [7, 0, 1, "", "Solution"],
            [7, 0, 1, "", "SolutionList"],
            [7, 0, 1, "", "VarSet"]
        ],
        "dadk.Solution_SolutionList.BitArray": [
            [7, 2, 1, "", "call_back"],
            [7, 1, 1, "", "draw"],
            [7, 1, 1, "", "evaluate"],
            [7, 2, 1, "", "name"],
            [7, 2, 1, "", "shape"],
            [7, 2, 1, "", "start"],
            [7, 2, 1, "", "step"],
            [7, 2, 1, "", "stop"]
        ],
        "dadk.Solution_SolutionList.Progress": [
            [7, 0, 1, "", "Entry"]
        ],
        "dadk.Solution_SolutionList.Progress.Entry": [
            [7, 2, 1, "", "energy"],
            [7, 2, 1, "", "penalty_energy"],
            [7, 2, 1, "", "time"]
        ],
        "dadk.Solution_SolutionList.Solution": [
            [7, 2, 1, "", "configuration"],
            [7, 1, 1, "", "decode"],
            [7, 1, 1, "", "draw"],
            [7, 1, 1, "", "encode"],
            [7, 2, 1, "", "energy"],
            [7, 1, 1, "", "extract_bit_array"],
            [7, 2, 1, "", "penalty_energy"],
            [7, 2, 1, "", "var_shape_set"]
        ],
        "dadk.Solution_SolutionList.SolutionList": [
            [7, 2, 1, "", "da_parameter"],
            [7, 1, 1, "", "decode"],
            [7, 1, 1, "", "display_graphs"],
            [7, 1, 1, "", "encode"],
            [7, 1, 1, "", "get_minimum_energy_solution"],
            [7, 1, 1, "", "get_solution_list"],
            [7, 1, 1, "", "get_sorted_solution_list"],
            [7, 1, 1, "", "get_time"],
            [7, 1, 1, "", "get_times"],
            [7, 2, 1, "", "inequalities"],
            [7, 2, 1, "", "min_solution"],
            [7, 2, 1, "", "penalty_qubo"],
            [7, 1, 1, "", "print_progress"],
            [7, 1, 1, "", "print_stats"],
            [7, 2, 1, "", "progress"],
            [7, 2, 1, "", "qubo"],
            [7, 2, 1, "", "scaling_parameter"],
            [7, 1, 1, "", "search_stats_info"],
            [7, 2, 1, "", "solver_times"]
        ],
        "dadk.Solution_SolutionList.VarSet": [
            [7, 1, 1, "", "extract_bit_array"]
        ],
        "dadk.SolverFactory": [
            [2, 3, 1, "", "create_solver"],
            [2, 3, 1, "", "get_solver_IDs"],
            [2, 3, 1, "", "has_solver"],
            [2, 3, 1, "", "register_solver"]
        ],
        "dadk.Utils": [
            [0, 3, 1, "", "configuration_2_string"],
            [0, 3, 1, "", "dadk_version_check"],
            [0, 3, 1, "", "i18n_get"],
            [0, 3, 1, "", "load_library"],
            [0, 3, 1, "", "math_prod"],
            [0, 3, 1, "", "merge_dicts"],
            [0, 3, 1, "", "round_to_n"],
            [0, 3, 1, "", "string_2_configuration"]
        ],
        "dadk.flask.QUBOServiceCPU": [
            [8, 3, 1, "", "delete_job"],
            [8, 3, 1, "", "get_job"],
            [8, 3, 1, "", "healthcheck"],
            [8, 3, 1, "", "hobo2qubo"],
            [8, 3, 1, "", "list_jobs"],
            [8, 3, 1, "", "minimize"]
        ],
        "dadk.flask.QUBOServiceDAv3c": [
            [9, 3, 1, "", "delete_job"],
            [9, 3, 1, "", "get_job"],
            [9, 3, 1, "", "healthcheck"],
            [9, 3, 1, "", "hobo2qubo"],
            [9, 3, 1, "", "list_jobs"],
            [9, 3, 1, "", "minimize"]
        ],
        "dadk.flask.QUBOServiceDAv4": [
            [10, 3, 1, "", "delete_job"],
            [10, 3, 1, "", "get_job"],
            [10, 3, 1, "", "healthcheck"],
            [10, 3, 1, "", "hobo2qubo"],
            [10, 3, 1, "", "list_jobs"],
            [10, 3, 1, "", "minimize"]
        ],
        "dadk.internal.ConnectionParameter": [
            [1, 0, 1, "", "ConnectionParameter"],
            [1, 0, 1, "", "ConnectionParameterWithAzureBlob"],
            [1, 0, 1, "", "RequestMode"],
            [1, 0, 1, "", "StorageAccountType"],
            [1, 3, 1, "", "load_connection_parameter"]
        ],
        "dadk.internal.ConnectionParameter.ConnectionParameter": [
            [1, 2, 1, "", "annealer_address"],
            [1, 2, 1, "", "annealer_path"],
            [1, 2, 1, "", "annealer_port"],
            [1, 2, 1, "", "annealer_protocol"],
            [1, 2, 1, "", "annealer_queue_size"],
            [1, 2, 1, "", "api_key"],
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "ignore_proxy"],
            [1, 2, 1, "", "proxy"],
            [1, 2, 1, "", "proxy_password"],
            [1, 2, 1, "", "proxy_port"],
            [1, 2, 1, "", "proxy_user"],
            [1, 2, 1, "", "request_mode"],
            [1, 2, 1, "", "ssl_disable_warnings"],
            [1, 2, 1, "", "ssl_verify"],
            [1, 2, 1, "", "timeout"]
        ],
        "dadk.internal.ConnectionParameter.ConnectionParameterWithAzureBlob": [
            [1, 1, 1, "", "as_text"],
            [1, 2, 1, "", "container_name"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "inequalities_blob_name"],
            [1, 2, 1, "", "inequalities_filename"],
            [1, 2, 1, "", "penalty_qubo_blob_name"],
            [1, 2, 1, "", "penalty_qubo_filename"],
            [1, 2, 1, "", "prolog_blob_name"],
            [1, 2, 1, "", "prolog_filename"],
            [1, 2, 1, "", "qubo_blob_name"],
            [1, 2, 1, "", "qubo_filename"],
            [1, 2, 1, "", "sas_token"],
            [1, 2, 1, "", "storage_account_name"],
            [1, 2, 1, "", "storage_account_type"],
            [1, 2, 1, "", "storage_connection_string"]
        ],
        "dadk.internal.DAv2Parameter": [
            [1, 0, 1, "", "DAv2Parameter"],
            [1, 0, 1, "", "GraphicsDetail"],
            [1, 0, 1, "", "OptimizationMethod"],
            [1, 0, 1, "", "SolutionMode"]
        ],
        "dadk.internal.DAv2Parameter.DAv2Parameter": [
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "graphics"],
            [1, 2, 1, "", "number_iterations"],
            [1, 2, 1, "", "number_replicas"],
            [1, 2, 1, "", "number_runs"],
            [1, 2, 1, "", "offset_increase_rate"],
            [1, 2, 1, "", "optimization_method"],
            [1, 1, 1, "", "plot_temperature_curve"],
            [1, 2, 1, "", "pt_replica_exchange_model"],
            [1, 2, 1, "", "pt_temperature_model"],
            [1, 2, 1, "", "sampling_parameter"],
            [1, 2, 1, "", "solution_mode"],
            [1, 2, 1, "", "temperature_decay"],
            [1, 2, 1, "", "temperature_end"],
            [1, 2, 1, "", "temperature_interval"],
            [1, 2, 1, "", "temperature_mode"],
            [1, 2, 1, "", "temperature_sampling"],
            [1, 2, 1, "", "temperature_start"]
        ],
        "dadk.internal.DAv3Parameter": [
            [1, 0, 1, "", "DAv3Parameter"]
        ],
        "dadk.internal.DAv3Parameter.DAv3Parameter": [
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "gs_max_penalty_coef"],
            [1, 2, 1, "", "gs_num_iteration_cl"],
            [1, 2, 1, "", "gs_num_iteration_factor"],
            [1, 2, 1, "", "gs_penalty_auto_mode"],
            [1, 2, 1, "", "gs_penalty_coef"],
            [1, 2, 1, "", "gs_penalty_inc_rate"],
            [1, 2, 1, "", "num_group"],
            [1, 2, 1, "", "num_output_solution"],
            [1, 2, 1, "", "num_solution"],
            [1, 2, 1, "", "target_energy"],
            [1, 2, 1, "", "time_limit_sec"]
        ],
        "dadk.internal.DAv3cParameter": [
            [1, 0, 1, "", "DAv3cParameter"]
        ],
        "dadk.internal.DAv3cParameter.DAv3cParameter": [
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "gs_max_penalty_coef"],
            [1, 2, 1, "", "gs_num_iteration_cl"],
            [1, 2, 1, "", "gs_num_iteration_factor"],
            [1, 2, 1, "", "gs_ohs_xw1h_num_iteration_cl"],
            [1, 2, 1, "", "gs_ohs_xw1h_num_iteration_factor"],
            [1, 2, 1, "", "gs_penalty_auto_mode"],
            [1, 2, 1, "", "gs_penalty_coef"],
            [1, 2, 1, "", "gs_penalty_inc_rate"],
            [1, 2, 1, "", "num_group"],
            [1, 2, 1, "", "num_output_solution"],
            [1, 2, 1, "", "num_solution"],
            [1, 2, 1, "", "ohs_xw1h_internal_penalty"],
            [1, 2, 1, "", "target_energy"],
            [1, 2, 1, "", "time_limit_sec"]
        ],
        "dadk.internal.DAv4Parameter": [
            [1, 0, 1, "", "DAv4Parameter"]
        ],
        "dadk.internal.DAv4Parameter.DAv4Parameter": [
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "gs_max_penalty_coef"],
            [1, 2, 1, "", "gs_num_iteration_cl"],
            [1, 2, 1, "", "gs_num_iteration_factor"],
            [1, 2, 1, "", "gs_ohs_xw1h_num_iteration_cl"],
            [1, 2, 1, "", "gs_ohs_xw1h_num_iteration_factor"],
            [1, 2, 1, "", "gs_penalty_auto_mode"],
            [1, 2, 1, "", "gs_penalty_coef"],
            [1, 2, 1, "", "gs_penalty_inc_rate"],
            [1, 2, 1, "", "num_group"],
            [1, 2, 1, "", "num_output_solution"],
            [1, 2, 1, "", "num_solution"],
            [1, 2, 1, "", "ohs_xw1h_internal_penalty"],
            [1, 2, 1, "", "target_energy"],
            [1, 2, 1, "", "time_limit_sec"]
        ],
        "dadk.internal.GraphicsData": [
            [1, 0, 1, "", "GraphicsData"]
        ],
        "dadk.internal.GraphicsData.GraphicsData": [
            [1, 2, 1, "", "bit_flips"],
            [1, 2, 1, "", "energies"],
            [1, 2, 1, "", "furnace_process_length"],
            [1, 1, 1, "", "get_bit_flip"],
            [1, 1, 1, "", "get_energy"],
            [1, 1, 1, "", "get_state"],
            [1, 1, 1, "", "get_temperature"],
            [1, 2, 1, "", "graphics"],
            [1, 2, 1, "", "hill_energies"],
            [1, 2, 1, "", "hill_steps"],
            [1, 2, 1, "", "min_energy"],
            [1, 2, 1, "", "min_step"],
            [1, 2, 1, "", "number_of_bits"],
            [1, 2, 1, "", "number_of_hills"],
            [1, 2, 1, "", "number_of_iterations"],
            [1, 2, 1, "", "number_of_valleys"],
            [1, 2, 1, "", "number_of_walks"],
            [1, 2, 1, "", "optimization_method"],
            [1, 1, 1, "", "plot_graphs"],
            [1, 2, 1, "", "replica_exchange_count"],
            [1, 2, 1, "", "replica_history"],
            [1, 2, 1, "", "start_states"],
            [1, 2, 1, "", "temperatures"],
            [1, 2, 1, "", "valley_energies"],
            [1, 2, 1, "", "valley_steps"],
            [1, 2, 1, "", "wait_cycles"]
        ],
        "dadk.internal.SamplingParameter": [
            [1, 0, 1, "", "ProbabilityModel"],
            [1, 0, 1, "", "SamplingParameter"]
        ],
        "dadk.internal.SamplingParameter.SamplingParameter": [
            [1, 2, 1, "", "annealing_steps"],
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "flip_probabilities"],
            [1, 2, 1, "", "probability_model"],
            [1, 2, 1, "", "sampling_runs"]
        ],
        "dadk.internal.ScalingParameter": [
            [1, 0, 1, "", "ScalingAction"],
            [1, 0, 1, "", "ScalingParameter"]
        ],
        "dadk.internal.ScalingParameter.ScalingParameter": [
            [1, 1, 1, "", "as_text"],
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "dict"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "scaling_action"],
            [1, 2, 1, "", "scaling_bit_precision"],
            [1, 2, 1, "", "scaling_factor"]
        ],
        "dadk.internal.SolverTimes": [
            [1, 0, 1, "", "SolverTimes"]
        ],
        "dadk.internal.SolverTimes.SolverTimes": [
            [1, 1, 1, "", "decode"],
            [1, 2, 1, "", "duration_account_occupied"],
            [1, 2, 1, "", "duration_anneal"],
            [1, 2, 1, "", "duration_cpu"],
            [1, 2, 1, "", "duration_dau_service"],
            [1, 2, 1, "", "duration_elapsed"],
            [1, 2, 1, "", "duration_execution"],
            [1, 2, 1, "", "duration_occupied"],
            [1, 2, 1, "", "duration_parse_response"],
            [1, 2, 1, "", "duration_prepare_qubo"],
            [1, 2, 1, "", "duration_prepare_request"],
            [1, 2, 1, "", "duration_queue"],
            [1, 2, 1, "", "duration_receive_response"],
            [1, 2, 1, "", "duration_reduce_degree"],
            [1, 2, 1, "", "duration_sampling"],
            [1, 2, 1, "", "duration_save_request"],
            [1, 2, 1, "", "duration_save_response"],
            [1, 2, 1, "", "duration_scaling"],
            [1, 2, 1, "", "duration_send_request"],
            [1, 2, 1, "", "duration_solve"],
            [1, 2, 1, "", "duration_waiting"],
            [1, 2, 1, "", "duration_waiting_2_finish"],
            [1, 2, 1, "", "duration_waiting_2_start"],
            [1, 1, 1, "", "encode"],
            [1, 2, 1, "", "end_account_occupied"],
            [1, 2, 1, "", "end_anneal"],
            [1, 2, 1, "", "end_cpu"],
            [1, 2, 1, "", "end_dau_service"],
            [1, 2, 1, "", "end_elapsed"],
            [1, 2, 1, "", "end_execution"],
            [1, 2, 1, "", "end_occupied"],
            [1, 2, 1, "", "end_parse_response"],
            [1, 2, 1, "", "end_prepare_qubo"],
            [1, 2, 1, "", "end_prepare_request"],
            [1, 2, 1, "", "end_queue"],
            [1, 2, 1, "", "end_receive_response"],
            [1, 2, 1, "", "end_reduce_degree"],
            [1, 2, 1, "", "end_sampling"],
            [1, 2, 1, "", "end_save_request"],
            [1, 2, 1, "", "end_save_response"],
            [1, 2, 1, "", "end_scaling"],
            [1, 2, 1, "", "end_send_request"],
            [1, 2, 1, "", "end_solve"],
            [1, 2, 1, "", "end_waiting"],
            [1, 2, 1, "", "end_waiting_2_finish"],
            [1, 2, 1, "", "end_waiting_2_start"],
            [1, 2, 1, "", "logger"],
            [1, 2, 1, "", "start_account_occupied"],
            [1, 2, 1, "", "start_anneal"],
            [1, 2, 1, "", "start_cpu"],
            [1, 2, 1, "", "start_dau_service"],
            [1, 2, 1, "", "start_elapsed"],
            [1, 2, 1, "", "start_execution"],
            [1, 2, 1, "", "start_occupied"],
            [1, 2, 1, "", "start_parse_response"],
            [1, 2, 1, "", "start_prepare_qubo"],
            [1, 2, 1, "", "start_prepare_request"],
            [1, 2, 1, "", "start_queue"],
            [1, 2, 1, "", "start_receive_response"],
            [1, 2, 1, "", "start_reduce_degree"],
            [1, 2, 1, "", "start_sampling"],
            [1, 2, 1, "", "start_save_request"],
            [1, 2, 1, "", "start_save_response"],
            [1, 2, 1, "", "start_scaling"],
            [1, 2, 1, "", "start_send_request"],
            [1, 2, 1, "", "start_solve"],
            [1, 2, 1, "", "start_waiting"],
            [1, 2, 1, "", "start_waiting_2_finish"],
            [1, 2, 1, "", "start_waiting_2_start"]
        ],
        "dadk.utils.CoordinatesUtils": [
            [0, 0, 1, "", "CoordinatesUtils"]
        ],
        "dadk.utils.CoordinatesUtils.CoordinatesUtils": [
            [0, 1, 1, "", "generate_coordinates"],
            [0, 1, 1, "", "plot_coordinates"]
        ],
        "dadk.utils.TSPUtils": [
            [0, 0, 1, "", "TSPUtils"]
        ],
        "dadk.utils.TSPUtils.TSPUtils": [
            [0, 1, 1, "", "create_QUBOS"],
            [0, 1, 1, "", "plot_route"]
        ]
    },
    "objnames": {
        "0": ["py", "class", "Python class"],
        "1": ["py", "method", "Python method"],
        "2": ["py", "property", "Python property"],
        "3": ["py", "function", "Python function"]
    },
    "objtypes": {
        "0": "py:class",
        "1": "py:method",
        "2": "py:property",
        "3": "py:function"
    },
    "terms": {
        "": [1, 2, 3, 5, 6, 12, 13, 14, 15, 16, 17, 18, 19, 22],
        "0": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 20],
        "000sec": 1,
        "01": 1,
        "02": 14,
        "05": 2,
        "0f": 5,
        "0xffffff": 5,
        "1": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 16, 17, 18, 19, 20, 21],
        "10": [0, 2, 3, 5, 6, 18, 19, 20, 23, 24],
        "100": [0, 1, 2, 3, 5, 6],
        "1000": [1, 2, 6, 13, 14, 16, 19],
        "10000": [14, 16],
        "100000": 2,
        "1000000": [1, 2],
        "100000000": 2,
        "1000000000": 3,
        "100000000000000000000": 2,
        "101": [0, 1],
        "102": [0, 1],
        "1024": [2, 24],
        "11": 16,
        "1100": 6,
        "12": [16, 24],
        "1200": 6,
        "128": 2,
        "14": 16,
        "15": [6, 16],
        "150": 2,
        "16": [0, 1, 2, 3],
        "170": 6,
        "18": 1,
        "1800": [0, 1, 13, 14, 15, 16],
        "1_000_000_000": 3,
        "1e": 2,
        "1hot": [1, 2],
        "1w1h": 3,
        "1wai": [1, 2],
        "2": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24],
        "20": [6, 16],
        "200": [2, 3, 6, 19],
        "2000": [13, 14, 15, 16],
        "2018": 21,
        "2020": 21,
        "21": 16,
        "21600": 14,
        "25": [6, 19],
        "27": 6,
        "299": 3,
        "2d": 3,
        "2f": 5,
        "2nd": 5,
        "2w1h": 3,
        "2wai": [1, 2],
        "2x": [3, 24],
        "2x_1": 3,
        "3": [2, 3, 5, 6, 13, 14, 15, 16, 17, 20, 21, 23, 24],
        "300": [0, 1],
        "30600": 14,
        "310": 23,
        "32": 3,
        "32707497e": 14,
        "32766812e": 14,
        "3600": 2,
        "36100946e": 14,
        "384": 24,
        "3c": 2,
        "3d": [5, 7],
        "3f": 3,
        "4": [1, 2, 3, 4, 5, 6, 7, 13, 14, 16, 20],
        "400": [13, 15, 16],
        "4000": 14,
        "40000": 6,
        "42": [2, 3, 14, 16, 19],
        "440": 2,
        "5": [0, 1, 2, 3, 4, 5, 6, 13, 14, 15, 16, 17, 20],
        "50": [1, 6],
        "500": 6,
        "5000": 1,
        "500px": 5,
        "50px": 5,
        "51": 16,
        "6": [5, 20],
        "6000": 14,
        "621": 2,
        "6230": 24,
        "63": [1, 2],
        "64": [1, 2, 3],
        "6mb": 2,
        "7": [1, 3, 4, 6, 7, 14, 20],
        "70": 6,
        "700": 6,
        "70000": 6,
        "8": [3, 20],
        "800": [2, 6],
        "8080": [0, 1],
        "81": [0, 1],
        "8192": 2,
        "9": [1, 3, 4, 5, 6, 7, 20],
        "90": 3,
        "900": 6,
        "90000": 6,
        "9223372036854775807": 2,
        "92882207e": 14,
        "99": 1,
        "9999": 2,
        "99999999999": 2,
        "9a": 4,
        "A": [1, 2, 3, 4, 5, 6, 7, 12, 16, 18, 20],
        "And": 3,
        "As": [6, 14, 15],
        "At": 15,
        "But": 23,
        "By": 2,
        "For": [1, 2, 3, 5, 6, 13, 15, 16, 17, 19, 20],
        "If": [0, 1, 2, 3, 4, 5, 6, 7, 19],
        "In": [0, 1, 2, 3, 5, 6, 7, 13, 14, 15, 16, 17, 19, 20, 21, 23],
        "Ising": [3, 12, 24],
        "It": [0, 2, 3, 5, 6, 7, 12, 14, 17, 19, 21, 22, 24],
        "NOT": 3,
        "No": [1, 2, 3, 6],
        "On": [2, 6],
        "One": [0, 3, 5, 15, 18],
        "Such": 17,
        "The": [0, 1, 2, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24],
        "Their": 5,
        "Then": [5, 6],
        "There": [15, 24],
        "These": 13,
        "To": [0, 2, 3, 4, 6, 16, 18, 19],
        "Will": [5, 7],
        "With": [2, 3, 4, 5, 6, 15, 16, 20],
        "_": [4, 5, 6, 17],
        "__class__": 0,
        "__ge__": 3,
        "__init__": 5,
        "__last_setting__": 5,
        "__le__": 3,
        "_constant": 5,
        "_decai": [1, 2],
        "_dllt": 0,
        "_pol": 3,
        "_qubo": 3,
        "_set": 11,
        "_toolbar_paramet": 6,
        "_widgetinandout": 5,
        "a_": [3, 15, 17],
        "a_1": 5,
        "ab": [3, 5],
        "abc": 11,
        "abl": 6,
        "about": [3, 7, 16, 20],
        "abov": [0, 1, 2, 3, 5, 6, 14, 15, 16, 19],
        "absolut": [0, 1, 3, 5, 6],
        "abstain": 20,
        "abstract": [2, 3],
        "acceler": 16,
        "accept": [1, 2, 3, 5],
        "access": [0, 1, 2, 3, 5, 6, 12, 23],
        "access_profile_fil": [0, 1, 5],
        "accord": [0, 1, 2, 3, 5, 6, 16, 19],
        "accordingli": [0, 3, 5, 6],
        "accordion": 5,
        "account": [0, 1, 2, 3, 5],
        "accuraci": 2,
        "achiev": [1, 2, 3, 5, 6],
        "across": 3,
        "act": 2,
        "action": [1, 2, 5, 6],
        "activ": 5,
        "activate_output": 11,
        "actual": [2, 6, 16, 19],
        "ad": [3, 4, 5, 6, 13, 15, 17, 20],
        "adam1": [13, 14, 15, 16, 17],
        "adam2": [13, 14, 15, 16, 17],
        "adam3": [13, 14, 15, 16, 17],
        "adam4": [13, 14, 15, 16, 17],
        "adapt": [1, 2, 3, 6],
        "add": [4, 6, 11, 13, 14, 15, 16, 18, 19],
        "add_edg": 19,
        "add_exactly_1_bit_on": [11, 15],
        "add_exactly_n_bits_on": [11, 13, 14, 15, 16],
        "add_figur": 11,
        "add_figure_button": 5,
        "add_json_lin": 11,
        "add_most_1_bit_on": [11, 13, 14, 15, 16],
        "add_nod": 19,
        "add_slack_penalti": 11,
        "add_slack_vari": 11,
        "add_tab": 11,
        "add_term": [11, 13, 14, 15, 16, 18, 19, 20],
        "add_vari": 11,
        "add_variable_penalti": 11,
        "addit": [0, 1, 2, 3, 5, 6, 7, 13, 16, 17],
        "address": [0, 1, 3, 5, 6],
        "adjac": [18, 19],
        "adjust": [1, 2, 3, 6],
        "admiss": [3, 6],
        "adopt": 7,
        "advanc": [12, 20],
        "advic": 16,
        "af": 6,
        "after": [2, 3, 5, 6, 14, 16, 19],
        "after_upd": 5,
        "afterward": 3,
        "again": [6, 13, 14, 16],
        "against": [3, 5, 13],
        "aggreg": 5,
        "al": 3,
        "algorithm": [1, 2, 3, 6, 12, 16, 22],
        "align": [3, 15],
        "all": [0, 1, 2, 3, 5, 6, 13, 14, 15, 16, 17, 19, 20],
        "allow": [2, 3, 4, 5, 6, 7, 15, 20],
        "allow_nan": 0,
        "along": [2, 3, 5, 13, 15, 16, 17],
        "alreadi": [0, 2, 3, 5, 14, 16, 17, 21],
        "also": [0, 1, 2, 3, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23],
        "alt": 6,
        "altern": [3, 5, 15, 18, 20],
        "alwai": [2, 6],
        "among": [6, 13, 15, 16, 17],
        "amount": [2, 15],
        "an": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 21, 23],
        "analysi": [2, 6],
        "anchor": 4,
        "ani": [0, 1, 2, 3, 5, 6, 7, 14, 16],
        "anneal": [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 13, 15, 16, 17, 18, 19, 22, 23, 24],
        "anneal_track": 6,
        "annealer_address": [0, 1],
        "annealer_path": [0, 1],
        "annealer_port": [0, 1],
        "annealer_protocol": [0, 1],
        "annealer_queue_s": [0, 1],
        "annealing_log": [6, 11],
        "annealing_step": 1,
        "annealinglogset": 11,
        "anoth": [3, 4, 5, 6],
        "api": [1, 2, 3],
        "api_kei": [0, 1],
        "app": 5,
        "appear": [6, 15],
        "append": [3, 5, 6, 7, 13, 14, 16, 17],
        "appli": [3, 5, 6, 7, 16, 17],
        "applic": 5,
        "applier": [5, 11],
        "approach": [13, 16, 19],
        "appropri": [0, 3, 6, 13],
        "approxim": 3,
        "ar": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24],
        "arbitrari": 2,
        "area": 7,
        "arg": [3, 5, 6],
        "arg_typ": 5,
        "argument": [2, 3, 5, 6, 7],
        "argumnt": 5,
        "arithmet": 3,
        "around": 5,
        "arrai": [0, 3, 5, 6, 7, 17, 20],
        "arrang": 5,
        "as_bqm": 11,
        "as_hbsolv": 11,
        "as_json": 11,
        "as_latex": [11, 20],
        "as_qbsolv": 11,
        "as_text": [1, 11],
        "asa": 5,
        "ascend": [3, 5],
        "ascendingab": 5,
        "ascii": 0,
        "asctim": 6,
        "ask": 17,
        "assign": [12, 14, 15, 16, 17],
        "associ": [1, 3, 6, 7],
        "assum": [1, 3, 5, 17],
        "attach": [2, 3],
        "attain": [1, 3, 13],
        "attempt": 0,
        "attribut": [0, 2, 3, 4, 5, 6, 7],
        "authent": 0,
        "auto": [1, 2],
        "auto_fill_cold_bit": 3,
        "auto_load": 6,
        "auto_sc": [1, 2, 16],
        "auto_start": 5,
        "autoload": 6,
        "automat": [1, 2, 3, 5, 6, 14, 16, 19, 20],
        "auxiliari": 13,
        "avail": [0, 1, 2, 3, 5, 6, 14, 21],
        "averag": [2, 5, 15],
        "avg": 5,
        "avoid": 3,
        "awai": [2, 3, 13, 17],
        "ax": 5,
        "axi": [1, 3, 5, 7],
        "axis_nam": [3, 7],
        "axis_offset": 7,
        "azur": [0, 1, 2, 5],
        "azure_blob": [0, 1],
        "azure_blob_cmd": [0, 1],
        "azureblobmixin": 11,
        "b": [3, 5],
        "back": [0, 3, 6, 7, 14],
        "background": 5,
        "backward": 7,
        "bar": [5, 6],
        "base": [0, 1, 2, 3, 4, 5, 6, 7, 15, 17],
        "base10": 3,
        "base_dao_file_name_without_suffix": 6,
        "basi": [0, 2],
        "basic": [2, 12, 16, 17, 20],
        "batch": [5, 6, 11],
        "batch_mod": [2, 5, 6],
        "batchset": 11,
        "becaus": 16,
        "becom": [5, 17],
        "been": [0, 3, 5, 6, 7, 12, 14, 15, 16, 17, 18, 19],
        "befor": [1, 2, 3, 5, 6, 13, 14, 16, 17, 20],
        "before_call_funct": 5,
        "before_upd": 5,
        "begin": [2, 3, 14, 15, 17, 18],
        "begin_group": 5,
        "behavior": 0,
        "behaviour": [5, 6],
        "being": [3, 5, 6, 15],
        "belong": [3, 6],
        "below": [0, 3, 5, 17, 24],
        "benchmark": 24,
        "besid": [3, 6],
        "best": [1, 2, 3, 14, 16, 23],
        "better": [1, 2, 3, 14],
        "between": [0, 1, 2, 3, 5, 13, 15],
        "bhp": 3,
        "bia": 3,
        "big": [3, 13],
        "bigger": [2, 3, 6],
        "bin": 3,
        "binari": [2, 4, 8, 9, 10, 11, 12, 13, 15, 18, 22],
        "binary_polynomi": [3, 7],
        "binary_polynomial_kei": 3,
        "binaryquadraticmodel": 3,
        "binpol": [0, 2, 4, 7, 11, 13, 14, 15, 16, 17, 18, 19, 20],
        "binpolgen": 11,
        "bit": [1, 2, 6, 7, 11, 13, 14, 16, 18, 24],
        "bit_arrai": 4,
        "bit_array_shap": 7,
        "bit_div": 4,
        "bit_factor": 4,
        "bit_flip": 1,
        "bit_mlt": 4,
        "bit_nam": 4,
        "bit_no": [3, 11],
        "bit_precis": 3,
        "bit_prod": 4,
        "bit_set": 19,
        "bit_valu": 3,
        "bit_var": 4,
        "bitarrai": [3, 11],
        "bitarrayshap": [7, 11, 13, 14, 15, 16, 17, 18, 19, 20],
        "black": [5, 14],
        "blank": [4, 5],
        "blob": [0, 1, 5, 11],
        "blob_nam": 2,
        "block": [5, 13, 14, 15, 16, 17],
        "blue": [5, 7],
        "bob1": [13, 14, 15, 16, 17],
        "bob2": [13, 14, 15, 16, 17],
        "bob3": [13, 14, 15, 16, 17],
        "bob4": [13, 14, 15, 16, 17],
        "bodi": 6,
        "boltzmann": 2,
        "boltzmannsampl": 11,
        "bookkeep": 2,
        "bool": [0, 1, 2, 3, 5, 6, 7],
        "boolean": [3, 6, 7],
        "border": 5,
        "both": [2, 3, 5, 16, 17, 19, 20, 24],
        "bound": [5, 6],
        "boundedfloattext": 5,
        "boundedinttext": 5,
        "box": [2, 19],
        "bqp": [11, 24],
        "bracket": [5, 15],
        "brc": 4,
        "break": 14,
        "break_after_stat": 5,
        "breakpoint": [5, 6],
        "bring": 3,
        "brought": 14,
        "bucket": 3,
        "budget": 15,
        "build": [3, 5, 6],
        "build_and_solv": 11,
        "build_qubo": [5, 11],
        "build_qubo_break_after_stat": 6,
        "build_qubo_on_cal": 6,
        "build_qubo_on_focu": 6,
        "build_qubo_paramet": 6,
        "build_qubo_requir": 6,
        "build_qubo_templ": 6,
        "build_qubo_titl": 6,
        "build_qubo_toolbar_paramet": 6,
        "built": [3, 5],
        "builtin": 5,
        "busi": [3, 6, 7, 16, 18, 20, 21, 23],
        "button": [5, 6],
        "button_width": [6, 11],
        "byte": 2,
        "bz2": 23,
        "c": [3, 4, 5],
        "c_": 4,
        "cach": 3,
        "calcul": [0, 1, 2, 3, 5, 6, 17],
        "calculate_hash": 11,
        "calculated_build_qubo_paramet": 6,
        "calculated_solver_paramet": 6,
        "call": [0, 1, 2, 3, 5, 6, 7, 12, 13, 17, 18, 19, 21],
        "call_back": [3, 7, 11],
        "call_clear_output": 6,
        "call_displai": 5,
        "call_funct": 11,
        "callabl": [0, 2, 3, 5, 6, 7],
        "callback": [2, 5],
        "can": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24],
        "cancel": 2,
        "cancel_job": 2,
        "candid": [2, 16, 19],
        "cannot": [5, 6, 13],
        "care": [3, 6, 16, 17],
        "cartesian": 6,
        "cartesian_product": 6,
        "case": [0, 1, 2, 3, 5, 6, 15, 16, 17, 19, 21, 23],
        "castabl": 3,
        "categor": [3, 6],
        "categori": 11,
        "caught": 16,
        "caus": 0,
        "caveat": 3,
        "cdot": [3, 15],
        "ceil": 3,
        "cell": [5, 6],
        "center": 1,
        "center_in_minimum_energi": 1,
        "center_in_perc": 1,
        "center_in_step": 1,
        "centr": 0,
        "central": 0,
        "certain": [1, 3, 5, 6, 7, 17],
        "chain": [3, 6],
        "chang": [1, 2, 3, 5, 6, 14, 16, 17],
        "changeabl": 5,
        "chapter": [3, 15, 20],
        "char": 3,
        "charact": [0, 5],
        "characterist": 3,
        "charly1": [13, 14, 15, 16, 17],
        "charly2": [13, 14, 15, 16, 17],
        "charly3": [13, 14, 15, 16, 17],
        "charly4": [13, 14, 15, 16, 17],
        "check": [0, 2, 3, 4, 5, 6, 8, 9, 10, 23],
        "check_circular": 0,
        "check_da_paramet": 11,
        "checkbox": 5,
        "child": 5,
        "choic": 5,
        "choos": [1, 2, 3, 5, 6],
        "chose": 16,
        "chosen": [2, 3],
        "circl": 0,
        "circular": 0,
        "circular_layout": 19,
        "citi": 0,
        "city_count": 6,
        "clamp": 3,
        "clamped_configur": 3,
        "clamping_bit": 3,
        "class": [0, 1, 2, 3, 4, 5, 6, 7, 14, 17, 20],
        "classic": [0, 1, 16],
        "classmethod": 11,
        "classnam": 0,
        "classvar": 3,
        "clazz": 0,
        "cleanup_job": 2,
        "cleanupazureblobstorag": 11,
        "cleanupjob": 11,
        "clear": [3, 5, 6],
        "click": [5, 6],
        "client": [0, 1],
        "clone": 11,
        "clone_warning_count": 3,
        "close": [2, 11, 19],
        "closest": 3,
        "coars": 7,
        "code": [2, 3, 4, 5, 6, 13, 14, 15, 16, 17, 19, 20],
        "coeffici": [1, 2, 3, 11],
        "coincid": 3,
        "col_head": 5,
        "col_result": 5,
        "cold": 3,
        "collect": [0, 1, 3, 13, 17],
        "color": [3, 5, 19],
        "color_pick": 5,
        "colresult": 5,
        "column": [3, 5, 6, 11],
        "com": [3, 12],
        "combin": [2, 3, 6, 21],
        "combinatori": [12, 20, 21],
        "come": [16, 19],
        "comfort": 19,
        "commandlin": [0, 1, 6],
        "comment": 5,
        "common": [0, 1, 3, 4, 5],
        "compact": [0, 3],
        "compar": [0, 3, 5, 17],
        "comparison": [5, 24],
        "compat": [3, 7],
        "complet": [1, 2, 3, 5, 6, 7, 19],
        "complex": [12, 21],
        "compliant": 0,
        "complic": [13, 17],
        "compon": 5,
        "compos": [6, 11],
        "compose_log": [6, 11],
        "compose_model": [5, 6],
        "compose_model_break_after_stat": 6,
        "compose_model_on_cal": 6,
        "compose_model_on_focu": 6,
        "compose_model_paramet": 6,
        "compose_model_requir": 6,
        "compose_model_titl": 6,
        "compose_model_toolbar_paramet": 6,
        "composelogset": 11,
        "composemodel": 6,
        "composeset": 11,
        "comput": [0, 2, 11, 14, 15, 19, 21],
        "concept": [3, 14, 17],
        "concret": [15, 16],
        "config": [2, 3, 14],
        "configur": [0, 1, 2, 3, 5, 6, 7, 11, 18, 19, 22],
        "configuration_2_str": 11,
        "connect": [0, 1, 2, 19],
        "connection_paramet": 2,
        "connectionparamet": [2, 11],
        "connectionparameterwithazureblob": 11,
        "consecut": 2,
        "consid": [3, 4, 15, 17, 18, 20],
        "consider": 3,
        "consist": [0, 2, 3, 4, 5, 6, 14, 17],
        "constant": [1, 2, 3, 7, 14, 16],
        "constant_bit": [3, 13, 14, 15, 16, 17],
        "constant_bits_x_bit": [13, 14, 15, 16, 17],
        "constant_bits_y_bit": 16,
        "constrain": [13, 22],
        "constraint": [0, 1, 2, 3, 7, 14, 16, 17, 22],
        "construct": [3, 5, 6],
        "constructor": [0, 2, 3, 5, 6, 7],
        "consumpt": 7,
        "contain": [0, 1, 3, 4, 5, 6, 7, 11, 16, 17, 19, 20, 23],
        "container_nam": [0, 1, 2],
        "content": [0, 1, 5, 6],
        "context": [5, 6],
        "continu": [1, 2, 6],
        "contrast": 3,
        "control": [2, 3, 4, 5, 6],
        "converg": [16, 17],
        "convers": 3,
        "convert": [0, 1, 2, 3, 5, 7, 8, 9, 10],
        "convert_to_binari": 11,
        "cool": [1, 2],
        "coordin": [0, 3, 5],
        "coordinatesutil": 11,
        "copi": [0, 3, 4, 5, 6, 7],
        "copy_to": 11,
        "core": [2, 18],
        "correct": [2, 17],
        "correctli": [1, 2, 3],
        "correl": 5,
        "correspond": [0, 2, 3, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19],
        "cost": [3, 18],
        "could": [3, 5, 6, 7, 14, 16, 17, 18, 19],
        "count": [2, 5, 6],
        "count_fil": 11,
        "countdown": 5,
        "coupl": 3,
        "cover": [3, 17],
        "coverag": [1, 2, 6],
        "cpu": [2, 19, 24],
        "cr": 3,
        "creat": [0, 1, 2, 3, 4, 5, 7, 11, 12, 24],
        "create_batch": 11,
        "create_contain": 2,
        "create_qubo": 0,
        "create_random_st": [11, 14],
        "create_rpoly_hd5": 11,
        "create_rpoly_script": 11,
        "create_sas_token": 2,
        "create_solv": 11,
        "create_summary_excel": [6, 11],
        "creation": [5, 6, 24],
        "creator": [5, 11],
        "criteria": 2,
        "criterion": 7,
        "crs2inequ": 11,
        "crs2qubo": 11,
        "css": 5,
        "css_style": 5,
        "csv": [5, 7],
        "csv_content": 11,
        "csv_report": 7,
        "ctype": 0,
        "current": [0, 1, 2, 3, 5, 6, 7, 14, 18],
        "current_work": 14,
        "curv": [1, 2, 5, 6],
        "custom": [0, 5],
        "cut": 18,
        "cutoff": [1, 2],
        "cycl": [1, 5],
        "d": [3, 4, 5, 13, 14, 15, 16, 17, 24],
        "d_s_w": 14,
        "da": [1, 2, 3, 21],
        "da_paramet": [2, 7, 11],
        "dadk": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22],
        "dadk_cpu_annealing_thread": 2,
        "dadk_local_minima_sampling_thread": 2,
        "dadk_local_minima_sampling_verbos": 2,
        "dadk_vers": 23,
        "dadk_version_check": 11,
        "dai": [0, 5, 13, 14, 15, 16, 17],
        "danger": 5,
        "dao": [5, 6],
        "dao_path": 6,
        "dao_pattern": 6,
        "daodecod": 11,
        "daoencod": 11,
        "darwin": 0,
        "dashboard": 6,
        "data": [0, 1, 2, 3, 5, 6, 7, 16, 17, 19],
        "date": [0, 5],
        "date_pick": 5,
        "datetim": [1, 2, 5],
        "datetimepick": 11,
        "dau": 3,
        "dav2": [1, 3, 6],
        "dav2paramet": [2, 7, 11],
        "dav3": [1, 3, 5, 7],
        "dav3c": [1, 2],
        "dav3cparamet": [2, 11],
        "dav3paramet": 11,
        "dav4": [1, 2],
        "dav4paramet": [2, 11],
        "dave1": [13, 14, 15, 16, 17],
        "dave2": [13, 14, 15, 16, 17],
        "dave3": [13, 14, 15, 16, 17],
        "dave4": [13, 14, 15, 16, 17],
        "dd": 0,
        "deactiv": 5,
        "deactivate_output": 11,
        "debug": [0, 1, 5, 6],
        "debug_except": 11,
        "decai": [1, 2],
        "decid": [2, 3, 5, 15],
        "decim": 3,
        "decis": [5, 17, 18, 20],
        "declar": [0, 2, 3, 5, 6, 7],
        "declin": 2,
        "decod": [0, 1, 5, 6, 11],
        "decoder_class": 0,
        "decreas": [2, 16, 19],
        "dedic": [5, 6],
        "deep": [1, 2],
        "default": [0, 1, 2, 3, 5, 6, 7, 16, 19],
        "default_solver_paramet": 6,
        "defin": [0, 1, 2, 4, 5, 6, 11, 15, 16, 17, 18, 19, 20, 22],
        "definit": [2, 3, 4, 5, 6, 17, 20],
        "deform": 15,
        "degre": [3, 11],
        "delet": [0, 2, 5, 6, 8, 9, 10],
        "delete_annealer_access_profil": 0,
        "delete_blob": 2,
        "delete_contain": 2,
        "delete_fil": [6, 11],
        "delete_job": 2,
        "deliv": [0, 3],
        "delta": [1, 2],
        "demand": [13, 14, 15, 16, 17],
        "demo_solver_paramet": 6,
        "depend": [5, 6, 15, 21],
        "deprec": [5, 6, 11],
        "depth": 4,
        "deriv": [2, 3, 6],
        "descend": [2, 5],
        "descendingab": 5,
        "describ": [2, 3, 4, 5, 6],
        "descript": [0, 3, 5, 6, 11, 17],
        "description_tooltip": 5,
        "description_width": [6, 11],
        "desir": 3,
        "detail": [2, 6, 14],
        "detect": 6,
        "determin": [1, 2, 3, 5, 6, 15, 16, 19],
        "develop": [5, 6, 18],
        "development_mod": [5, 6],
        "deviat": 16,
        "dialog": 5,
        "dict": [0, 1, 2, 3, 6, 7, 11],
        "dictionari": [0, 1, 2, 3, 5, 6, 7, 14, 17],
        "differ": [2, 3, 4, 5, 6, 15, 16, 18, 19],
        "digit": [0, 1, 2, 3, 5, 8, 9, 10, 13, 14, 15, 17, 18, 19, 22, 23, 24],
        "dim": 5,
        "dimens": [3, 5, 7, 11, 17, 20],
        "dimension": [3, 17, 20, 24],
        "dimod": 3,
        "direct": [3, 5, 16],
        "directli": [0, 1, 3, 5, 6, 13, 15],
        "directmanag": 6,
        "directori": [0, 2, 5, 6, 11],
        "directory_selector": 5,
        "directorymanag": 11,
        "directoryselector": 11,
        "disabl": [0, 1, 5, 6, 11],
        "disappear": 5,
        "discard": 14,
        "discontinu": 3,
        "discret": [3, 20],
        "discrimin": 7,
        "disjoint": 3,
        "displai": [2, 6, 7, 11],
        "display_appli": 11,
        "display_blob": 2,
        "display_cont": 11,
        "display_control": 11,
        "display_cr": 11,
        "display_graph": [11, 14],
        "display_job": 2,
        "displayfmt": 5,
        "disrupt": 21,
        "distanc": [0, 2],
        "distribut": [1, 2, 15, 16],
        "div": 4,
        "dll": 0,
        "do": [3, 5],
        "do_sampl": 2,
        "do_sort": 3,
        "doc": 3,
        "document": [0, 2, 3, 4, 5, 6, 7, 12],
        "doe": [1, 2, 3, 7, 15, 16, 19],
        "don": 5,
        "done": [2, 3, 5, 6, 16, 18, 20],
        "dot": 7,
        "doubl": 3,
        "down": [5, 14, 16],
        "download": [2, 5, 6, 23],
        "download_blob": 2,
        "download_job": 2,
        "dpi": [0, 1],
        "drastic": 13,
        "draw": [6, 11],
        "draw_networkx": 19,
        "drive": [5, 6],
        "driven": [3, 6],
        "drop": [3, 15, 16, 17],
        "dropdown": [6, 11],
        "dtype": [3, 13, 14, 15, 16, 17],
        "due": [2, 21],
        "dummi": 5,
        "duplic": 3,
        "durat": [1, 2, 5, 6],
        "duration_account_occupi": 1,
        "duration_ann": 1,
        "duration_cpu": 1,
        "duration_dau_servic": 1,
        "duration_elaps": 1,
        "duration_execut": 1,
        "duration_occupi": 1,
        "duration_parse_respons": 1,
        "duration_prepare_qubo": 1,
        "duration_prepare_request": 1,
        "duration_queu": 1,
        "duration_receive_respons": 1,
        "duration_reduce_degre": 1,
        "duration_sampl": 1,
        "duration_save_request": 1,
        "duration_save_respons": 1,
        "duration_sc": 1,
        "duration_send_request": 1,
        "duration_solv": 1,
        "duration_wait": 1,
        "duration_waiting_2_finish": 1,
        "duration_waiting_2_start": 1,
        "dure": [0, 1, 2, 3, 5, 6, 7, 13, 16],
        "dwavesi": 3,
        "dwavesystem": 3,
        "dylib": 0,
        "dynam": [1, 2, 17],
        "e": [0, 1, 2, 3, 5, 6, 13, 14, 15, 16, 17, 18, 19, 21, 23],
        "e_i": 2,
        "e_j": 2,
        "each": [0, 1, 2, 3, 5, 6, 13, 14, 15, 16, 17, 18, 19, 20],
        "earl1": [13, 14, 15, 16, 17],
        "earl2": [13, 14, 15, 16, 17],
        "earl3": [13, 14, 15, 16, 17],
        "earl4": [13, 14, 15, 16, 17],
        "earli": [13, 14, 15, 16, 17],
        "earlier": [6, 14],
        "easi": [14, 19, 20],
        "easili": [15, 20],
        "econom": 15,
        "edg": [18, 19],
        "edit": 5,
        "editor": 5,
        "effect": [3, 22],
        "effici": [12, 15, 18, 21, 22],
        "effort": 6,
        "either": [0, 1, 2, 5, 6, 15, 16],
        "elaps": 5,
        "element": [0, 2, 3, 5, 6],
        "elimin": [0, 14],
        "els": [0, 2, 3, 5, 6, 7, 14, 16, 17, 19],
        "emploi": 22,
        "empti": [0, 2, 3, 5, 6],
        "emul": [1, 2, 6, 12, 24],
        "en": 3,
        "enabl": [0, 1, 5, 6],
        "encapsul": 6,
        "encod": [0, 1, 3, 6, 11, 19],
        "encoder_class": 0,
        "end": [1, 2, 3, 5, 15, 16, 17, 18, 19],
        "end_account_occupi": 1,
        "end_ann": 1,
        "end_cpu": 1,
        "end_dau_servic": 1,
        "end_elaps": 1,
        "end_execut": 1,
        "end_group": 5,
        "end_occupi": 1,
        "end_parse_respons": 1,
        "end_prepare_qubo": 1,
        "end_prepare_request": 1,
        "end_progress_prob": 1,
        "end_queu": 1,
        "end_receive_respons": 1,
        "end_reduce_degre": 1,
        "end_sampl": 1,
        "end_save_request": 1,
        "end_save_respons": 1,
        "end_scal": 1,
        "end_send_request": 1,
        "end_solv": 1,
        "end_wait": 1,
        "end_waiting_2_finish": 1,
        "end_waiting_2_start": 1,
        "energi": [1, 2, 6, 7, 11, 14, 16, 19],
        "energy_list": 2,
        "enforc": [0, 3, 15],
        "enlarg": 13,
        "enorm": 6,
        "ensur": [0, 3, 14],
        "ensure_ascii": 0,
        "entri": [0, 2, 3, 5, 6, 11, 17, 20],
        "enum": 11,
        "enumer": [1, 3, 5, 6, 13, 14, 15, 16, 17],
        "environ": [2, 6, 23],
        "epoch": [1, 2],
        "equal": [1, 2, 3, 7, 13, 15],
        "equat": [17, 18],
        "equival": 3,
        "eras": 3,
        "error": [2, 3, 6],
        "escap": [0, 2, 16],
        "especi": [3, 16, 17, 22, 24],
        "essenti": 22,
        "establish": 2,
        "estim": 1,
        "etc": 0,
        "evalu": [2, 11, 13, 14, 16],
        "even": [3, 6, 14, 16, 17, 21],
        "evenli": [13, 15, 16, 17],
        "event": 6,
        "eventu": 6,
        "everi": [1, 2, 3, 5, 6, 16, 17, 19, 20],
        "everytim": 5,
        "exactli": [0, 3, 17],
        "exactly_1_bit_on": 11,
        "exactly_n_bits_on": 11,
        "examin": 6,
        "exampl": [2, 3, 5, 6, 7, 14, 15, 16, 19, 20],
        "exceed": [3, 5],
        "excel": 6,
        "excel_summary_column": [6, 11],
        "except": [0, 2, 5, 6, 11, 16],
        "exchang": [1, 2, 16],
        "exclud": 3,
        "execut": [1, 2, 3, 5, 6],
        "execution_time_receiv": 5,
        "exist": [0, 2, 5, 6, 7],
        "exit": [1, 2],
        "exp": [2, 4],
        "expect": [1, 2, 3, 5, 6, 7, 20],
        "expectation_valu": 1,
        "experi": [2, 14, 15],
        "experiment": 11,
        "explain": [3, 13],
        "explan": 5,
        "explicit": 2,
        "explicitli": [5, 18],
        "expon": 3,
        "exponenti": [1, 2],
        "export": 7,
        "express": [3, 4, 5, 6, 13],
        "extend": [2, 3, 5, 14, 24],
        "extens": [3, 23],
        "extern": 0,
        "extra": 6,
        "extract": [7, 16, 19],
        "extract_bit_arrai": [3, 11],
        "f": 17,
        "facilit": 5,
        "fact": [5, 16],
        "factor": [0, 1, 2, 3, 4, 13, 15],
        "factor_a": 6,
        "factor_dist": 0,
        "factor_rul": 0,
        "fail": 2,
        "fals": [0, 1, 2, 3, 5, 6, 7, 14, 16, 19],
        "fancy_grid": 5,
        "far": [2, 3],
        "fast": [1, 2, 24],
        "faster": 20,
        "featur": [1, 2, 12, 15, 21, 24],
        "few": [14, 15, 17],
        "ff": 5,
        "fibonacci": [3, 4],
        "field": [2, 5, 6, 7],
        "fig": 21,
        "fig_report": 7,
        "figsiz": [0, 1, 5, 6, 7, 11],
        "figur": [0, 1, 5, 6, 7, 14, 21, 24],
        "figure_height": 5,
        "figure_width": 5,
        "file": [0, 1, 2, 3, 5, 6, 7, 11, 24],
        "file_nam": [2, 5, 6],
        "file_name_pattern": 6,
        "file_selector": 5,
        "file_size_info": 11,
        "filenam": [0, 1, 2, 3, 5, 6, 7],
        "files": 2,
        "fileselector": 11,
        "fill": [3, 5],
        "filter": [3, 5, 6],
        "filter_by_tag": 5,
        "final": [2, 5, 6, 7, 14],
        "find": [2, 5, 6, 8, 9, 10, 14, 18],
        "finest": 7,
        "finish": [2, 5],
        "first": [0, 1, 3, 5, 6, 7, 14, 15, 16, 17, 18, 21],
        "fit": [3, 6],
        "five": 19,
        "fix": [1, 2, 3, 5, 6, 13, 15, 17, 20],
        "fix_point_precis": 2,
        "fixed_paramet": 5,
        "fixpoint": 2,
        "fjj": 3,
        "flag": 6,
        "flask": [8, 9, 10],
        "flat": 3,
        "flatten": 3,
        "flavour": 5,
        "flip": [1, 2, 7, 16],
        "flip_prob": 1,
        "flippabl": 17,
        "float": [0, 1, 2, 3, 5, 6, 7, 16, 20],
        "float64": 3,
        "float_arrai": 5,
        "float_bound": 5,
        "float_log_slid": 5,
        "float_range_slid": 5,
        "float_slid": 5,
        "floor": 3,
        "focu": 6,
        "folder": 23,
        "follow": [1, 2, 3, 4, 5, 6, 13, 14, 15, 17, 18, 19, 23],
        "follow_up": 5,
        "foral": 13,
        "forc": 16,
        "form": [0, 2, 3, 5, 6, 7, 18],
        "formal": 3,
        "format": [0, 1, 2, 3, 5, 6, 19, 24],
        "formatt": 3,
        "formul": [3, 12, 13, 15, 17, 18, 20],
        "formula": [3, 13, 15, 20],
        "formular": 6,
        "found": [0, 1, 3, 5, 6, 7, 14, 23, 24],
        "four": 13,
        "frac": [3, 15],
        "framework": [2, 5, 6],
        "free": 17,
        "freez": 5,
        "freeze_var_shape_set": [11, 13, 14, 15, 16, 17, 18, 19, 20],
        "frequenc": 7,
        "fresh": 23,
        "fridai": [13, 14, 15, 16, 17],
        "friendli": 22,
        "from": [0, 1, 2, 3, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 20],
        "from_qpoli": 11,
        "from_scratch": 6,
        "front": 5,
        "frozen": 5,
        "fujitsu": [2, 12, 22],
        "fulfil": [13, 14, 15],
        "full": [0, 5, 13, 14, 15, 16, 17, 22],
        "fulli": [0, 5],
        "function": [4, 6, 7, 11, 16, 17, 18, 24],
        "function_nam": 4,
        "furnac": [1, 2],
        "furnace_mean_energy_list": 2,
        "furnace_process_length": 1,
        "furnace_replica_list": 2,
        "furnaces_replica_histori": 2,
        "further": [0, 2, 3, 4, 5, 7, 24],
        "furthermor": [12, 17],
        "futur": 21,
        "g": [3, 5, 6, 17, 18, 19, 23],
        "gauss": 3,
        "gb": 24,
        "ge": 3,
        "gen_latex": 11,
        "gen_poli": 4,
        "gen_pretty_str": 11,
        "gen_python": 11,
        "gener": [0, 1, 2, 3, 5, 6, 7, 11, 14, 15, 16],
        "generate_coordin": 0,
        "generate_penalty_polynomi": [11, 15],
        "generate_replica_furnace_exchange_step": 2,
        "generate_slack_polynomi": 11,
        "geq": 3,
        "germani": 12,
        "get": [0, 5, 6, 11, 12, 14, 16],
        "get_active_paramet": 11,
        "get_annealer_access_profil": 0,
        "get_bit": 11,
        "get_bit_flip": 1,
        "get_bits_of_1w1h_group": 11,
        "get_bits_of_2w1h_group": 11,
        "get_default_set": 11,
        "get_dict": 3,
        "get_energi": 1,
        "get_false_bit_indic": 11,
        "get_free_bit_indic": 11,
        "get_index": 11,
        "get_json": 11,
        "get_json_gener": 11,
        "get_max_abs_coeffici": 11,
        "get_minimum_energy_solut": [11, 14, 19],
        "get_normalize_factor": 11,
        "get_not_false_bit_indic": 11,
        "get_parameter_definit": 11,
        "get_persistent_data": 5,
        "get_persistent_set": 11,
        "get_persistent_tag": 5,
        "get_result": 11,
        "get_scale_factor": 11,
        "get_solution_list": 11,
        "get_solver_id": 11,
        "get_sorted_solution_list": 11,
        "get_stat": 1,
        "get_summari": 11,
        "get_symbol": 11,
        "get_temperatur": 1,
        "get_tim": 11,
        "get_true_bit_indic": 11,
        "get_valu": 11,
        "get_weight": 11,
        "get_widget": 11,
        "ghz": 24,
        "github": 5,
        "give": [1, 3, 5, 14, 23],
        "given": [0, 2, 3, 4, 5, 6, 7, 15, 18, 20],
        "global": [1, 2, 3],
        "gmbh": 12,
        "goal": [6, 18, 19],
        "goe": 0,
        "gold": 24,
        "good": 14,
        "granular": [3, 6],
        "graph": [6, 7, 18, 19],
        "graphic": [1, 2, 6, 7],
        "graphics_data": 7,
        "graphicsdata": [7, 11],
        "graphicsdetail": [2, 11],
        "graphs_figs": [6, 11],
        "greater": [2, 3],
        "green": [3, 5],
        "grei": 19,
        "grid": 5,
        "group": [3, 5, 14, 15, 16],
        "gs_max_penalty_coef": [1, 2],
        "gs_num_iteration_cl": [1, 2],
        "gs_num_iteration_factor": [1, 2],
        "gs_ohs_xw1h_num_iteration_cl": [1, 2],
        "gs_ohs_xw1h_num_iteration_factor": [1, 2],
        "gs_penalty_auto_mod": [1, 2],
        "gs_penalty_coef": [1, 2],
        "gs_penalty_inc_r": [1, 2],
        "guarante": [0, 14],
        "gui": [0, 1, 2, 5, 6],
        "gui_build": 11,
        "gui_compos": [6, 11],
        "gui_default_valu": 11,
        "gui_load": [6, 11],
        "gui_reset_valu": 11,
        "gui_save_valu": 11,
        "gui_solv": 11,
        "guidanc": [2, 22],
        "guidance_config": [2, 14],
        "gzip": [0, 1],
        "gzip_verbos": [0, 1],
        "h": [5, 18],
        "h2": 3,
        "h5": 3,
        "h_": 3,
        "h_1": 3,
        "h_i": 3,
        "h_n": 3,
        "ha": [0, 2, 3, 5, 6, 7, 12, 13, 14, 15, 16, 17],
        "had": 17,
        "half": [1, 19],
        "ham": 2,
        "hand": [3, 6, 13],
        "handl": [0, 2, 3, 5, 6, 17, 20, 21],
        "handler": 5,
        "happen": [2, 16, 19],
        "har": 22,
        "hard": [1, 2],
        "hardwar": [12, 21],
        "has_solv": 11,
        "hash": 3,
        "hash_step_s": 3,
        "have": [0, 1, 2, 3, 5, 6, 7, 14, 15, 16, 17, 18, 19],
        "hbox": 11,
        "hbsolv": 3,
        "hd5": 3,
        "hd_file_nam": 3,
        "hdf5": 3,
        "header": [5, 11],
        "healthcheck": 2,
        "height": [3, 5],
        "help": [1, 2, 3, 6, 14, 17, 18, 22],
        "helper": [2, 3, 7],
        "henc": [3, 5],
        "here": [3, 5, 6, 15, 17, 18, 23, 24],
        "hex": 0,
        "hexadecim": 3,
        "hi": 5,
        "hidden": 18,
        "hide": [5, 6],
        "hierarchi": 5,
        "higher": [3, 7, 8, 9, 10],
        "highest": [2, 3],
        "highlight": [0, 5],
        "hill": 1,
        "hill_energi": 1,
        "hill_step": 1,
        "histogram": 3,
        "histor": 2,
        "histori": 2,
        "hobo": [3, 8, 9, 10],
        "hold": 5,
        "hole": 3,
        "home": 0,
        "hook": 6,
        "horizont": [2, 5],
        "host": [0, 1],
        "hot": [0, 1, 3, 14, 15, 16],
        "hour": [1, 2, 5],
        "how": [0, 1, 2, 5, 7, 13, 18],
        "howev": 19,
        "hp": 6,
        "html": [3, 5, 6],
        "http": [0, 1, 3],
        "httpconnect": [0, 1],
        "human": 2,
        "hybrid": 2,
        "hyperparamet": 6,
        "i": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24],
        "i18n_get": 11,
        "i_1": 4,
        "i_2": 4,
        "icon": 5,
        "id": [0, 2, 5, 8, 9, 10],
        "ident": [2, 3, 5],
        "identifi": [3, 5, 6],
        "iff": [3, 5],
        "ignor": [1, 3, 4, 5],
        "ignore_proxi": 1,
        "imag": [0, 1],
        "immedi": [5, 6],
        "immediate_load": 6,
        "immediate_run": 6,
        "implement": [2, 3, 6, 17, 18, 20, 22, 23],
        "impli": 2,
        "implicitli": [2, 5],
        "import": [2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 20, 23, 24],
        "importantli": 23,
        "improv": [1, 2, 14, 15, 16, 17],
        "inappropri": 3,
        "inch": [0, 1, 5, 7],
        "incl": [1, 5, 6],
        "includ": [2, 3, 5, 6, 12, 15, 16, 17],
        "incom": 0,
        "incomplet": 6,
        "incorpor": 17,
        "increas": [1, 2, 3, 6, 14, 16, 19],
        "increment": [1, 2, 3, 5, 6],
        "incub": 12,
        "ind": 5,
        "inde": 19,
        "indent": [0, 4, 5],
        "indent_level": [4, 5],
        "indent_width": 5,
        "indentifi": 3,
        "independ": [1, 2, 16, 19],
        "index": [0, 1, 2, 3, 5, 6, 7, 11, 13, 14, 15, 16, 17, 20],
        "index_filt": 3,
        "index_offset": 3,
        "indic": [0, 1, 2, 3, 17, 20],
        "individu": 7,
        "ineq": 3,
        "inequ": [0, 1, 2, 7, 11, 14, 16],
        "inequalities2cr": 11,
        "inequalities_blob_nam": [0, 1],
        "inequalities_filenam": [0, 1],
        "inequalitysign": 11,
        "infin": 0,
        "infinit": [0, 15],
        "inflate_clamped_configur": 11,
        "info": [3, 5, 7],
        "inform": [1, 2, 3, 5, 6, 7, 16],
        "inher": 2,
        "inherit": 3,
        "initi": [1, 2, 3, 4, 5, 6, 14, 16, 19],
        "initial_select": 6,
        "inplac": 3,
        "input": [0, 2, 3, 4, 5, 6, 7, 17],
        "insensit": 5,
        "insert": [0, 5, 6],
        "insid": [1, 2, 3, 5, 6],
        "insight": 14,
        "inspect": 2,
        "inspir": [12, 21],
        "instal": [0, 1, 2, 22],
        "instanc": [0, 3, 6, 7, 24],
        "instanti": [5, 6, 7, 20],
        "instead": [1, 3, 5, 6, 7, 15, 18],
        "int": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "int64": 3,
        "int8": [3, 13, 14, 15, 16, 17],
        "int_arrai": 5,
        "int_bound": 5,
        "int_range_slid": 5,
        "int_slid": 5,
        "integ": [0, 2, 3, 5, 7, 15, 20],
        "integr": 21,
        "intel": [23, 24],
        "intend": [3, 5],
        "interest": 15,
        "interfac": [0, 1, 5],
        "intermedi": 5,
        "intern": [2, 3, 4, 5, 6, 7, 11, 17],
        "interpol": 2,
        "interpret": 3,
        "interrupt": [5, 6, 11],
        "interruptkei": 11,
        "interv": [1, 2, 3],
        "intiti": 6,
        "intro": 5,
        "introduc": [3, 21],
        "introduct": 23,
        "intuit": 20,
        "invalid": 2,
        "invers": [1, 2],
        "inverse_root": [1, 2],
        "invis": [3, 5],
        "involv": 3,
        "ip": [0, 1],
        "ipynb": 23,
        "is_empti": 11,
        "is_vis": 5,
        "isingpol": 11,
        "issu": 3,
        "item": [0, 5],
        "item_separ": 0,
        "iter": [0, 1, 2, 3, 6, 14, 16, 19],
        "its": [0, 1, 2, 3, 5, 6],
        "itself": [3, 5, 18],
        "j": [2, 18, 19],
        "jason": 0,
        "javascript": 0,
        "jira": 5,
        "job": [2, 8, 9, 10],
        "job_id": [2, 8, 9, 10],
        "jobstatu": 11,
        "json": [0, 1, 2, 3, 5, 6, 7],
        "jsonencod": [6, 11],
        "jsontool": 11,
        "jupyt": [5, 6, 23],
        "jupytertool": [2, 11],
        "just": [3, 4, 5, 17],
        "k": 3,
        "keep": [1, 2, 17, 20],
        "kei": [0, 1, 2, 3, 5, 6, 7, 12],
        "kept": 2,
        "key_separ": 0,
        "keyboard": 6,
        "keyword": [1, 3, 6, 7],
        "keyword_account_occupied_tim": 1,
        "keyword_anneal_tim": 1,
        "keyword_calculated_paramet": 6,
        "keyword_cpu_tim": 1,
        "keyword_dau_service_tim": 1,
        "keyword_default_paramet": 6,
        "keyword_elapsed_tim": 1,
        "keyword_execution_tim": 1,
        "keyword_fixed_paramet": 6,
        "keyword_occupied_tim": 1,
        "keyword_on_cal": 6,
        "keyword_paramet": 6,
        "keyword_parse_response_tim": 1,
        "keyword_prep_result": 6,
        "keyword_prepare_qubo_tim": 1,
        "keyword_prepare_request_tim": 1,
        "keyword_queue_tim": 1,
        "keyword_receive_response_tim": 1,
        "keyword_reduce_degree_tim": 1,
        "keyword_sampling_tim": 1,
        "keyword_save_request_tim": 1,
        "keyword_save_response_tim": 1,
        "keyword_scaling_tim": 1,
        "keyword_send_request_tim": 1,
        "keyword_solve_tim": 1,
        "keyword_waiting_2_finish_tim": 1,
        "keyword_waiting_2_start_tim": 1,
        "keyword_waiting_tim": 1,
        "kind": 1,
        "kit": 18,
        "know": [12, 17],
        "knowledg": 22,
        "known": [3, 14],
        "kozuchi": 3,
        "kwarg": [2, 5, 6, 7],
        "l": [3, 5],
        "l1": 3,
        "l2": 3,
        "l3": 3,
        "l_": 3,
        "label": [5, 6, 7, 11],
        "label_offset": 5,
        "lai": 3,
        "lambda": [3, 5],
        "lambda_a": 6,
        "lambda_valu": [3, 11, 13, 14, 16],
        "languag": 20,
        "larg": [3, 12, 13, 14, 15, 21, 24],
        "larger": [1, 2, 3, 13, 15, 20],
        "largest": 3,
        "last": [2, 3, 5, 14, 17, 18, 19, 20],
        "last_modifi": 2,
        "late": [13, 14, 15, 16, 17],
        "later": [3, 5, 6, 13],
        "latest": [3, 6, 24],
        "latex": [3, 4, 5, 20],
        "latex_booktab": 5,
        "latex_longt": 5,
        "latex_raw": 5,
        "latter": [0, 3, 6, 13, 14, 16, 18],
        "launch": 5,
        "layout": 5,
        "lbrace": [17, 18],
        "lceil": 3,
        "ldot": [17, 18, 20],
        "le": 3,
        "lead": [1, 2, 14, 16],
        "learn": [17, 20],
        "least": [0, 15, 16, 17],
        "left": [3, 5, 6, 13, 14, 15, 17, 18],
        "left_axi": 5,
        "left_qubo": [3, 11],
        "legend": 6,
        "len": [3, 5, 13, 14, 15, 16, 17],
        "length": [2, 3, 5, 6, 11],
        "leq": [3, 13],
        "less": [3, 13, 14, 17],
        "let": [13, 15],
        "level": [0, 1, 3, 4, 6, 7, 17],
        "levelnam": 6,
        "leverag": 6,
        "lfloor": 3,
        "li": [3, 17],
        "librari": [0, 12, 23],
        "light": [5, 7, 23],
        "lightblu": 19,
        "like": [0, 3, 5, 6, 13, 16],
        "limit": [1, 2, 3, 5, 6, 16],
        "line": [0, 3, 5, 6, 14, 17, 18, 19],
        "linear": [2, 3, 13, 15, 17, 18],
        "link": [5, 20],
        "linux": [0, 23],
        "list": [0, 1, 2, 3, 5, 6, 8, 9, 10, 11, 13, 14, 17],
        "list_annealer_access_profil": 0,
        "list_blob": 2,
        "list_contain": 2,
        "list_job": 2,
        "listen": 6,
        "littl": 3,
        "littlegauss": 3,
        "ln": 3,
        "load": [0, 1, 3, 5, 11],
        "load_break_after_stat": 6,
        "load_connection_paramet": 11,
        "load_hdf5": 11,
        "load_json": 11,
        "load_librari": 11,
        "load_on_cal": 6,
        "load_on_focu": 6,
        "load_paramet": 6,
        "load_qbsolv": 11,
        "load_requir": 6,
        "load_titl": 6,
        "local": [0, 1, 2, 14, 16, 19],
        "localminimasampl": 11,
        "locat": [0, 3, 23],
        "log": [2, 3, 5, 11],
        "log_": 3,
        "log_data": 11,
        "log_format": 6,
        "log_level": 6,
        "log_memory_s": [6, 11],
        "log_msg": 11,
        "logarithm": 5,
        "logger": [1, 2],
        "logic": [6, 17],
        "long": [2, 5],
        "longer": 3,
        "look": [0, 13, 14],
        "loop": [2, 5],
        "low": [1, 2],
        "lower": [1, 2, 5, 6],
        "lowest": [2, 3, 7],
        "ly": 2,
        "m": [3, 13, 23],
        "mac": 23,
        "machin": [0, 1, 3, 12, 24],
        "made": [3, 5, 6],
        "mai": [0, 1, 2, 3, 5, 13, 17],
        "main": [0, 3, 4, 5, 6, 21],
        "mainli": [3, 5],
        "maintain": [1, 2, 12, 16],
        "make": [2, 3, 5, 6, 14, 16, 17, 19],
        "make_clamp": 11,
        "make_qubo": [2, 11],
        "makequbo_precis": [3, 11],
        "makequboerror": 11,
        "makequbonotusedbitwarn": 11,
        "makequboroundedtozerowarn": 11,
        "makequbowarn": 11,
        "manag": [2, 5, 6, 20],
        "mandatori": 5,
        "mani": [1, 2, 3, 5, 6, 12, 13, 14, 15, 17, 24],
        "manipul": [3, 12, 24],
        "manual": [14, 16, 19],
        "map": [2, 3, 17],
        "mark": [5, 14],
        "markdown": 5,
        "match": [3, 5, 7],
        "materi": 23,
        "math": 3,
        "math_prod": 11,
        "mathbb": 3,
        "mathcal": [13, 15, 17],
        "mathemat": [6, 16, 17, 18, 20],
        "mathrm": 3,
        "matplotlib": [0, 5, 7, 19],
        "matrix": [3, 17, 18],
        "max": [1, 2, 5, 11],
        "max_abs_coeffici": 3,
        "max_attempt": 6,
        "max_ax_per_row": 7,
        "max_bit": [3, 11],
        "max_fixpoint_iter": 2,
        "max_job_list_retri": 2,
        "max_number_of_bit": 11,
        "max_tab_report": 6,
        "maxcut": 18,
        "maxim": [1, 2, 3, 5, 16, 17, 18, 19],
        "maximum": [0, 1, 2, 3, 5, 6],
        "mb": 6,
        "md5": 3,
        "md5sum": 6,
        "mean": [2, 3, 5, 6, 13, 14, 16],
        "mean_devi": 16,
        "measur": [1, 5, 6],
        "median": 5,
        "mediawiki": 5,
        "member": [0, 3, 5],
        "memori": [6, 24],
        "merg": [0, 3],
        "merge_dict": 11,
        "messag": [3, 6],
        "method": [11, 15, 16, 18, 19, 20],
        "metric": 6,
        "might": [3, 14, 15, 17, 18],
        "min": [1, 2, 5, 11],
        "min_dadk_vers": 0,
        "min_dist": 0,
        "min_energi": 1,
        "min_servic": 6,
        "min_solut": [7, 11, 16],
        "min_step": 1,
        "minim": [0, 3, 5, 6, 7, 11, 13, 14, 15, 16, 17, 18, 19],
        "minima": [1, 2, 3],
        "minimize_work": 16,
        "minimum": [0, 1, 2, 3, 5, 6, 7, 14, 16, 19],
        "minu": 3,
        "minut": 5,
        "miscellan": 11,
        "mismatch": 3,
        "miss": [3, 5],
        "mixin": 2,
        "mlt": 4,
        "mm": 0,
        "mod": 4,
        "mode": [1, 2, 5, 6],
        "model": [1, 2, 3, 6, 16, 17],
        "model_data": 6,
        "modifi": [2, 3, 5, 6],
        "modul": 5,
        "moinmoin": 5,
        "mondai": [13, 14, 15, 16, 17],
        "monomi": 3,
        "monoton": 2,
        "month": 5,
        "more": [2, 3, 5, 6, 14, 15, 16, 17, 18, 20, 23, 24],
        "moreov": [3, 13, 15, 19],
        "most": [0, 3, 13, 16, 24],
        "most_1_bit_on": 11,
        "move": 6,
        "move_to": 11,
        "msg": 5,
        "much": 16,
        "multi": [3, 5, 6, 24],
        "multidimension": [3, 7],
        "multipl": [2, 3, 5, 6, 7, 17, 18],
        "multiple_output": 5,
        "multipli": [1, 2, 5, 11, 15, 18],
        "multiply_scalar": [11, 18],
        "must": [2, 3, 5, 6],
        "my_tabl": 5,
        "mycustomwidget": 5,
        "mycustomwidgetrefer": 5,
        "n": [3, 4, 5, 11, 13, 14, 16, 18, 19, 20],
        "name": [0, 1, 2, 3, 4, 5, 6, 7, 11, 13, 14, 15, 16, 17, 18, 19, 20],
        "name1": 5,
        "name2": 5,
        "name3": 5,
        "name4": 5,
        "name5": 5,
        "name_in_profile_stor": 0,
        "name_title_pair": 5,
        "nan": 0,
        "nbsp": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "ndarrai": [1, 3, 7],
        "necessari": [3, 6, 15, 20],
        "necessarili": 3,
        "need": [0, 1, 3, 5, 6, 7, 15, 17, 18, 20, 23],
        "neg": 0,
        "negat": 18,
        "neglect": 15,
        "neighbor": 2,
        "neighbour": [1, 2],
        "nest": [5, 6, 7],
        "network": [0, 1],
        "networkx": 19,
        "new": [0, 1, 2, 3, 5, 6, 7, 14],
        "new_data": 5,
        "newli": [2, 6],
        "newlin": [0, 5],
        "next": [1, 2, 3, 16, 19, 20],
        "night": [13, 14, 15, 16, 17],
        "no_gener": 6,
        "no_shift_for_workers_left": 14,
        "no_wai": 3,
        "node": 19,
        "node_color": 19,
        "node_s": 19,
        "non": [0, 1, 3, 5],
        "none": [0, 1, 2, 3, 4, 5, 6, 7, 13, 15, 17],
        "normal": [0, 1, 5, 6, 11],
        "notat": 5,
        "note": [1, 2, 3, 6, 13, 14, 15, 16, 17, 19],
        "notebook": [1, 5, 6, 7, 23],
        "noth": [0, 1, 2, 6],
        "notic": 5,
        "notif": 6,
        "notifi": 6,
        "notion": 7,
        "now": [5, 14, 17],
        "np": [1, 3, 13, 14, 15, 16, 17],
        "nr_shifts_of_work": 14,
        "nr_shifts_per_work": [13, 15, 16],
        "nr_worker": 14,
        "num_bin": 3,
        "num_group": [1, 2],
        "num_output_solut": [1, 2],
        "num_solut": [1, 2],
        "number": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19],
        "number_iter": [1, 2, 14, 16, 19],
        "number_of_bit": 1,
        "number_of_hil": 1,
        "number_of_iter": 1,
        "number_of_sampl": 7,
        "number_of_vallei": 1,
        "number_of_walk": 1,
        "number_replica": [1, 2],
        "number_run": [1, 2, 14, 16, 19],
        "numberplot": 11,
        "numbert": 11,
        "numer": [3, 5, 6],
        "numpi": [3, 7, 17],
        "nx": 19,
        "o": 2,
        "ob": [0, 3, 6],
        "obj": [0, 1, 7],
        "object": [0, 1, 2, 3, 4, 5, 6, 7, 14, 16, 17, 18, 19, 20],
        "object_hook": [0, 6],
        "obligatori": 5,
        "observ": 6,
        "obtain": [2, 3, 15, 23],
        "occur": 1,
        "ocean": 3,
        "off": [1, 2, 3, 7, 14],
        "offer": [3, 5, 6],
        "offline_request_fil": 2,
        "offline_response_fil": 2,
        "offset": [1, 2, 3, 7],
        "offset_energi": 2,
        "offset_increase_r": [1, 2, 14, 16, 19],
        "often": [3, 15, 16, 17, 18],
        "ohs_xw1h_internal_penalti": [1, 2],
        "old": [0, 5, 6],
        "older": 2,
        "omit": [3, 5],
        "omit_tab": 5,
        "on_chang": 5,
        "on_change_cod": [5, 11],
        "on_click": 6,
        "onc": [2, 5, 6],
        "one": [0, 1, 2, 3, 5, 6, 7, 14, 15, 16, 17, 18, 19, 20],
        "one_hot": [0, 3, 11, 17],
        "one_hot_group": [3, 13, 15, 16, 17],
        "one_hot_groups_list": [13, 15, 17],
        "one_wai": [3, 17],
        "onehot": [0, 2, 11, 17],
        "onehotgroup": [11, 13, 15, 17],
        "onli": [0, 1, 2, 3, 5, 6, 7, 13, 14, 16, 17, 20, 23],
        "only_parameter_marked_as_calcul": 6,
        "onward": 13,
        "oop": 5,
        "opac": 5,
        "oper": [2, 5, 11],
        "operand": 3,
        "opposit": 5,
        "optim": [1, 2, 3, 5, 8, 9, 10, 11, 12, 14, 16, 17, 19, 20, 21, 22],
        "optimization_method": [1, 2],
        "optimizationmethod": [2, 11],
        "optimizer_model": [5, 6],
        "optimizermodel": [5, 11],
        "optimizermodelcomplex": 11,
        "optimizermodelcomplexsolv": 6,
        "optimizerset": 11,
        "optimizertab": 11,
        "optimizertabrequir": 11,
        "optimum": 6,
        "option": [0, 1, 2, 3, 4, 5, 6, 7],
        "optuna": 11,
        "optuna_data": 6,
        "optuna_dirnam": [6, 11],
        "optuna_on_focu": 6,
        "optuna_requir": 6,
        "optuna_set": 6,
        "optuna_titl": 6,
        "optuna_toolbar_paramet": 6,
        "optunaset": 11,
        "order": [1, 2, 3, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 23],
        "organ": [3, 17],
        "orgtbl": 5,
        "origin": [3, 6, 16, 19, 20],
        "other": [0, 3, 5, 6, 11, 13, 16, 18, 19, 24],
        "otherwis": [0, 1, 3, 4, 5, 6, 7, 15],
        "our": [14, 15, 16, 17, 19],
        "out": [5, 16],
        "outer": 2,
        "output": [0, 1, 2, 3, 4, 5, 6, 7],
        "outputtab": 11,
        "over": [3, 5, 6, 15, 17],
        "overal": [0, 1, 2, 17],
        "overcom": 2,
        "overflow": 3,
        "overflow_coeffici": 3,
        "overflow_indic": 3,
        "overload": [3, 6],
        "overload_print": [5, 6],
        "overrid": [3, 6],
        "overview": [5, 6, 11, 21, 23],
        "overwrit": [2, 6],
        "overwritten": [3, 6],
        "own": [5, 17, 19],
        "p": [3, 11],
        "p_ex": 2,
        "packag": [0, 1],
        "page": 6,
        "paint": 5,
        "pair": [0, 2, 3, 5, 6],
        "panel": 5,
        "paragraph": 3,
        "parallel": [1, 2, 3, 5],
        "parallel_temp": [1, 2],
        "paralleltemperingmanag": 11,
        "param": [4, 6],
        "paramet": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 16, 19],
        "parameter_definit": [5, 6],
        "parameter_log": [5, 6, 11],
        "parameterlog": [6, 11],
        "parametr": 6,
        "pareto": [5, 11],
        "pareto_data": 5,
        "pareto_dirnam": [6, 11],
        "pareto_on_focu": 6,
        "pareto_requir": 6,
        "pareto_set": 6,
        "pareto_titl": 6,
        "pareto_toolbar_paramet": 6,
        "paretoplot": 11,
        "paretoset": 11,
        "pars": [0, 4],
        "parse_poli": 11,
        "parser": 6,
        "part": [1, 2, 3, 5, 7, 13, 16, 17, 19],
        "partialconfig": [2, 11, 14],
        "particular": [0, 2, 3, 6, 13, 14, 15, 17, 20],
        "pass": [3, 5, 6, 7],
        "password": [0, 1],
        "path": [0, 1, 5, 6, 11],
        "path_to": 23,
        "pattern": [3, 5, 6],
        "pdf": 6,
        "penal": 3,
        "penalti": [1, 2, 3, 6, 7, 13],
        "penalty_binary_polynomi": 7,
        "penalty_energi": [7, 11],
        "penalty_factor": 3,
        "penalty_poli": 3,
        "penalty_qubo": [0, 1, 2, 3, 7, 11, 13, 14, 16],
        "penalty_qubo_blob_nam": [0, 1],
        "penalty_qubo_filenam": [0, 1],
        "per": [1, 2, 4, 6, 7, 15, 16, 17],
        "percent": 1,
        "percentag": 1,
        "perform": [6, 7, 15, 16],
        "perman": 5,
        "permiss": 2,
        "persist": [5, 6],
        "persistent_fil": 6,
        "persistent_read_funct": 5,
        "persistent_set": 5,
        "persistent_write_funct": 5,
        "phase": [6, 16],
        "phi": 3,
        "physic": [3, 6],
        "pickl": 6,
        "pip": 23,
        "pipe": 5,
        "pixel": 6,
        "place": [0, 3, 5, 6],
        "plain": [0, 1, 3, 5, 6],
        "platform": 2,
        "pleas": [5, 6],
        "plot": [0, 1, 3, 5, 6, 7, 14, 24],
        "plot_coordin": 0,
        "plot_graph": 1,
        "plot_histogram": 11,
        "plot_rout": 0,
        "plot_temperature_curv": 1,
        "plt": [1, 19],
        "plu": [3, 5, 6],
        "png": [0, 1, 7],
        "po": 19,
        "point": [1, 3, 5, 6, 7, 15],
        "poli": 3,
        "poll": 2,
        "poly_brc": 4,
        "poly_factor": 4,
        "poly_term": 4,
        "polynomi": [1, 2, 5, 6, 7, 11, 12, 15, 18, 22],
        "pop": 14,
        "port": [0, 1],
        "portion": 1,
        "posit": [3, 5],
        "possibl": [0, 1, 2, 3, 5, 6, 7, 12, 14, 15, 16, 17, 21],
        "possibli": [3, 6],
        "potenti": 22,
        "power": [11, 17, 18, 19],
        "power2": [11, 13, 15, 16],
        "practic": 15,
        "pre": 13,
        "preced": 0,
        "precis": [1, 3, 15],
        "prefer": [18, 20],
        "prefix": [0, 1],
        "prep_result": 11,
        "prepar": [2, 5, 6],
        "prepare_cont": 11,
        "prescrib": [3, 17],
        "present": [3, 5, 6, 7],
        "preserv": 5,
        "press": [5, 6],
        "presto": 5,
        "pretti": [0, 4, 5],
        "prettifi": 4,
        "prevent": 0,
        "previou": [3, 13, 14, 15, 16, 19],
        "previous": 3,
        "prf": 0,
        "print": [0, 5, 6, 7, 14, 16, 19],
        "print_progress": 11,
        "print_stat": 11,
        "print_to_output": 11,
        "probability_model": [1, 2],
        "probabilitymodel": 11,
        "probabl": [1, 2, 3, 16],
        "problem": [0, 1, 2, 3, 6, 12, 15, 20, 21, 24],
        "problemat": 13,
        "procedur": [1, 6],
        "process": [1, 2, 3, 5, 6, 7, 14, 16, 19],
        "processor": 24,
        "prodid": 6,
        "produc": [0, 1, 2, 3, 5, 6, 14],
        "product": [0, 1, 6, 11],
        "profil": [0, 1, 5, 6],
        "profile_fil": 6,
        "profileutil": 11,
        "program": [13, 14, 15, 16, 17, 18, 19, 20],
        "progress": [2, 5, 6, 11],
        "progress_hook": 2,
        "progressbar": 11,
        "project": 3,
        "prolog": [0, 1, 11],
        "prolog_blob_nam": [0, 1],
        "prolog_filenam": [0, 1],
        "proof": 21,
        "propag": [5, 6],
        "proper": 5,
        "properti": 11,
        "protocol": [0, 1],
        "prototyp": [5, 6],
        "proven": 15,
        "provid": [0, 2, 3, 5, 6, 7, 12, 13, 17, 19, 20, 22],
        "proxi": [0, 1],
        "proxy_password": [0, 1],
        "proxy_port": [0, 1],
        "proxy_us": [0, 1],
        "pseudocod": 5,
        "psql": [1, 2, 5],
        "pt_replica_exchange_model": [1, 2],
        "pt_replica_exchange_model_far_jump": 2,
        "pt_replica_exchange_model_neighbour": 2,
        "pt_temperature_model": [1, 2],
        "pt_temperature_model_exponenti": 2,
        "pt_temperature_model_hukushima": 2,
        "pt_temperature_model_linear": 2,
        "ptional": 5,
        "purpos": [4, 12, 21],
        "px": 5,
        "pyplot": [7, 19],
        "pyqubo": 24,
        "python": [0, 1, 3, 4, 5, 12, 24],
        "q": 3,
        "qbsolv": 3,
        "qpoli": 3,
        "quadrat": [2, 3, 8, 9, 10, 12, 13, 16, 17, 20, 22],
        "qualifi": [0, 5],
        "qualiti": [6, 21],
        "quantiti": 0,
        "quantum": [3, 12, 21],
        "qubo": [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 20, 22, 24],
        "qubo1": 3,
        "qubo2": 3,
        "qubo2cr": 11,
        "qubo_blob_nam": [0, 1],
        "qubo_filenam": [0, 1],
        "qubo_hash_step_s": 6,
        "qubo_matrix_arrai": 3,
        "qubo_tmp": [13, 15, 16],
        "quboeditor": 11,
        "qubosizeexceedssolvermaxbit": 11,
        "qubosolverbas": [1, 11],
        "qubosolverbasev2": 11,
        "qubosolverbasev3": 11,
        "qubosolvercpu": [11, 13, 14, 16, 19],
        "qubosolverdav3c": 11,
        "qubosolverdav4": 11,
        "qubosolveremul": [1, 11],
        "queue": [0, 1, 2, 5],
        "quick": [1, 2, 12],
        "quicker": 2,
        "quickli": 22,
        "quotient": 2,
        "r": [1, 2, 3],
        "radio": 5,
        "rais": [0, 3, 6],
        "ramp": 21,
        "random": [1, 2, 3, 16, 19],
        "random_se": [2, 14, 16, 19],
        "randomli": [2, 3, 14],
        "randomwalk_count": 2,
        "randomwalk_length": 2,
        "rang": [3, 5, 6, 7, 18, 19, 20],
        "rapid": 5,
        "rapidli": 2,
        "rate": [1, 2],
        "rceil": 3,
        "re": 6,
        "reach": [1, 2, 5],
        "read": [0, 2, 3, 5, 6],
        "read_json_fil": 11,
        "read_onli": [5, 6],
        "read_text_fil": 11,
        "readabl": [2, 4],
        "readonli": [1, 2, 3, 4, 5, 6, 7],
        "real": [6, 17, 21],
        "realiz": [2, 3, 5, 7],
        "recal": [16, 19],
        "receiv": 6,
        "recommend": [1, 2, 3, 16, 23],
        "record": [1, 2, 5, 6],
        "recov": [6, 19],
        "recoveri": 6,
        "rectangl": 3,
        "rectangular": 5,
        "recurs": [0, 6],
        "recursionerror": 0,
        "red": [5, 7],
        "redirect": 5,
        "reduc": [1, 2, 3, 14],
        "reduce_factor": 3,
        "reduce_higher_degre": 11,
        "reduce_higher_degree_to_qubo": 11,
        "reduct": [2, 3],
        "refer": [0, 1, 2, 3, 5, 6, 17],
        "refresh": 2,
        "regener": 6,
        "regist": [0, 2, 5, 8, 9, 10],
        "register_decod": 0,
        "register_encod": 0,
        "register_funct": 0,
        "register_logging_output": 11,
        "register_solv": 11,
        "register_widget_class": 11,
        "registered_funct": 0,
        "registered_nam": [0, 5],
        "registr": 5,
        "registri": [0, 5],
        "regress": 0,
        "regular": [5, 6],
        "rel": [5, 6],
        "relat": [1, 2, 3, 5, 7, 15],
        "relative_directori": [5, 11],
        "relax": 13,
        "remain": [3, 5, 16],
        "rememb": 14,
        "remov": [0, 2, 5, 11],
        "remove_al": 2,
        "remove_all_tab": 11,
        "remove_fil": 11,
        "remove_job_id": 2,
        "remove_obsolet": 2,
        "render": 5,
        "reorgan": 2,
        "repeat": 14,
        "repeatedli": [1, 2],
        "replac": [3, 5, 6],
        "replai": 6,
        "replica": [1, 2],
        "replica_count": 2,
        "replica_exchange_count": 1,
        "replica_exchange_model": 2,
        "replica_furnace_histori": 2,
        "replica_furnace_list": 2,
        "replica_histori": 1,
        "replica_temperature_list": 2,
        "replicaexchangemodel": 11,
        "replicaexchangemodelfarjump": 11,
        "report": [3, 5, 6],
        "report1": 6,
        "report9": 6,
        "report_": 6,
        "report_fact": 11,
        "report_hol": 3,
        "report_on_focus_list": 6,
        "report_prefix": 3,
        "report_requirement_list": 6,
        "report_round_to_zero": 3,
        "report_title_list": 6,
        "report_toolbar_parameter_list": 6,
        "repres": [1, 2, 3, 4, 5, 6, 7, 18, 19],
        "represent": [0, 1, 3, 4, 5, 6, 7, 20],
        "reproduc": 16,
        "request": [0, 1, 2, 3, 5, 6, 14, 17],
        "request_mod": [0, 1],
        "requestmod": [0, 11],
        "requir": [1, 2, 3, 6, 13, 14, 15, 16, 17, 20, 21],
        "reset": [5, 6],
        "resolut": [0, 1],
        "resort": 7,
        "resourceexistserror": 2,
        "respect": [1, 2, 3, 5, 6, 14, 16, 17],
        "respons": [2, 8, 9, 10],
        "rest": [0, 1, 2, 3],
        "restmixin": 11,
        "restor": 11,
        "restrict": [0, 3, 5, 6, 17],
        "restrict_search": 0,
        "restriction_search": 0,
        "result": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 16, 19],
        "result_receiv": 5,
        "resum": 5,
        "retri": 2,
        "retriev": [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 14, 16, 19],
        "return": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 16, 19, 20],
        "rfloor": 3,
        "right": [3, 5, 15, 17, 18],
        "right_axi": 5,
        "right_qubo": [3, 11],
        "root": [0, 1],
        "round": [0, 3, 5],
        "round_to_n": 11,
        "rout": 0,
        "routin": [3, 6, 7],
        "row": [3, 5, 6, 7, 11],
        "row_label": [5, 11],
        "rpoli": 3,
        "rsp": 5,
        "rst": 5,
        "rule": 3,
        "run": [1, 2, 3, 5, 7, 11, 16, 19, 24],
        "sa": [1, 2],
        "safe": 6,
        "sai": 3,
        "salesperson": 24,
        "same": [0, 2, 3, 5, 6, 7, 14, 15, 16, 17, 18, 19],
        "sampl": [1, 2, 3],
        "samplefilt": 11,
        "sampler": 2,
        "sampling_dist": 2,
        "sampling_paramet": [1, 2],
        "sampling_run": [1, 2],
        "samplingbasi": 11,
        "samplingparamet": [2, 11],
        "samplingutil": 2,
        "sas_token": [0, 1],
        "satisfi": [3, 16],
        "save": [2, 3, 5, 6, 15, 17],
        "save_as_hdf5": 11,
        "save_graph": [6, 11],
        "save_graphs_figs": [6, 11],
        "save_tim": [6, 11],
        "save_times_figs": [6, 11],
        "scalar": [3, 13, 14, 15, 16, 18],
        "scale": [1, 2, 5, 7, 11, 16],
        "scaling_act": [1, 2, 16],
        "scaling_bit_precis": [1, 2],
        "scaling_factor": [1, 2],
        "scaling_paramet": [2, 7, 11],
        "scalingact": [2, 11, 16],
        "scalingparamet": [2, 7, 11],
        "scatter": 0,
        "scenario": [6, 17, 21],
        "scheme": 3,
        "scratch": 6,
        "script": [3, 4, 6],
        "sdk": [23, 24],
        "seach": 3,
        "seamlessli": 21,
        "search": [0, 1, 2, 5, 6, 7, 11, 13],
        "search_pattern": 5,
        "search_stats_info": 11,
        "sec": [0, 1, 2, 5],
        "second": [1, 2, 3, 5, 6, 7, 15, 16, 17],
        "section": [3, 13, 14, 15, 16, 17, 19],
        "see": [0, 1, 2, 3, 5, 6, 7, 14, 17, 20, 23],
        "seed": [2, 16, 19],
        "seen": [5, 18],
        "select": [0, 1, 2, 3, 5, 6],
        "select_inst": 11,
        "select_multipl": 5,
        "select_range_slid": 5,
        "select_slid": 5,
        "select_tab": 11,
        "selected_optim": 6,
        "selected_run": 1,
        "selection_set": 5,
        "self": [3, 5, 6],
        "send": [0, 1, 7],
        "sens": 17,
        "sensibl": 0,
        "sent": [0, 1, 3, 5, 6],
        "sep": 5,
        "separ": [0, 2, 13, 15],
        "seper": 6,
        "sequenc": [2, 3],
        "sequenti": [2, 3, 4, 5],
        "seri": [5, 6],
        "serial": [0, 6],
        "server": [0, 1, 5, 8, 9, 10],
        "servic": [0, 1, 2, 5, 7, 8, 9, 10, 12],
        "set": [0, 1, 2, 3, 4, 5, 6, 7, 15, 16, 17, 18, 19, 20, 23],
        "set_bit": [11, 14],
        "set_build_qubo_details_refer": 11,
        "set_calculated_build_qubo_paramet": 11,
        "set_calculated_solver_paramet": 11,
        "set_clone_warning_count": 11,
        "set_compose_model_details_refer": 11,
        "set_directori": 11,
        "set_dis": 11,
        "set_fixed_solver_paramet": 11,
        "set_load_details_refer": 11,
        "set_persistent_set": 11,
        "set_persistent_tag": 5,
        "set_prep_result_details_refer": 11,
        "set_solver_paramet": 11,
        "set_term": 11,
        "set_titl": 11,
        "set_valu": 11,
        "set_vari": 11,
        "set_vis": 11,
        "setannealerprofil": [0, 1, 11],
        "setminu": [18, 19],
        "setter": 5,
        "setup": 6,
        "sever": [2, 6, 15, 16, 17],
        "shallow": 0,
        "shape": [3, 5, 7, 11, 13, 14, 15, 17, 18, 19, 20],
        "sheet": 6,
        "shift": [14, 15, 16, 17],
        "shifts_of_work": [14, 16],
        "shirt": 5,
        "short": 5,
        "shorter": [3, 5],
        "should": [0, 1, 2, 3, 4, 5, 6, 7, 15, 17, 18, 20],
        "show": [3, 5, 6, 7, 14, 19, 21],
        "show_build": 6,
        "show_compos": 6,
        "show_config": 6,
        "show_job_id": 2,
        "show_load": 6,
        "show_optuna": 6,
        "show_pareto": 6,
        "show_report": 6,
        "show_solv": 6,
        "show_summari": [6, 11],
        "show_test": 6,
        "show_timeseri": 6,
        "show_track": 6,
        "show_visu": 6,
        "shown": [1, 5, 6, 7, 21],
        "showsolverparamet": 11,
        "side": [3, 5, 6, 13, 17, 20],
        "sign": [3, 11],
        "signal": [3, 6],
        "signatur": 2,
        "signific": 0,
        "silent": [0, 5, 11],
        "similar": [2, 3, 5, 6, 16, 17, 19],
        "similarli": [3, 6, 15],
        "simpl": [0, 1, 2, 3, 4, 5, 6, 14],
        "simple_verbos": [0, 1],
        "simpli": [0, 3, 7],
        "simplifi": 15,
        "simul": [16, 19],
        "simultan": 6,
        "sinc": [3, 5, 14, 15, 16, 18, 19, 20],
        "singl": [0, 1, 2, 3, 5, 6, 7, 15, 16, 17],
        "situat": 2,
        "size": [0, 1, 2, 3, 5, 6, 7, 11, 14, 16],
        "skip": 0,
        "skipkei": 0,
        "slack": [3, 7],
        "slack_nam": 4,
        "slack_typ": 3,
        "slacktyp": [7, 11],
        "sleep": 2,
        "sleep_dur": 2,
        "sleep_funct": 2,
        "slider": 5,
        "slow": [14, 16],
        "smaller": [2, 3, 15],
        "smallest": 3,
        "so": [0, 2, 3, 5, 6, 12, 15, 17],
        "softwar": [21, 23],
        "sol_i": 16,
        "sol_x": 16,
        "sole": 17,
        "solut": [1, 2, 3, 6, 8, 9, 10, 11, 14, 18, 21],
        "solution_list": [14, 16, 19],
        "solution_mod": [1, 2],
        "solution_solutionlist": [2, 3, 7],
        "solutionlist": [2, 11, 16, 19],
        "solutionmod": [2, 11],
        "solv": [1, 2, 5, 6, 12, 21, 22],
        "solve_dav2": [6, 11],
        "solve_gui": [6, 11],
        "solve_on_focu": 6,
        "solve_paramet": 6,
        "solve_prep_result": 6,
        "solve_processor": 6,
        "solve_requir": 6,
        "solve_templ": 6,
        "solve_titl": 6,
        "solve_toolbar_paramet": 6,
        "solvedav2set": 11,
        "solveguiset": 11,
        "solver": [0, 1, 3, 5, 6, 7, 11, 12, 13, 21],
        "solver_class": 2,
        "solver_id": 2,
        "solver_paramet": 6,
        "solver_tim": [7, 11],
        "solverbas": 11,
        "solverfactori": 11,
        "solvertim": [7, 11],
        "some": [3, 6, 14, 15, 16, 17, 24],
        "sometim": 18,
        "sort": [0, 3, 7, 11, 14, 17],
        "sort_column": 5,
        "sort_control": 5,
        "sort_direct": 5,
        "sort_kei": 0,
        "sorted_work": 14,
        "sourc": [4, 5, 6, 11],
        "space": [1, 2, 5, 7, 11, 13],
        "span": 6,
        "special": [3, 5, 6, 12, 17, 21],
        "special_cas": 0,
        "specif": [0, 1, 2, 3, 5, 6, 12, 17],
        "specifi": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 17],
        "speed": 24,
        "spinner": 5,
        "split": [17, 18, 19],
        "spread": [13, 15],
        "sqrt": 3,
        "squar": 3,
        "ssl": [0, 1],
        "ssl_disable_warn": [0, 1],
        "ssl_verifi": [0, 1],
        "stai": 3,
        "stamp": 2,
        "stand": [1, 3, 7],
        "standard": 7,
        "start": [0, 1, 2, 3, 5, 6, 7, 11, 12, 14],
        "start_account_occupi": 1,
        "start_ann": 1,
        "start_cpu": 1,
        "start_dau_servic": 1,
        "start_elaps": 1,
        "start_execut": 1,
        "start_index": 3,
        "start_occupi": 1,
        "start_parse_respons": 1,
        "start_prepare_qubo": 1,
        "start_prepare_request": 1,
        "start_queu": 1,
        "start_receive_respons": 1,
        "start_reduce_degre": 1,
        "start_sampl": 1,
        "start_save_request": 1,
        "start_save_respons": 1,
        "start_scal": 1,
        "start_send_request": 1,
        "start_solv": 1,
        "start_stat": 1,
        "start_stop": 3,
        "start_tim": 2,
        "start_wait": 1,
        "start_waiting_2_finish": 1,
        "start_waiting_2_start": 1,
        "state": [1, 2, 3, 5, 7, 13, 14, 15, 16],
        "statement": [3, 5, 6, 20],
        "static": 5,
        "staticmethod": 11,
        "statist": [2, 7],
        "stats_info": 7,
        "statu": [2, 5, 6, 8, 9, 10],
        "status_output": 6,
        "stdout": 5,
        "stem": 7,
        "step": [1, 2, 3, 5, 6, 7, 11, 16, 19],
        "stepwis": [2, 16, 19],
        "still": [2, 5, 14],
        "sting": 3,
        "stochast": [1, 2],
        "stop": [3, 5, 7, 11],
        "stop_replay_on_error": 6,
        "stopwatch": 5,
        "storag": [0, 1, 2],
        "storage_account_nam": [0, 1],
        "storage_account_typ": [0, 1],
        "storage_connection_str": [0, 1],
        "storageaccounttyp": [0, 11],
        "store": [0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 19],
        "store_annealer_access_profil": 0,
        "store_graph": [6, 11],
        "str": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        "strategi": 2,
        "stream": [0, 1, 5],
        "stream_verbos": [0, 1],
        "string": [0, 2, 3, 4, 5, 6, 20],
        "string_2_configur": 11,
        "structur": [0, 1, 2, 5, 6, 7, 11, 22, 24],
        "studi": [6, 23],
        "style": 5,
        "sub": [0, 1, 2, 4, 5, 7, 14, 18, 19],
        "sub_id": 0,
        "subdirectori": 6,
        "subplot": 7,
        "subplotpar": 7,
        "subplotparam": 7,
        "subsequ": [5, 20],
        "subset": [5, 17, 18, 19],
        "subspac": 3,
        "substitut": 3,
        "success": [2, 5, 6, 7],
        "suffici": [1, 2, 15],
        "suffix": 5,
        "suggest": 15,
        "sum": [4, 5, 6, 11, 13, 15, 16],
        "sum_": [3, 4, 13, 15, 18, 20],
        "sumar": 3,
        "summar": 16,
        "summari": [3, 5, 6],
        "summary_column": [6, 11],
        "summary_definit": 5,
        "summat": 3,
        "support": [2, 3, 5, 6, 13, 14, 15, 17, 19, 20],
        "suppos": [13, 17],
        "suppress": 5,
        "swap": 2,
        "switch": [1, 2, 3, 6],
        "sy": 5,
        "symbol": [3, 5, 17],
        "symmetr": [3, 19],
        "system": [0, 5, 6],
        "t": [0, 1, 2, 5],
        "t_i": 2,
        "t_j": 2,
        "tab": 11,
        "tab_build": [6, 11],
        "tab_compos": [6, 11],
        "tab_load": [6, 11],
        "tab_nam": 5,
        "tab_optuna": [6, 11],
        "tab_out": [5, 11],
        "tab_pareto": [6, 11],
        "tab_report": 11,
        "tab_solv": [6, 11],
        "tab_timeseri": [6, 11],
        "tab_visu": [6, 11],
        "tab_width_perc": 5,
        "tabl": [1, 2, 3, 5, 6, 7],
        "tablefmt": [1, 2, 5],
        "tabul": [1, 2, 3, 5],
        "tag": [5, 6],
        "tag_nam": 6,
        "taggedguicontrol": 11,
        "take": [0, 2, 3, 5, 6, 13, 17, 20],
        "take_tim": 11,
        "taken": [2, 3, 5, 7],
        "tar": 23,
        "target": [0, 1, 2, 6],
        "target_directory_manag": 6,
        "target_energi": [1, 2],
        "target_valu": 3,
        "task": [6, 7, 17, 20],
        "technic": 3,
        "techniqu": 18,
        "technologi": [12, 21],
        "temper": [1, 2, 3],
        "temperatur": [1, 2, 14, 16, 19],
        "temperature_adjustment_count": 2,
        "temperature_decai": [1, 2],
        "temperature_end": [1, 2, 14],
        "temperature_high": 2,
        "temperature_interv": [1, 2, 14, 16, 19],
        "temperature_list": 2,
        "temperature_low": 2,
        "temperature_mod": [1, 2],
        "temperature_model": 2,
        "temperature_monotonicity_check": 2,
        "temperature_sampl": [1, 2, 14, 16, 19],
        "temperature_start": [1, 2, 6, 14],
        "temperaturemodel": 11,
        "temperaturemodelexponenti": 11,
        "temperaturemodelhukushima": 11,
        "temperaturemodellinear": 11,
        "templat": 6,
        "temporarili": [2, 16, 19],
        "ten": 19,
        "term": [4, 5, 6, 11, 15, 17, 18, 20],
        "termin": [1, 2, 3, 6],
        "test": [0, 5, 6, 14, 16, 23, 24],
        "text": [4, 5, 6, 17, 18],
        "text_area": 5,
        "text_cont": 11,
        "text_width": 5,
        "textbf": 4,
        "textil": 5,
        "than": [1, 2, 3, 5, 13, 14, 15, 16, 18, 20],
        "thei": [3, 5, 6],
        "them": [5, 15],
        "therefor": [2, 3, 5, 16],
        "thi": [0, 1, 2, 3, 4, 5, 6, 7, 13, 14, 15, 16, 17, 18, 19, 20, 22],
        "thing": 6,
        "third": [3, 5, 6, 17],
        "those": [0, 2, 3, 5, 6, 13, 15, 17],
        "though": 3,
        "thread": [2, 5, 6],
        "three": [2, 5, 6, 16, 17, 18],
        "threshold": [1, 2, 3],
        "through": 6,
        "thrown": [2, 3],
        "thursdai": [13, 14, 15, 16, 17],
        "tick": 7,
        "tile": 6,
        "time": [0, 1, 2, 3, 5, 6, 7, 11, 14, 15, 17, 21],
        "time_limit_sec": [1, 2, 6],
        "time_pick": 5,
        "time_seri": 6,
        "time_series_set": 6,
        "time_track": 6,
        "timedelta": [1, 2, 5],
        "timeout": [0, 1],
        "timepick": 11,
        "timereport": 11,
        "times_figs": [6, 11],
        "timeseri": 11,
        "timeseries_dirnam": [6, 11],
        "timeseries_on_focu": 6,
        "timeseries_plot": 6,
        "timeseries_requir": 6,
        "timeseries_set": 6,
        "timeseries_t": 6,
        "timeseries_titl": 6,
        "timeseries_toolbar_paramet": 6,
        "timeseriesset": 11,
        "timestamp": [1, 6],
        "timetrack": 11,
        "titl": [3, 5, 6, 7],
        "tmp_qubo": [13, 14, 16, 18, 19],
        "to_qpoli": 11,
        "todai": 21,
        "togeth": [15, 18],
        "token": [1, 2],
        "too": [3, 14, 15],
        "tool": [5, 22, 24],
        "toolbar": 6,
        "tooltip": 5,
        "top": [1, 6],
        "topic": [5, 17],
        "total": [1, 2, 6, 7],
        "traceabl": 19,
        "track": [2, 5, 6],
        "track_on_focu": 6,
        "track_requir": 6,
        "track_titl": 6,
        "track_toolbar_paramet": 6,
        "tracker": [5, 6],
        "transfer": 2,
        "transform": [0, 5, 19],
        "translat": [0, 6, 7],
        "trap": 14,
        "trash": 5,
        "travel": [0, 24],
        "travers": 5,
        "traverse2plot": 11,
        "traverseplot": 11,
        "treat": 3,
        "tree": [0, 4, 11],
        "tri": 6,
        "trial": 6,
        "triangl": 14,
        "triangular": 3,
        "trigger": [3, 5, 6, 18],
        "trigger_change_handl": 11,
        "true": [0, 1, 2, 3, 5, 6, 7, 14, 16, 19],
        "truncat": 7,
        "try": [6, 14],
        "tsp": 0,
        "tsputil": 11,
        "tsv": 5,
        "tuesdai": [13, 14, 15, 16, 17],
        "tui": 2,
        "tune": 6,
        "tupel": 5,
        "tupl": [0, 1, 3, 5, 6, 7, 8, 9, 10],
        "turn": 3,
        "tutori": 6,
        "two": [2, 3, 4, 5, 6, 16, 17, 18, 19],
        "two_hot": 3,
        "two_line_cr": 5,
        "two_wai": [0, 3],
        "twosideinequ": 11,
        "typ": 7,
        "type": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 19, 20],
        "type_convert": 5,
        "typeerror": [0, 3],
        "typic": [3, 6, 7, 20],
        "u": 23,
        "unari": [3, 4],
        "unconstrain": [3, 8, 9, 10, 12, 13, 17, 20, 22],
        "under": [0, 3, 5, 6],
        "underli": [3, 6, 7],
        "underpin": 24,
        "unintend": 3,
        "union": [0, 1, 2, 3, 5, 6, 7],
        "uniqu": [3, 6, 17],
        "unit": 21,
        "unknown": 2,
        "unpack": 17,
        "unravel": 7,
        "unsafehtml": 5,
        "unsort": 7,
        "until": [1, 2, 3, 5],
        "unus": 3,
        "up": [2, 3, 5, 6, 13, 15, 23, 24],
        "updat": [0, 1, 2, 5, 6],
        "update_annealer_access_profil": 0,
        "update_cont": 11,
        "update_gui": 5,
        "update_last_set": 11,
        "upload": 2,
        "upload_blob": 2,
        "upon": [5, 6],
        "upper": [1, 2, 3, 5, 6, 14],
        "upper_limit": [3, 11],
        "upper_shift_limit": [13, 14, 16],
        "upper_triangular": 3,
        "url": [2, 6, 11],
        "us": [0, 1, 2, 3, 4, 5, 6, 7, 12, 15, 16, 17, 18, 19, 20, 23, 24],
        "usag": [0, 3, 5, 6, 14, 16, 17, 20, 24],
        "use_access_profil": 5,
        "use_compact": 3,
        "use_default_compose_model": 6,
        "user": [0, 1, 2, 3, 5, 6, 7, 17],
        "usual": [5, 7, 16],
        "util": [2, 11, 22],
        "v": [18, 19],
        "v3": 3,
        "v3c": 2,
        "v4": 2,
        "valid": [2, 3, 4, 5, 12, 14],
        "vallei": 1,
        "valley_energi": 1,
        "valley_step": 1,
        "valu": [0, 1, 2, 3, 5, 6, 7, 11, 13, 14, 16, 17],
        "valueerror": [0, 3],
        "values_per_row": [6, 11],
        "valuewidget": 6,
        "var": 3,
        "var_arrai": 4,
        "var_attribut": 6,
        "var_def": 3,
        "var_fil": 5,
        "var_imp": 4,
        "var_nam": [4, 5],
        "var_object": 6,
        "var_shape_set": [3, 7, 11, 13, 14, 15, 16, 17, 18, 19, 20],
        "var_valu": [5, 6],
        "vari": [6, 17],
        "variabl": [2, 4, 5, 7, 11, 13, 14, 15, 16, 22, 24],
        "variablebase10": 11,
        "variablebinari": 11,
        "variablefibonacci": 11,
        "variablelittlegauss": 11,
        "variablesequenti": 11,
        "variableunari": 11,
        "variat": [3, 16],
        "variou": [7, 24],
        "varset": 11,
        "varshapeset": [0, 7, 11, 13, 14, 15, 16, 18, 19, 20],
        "varslack": 11,
        "vbox": 11,
        "vector": [3, 7],
        "venv": 23,
        "verbos": [0, 1, 2, 3, 5],
        "veri": [0, 1, 2, 6, 13, 14, 16, 17, 20, 21],
        "verif": [0, 1],
        "verifi": 14,
        "version": [0, 2, 3, 5, 13, 21, 23, 24],
        "vertex": 18,
        "vertic": [18, 19],
        "via": [0, 1, 2, 3, 6, 17],
        "view": [1, 2, 3, 15],
        "viewport": 1,
        "violat": [3, 13, 16],
        "virtual": 23,
        "visibl": [5, 6, 11],
        "visit": [0, 1],
        "visual": [1, 3, 5, 6, 7],
        "visualis": 5,
        "visualization_on_focu": 6,
        "visualization_requir": 6,
        "visualization_titl": 6,
        "visualization_toolbar_paramet": 6,
        "w": [1, 2, 4, 13, 14, 15, 16, 17],
        "w_1": 3,
        "w_2": 3,
        "w_coef": 3,
        "w_i": 3,
        "w_irow": 3,
        "w_jcol": 3,
        "w_n": 3,
        "wa": [1, 2, 3, 5, 6, 7, 14, 16, 18, 19, 21],
        "wai": [1, 3, 7, 13, 15, 17, 19, 22],
        "wait": [1, 2],
        "wait_cycl": 1,
        "walk": [2, 16, 19],
        "walker": 1,
        "want": [2, 6, 17],
        "warn": [0, 1, 5, 11],
        "watch": 5,
        "wave": 24,
        "we": [0, 1, 3, 5, 13, 14, 15, 16, 17, 18, 19, 20, 23],
        "web": [0, 1, 2, 5],
        "webapi": 3,
        "wednesdai": [13, 14, 15, 16, 17],
        "week": [16, 17],
        "weight": [0, 1, 2, 3, 13, 14, 15, 16],
        "welcom": 12,
        "well": [3, 4, 5, 6, 7, 15, 16, 18, 19],
        "were": [3, 6, 24],
        "what": 17,
        "when": [1, 2, 3, 5, 6, 16, 17],
        "whenev": [6, 14, 16, 17],
        "where": [0, 1, 2, 3, 5, 6, 7, 15, 17, 18, 20],
        "whether": [1, 2, 5],
        "which": [0, 1, 2, 3, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23],
        "while": [2, 3, 5, 6, 14, 17],
        "whitespac": 0,
        "who": 1,
        "whole": [3, 6],
        "why": 22,
        "widget": [5, 6],
        "widget_arg": 5,
        "widget_class": 5,
        "widget_gui": 6,
        "widget_in_and_out": 5,
        "widget_nam": [5, 6],
        "widgetboundedfloatarrai": 11,
        "widgetboundedfloatrang": 11,
        "widgetboundedintarrai": 11,
        "widgetboundedintrang": 11,
        "widgetboundednumberarrai": 11,
        "widgetdefinit": [2, 6, 11],
        "widgetfloatarrai": 11,
        "widgetfloatrang": 11,
        "widgetgui": [6, 11],
        "widgetintarrai": 11,
        "widgetintrang": 11,
        "widgetnumberarrai": 11,
        "widgettab": 11,
        "width": [1, 3, 5, 6],
        "width_in_perc": 1,
        "width_in_step": 1,
        "wigdet": 5,
        "wigget": 6,
        "window": [0, 23],
        "wise": [5, 6],
        "within": [3, 5, 6, 7, 15, 16, 20],
        "without": [0, 1, 2, 3, 5, 6, 16],
        "work": [0, 1, 2, 3, 5, 6, 13, 14, 15, 16, 17, 19, 23],
        "worker": [14, 15, 16, 17],
        "workers_in_f": 14,
        "world": 21,
        "worsen": 1,
        "worth": 14,
        "would": [0, 3, 6, 16],
        "wrapper": [5, 6, 7],
        "wrapper_funct": 5,
        "write": [2, 5, 6],
        "write_json_fil": 11,
        "write_text_fil": 11,
        "written": [6, 18],
        "wrong": 3,
        "x": [0, 1, 3, 4, 5, 13, 14, 15, 16, 17, 18, 19, 20],
        "x_": [13, 15, 17, 18, 20],
        "x_0": 3,
        "x_1": 3,
        "x_2": 3,
        "x_3": 3,
        "x_7": 3,
        "x_bit_array_shap": [13, 14, 15, 16, 17, 18, 19, 20],
        "x_bit_array_shape_list": 17,
        "x_column": 5,
        "x_i": [3, 18, 20],
        "xeon": 24,
        "xlim": 0,
        "xlsx": 6,
        "y": [0, 3, 5, 13, 14, 15, 16, 17],
        "y_": [3, 15, 17],
        "y_bit_array_shap": [13, 14, 15, 16, 17],
        "y_colum": 5,
        "y_column": 5,
        "y_i": 3,
        "y_l": 3,
        "y_w": 15,
        "yaml": 0,
        "yconstraint": 3,
        "year": 5,
        "yellow": 5,
        "yield": 3,
        "ylim": 0,
        "you": [0, 2, 5, 6, 12, 14, 17, 18, 19, 20, 22, 23],
        "your": 5,
        "youtrack": 5,
        "yyyi": 0,
        "z": [3, 4, 5],
        "z_": 4,
        "za": 4,
        "zero": [1, 2, 3, 5, 13, 14, 17]
    },
    "titles": ["Utils", "Internal", "Solvers", "Polynomials", "Generate Polynomials and Description", "JupyterTools", "Optimizer", "Optimization Solutions", "QUBOServiceCPU", "QUBOServiceDAv3c", "QUBOServiceDAv4", "Digital Annealer Development Kit", "Digital Annealer Development Kit", "Constraints", "Guidance Configuration", "Quadratic Unconstrained Optimization", "Solving Constrained QUBOs", "Structured Variables", "Quadratic Unconstrained Optimization", "Solving QUBOs", "Structured Variables", "DADK and Fujitsu\u2019s Digital Annealer", "User Guide", "Installation", "Why DADK"],
    "titleterms": {
        "": 21,
        "1": 15,
        "One": 17,
        "The": 3,
        "_set": 6,
        "abc": [2, 3],
        "activate_output": 5,
        "add": [3, 5],
        "add_exactly_1_bit_on": 3,
        "add_exactly_n_bits_on": 3,
        "add_figur": 5,
        "add_json_lin": 3,
        "add_most_1_bit_on": 3,
        "add_slack_penalti": 3,
        "add_slack_vari": 3,
        "add_tab": 5,
        "add_term": 3,
        "add_vari": 3,
        "add_variable_penalti": 3,
        "advanc": 22,
        "analyz": [16, 19],
        "anneal": [11, 12, 14, 21],
        "annealinglogset": 6,
        "api": 12,
        "approach": 14,
        "as_bqm": 3,
        "as_hbsolv": 3,
        "as_json": 3,
        "as_latex": 3,
        "as_qbsolv": 3,
        "as_text": 3,
        "azureblobmixin": 2,
        "basic": 22,
        "batchset": 6,
        "binari": [3, 20],
        "binpol": 3,
        "binpolgen": 4,
        "bit": [3, 15, 17],
        "bitarrai": 7,
        "bitarrayshap": 3,
        "blob": 2,
        "boltzmannsampl": 2,
        "bqp": 3,
        "build_and_solv": 6,
        "build_qubo": 6,
        "calculate_hash": 3,
        "call_funct": 5,
        "categori": 3,
        "check_da_paramet": 2,
        "choos": [16, 19],
        "classmethod": [0, 1, 2, 3, 7],
        "cleanupazureblobstorag": 5,
        "cleanupjob": 5,
        "clone": 3,
        "close": 5,
        "comparison": 14,
        "composelogset": 6,
        "composeset": 6,
        "comput": 3,
        "configur": 14,
        "configuration_2_str": 0,
        "connectionparamet": 1,
        "connectionparameterwithazureblob": 1,
        "constant": 17,
        "constrain": 16,
        "constraint": [13, 15],
        "contact": 12,
        "contain": 2,
        "content": 11,
        "convert_to_binari": 3,
        "coordinatesutil": 0,
        "copy_to": 6,
        "count_fil": 6,
        "creat": 6,
        "create_batch": 6,
        "create_random_st": 3,
        "create_rpoly_hd5": 3,
        "create_rpoly_script": 3,
        "create_solv": 2,
        "crs2inequ": 3,
        "crs2qubo": 3,
        "csv_content": 5,
        "dadk": [21, 23, 24],
        "dadk_version_check": 0,
        "daodecod": 0,
        "daoencod": 0,
        "datetimepick": 5,
        "dav2paramet": 1,
        "dav3cparamet": 1,
        "dav3paramet": 1,
        "dav4paramet": 1,
        "deactivate_output": 5,
        "debug_except": 5,
        "decod": 7,
        "defin": 3,
        "delete_job": [8, 9, 10],
        "deprec": 3,
        "descript": 4,
        "determin": 14,
        "develop": [11, 12],
        "dict": 5,
        "digit": [11, 12, 21],
        "directorymanag": 6,
        "directoryselector": 5,
        "displai": 5,
        "display_appli": 5,
        "display_cont": 5,
        "display_control": 5,
        "display_cr": 5,
        "display_graph": 7,
        "draw": 7,
        "dropdown": 5,
        "element": 4,
        "encod": 7,
        "entri": 7,
        "enum": [1, 3, 5, 6],
        "evalu": [3, 7],
        "exactli": 15,
        "exactly_1_bit_on": 3,
        "exactly_n_bits_on": 3,
        "exampl": [4, 13, 17, 18],
        "except": 3,
        "experiment": 3,
        "extract_bit_arrai": 7,
        "file_size_info": 2,
        "fileselector": 5,
        "final": 15,
        "formul": [16, 19],
        "freeze_var_shape_set": 3,
        "from_qpoli": 3,
        "fujitsu": 21,
        "function": [0, 1, 2, 3, 5, 8, 9, 10],
        "gen_latex": 4,
        "gen_pretty_str": 4,
        "gen_python": 4,
        "gener": 4,
        "generate_penalty_polynomi": 3,
        "generate_slack_polynomi": 3,
        "get": [3, 22],
        "get_active_paramet": 5,
        "get_bit": 3,
        "get_bits_of_1w1h_group": 3,
        "get_bits_of_2w1h_group": 3,
        "get_default_set": 5,
        "get_false_bit_indic": 3,
        "get_free_bit_indic": 3,
        "get_index": 3,
        "get_job": [8, 9, 10],
        "get_json": 3,
        "get_json_gener": 3,
        "get_max_abs_coeffici": 3,
        "get_minimum_energy_solut": 7,
        "get_normalize_factor": 3,
        "get_not_false_bit_indic": 3,
        "get_parameter_definit": 2,
        "get_persistent_set": 5,
        "get_result": 6,
        "get_scale_factor": 3,
        "get_solution_list": 7,
        "get_solver_id": 2,
        "get_sorted_solution_list": 7,
        "get_summari": 5,
        "get_symbol": 3,
        "get_tim": [6, 7],
        "get_true_bit_indic": 3,
        "get_valu": 5,
        "get_weight": 3,
        "get_widget": [5, 6],
        "grammar": 4,
        "graphicsdata": 1,
        "graphicsdetail": 1,
        "greedi": 14,
        "group": 17,
        "gui_build": 6,
        "gui_default_valu": 5,
        "gui_reset_valu": 5,
        "gui_save_valu": 5,
        "gui_solv": 6,
        "guid": [12, 22],
        "guidanc": 14,
        "has_solv": 2,
        "hbox": [5, 6],
        "healthcheck": [8, 9, 10],
        "hobo2qubo": [8, 9, 10],
        "hot": 17,
        "i18n_get": 0,
        "index": 12,
        "inequ": [3, 13],
        "inequalities2cr": 3,
        "inequalitysign": 3,
        "inflate_clamped_configur": 3,
        "instal": 23,
        "intern": 1,
        "interpret": [16, 19],
        "interruptkei": 6,
        "is_empti": 3,
        "isingpol": 3,
        "jobstatu": 2,
        "jsonencod": 0,
        "jsontool": 0,
        "jupytertool": 5,
        "kit": [11, 12],
        "lark": 4,
        "list": 7,
        "list_job": [8, 9, 10],
        "load": 6,
        "load_connection_paramet": 1,
        "load_hdf5": 3,
        "load_json": 3,
        "load_librari": 0,
        "load_qbsolv": 3,
        "localminimasampl": 2,
        "log": 6,
        "log_data": 6,
        "log_msg": 5,
        "make_clamp": 3,
        "make_qubo": 3,
        "makequboerror": 3,
        "makequbonotusedbitwarn": 3,
        "makequboroundedtozerowarn": 3,
        "makequbowarn": 3,
        "math_prod": 0,
        "max_number_of_bit": 2,
        "merge_dict": 0,
        "method": [0, 1, 2, 3, 4, 5, 6, 7],
        "minim": [2, 8, 9, 10],
        "miscellan": 3,
        "most": 15,
        "most_1_bit_on": 3,
        "move_to": 6,
        "multipli": 3,
        "multiply_scalar": 3,
        "n": 15,
        "normal": 3,
        "numberplot": 5,
        "numbert": 5,
        "object": [13, 15],
        "onehot": 3,
        "onehotgroup": 3,
        "oper": 3,
        "optim": [6, 7, 13, 15, 18],
        "optimizationmethod": 1,
        "optimizermodel": 6,
        "optimizermodelcomplex": 6,
        "optimizerset": 6,
        "optimizertab": 6,
        "optimizertabrequir": 6,
        "optuna": 6,
        "optunaset": 6,
        "other": [2, 15],
        "outputtab": 5,
        "overview": 2,
        "paralleltemperingmanag": 2,
        "parameterlog": 5,
        "pareto": 6,
        "paretoplot": 5,
        "paretoset": 6,
        "parse_poli": 4,
        "partialconfig": 3,
        "per": 13,
        "perform": 24,
        "plot_histogram": 3,
        "polynomi": [3, 4, 20],
        "power": 3,
        "power2": 3,
        "prep_result": 6,
        "prepare_cont": 5,
        "print_progress": 7,
        "print_stat": 7,
        "print_to_output": 5,
        "probabilitymodel": 1,
        "problem": [13, 14, 16, 17, 18, 19],
        "product": 3,
        "profileutil": 0,
        "progress": 7,
        "progressbar": 2,
        "prolog": 5,
        "properti": [1, 2, 3, 4, 5, 6, 7],
        "python": 23,
        "quadrat": [15, 18],
        "qubo": [15, 16, 18, 19],
        "qubo2cr": 3,
        "quboeditor": 5,
        "quboservicecpu": 8,
        "quboservicedav3c": 9,
        "quboservicedav4": 10,
        "qubosizeexceedssolvermaxbit": 3,
        "qubosolverbas": 2,
        "qubosolverbasev2": 2,
        "qubosolverbasev3": 2,
        "qubosolvercpu": 2,
        "qubosolverdav3c": 2,
        "qubosolverdav4": 2,
        "qubosolveremul": 2,
        "read_json_fil": 6,
        "read_text_fil": 6,
        "reduce_higher_degre": 3,
        "reduce_higher_degree_to_qubo": 3,
        "register_logging_output": 5,
        "register_solv": 2,
        "register_widget_class": 5,
        "remov": 6,
        "remove_all_tab": 5,
        "remove_fil": 6,
        "replicaexchangemodel": 2,
        "replicaexchangemodelfarjump": 2,
        "report_fact": 5,
        "requestmod": 1,
        "restmixin": 2,
        "restor": 3,
        "round_to_n": 0,
        "run": 6,
        "samplefilt": 3,
        "samplingbasi": 2,
        "samplingparamet": 1,
        "save_as_hdf5": 3,
        "scale": 3,
        "scalingact": 1,
        "scalingparamet": 1,
        "search": 3,
        "search_stats_info": 7,
        "select_inst": 6,
        "select_tab": 5,
        "set": 14,
        "set_bit": 3,
        "set_build_qubo_details_refer": 6,
        "set_calculated_build_qubo_paramet": 6,
        "set_calculated_solver_paramet": 6,
        "set_clone_warning_count": 3,
        "set_compose_model_details_refer": 6,
        "set_directori": 5,
        "set_dis": [5, 6],
        "set_fixed_solver_paramet": 6,
        "set_load_details_refer": 6,
        "set_persistent_set": 5,
        "set_prep_result_details_refer": 6,
        "set_solver_paramet": 6,
        "set_term": 3,
        "set_titl": 5,
        "set_valu": 5,
        "set_vari": 3,
        "set_vis": 5,
        "setannealerprofil": 5,
        "shift": 13,
        "showsolverparamet": 5,
        "slacktyp": 3,
        "solut": [7, 16, 19],
        "solutionlist": 7,
        "solutionmod": 1,
        "solv": [14, 16, 19],
        "solvedav2set": 6,
        "solveguiset": 6,
        "solver": [2, 14, 16, 19],
        "solverbas": 2,
        "solverfactori": 2,
        "solvertim": 1,
        "sort": 5,
        "space": 3,
        "start": [4, 22],
        "staticmethod": 5,
        "stop": 6,
        "storageaccounttyp": 1,
        "string_2_configur": 0,
        "structur": [3, 17, 20],
        "sum": 3,
        "support": 23,
        "syntax": 4,
        "system": 23,
        "tab": [5, 6],
        "tab_report": 6,
        "taggedguicontrol": 5,
        "take_tim": 5,
        "temperaturemodel": 2,
        "temperaturemodelexponenti": 2,
        "temperaturemodelhukushima": 2,
        "temperaturemodellinear": 2,
        "term": 3,
        "text_cont": 5,
        "timepick": 5,
        "timereport": 5,
        "timeseri": 6,
        "timeseriesset": 6,
        "timetrack": 5,
        "to_qpoli": 3,
        "traverse2plot": 5,
        "traverseplot": 5,
        "trigger_change_handl": 5,
        "tsputil": 0,
        "tutori": 23,
        "twosideinequ": 3,
        "unconstrain": [15, 18],
        "update_cont": 5,
        "update_last_set": 5,
        "us": 14,
        "usag": 22,
        "user": [12, 22],
        "util": 0,
        "v2": 21,
        "v3": 21,
        "variabl": [3, 17, 18, 20],
        "variablebase10": 3,
        "variablebinari": 3,
        "variablefibonacci": 3,
        "variablelittlegauss": 3,
        "variablesequenti": 3,
        "variableunari": 3,
        "varset": 7,
        "varshapeset": [3, 17],
        "varslack": 3,
        "vbox": 5,
        "warn": 3,
        "week": 13,
        "why": 24,
        "widgetboundedfloatarrai": 5,
        "widgetboundedfloatrang": 5,
        "widgetboundedintarrai": 5,
        "widgetboundedintrang": 5,
        "widgetboundednumberarrai": 5,
        "widgetdefinit": 5,
        "widgetfloatarrai": 5,
        "widgetfloatrang": 5,
        "widgetgui": 5,
        "widgetintarrai": 5,
        "widgetintrang": 5,
        "widgetnumberarrai": 5,
        "widgettab": 5,
        "without": 14,
        "worker": 13,
        "write_json_fil": 6,
        "write_text_fil": 6
    }
})