{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50d4e732",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cfbcb06",
   "metadata": {},
   "source": [
    "# Digital Annealer Service Management\n",
    "The ``dadk`` library supports the access to Digital Annealer services via solver classes. Those classes encapsulate all actions towards a DA service like addressing the service, client authentication, sending QUBOs to the service, starting minimization or sampling jobs for a QUBO, transferring results, monitoring running jobs or cleaning orphaned jobs in an account. The ``Optimizer`` framework connects most of these functions to Jupyter notebooks and allows rapid prototyping for optimizations projects. In this notebook we present some auxiliary notebook functions for credential administration and management of jobs."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dee9a109",
   "metadata": {},
   "source": [
    "## Administrate credentials: ``SetAnnealerProfile``\n",
    "It is best practice not to write service credentials directly into the Python code or a Jupyter notebook. The ``dadk`` uses profiles to access annealer services. Those contain all access specific content like addresses and credentials, proxies etc. \n",
    "\n",
    "### Profile storage and detection\n",
    "The profiles are detected and automatically loaded and used by the ``dadk``. Profiles are JSON files of a certain structure named ``annealer.prf``. Profiles are searched and stored at different locations. First it is searched in the current working directory, normally the directory  containing your Jupyter notebook. This is the local profile and if it exists it has priority versus a central profile. You can have many local profiles in different directories e.g. with credentials for different projects.\n",
    "```\n",
    "   ...\n",
    "   +---_working_directory\n",
    "   |   +--- annealer.prf\n",
    "   |   +--- my_notebook.ipynb\n",
    "   |   +--- ...\n",
    "\n",
    "```\n",
    "If no local profile exists it is searched for a file ``annealer.prf`` in the subdirectory ``.dadk`` within the current user's home directory. This is called the central profile and there is only one per user. It can be used as a default access point for many different projects. Beside this there is a subdirectory ``profiles`` parallel to the central ``annealer.prf``. This directory always contains a profile ``localhost.prf``, which is automatically created and points to the local machine with CPU emulation and without credentials. The ``profile`` directory can contain many profiles with different names. This is a store of passive profiles. To use them, they are copied under the name ``annealer.prf`` to the central or local location. \n",
    "\n",
    "```\n",
    "   ...\n",
    "   +---_ ~user_home_directory\n",
    "   |   +---_.dadk\n",
    "   |   |   +---_profiles\n",
    "   |   |   |   +--- localhost.prf\n",
    "   |   |   |   +--- my_profile1.prf\n",
    "   |   |   |   +--- my_profile2.prf\n",
    "   |   |   |   +--- my_profile3.prf\n",
    "   |   |   |   +--- ...\n",
    "   |   |   +--- annealer.prf\n",
    "   |   |   ...\n",
    "   \n",
    "```\n",
    "\n",
    "The ``Optimizer`` GUI offers a field ``service`` within the ``Solve QUBO`` tab. The selection ``AUTO`` enables the described search hierarchy. This is the recommended setting for portable ``dao`` files, since it adapts to the setup of the respective host. For rapid switching between different services it is possible to select the local ``annealer.prf`` as ``LOCAL`` and the central profile as ``CENTRAL``. Further all store content is available in the selection list, the filename of the stored profile is prefixed by ``store:``. \n",
    "\n",
    "### Managing  profiles \n",
    "\n",
    "The editing, movement and deletion of profiles need not to be done manually but is supported by the function ``SetAnnealerProfile``. When you call the function in your Jupyter notebook, it executes the action ``get effective`` which follows the search strategy local prior to central. The discovered location is displayed and the content of the profile is loaded into the GUI fields. Selecting an action and pressing the call button, credential data can be retrieved (get) or stored (put) to the local, central or store position. To determine the source in the store, select some item from ``stored profiles``. The target name for storing a profile into the store has to be entered as ``Name in profile store`` before applying the action ``put store``. For Housekeeping it is supported to delete the local and centrally stored profile.\n",
    "\n",
    "There are different access methods to Digital annealer services. The method can be selected as ``connection_method``. This opens panes with fields to enter the corresponding address and credential information. The specific meaning of the fields is described by the labels and a help text is displayed when the mouse is over the label.\n",
    "\n",
    "Once you have entered your profile data, you can run a solve QUBO action in the ``Optimizer`` framework to test the connection.\n",
    "\n",
    "**Security recommendation: You should not save the fields of the ``SetAnnealerProfile`` via Notebook Widget State because they typically contain sensitive access information. It is best practice to delete the complete cell after entering and testing the profile information. It is persistent in the ``annealer.prf`` file and not needed in the Jupyter notebook any more. Also make sure not to check in a local annealer profile into a source management system like git.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e471d6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.JupyterTools import SetAnnealerProfile\n",
    "SetAnnealerProfile()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "479d2417",
   "metadata": {},
   "source": [
    "## Manage jobs: ``CleanupJobs``\n",
    "Normally the ``dadk`` manages the complete job life cycle in your account. QUBO data is transfered, a job is started, it is waited for completion, the result is retrieved and the job resources on service side are cleaned up. If in ASYNChronous mode the client side gives up before the job is completed (e.g. by kernel restart or shut down of the pc), then it might happen that orphaned jobs complete but are never retrieved and cleaned up. Since the number of simultaneous jobs for an account is limited, this might block new jobs from being started. Call ``CleanupJobs()`` from your Jupyter notebook and select the desired annealer profile from the drop down menu `service`. Using the function ``CleanupJobs`` you can list your current jobs, get information about or delete single jobs by entering their job id; It is possible to remove most likely orphaned jobs as obsolete jobs, which are completed for more than one hour. If you are sure about the usage of your account you can also remove all jobs. Handle this function with care, in order not to remove valuable results before they could be retrieved by the respective client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64c4bc05",
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.JupyterTools import CleanupJobs\n",
    "CleanupJobs()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
