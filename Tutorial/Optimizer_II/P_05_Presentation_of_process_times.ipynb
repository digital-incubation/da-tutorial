{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87bdf3b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74348a9a",
   "metadata": {},
   "source": [
    "# Presentation of Process Times\n",
    "\n",
    "In this notebook, we give an overview of the time information which are usually recorded while solving an optimization problem using this library. We explain them and their graphical representation in detail.\n",
    "\n",
    "## Overview of Times\n",
    "\n",
    "First, we give an overview of the sequence of processing steps and the respective processing times, see figure below.\n",
    "\n",
    "<img src=\".\\images/workflow-GUI_mode_DA2_cloud_asynchronous.png\" style=\"display: block; margin: 0 auto; padding: 25px\" /> \n",
    "\n",
    "Instead of working with the `Optimizer` GUI (see start point), you can also use the `Batch Mode`, as described in the chapter [M_07_BatchMode](../Development_KIT_Miscellaneous/M_07_BatchMode/M_07_BatchMode.ipynb). Furthermore, this picture is specific for using Digital Annealer Service version 2. If you use version 3, there is no difference between \"DAU Time\" and \"CPU Time\" and you'll see one \"Solve Time\" instead.\n",
    "\n",
    "The time information can be found in the `Optimizer GUI` in the compose model (in the `compose tracker` and the `anneal tracker` tab) or they are stored separately. The detailed time measurements are stored in an `*.xlsx` file if a `compose_model` method is defined, in the same folder where the `Optimizer` model's `*.dao` file is stored and with the same name as the `*.dao` file. Please note that the `*.dao` file name is the default for the `*.xlsx` file, but the name can be changed by setting the parameter `profile_file` in the `OptimizerModel class`.\n",
    "\n",
    "## Detailed Time Descriptions\n",
    "\n",
    "| Name | Description\n",
    "| :----------- | :----------- |\n",
    "| Load Time     | Time for Setup of Scenario (Elapsed Time for Execution of Method Load)     |\n",
    "| User WAIT Time* | Time between different stages of the optimization (e.g., setup scenario and build qubo). User dependent time between finishing one stage and clicking the call button of the next stage in the user interface. Will be 0 in batch_mode. |\n",
    "| Compose Model Time | Time after execution of the load method (and after \"User WAIT time\") and before and after every call of the build_qubo method and the prep_result method. Usually, time to perform pre- and post-processing between optimization runs and time to store solutions of optimization runs. |\n",
    "| Build QUBO Time | Time required by the build_qubo method to execute. |\n",
    "| CheckMD5 Time* | Time to calculate MD5 sum of all QUBO parts. Needed during development to verify validity of recorded results based on equality of freshly  generated QUBO. Not needed in productive mode. Can be turned off by setting Optimizer parameter qubo_hash_step_size to 0. |\n",
    "| Prepare QUBO Time | Time for converting BinPol with higher degree to a QUBO, converting all coefficients to integer values and checking that all coefficients are in the selected number range, defined by the parameter bit_precision. (Elapsed time for execution of method make_qubo, internal, done by the method minimize). |\n",
    "| Account Occupied Time* | Time to check if queue is full (usually queue can handle 16 requests). If queue is full, wait a second and try again until queue can handle new request. Only in asynchronous mode. Can be turned off by setting queue-size to 0 in annealer profile file. |\n",
    "| Prepare Request Time | Time for preparing the http request if needed. |\n",
    "| Send Request Time | Only seen in asynchronous mode. Time for QUBO transfer over network to DAU service. |\n",
    "| Waiting Time | Only seen in asynchronous mode. Includes Queue time and Solve time. |\n",
    "| Queue Time* | Time for the DAU-job to wait in the queue before execution. Only in asynchronous mode. |\n",
    "| Receive Response Time | Only seen in asynchronous mode. Time for result transfer over network back from DAU service. |\n",
    "| DAU Service Time | Only seen in synchronous mode in commercial Japanese DAU service. |\n",
    "| Solve Time | Time for the DAU-job to execute. Reported back by the web-api. Contains the CPU and DAU time (version 2). |\n",
    "| CPU Time | Time for the DAU-job to execute on the CPU (preparation) before the job is executed on the DAU itself. Reported back by the web-api. |\n",
    "| Anneal Time | Time for the DAU-job to execute on the DAU itself. Reported back by the web-api. Only this time will be charged against the monthly time budget of Digital Annealer Service. |\n",
    "| Parse Response Time | Time for reading the http response. |\n",
    "| PrepResult Time | Time for converting the solution back to business-data (elapsed time for execution of method prep_result). |\n",
    "\n",
    "Times marked with * can and should be turned off by using the respective method in the description or neglected. This is recommended if you are interested in an overall time measurements giving an idea of possible achievable runtimes.\n",
    "\n",
    "## Presentation of runtimes in the Anneal Tracker\n",
    "\n",
    "In the following, we show and explain the `anneal tracker` tab in the `Optimizer` GUI for a call to the Digital Annealer Service version 2 in asynchronous mode. To understand the times and their occurence better, the next image shows some examples of the correspondences from the first workflow digram and the following image from the `anneal tracker`.\n",
    "\n",
    "<img src=\".\\images/corresponding_times_workflow_tracker.png\" style=\"display: block; margin: 0 auto; padding: 25px\" /> \n",
    "\n",
    "In the `anneal tracker`you can find the times in detail. To open the `anneal tracker` please go to the `Optimizer` GUI and open  the `anneal tracker` tab. You can also find the information in a `*.xlsx` file as described above.\n",
    "\n",
    "<img src=\".\\images/Anneal_tracker_asynchronous_CE_DA2_cloud.png\" style=\"display: block; margin: 0 auto; padding: 15px\" /> \n",
    "\n",
    "Anneal tracker for asynchronous Digital Annealer Service version 2. Times are shown in rows for different perspectives, i.e., from client side, network side, service side, queue side and CPU/DAU side (version 3: no difference between CPU Time and DAU Time, given as one Solving Time). Separate times are shown consecutively as different coloured bars in each row. Note that whenever a time bar in a lower row is located below a time bar in a higher row, then the time in the higher row includes the times in the lower row. For example, the bar for waiting in the network row includes the bar execution in the service row, which in turn includes the queue time and the solve time in the queue row. The whole waiting time (in the row network) should be covered by times in the row service."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
