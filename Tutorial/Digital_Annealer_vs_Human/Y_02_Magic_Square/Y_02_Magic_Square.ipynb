{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Magic Squares\n",
    "\n",
    "With the advent of math based games such as Sudoku, the complement of numerical series, and others, magic squares have gained popularity. A magic square is a square in which the numbers are arranged in such a way that the sum of values in each row, column, and both of the diagonals adds up to the same number, the so-called \"magic number\" or \"magic constant\". The oldest known magic square is probably the Chinese Luoshu Square which is a $3 \\times 3$ magic square with the \"magic number\" $15$. You can read more about the interesting history of this square [here](https://en.wikipedia.org/wiki/Luoshu_Square).\n",
    "<img src=\"./figures/Magic_Square_Lo_Shu.png\" width=\"200\"/>\n",
    "\n",
    "One of the most famous $4 \\times 4$ magic squares can be found in Albrecht Dürer's (1471 - 1528) engraving \"Melencolia I\" (1514). Here the corresponding magic number is $34$. For example, in the bottom line: $4 + 15 + 14 + 1 = 34$.\n",
    "\n",
    "<table style=\"border: 0px;\">\n",
    "    <tr>\n",
    "        <td style=\"border-right: 1px solid #000;\">\n",
    "            <img src=\"./figures/Melencolia_I_(Durero).jpg\" width=\"350\"/>\n",
    "        </td>\n",
    "        <td>\n",
    "            <img src=\"./figures/Albrecht_Duerer_-_Melencolia_I__detail_.jpg\" width=\"425\"/>\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "Magic Squares can have different sizes, but never smaller than $3 \\times 3$. If you play against our Digital Annealer, try to solve the squares right and faster. Some hints, how to solve a magic square by yourself, can be found on this [website](https://mathcommunities.org/magic-squares/), but there are many more strategies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Magic Squares as Combinatorial Optimization Problems\n",
    "\n",
    "There are many ways to construct magic squares. For example for an order of a multiple of 4 one can fill the square row wise in ascending order from $1$ to $n^2$. Then the square is partitioned in 4 by 4 sub squares and all entries in the diagonals of these sub squares are kept unchanged. The other entries are exchanged with the row and column wise diametrical entry in the whole square. It is easy to see, that this balances the row an column sums and it can be tested in the next box."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 2 * 4\n",
    "square = [[n * r + c + 1 if (c + r + 1) % 4 == 0 or (c - r) % 4 == 0 else n * (n - 1 - r) + (n - 1 - c) + 1 for c in range(n)] for r in range(n)]\n",
    "\n",
    "linebreak = '\\n'\n",
    "print(f'{linebreak.join([\" \".join([f\"{cell:4d}\" for cell in line]) for line in square])}\\n')\n",
    "print('Row sums: ', [sum(row) for row in square])\n",
    "print('Col sums: ', [sum(col) for col in zip(*square)])\n",
    "print('Dia sums: ', [sum(square[i][i] for i in range(n)), sum(square[n - 1 - i][i] for i in range(n))])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For odd and even order not divisible by 4 there are other construction schemes. Instead of construction you might want to discover magic squares. The simplest way is to generate brute forces all possible combinations and check the row, column and diagonal sums. For $n=3$ there are $(n^2)! = 362,889$ combinations which might be doable, but for $n=4$ there are over 20 trillion combinations. Branch and bound is a possibility to reduce the search. In this notebook we use optimization technology to find valid constructions of magic squares. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Definition\n",
    "\n",
    "Next, we set the stage to solve Magic Squares using Quadratic Unconstraint Optimization (QUBO) problems. This means we have to formulate the Magic Square rules in form of a QUBO model. \n",
    "In general, a magic square is a square of $n \\times n$ cells, containing the values $[1, 2, \\dots, n^{2}]$. The sum of values in each row, column, and both diagonals must add up to the same value. To formulate all rules, we define the following variables:\n",
    "\n",
    "* $N=[1,\\dots,n]$\n",
    "* $V=[1,\\dots,n^2]$\n",
    "* $n$ is the order of the magic square\n",
    "* $m$ is the magic number\n",
    "* The $n$ rows are denoted by $r\\in N$ (In the figures indicated as numbers in blue circles).\n",
    "* The $n$ columns are denoted by $c\\in N$ (In the figures indicated as numbers in a red circle).\n",
    "* The $n^2$ values are denoted by $v\\in V$.\n",
    "* Each cell represent by the tuple $(r,c)$ contains exactly one value $v$.\n",
    "\n",
    "The following Figure shows an overview of the setup.\n",
    "![Overview](./figures/bits-overview.gif)\n",
    "\n",
    "The decision variable $x$ indicates if value $v$ is placed in cell $(r,c)$:\n",
    "$$\n",
    "x_{r,c,v} = \\begin{cases}\n",
    "    1 & \\text{if cell } (r, c) \\text{ contains value } v \\\\ \n",
    "    0 & \\text{otherwise.}\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "In the following Figure the magic square is a $3\\times3$ square with $n=3$, i.e., $c=[1,\\dots,3]$, $r=[1,\\dots,3]$, and $v=[1,\\dots,9]$.\n",
    "\n",
    "![Special Example](./figures/bits-special.gif)\n",
    "\n",
    "For example, we highlighted cell $(3,2)$ at value $v=4$. The decision variable in this case would be\n",
    "$$\n",
    "x_{r, c, v} = x_{3, 2, 4} = 1 \\, .\n",
    "$$\n",
    "\n",
    "We can also calculate how many bits are needed, to solve the magic square. Each cell $(r,c)$ contains one of $n^2$ numbers. For each number we have to set $x=0$ or $x=1$. The total number of bits is $N_{bits} = n^2 \\text{ cells }\\times n^2 \\text{ values }=n^4$. In the example above $n=3$ and $N_{bits} = 81$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a Magic Square\n",
    "\n",
    "Before starting to implement the rules for magic squares, we create in the following cell an empty magic square of size $3\\times3$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.BinPol import *\n",
    "import itertools\n",
    "\n",
    "order = 3\n",
    "print('\\nMagic Square created:')\n",
    "print(f'Order          = {order}')\n",
    "\n",
    "magic_constant = order * (order ** 2 + 1) // 2\n",
    "print(f'Magic_constant = {magic_constant}')\n",
    "\n",
    "var_shape_set = VarShapeSet(BitArrayShape('x', (order, order, order ** 2),\n",
    "                                          index_offsets=(1,1,1),\n",
    "                                          one_hot=OneHot.one_way,\n",
    "                                          axis_names=['Row', 'Column', 'Value']))\n",
    "\n",
    "rows = tuple(range(1, order+1))\n",
    "columns = tuple(range(1, order+1))\n",
    "values = tuple(range(1, order**2+1))\n",
    "\n",
    "print(f'Rows           = {rows}')\n",
    "print(f'Columns        = {columns}')\n",
    "print(f'Values         = {values}')\n",
    "\n",
    "\"\"\"for row in rows:\n",
    "    for column in columns:\n",
    "        for value in values:\n",
    "            print(f'row={row}, column={column}, value={value} x_{row},{column},{value} -> {var_shape_set._flat_index((\"x\", row, column, value))}')\n",
    "\"\"\"\n",
    "\n",
    "print(f'Number of bits = {var_shape_set.length}')\n",
    "\n",
    "BinPol.freeze_var_shape_set(var_shape_set)\n",
    "\n",
    "parts = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building the QUBO\n",
    "\n",
    "The QUBO is setup \"rule by rule\". In each step we formulate one of the magic square rules as a QUBO.\n",
    "\n",
    "In the case of solving a magic square there is no optimization objective as you might have encountered in the other examples. Here, it is all about obeying the magic square rules. Therefore, the QUBO only contains \"penalty\"-terms. The goal in \"optimization\" of such a QUBO is to find a valid solution, i.e. a solution with all penalties equal to zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $H_1$: Each cell has exactly one value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start with the first rule: Each cell must contain exactly one value.\n",
    "\n",
    "In the following Figure on the left we highlight cell $(r=3, c=3)$ as an example to better understand the rule.\n",
    "In cell $(r=3, c=3)$ we have to place exactly one value $v$. The penalty for this example reads:\n",
    "$$\n",
    "\\bar{H}_{1}  = \\left( \\sum _{v \\, \\in \\, V} x_{3, 3, v} - 1 \\right) ^{2}\n",
    "$$\n",
    "\n",
    "If one of the terms in the sum $\\sum _{v \\, \\in \\, V}$ is one, then the penalty is zero. Otherwise we violate the rule and pay a penalty greater zero.\n",
    "\n",
    "Of course this rule applies to all cells, as shown on the right in the Figure. The corresponding QUBO formulation for all cells reads:\n",
    "\n",
    "$$\n",
    "H_{1} = \\sum_{r \\, \\in \\, N} \\sum_{c \\, \\in \\, N} \\left( \\sum _{v \\, \\in \\, V} x_{r, c, v} - 1 \\right) ^{2}\n",
    "$$\n",
    "\n",
    "<table style=\"border: 0px solid;\">\n",
    "    <tr style=\"border-bottom: 1px solid #ddd;\">\n",
    "        <td style=\"text-align:center; border-right: 1px solid #000; background-color: #FFF;\">\n",
    "            <h3>$H_1$ for cell (3, 3)</h3>\n",
    "        </td>\n",
    "        <td style=\"text-align:center;background-color: #FFF\">\n",
    "            <h3>$H_1$ for all cells</h3>\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td style=\"border-right: 1px solid #000;\">\n",
    "            <img src=\"./figures/cell-single.gif\">\n",
    "        </td>\n",
    "        <td>\n",
    "            <img src=\"./figures/cell-all.gif\">\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "In the next cell we implemented $H_1$. Please execute the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H1 = BinPol()\n",
    "parts['H1'] = {}\n",
    "for row, column in itertools.product(rows,columns):\n",
    "    h = BinPol()\n",
    "    for value in values:\n",
    "        h.add_term(1, ('x', row, column, value))\n",
    "    h.add_term(-1)\n",
    "    h.power(2)\n",
    "    H1.add(h)\n",
    "    parts['H1'][(row, column)] = h\n",
    "\n",
    "print()\n",
    "print(\"H1 created!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $H_2$: Magic number for rows"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next rule deals with the magic number: In each row, the sum of the values must add up to the magic number $m$.\n",
    "\n",
    "In the following Figure on the left we highlight row $r=3$ as an example to better understand the rule.\n",
    "The penalty for this example reads:\n",
    "\n",
    "$$\n",
    "\\bar{H}_{2} = \\left( \\sum _{c \\, \\in \\, N} \\sum_{v \\, \\in \\, V} x_{3, c, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "\n",
    "If the second sum $\\sum _{c \\, \\in \\, N}$ is exactly the magic number $m$, then the penalty is zero. Otherwise we violate the rule and pay a penalty greater zero.\n",
    "\n",
    "Of course this rule applies to all rows, as shown on the right in the Figure. The corresponding QUBO formulation for all cells reads:\n",
    "\n",
    "$$\n",
    "H_{2} = \\sum_{r \\, \\in \\, N} \\left( \\sum _{c \\, \\in \\, C} \\sum_{v \\, \\in \\, V} x_{r, c, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "\n",
    "<table style=\"border: 0px solid;\">\n",
    "    <tr style=\"border-bottom: 1px solid #ddd;\">\n",
    "        <td style=\"text-align:center; border-right: 1px solid #000; background-color: #FFF;\">\n",
    "            <h3>$H_2$ for row=3</h3>\n",
    "        </td>\n",
    "        <td style=\"text-align:center;background-color: #FFF\">\n",
    "            <h3>$H_2$ for all rows</h3>\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td style=\"border-right: 1px solid #000;\">\n",
    "            <img src=\"./figures/row-single.gif\">\n",
    "        </td>\n",
    "        <td>\n",
    "            <img src=\"./figures/row-all.gif\">\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "Please execute the next cell where we implemented $H_2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H2 = BinPol()\n",
    "parts['H2'] = {}\n",
    "for row in rows:\n",
    "    h = BinPol()\n",
    "    for column, value in itertools.product(columns, values):\n",
    "        h.add_term(value, ('x', row, column, value))\n",
    "    h.add_term(-magic_constant)\n",
    "    h.power(2)\n",
    "    H2.add(h)\n",
    "    parts['H2'][row] = h\n",
    "\n",
    "print()\n",
    "print(\"H2 created!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $H_3$: Magic number for columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This rule also deals with the magic number, however now for columns: In each column, the sum of the values must add up to the magic number $m$.\n",
    "\n",
    "In the following Figure on the left we highlight column $c=3$ as an example to better understand the rule.\n",
    "The penalty for this example reads:\n",
    "\n",
    "$$\n",
    "\\bar{H}_{3} = \\left( \\sum _{r \\, \\in \\, N} \\sum_{v \\, \\in \\, V} x_{r, 3, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "\n",
    "If the second sum $\\sum _{r \\, \\in \\, N}$ is exactly the magic number $m$, then the penalty is zero. Otherwise we violate the rule and pay a penalty greater zero.\n",
    "\n",
    "Of course this rule applies to all columns, as shown on the right in the Figure. The corresponding QUBO formulation for all cells reads: \n",
    "\n",
    "$$\n",
    "H_{3} = \\sum_{c \\, \\in \\, N} \\left( \\sum _{r \\, \\in \\, N} \\sum_{v \\, \\in \\, V} x_{r, c, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "\n",
    "<table style=\"border: 0px solid;\">\n",
    "    <tr style=\"border-bottom: 1px solid #ddd;\">\n",
    "        <td style=\"text-align:center; border-right: 1px solid #000; background-color: #FFF;\">\n",
    "            <h3>$H_3$ for column=3</h3>\n",
    "        </td>\n",
    "        <td style=\"text-align:center;background-color: #FFF\">\n",
    "            <h3>$H_3$ for all columns</h3>\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td style=\"border-right: 1px solid #000;\">\n",
    "            <img src=\"./figures/column-single.gif\">\n",
    "        </td>\n",
    "        <td>\n",
    "            <img src=\"./figures/column-all.gif\">\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "Please execute the next cell to create the QUBO $H_3$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H3 = BinPol()\n",
    "parts['H3'] = {}\n",
    "for column in columns:\n",
    "    h = BinPol()\n",
    "    for row, value in itertools.product(rows, values):\n",
    "        h.add_term(value, ('x', row, column, value))\n",
    "    h.add_term(-magic_constant)\n",
    "    h.power(2)\n",
    "    H3.add(h)\n",
    "    parts['H3'][column] = h\n",
    "\n",
    "print()\n",
    "print(\"H3 created!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $H_4$ and $H_5$: Magic number for diagonals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, sum of the values on each diagonal must also add up to the magic number $m$. In the following Figure both diagonals are highlighted. The corresponding QUBO formulation for each diagonal reads:\n",
    "\n",
    "$$\n",
    "H_{4} = \\left( \\sum _{r \\, \\in \\, N} \\sum_{v \\, \\in \\, V} x_{r, r, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "<br>\n",
    "$$\n",
    "H_{5} = \\left( \\sum _{r \\, \\in \\, N} \\sum_{v \\, \\in \\, V} x_{r, n-r+1, v} \\cdot v - m \\right) ^{2}\n",
    "$$\n",
    "\n",
    "<table style=\"border: 0px solid;\">\n",
    "    <tr style=\"border-bottom: 1px solid #ddd;\">\n",
    "        <td style=\"text-align:center; border-right: 1px solid #000; background-color: #FFF;\">\n",
    "            <h3>$H_4$ for the one diagonal</h3>\n",
    "        </td>\n",
    "        <td style=\"text-align:center;background-color: #FFF\">\n",
    "            <h3>$H_5$ for the other diagonal</h3>\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td style=\"border-right: 1px solid #000;\">\n",
    "            <img src=\"./figures/diagonal_1.gif\">\n",
    "        </td>\n",
    "        <td>\n",
    "            <img src=\"./figures/diagonal_2.gif\">\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>\n",
    "\n",
    "Please execute the next cell to create the QUBOs $H_4$ and $H_5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H4 = BinPol()\n",
    "for row, value in itertools.product(rows, values):\n",
    "    H4.add_term(value, ('x', row, row, value))\n",
    "H4.add_term(-magic_constant)\n",
    "H4.power(2)\n",
    "parts['H4'] = H4\n",
    "\n",
    "H5 = BinPol()\n",
    "for row, value in itertools.product(rows, values):\n",
    "    H5.add_term(value, ('x', row, order - row + 1, value))\n",
    "H5.add_term(-magic_constant)\n",
    "H5.power(2)\n",
    "parts['H5'] = H5\n",
    "\n",
    "print()\n",
    "print(\"H4 & H5 created!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### $H_6$: Each value to be used exactly once"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each value $v = 1,\\dots,9$ must be used exactly once:\n",
    "$$\n",
    "H_{6} = \\sum_{v \\, \\in \\, V} \\left(\\sum_{c \\, \\in \\,N} \\sum _{r \\, \\in \\, N} x_{r, c, v} - 1 \\right) ^{2}\n",
    "$$\n",
    "\n",
    "Please execute the next cell to create QUBO $H_6$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H6 = BinPol()\n",
    "parts['H6'] = {}\n",
    "for value in values:\n",
    "    h = BinPol()\n",
    "    for row, column in itertools.product(rows, columns):\n",
    "        h.add_term(1, ('x', row, column, value))\n",
    "    h.add_term(-1)\n",
    "    h.power(2)\n",
    "    H6.add(h)\n",
    "    parts['H6'][value] = h\n",
    "\n",
    "print()\n",
    "print(\"H6 created!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The total QUBO H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have all parts of the QUBO and can put them together:\n",
    "\n",
    "$$\n",
    "H=\\alpha H_{1}+\\beta H_{2}+\\gamma H_{3}+\\delta H_{4}+\\epsilon H_{5}+\\zeta H_{6}\n",
    "$$\n",
    "\n",
    "The greek letters are factors which determine the strength of the penalty. In order to find a valid solution the the magic square they have to be tuned. For the rest of the exercise we use $\\alpha = \\zeta = 100$ and all others are set to $1$. If you want to learn more about parameter tuning, please have a look at the chapter [Tuning](../../Development_KIT_Miscellaneous/M_06_Tuning/M_06_Tuning.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create solver and run annealing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, with the QUBO , it's time to run the solver. Please execute the next cells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.QUBOSolverCPU import *\n",
    "\n",
    "qubo = 100 * H1 + H2 + H3 + H4 + H5 + 100 * H6\n",
    "\n",
    "solver = QUBOSolverCPU(\n",
    "    number_iterations=qubo.N ** 2,  # Total number of iterations per run.\n",
    "    number_runs=16,                 # Number of stochastically independent runs.\n",
    "    temperature_start=10000,        # Start temperature of the annealing process.\n",
    "    temperature_end=1,              # End temperature of the annealing process.\n",
    "    temperature_mode=0,             # 0, 1, or 2 to define the cooling curve:\n",
    "                                    #    0, 'EXPONENTIAL':\n",
    "                                    #       reduce temperature by factor (1-temperature_decay) every temperature_interval steps\n",
    "                                    #    1, 'INVERSE':\n",
    "                                    #       reduce temperature by factor (1-temperature_decay*temperature) every temperature_interval steps\n",
    "                                    #    2, 'INVERSE_ROOT':\n",
    "                                    #       reduce temperature by factor (1-temperature_decay*temperature^2) every temperature_interval steps\n",
    "    offset_increase_rate=20,        # Increase of dynamic offset when no bit selected. Set to 0.0 to switch off dynamic energy feature.\n",
    "    graphics=GraphicsDetail.SINGLE  # Switch on graphics output.\n",
    ")\n",
    "\n",
    "print('Start annealing ... ', end='')\n",
    "\n",
    "solution_list = solver.minimize(qubo)\n",
    "\n",
    "print('\\nTime needed: ')\n",
    "print(solution_list.solver_times.duration_execution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary of the results\n",
    "\n",
    "Magic squares can have more than one solution or symmetric solutions (squares can change into each other, if you reflect them at the axes of symmetry). So during the annealing process different solutions are found, too. You can check every of them (and how often it was found) below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tabulate import tabulate\n",
    "import ipywidgets as widgets\n",
    "from IPython.display import display, clear_output\n",
    "\n",
    "valid_solution_counter = 0\n",
    "invalid_solution_counter = 0\n",
    "\n",
    "options = []\n",
    "for solution_no, solution in enumerate(solution_list.get_solution_list()):\n",
    "    energy = qubo.compute(solution.configuration)\n",
    "    if energy == 0:\n",
    "        display(widgets.HTML(f'<h4>No {solution_no}, frequency {solution.frequency}</h4>'))\n",
    "        magic_square = np.full((order, order), -1)\n",
    "        for (row, column, value) in np.argwhere(solution['x'].data == 1):\n",
    "            magic_square[row, column] = value + 1\n",
    "        print(tabulate(reversed(magic_square), tablefmt=\"fancy_grid\"))\n",
    "        valid_solution_counter += solution.frequency\n",
    "        options.append((f'No {solution_no}, frequency {solution.frequency}, valid', solution))\n",
    "    else:\n",
    "        invalid_solution_counter += solution.frequency\n",
    "        options.append((f'No {solution_no}, frequency {solution.frequency}, invalid, energy {energy}', solution))\n",
    "\n",
    "display(widgets.HTML(f'<h4>found {valid_solution_counter} valid and {invalid_solution_counter} invalid solution(s)</h4>'))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bit representation of solutions found by Digital Annealer\n",
    "\n",
    "The next cell will evaluate the bit presentation of each solution, also the invalid one. For each value the magic square with the active (red) and non-active (blue) bits is shown."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "select = widgets.Dropdown(description='Solution:', options=options)\n",
    "display(select)\n",
    "output = widgets.Output();\n",
    "display(output)\n",
    "\n",
    "def on_value_change(change):\n",
    "    with output:\n",
    "        clear_output()\n",
    "        solution = change['new']\n",
    "        energy = qubo.compute(solution.configuration)\n",
    "        display(widgets.HTML(f'<h4>frequency {solution.frequency}</h4>'))\n",
    "        magic_square = [[None for column in range(order)] for row in range(order)]\n",
    "        for (row, column, value) in np.argwhere(solution['x'].data == 1):\n",
    "            magic_square[row][column] = value + 1\n",
    "        print(tabulate(reversed(magic_square), tablefmt=\"fancy_grid\"))\n",
    "\n",
    "        if energy != 0:\n",
    "            if H1.compute(solution.configuration) != 0:\n",
    "                for row in rows:\n",
    "                    for column in columns:\n",
    "                        if parts['H1'][(row, column)].compute(solution.configuration) != 0:\n",
    "                            print(f'cell row {row}, column {column} has not exactly one value!')\n",
    "            if H2.compute(solution.configuration) != 0:\n",
    "                for row in rows:\n",
    "                    if parts['H2'][row].compute(solution.configuration) != 0:\n",
    "                        print(f'in row {row} the sum of the values is not the magic constant {magic_constant}!')\n",
    "            if H3.compute(solution.configuration) != 0:\n",
    "                for column in columns:\n",
    "                    if parts['H3'][column].compute(solution.configuration) != 0:\n",
    "                        print(f'in column {column} the sum of the values is not the magic constant {magic_constant}!')\n",
    "            if H4.compute(solution.configuration) != 0:\n",
    "                print(f'in diagonal \"/\" the sum of the values is not the magic constant {magic_constant}!')\n",
    "            if H5.compute(solution.configuration) != 0:\n",
    "                print(f'in diagonal \"\\\\\" the sum of the values is not the magic constant {magic_constant}!')\n",
    "            if H6.compute(solution.configuration) != 0:\n",
    "                for value in values:\n",
    "                    if parts['H6'][value].compute(solution.configuration) != 0:\n",
    "                        print(f'value {value} is not used exactly once!')\n",
    "\n",
    "        solution.draw(name='x', figsize=(12, 12), max_ax_per_row=order, order=[1, 0, 2], axis_offsets=[1, 1, 1])\n",
    "\n",
    "select.observe(on_value_change, names='value')\n",
    "on_value_change({'new': select.value})\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The annealing information\n",
    "\n",
    "If you want to know more about the annealing parameters, preparation and solution times, please have a look at the next table and figures. The stats will give you some helpful information which you can use for comparing with other annealing runs or optimization methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solution_list.print_stats()\n",
    "solution_list.display_graphs()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Another way to write the code for implementing the QUBO\n",
    "\n",
    "There are different ways how to write the code for a QUBO. In this notebook we use the `BinPol()` class and create sums used for loops. However, there are other ways inside of the `dadk`. You can find an example for all six QUBO terms using `BinPol.sum()` which results in very short and condensed code above. For further information, please hava a look into the [documentation](../../Development_KIT_Documentation/index.html).\n",
    "\n",
    "```\n",
    "nH1 = var_shape_set.generate_penalty_polynomial('x')\n",
    "\n",
    "nH2 = BinPol.sum((BinPol.sum(Term(value, ('x', row, column, value)) for value in values for column in columns) - magic_constant) ** 2 for row in rows)\n",
    "\n",
    "nH3 = BinPol.sum((BinPol.sum(Term(value, ('x', row, column, value)) for value in values for row in rows) - magic_constant) ** 2 for column in columns)\n",
    "\n",
    "nH4 = (BinPol.sum(Term(value, ('x', row, row, value)) for value in values for row in rows) - magic_constant) ** 2\n",
    "\n",
    "nH5 = (BinPol.sum(Term(value, ('x', row, order - row + 1, value)) for value in values for row in rows) - magic_constant) ** 2\n",
    "\n",
    "nH6 = BinPol.sum((BinPol.sum(Term(1, ('x', row, column, value)) for column in columns for row in rows) - 1) ** 2 for value in values)\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
