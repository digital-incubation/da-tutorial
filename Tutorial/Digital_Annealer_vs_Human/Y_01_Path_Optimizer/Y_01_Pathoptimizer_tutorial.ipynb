{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PathOptimizer - Move Clever and Keep Distance\n",
    "***Let's play a game - it will inspire you to find the path to optimization!***\n",
    "\n",
    "The goal of the game is to move robots (big colored circles) from their start position (small circle with same color) to an end position (small square) in as few steps as possible. You cannot move through walls, therefore maneuver around the black obstacles.\n",
    "\n",
    "Rules:\n",
    "- Robots can be moved by drag and drop. \n",
    "- Robots can move one field left, right, up, down or diagonal. \n",
    "- Robots have to maintain a security distance between each other. They have an aura around themselves. The auras of two robots must not overlap after or during a move.\n",
    "\n",
    "## Get engaged!\n",
    "Run the following line, start playing, and try to beat the Digital Annealer by finding the optimal moves. To see how you competed against the Digital Annealer, click on \"Show DA score\" to see the minimum number of moves."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathoptimizer import game\n",
    "gm = game.Game(play_levels=True, start_level=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tackling the optimization problem\n",
    "\n",
    "Playing the game and trying to use as few moves as possible, you had a strategy in mind and quickly realized that finding the optimal moves depends on combinatorial decisions and relationships of the robots' positions (if I move this robot to this field then another robot can only move in that way, etc.). Such problems are paradigm problems which can mathematically be described by \"Quadratic Unconstraint Binary Optimization\" (QUBO) models and solved very efficiently on the Digital Annealer Unit. We postpone details on the mathematical model, its formulation and implementation to the section \"Problem Formulation as QUBO\" below.\n",
    "\n",
    "\n",
    "## Compare yourself to the optimal solution\n",
    "\n",
    "Please execute the following cell to generate a dashboard. You have to execute all tabs of the dashboard from left to right.\n",
    "Each time you made changes in a tab, you need to press the \"Call\" button.\n",
    "\n",
    "**Setup scenario**  \n",
    "In this tab you are able to design the board, choose the number of robots and their initial and final positions. Try to rebuild the levels from the game above. Hint: In the field \"Robot positions\" choose \"manual\" and set the robots initial and final positions by drag and drop on the board.\n",
    "\n",
    "**Build QUBO**  \n",
    "Here, you can balance the different constraints and the optimization goal. For now, keep the default values and press the \"Call\" button.\n",
    "\n",
    "**Solve Annealing**\n",
    "The technical parameters to control the annealing process are provided and can be tuned in this tab. In the field \"Processor\" choose \"CPU\", keep the default values and press the \"Call\" button. This will run an annealing algorithm which is emulated on the CPU.\n",
    "\n",
    "**Anneal tracker**  \n",
    "This tab provides information about run times and the annealing process.\n",
    "\n",
    "**Report**   \n",
    "A report shows the position of each robot at each time step as a red dot. Time increases from the left to the right column. Each field of the board is given a number from $0$ to $width \\times height - 1$ and displayed from bottom to top.\n",
    "\n",
    "**Board**  \n",
    "This tab shows you, the moves obtained by an optimization using a Digital Annealing algorithm on the CPU. You see the board which you created in the beginning. Move the slider \"Time\" to the right and you will move by move."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from sources.Optimizer_Pathoptimizer_tutorial import * \n",
    "\n",
    "optimizer = Optimizer(PathoptimizerModel(), immediate_load=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimization results obtained by the Digital Annealer Unit\n",
    "\n",
    "Of course the above optimization of the robots' movement using the CPU is not optimal. The reason is that the CPU is not tailored for these kind of optimization problems. Using the CPU we need a lot of iterations during the annealing process in order to come close to an optimum solution. The Digital Anneal on the other hand is specifically designed to solve combinatorial optimization problems. It is able to calculate millions of iterations multiple times, yielding an optimal solution in just a few seconds.\n",
    "\n",
    "Therefore we made use of the Digital Annealer Unit to optimize the levels you played in the beginning. You can load the results obtained by the Digital Annealer by executing the following cell. A drop-down menu will appear which allows you to select a solution for a specific level.\n",
    "\n",
    "***This will show you, what you are waiting for: What moves exactly lead to the minimum number of steps and how did the Digital Annealer beat me?***"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from sources.Optimizer_Pathoptimizer_tutorial import * \n",
    "\n",
    "selector = Optimizer.select_instance(\n",
    "    optimizer_model=PathoptimizerModel,\n",
    "    dao_path='.', dao_pattern='pathoptimizer_level',\n",
    "    initial_selection=None, immediate_load=True, read_only=True\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More on PathOptimizer and its application\n",
    "On a more abstract level, the PathOptimizer game and its optimization is about being safe and saving resources in a complex environment. There are a lot of potential applications in many business areas. Examples of use cases are for instance:\n",
    "\n",
    "1. Collision free movement of many autonomous robots in a warehouse.\n",
    "<center><b><img src=\"pictures/warehouse_robotics.jpg\" width=440/></b></center>\n",
    "<font><center><b>Fig.1 </b><a href=\"https://roboticsandautomationnews.com/2020/01/31/how-robotics-can-be-extremely-beneficial-to-warehouse-business/29344/\">https://roboticsandautomationnews.com/2020/01/31/how-robotics-can-be-extremely-beneficial-to-warehouse-business/29344/</a></center></font>\n",
    "\n",
    "<p></p>\n",
    "\n",
    "2. Routing planes in complex areas, e.g., optimizing air traffic around airports.\n",
    "<center><b><img src=\"pictures/airtraffic.jpg\" width=440/></b></center>\n",
    "<font><center><b>Fig.2 </b><a href=\"https://www.blackbox.co.uk/gb-gb/page/39931/Solutions/By-Industry/Airports-and-Air-Traffic-Control\">https://www.blackbox.co.uk/gb-gb/page/39931/Solutions/By-Industry/Airports-and-Air-Traffic-Control/</a></center></font>\n",
    "\n",
    "<p></p>\n",
    "\n",
    "3. Declaring optimal routes, e.g., through a building while respecting a corona safety distance.\n",
    "<center><b><img src=\"pictures/routesbuilding.jpg\" width=440/></b></center>\n",
    "<font><center><b>Fig.3 </b><a href=\"https://blog.de.fujitsu.com/veranstaltungen/fujitsu-forum-muenchen/fujitsu-forum-2019-alles-fuer-die-digitale-transformation/\">https://blog.de.fujitsu.com/veranstaltungen/fujitsu-forum-muenchen/fujitsu-forum-2019-alles-fuer-die-digitale-transformation/</a></center></font>\n",
    "\n",
    "<p></p>\n",
    "\n",
    "4. Safely maneuvering multiple surveillance drones.\n",
    "<center><b><img src=\"pictures/drones.jpg\" width=440/></b></center>\n",
    "<font><center><b>Fig.4 </b><a href=\"https://www.securitymagazine.com/articles/88990-chicago-bill-for-drone-surveillance-of-large-scale-events-sparks-privacy-debate\">https://www.securitymagazine.com/articles/88990-chicago-bill-for-drone-surveillance-of-large-scale-events-sparks-privacy-debate/</a></center></font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem formulation as QUBO I\n",
    "Since the PathOptimizer problem is a optimization task, we introduce in the following the mathematical formulation of the problem as a Quadratic Unconstraint Binary Optimization (QUBO) task which can be solved highly efficiently with the Digital Annealer Unit.\n",
    "\n",
    "As you have learned by playing the game, you have to respect the board with its obstacles, the start and end position of the robots, and of course you have to play by the rules. These constraints also have to be respected and taken into account when it comes to finding the optimum of moving the robots. The goal is to minimize the maximum of steps each robots needs to find its destination.\n",
    "We model these constrains as a QUBO model, the Digital Annealer then finds a minimum value for this QUBO. The solution, i.e., the minimum energy value of the QUBO, can finally be associated with the moves the robots have to take such that the number of moves is optimal. \n",
    "\n",
    "### Input data\n",
    "Let $R$ be the number of robots, represented by variable $r \\in \\{0,1,..., R-1\\}$.\n",
    "The board consists of $F$ fields $f \\in \\{0,1,...,F-1\\}$. You are given the opportunity to design your own board with obstacles when using the Digitial Annealer Optimzer Model.\n",
    "Each time step is described by variable $t \\in \\{0,1,.., T-1\\}$, in total there are $T$ time steps. The total number of time steps should be estimated before hand with a realistic upper limit, depending on the complexity of the level.\n",
    "\n",
    "### Bit model\n",
    "Let $x_{r,f,t}$ for $r \\in \\{0,1,..., R-1\\}$, $f \\in \\{0,1,...,F-1\\}$ and $t \\in \\{0,1,.., T-1\\}$ be binary variables. The decision variable $x_{r,f,t}=1$ if robot $r$ occupies field $f$ at time $t$ and $x_{r,f,t}=0$ if not.\n",
    "\n",
    "### Constraints and optimization modeling\n",
    "\n",
    "#### Constraint 1: Robots can only be on one field at a time\n",
    "This constraint expresses that every robot $r$ occupies exactly one field $f$ at time step $t$. We can formulate this fact as \n",
    "$\\sum_{f=0}^{F-1} x_{r,f,t} = 1 \\forall r, t$. Casting this expression to QUBO form we obtain\n",
    "$$\n",
    "H_{\\rm{field}} = \\sum_{r=0}^{R-1} \\sum_{t=0}^{T-1} \\left( 1 - \\sum_{f=0}^{F-1} x_{r,f,t} \\right)^2 .\n",
    "$$\n",
    "In the above QUBO expression, by fixing $r$ and $t$ you can easily see that if a robot wanted to occupy more than one field or no field, the squared term becomes greater zero. As a result, $H_{\\rm{field}}$ penalizes if a robot wants to occupy multiple field at once.\n",
    "\n",
    "#### Constraint 2: Robots need to keep a safety distance\n",
    "In order to describe that robots need to respect a safety distance among each other, we define a neighborhood $N(f)$. If robot $r$ is occupying field $f$, any other robot $r'$ is not allowed to step into the neighborhood $N(f)$ of robot $r$.\n",
    "<img src=\"pictures/neighborhood.jpg\" width=200/>\n",
    "<font><center><b>Fig.5 </b>The robot is depicted by the blue dot and occupying field $f$. The neighborhood $N(f)$ of allowed fields to move to is shown in orange. </center></font>\n",
    "With the following expression we constrain that at each time step no other robot $r_1$ is within robot's $r_0$ neighborhood\n",
    "$$\n",
    "H_{\\rm{single}} = \\sum_{t=0}^{T-1} \\sum_{r_0=0}^{R-2} \\sum_{r_1=r_0+1}^{R-1} \\sum_{f_0=0}^{F-1} \\sum_{f_1\\in N(f_0)} x_{r_0,f_0,t} x_{r_1, f_1, t} \n",
    "$$\n",
    "\n",
    "#### Constraint 3: Robots are allowed to move in neighborhood\n",
    "Robots are allowed to move left right, up, down and diagonal, however only within their neighborhood $N(f)$. This means we want to penalize moves which are outside the allowed neighborhood, i.e., if the blue robot in **Fig.5** moves to a blue field.\n",
    "This constraint is described by the QUBO term\n",
    "$$\n",
    "H_{\\rm{step}} = \\sum_{t=0}^{T-2} \\sum_{r=0}^{R-1} \\sum_{f_0=0}^{F-1} \\sum_{f_1\\not\\in N(f_0)} x_{r,f_0,t} x_{r,f_1,t+1}\n",
    "$$\n",
    "Here, the summation over fields excludes the allowed (orange) fields, $f_1\\not\\in N(f_0)$. Differently speaking, it is favorable for the robot to move in its neighborhood.\n",
    "\n",
    "#### Constraint 4: Robots should keep moving\n",
    "As long as the robots have not reached the destination, they should keep moving. We can write this constraint as a QUBO term in the following way: \n",
    "$$\n",
    "H_{\\rm{keep-moving}} = \\sum_{t=0}^{T-2} \\sum_{r=0}^{R-1} \\sum_{f\\neq d_r} x_{r,f,t} x_{r,f,t+1} \\, .\n",
    "$$\n",
    "Notice, that we explicitly exclude the destination field $d_r$ in the summation. As a result, if a robot reaches its destination, it is allowed to move away from it in the next time step, i.e., it is not \"pinned\" to it. In some scenarios this might be favorable, e.g., if the robot on the destination field moves away from it, in order to let another robot pass. We view this constraint as a soft constraint forbidding robots to stroll. The weight of this constraint should be chosen in alignment with the underlying use case.\n",
    "\n",
    "#### Constraint 5: Robots reaching their destination\n",
    "Next, we want to enforce that the robot stays in the destination once it has reached it. Further moves are penalized by\n",
    "$$\n",
    "H_{\\rm{destination}} = \\sum_{t=0}^{T-2} \\sum_{r=0}^{R-1} x_{r,d_r,t} \\left ( 1 - x_{r,d_r,t+1} \\right ) \\, .\n",
    "$$\n",
    "As it is the case for constraint 4, constraint 5 is a soft constraint.\n",
    "\n",
    "\n",
    "**Remark on constraint 4 and constraint 5:** These two  compete, depending on the actual scenario/business case we want to describe using the PathOptimizer. Imagine the scenario of optimizing air traffic around an airport with lots of landing and take-offs. The destination of the airplane (robot) would be the airport, and an airplane which has landed should stay on ground. In this example constraint 5 should be enforced over constraint 4.\n",
    "In another scenario, it might be preferable if robots are allowed to move away from a destination again. Imagine warehouse robots and a robot already at its destination has to moves, in order to let another robot pass. Here, it would be preferable to enforce constraint 4 over constraint 5.\n",
    "\n",
    "#### Optimization: Distance\n",
    "In the end, we want to optimize the distance (moves) the robots travel. This is done by minimizing\n",
    "$$\n",
    "H_{\\rm{dist}} = \\sum_{t=0}^{T-2} \\sum_{r=0}^{R-1} \\sum_{f_0=0}^{F-1} \\sum_{f_1=0}^{F-1} dist_{f_0, f_1} x_{r,f_0,t}x_{r,f_1,t+1} \\, ,\n",
    "$$\n",
    "where we sum each robot's steps over all possible steps, weighted by the corresponding distance $\\rm{dist}$ between start and destination field of the respective step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get engaged again!\n",
    "\n",
    "Now, that you have a better understanding of the model behind the game, please execute the following cell. You can now start to try different QUBO and annealing settings and see how their change influences the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from sources.Optimizer_Pathoptimizer_tutorial import * \n",
    "\n",
    "optimizer = Optimizer(PathoptimizerModel(), immediate_load=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem formulation as QUBO II\n",
    "\n",
    "Please follow this link [pathoptimizer](sources/Optimizer_Pathoptimizer_tutorial.py) to revisit the code.\n",
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
