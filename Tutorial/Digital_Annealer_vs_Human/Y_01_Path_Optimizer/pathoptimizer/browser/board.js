// does not work due to missing referrer
// import d3 from "http://localhost:8888/files/python_projects/da-pathoptimizer-widget/pathoptimizer_new/browser/d3.min.js";
// import * as d3 from "https://esm.sh/d3@7";
// import { d3 } from 'http://localhost:8888/files/python_projects/da-pathoptimizer-widget/pathoptimizer_new/browser/d3.min';

// Custom View. Renders the widget model.
var colors = ["#4e79a7", "#f28e2c", "#e15759", "#76b7b2", "#59a14f", "#edc949", "#af7aa1", "#ff9da7", "#9c755f", "#bab0ab"];
var dist = 2;
var segment_width = 75;
var segment_height = 75;
var ATTRIBUTES = ['width', 'height', 'excluded', 'title', 'org', 'dest', 'paths', 'scale', 'aura', 'time_tick', 'level_finished'];
var DEFAULTS = {
    width: 5, height: 5, excluded: [], name: 'Level x', da_score: 'n.a.', title: 'New board', teaser: 'News about combinatorial optimization',
    org: [], dest: [], paths: [], scale: 100, aura: 0.5, time_tick: 0, max_time_tick: 0, level_finished: false, game_over: false, value: {not_set: true},
    robot_image: ''
};

function get_unique_counter() {
    if (typeof board_view_unique_conter == 'undefined')
        window.board_view_unique_conter = 0;
    return board_view_unique_conter++;
}

/**
 * The BoardView provides the GUI for the pathoptimizer widget. It can run in 2 modes, a play mode and an
 * edit mode.
 *
 * Edit mode:
 *
 * When the widget is initialized with a ``value`` it runs in edit mode. This mode allows to setup
 * a game. Change of borad dimensions and the number and positions of robots can be done by modifying the
 * ``height`` and ``width`` and the ``org``, ``dest`` and ``paths`` attributes in python. The origin and destination
 * for each robot can be defined by dragging the circles and squares of the respective colors and forbidden fields
 * are toggled by just clicking to a field. Each change in definition updates the ``value`` attribute, which holds
 * the definition of the game. Internally the class discriminates the mode by attribute ``play_mode==false``.
 *
 * Play mode:
 * Initialization without ``value`` attribute runs the widget in play mode. In this mode the definition
 * has to be given using the parameters ``width``, ``height``, ``excluded``, ``title``, ``org``, ``dest``,
 * ``paths``, ``scale`` and ``aura``. ``time_tick`` is a bidirectional parameter. The robots can be set to a certain
 * time tick. If new steps are created by playing then ``time_tick`` is set to the final position.
 ``max_time_tick`` holds the maximum time tick and is increased when new steps are created.
 ``level_finished`` indicated that all robots are on the destination position and the game is over.,
 *
 * @type {any}
 */
class BoardView {
    constructor(model, el) {  // Constructor
        this.model = model;
        this.el = el;
        this.robot_image = this.model.get('robot_image');
        if(typeof this.robot_image == 'undefined')
            this.robot_image = DEFAULTS.robot_image;
        if(this.robot_image)
            this.robot_image_url = window.location.pathname.replace(/\/notebooks\/|\/(lab|doc)(\/workspaces)?(\/auto-S)?(\/tree)?\//, '/files/').replace(/[^/]*$/, 'pathoptimizer/browser/' + this.robot_image)
        else
            this.robot_image_url = false;
    }

    render() {
        function loadCss(url) {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = url;
            document.getElementsByTagName("head")[0].appendChild(link);
        }

        //loadCss('BoardChartModule/BoardChart.css')
        this.scale = 100.0;
        this.aura = 0.5
        this.dx = 75;
        this.dy = 75;
        this.fields = [];
        this.excluded_points = [];
        this.lines = [];
        this.level_finished = false;
        this.replay_running = false;

        // Python -> JavaScript update
        var current = this;
        this.model.on('change:width', () => current.width_changed());
        this.model.on('change:height', () => current.height_changed());
        this.model.on('change:title', () => current.title_changed());
        this.model.on('change:org', () => current.org_changed());
        this.model.on('change:dest', () => current.dest_changed());
        this.model.on('change:excluded', () => current.excluded_changed());
        this.model.on('change:paths', () => current.paths_changed());
        this.model.on('change:scale', () => current.scale_changed());
        this.model.on('change:aura', () => current.aura_changed());
        this.model.on('change:time_tick', () => current.time_tick_changed());
        this.model.on('change:value', () => current.value_changed());

        this.play_mode = typeof this.model.get('value') == 'undefimed' | this.model.get('value').not_set | false;
        if (this.play_mode) {
            this.get_all_attributes();

            this.robot_touched = Array(this.paths.length).fill(true);
            this.fill_paths_length();
        } else
            this.get_all_value_attributes();

        this.excluded_points_update();

        this.set_svg();

        this.board_update();

        this.draw_paths();
        this.draw_robots();

        this.scale_paths();
        this.scale_robots();
        this.aura_robots();

        this.activity_checker_active = false;
        this.create_activity_checker();
    }

    ///////////////////////////////////////////////////////////////////////
    // data retrieval methods
    ///////////////////////////////////////////////////////////////////////

    get_all_value_attributes() {
        var value = this.model.get('value');
        for (var index in ATTRIBUTES) {
            var name = ATTRIBUTES[index];
            this[name] = (name in value) ? value[name] : this.model.get(name);
        }
    }

    get_attribute(name) {
        this[name] = this.model.get(name);
        if(typeof this[name] == 'undefined')
            this[name] = DEFAULTS[name];
        if(typeof DEFAULTS[name] == 'number')
            this[name] *= 1;

    }

    get_all_attributes() {
        for (var index in ATTRIBUTES)
            this.get_attribute(ATTRIBUTES[index]);
    }

    ///////////////////////////////////////////////////////////////////////
    // change handler
    ///////////////////////////////////////////////////////////////////////

    width_changed() {
        this.get_attribute('width');
        this.excluded_points_update();
        this.board_update();
    }

    height_changed() {
        this.get_attribute('height');
        this.excluded_points_update();
        this.board_update();
    }

    title_changed() {
        this.get_attribute('title');
        this.board_update();
    }

    org_changed() {
        this.get_attribute('org');
        this.board_update();
    }

    dest_changed() {
        this.get_attribute('dest');
        this.board_update();
    }

    excluded_changed() {
        this.get_attribute('excluded');
        this.excluded_points_update();
        this.board_update();
    }

    paths_changed() {
        this.get_attribute('paths');
        this.fill_paths_length();
        // initialize all robots as touched to start a new round with first drag
        this.robot_touched = Array(this.paths.length).fill(true);
        this.board_update();
        this.draw_paths();
        this.draw_robots(true);
        this.scale_paths();
        this.scale_robots();
    }

    time_tick_changed() {
        this.get_attribute('time_tick');
        this.report_activity()
        if(this.replay_running)
            this.stop_replay(this.time_tick)
        else
            this.scale_robots();

    }

    scale_changed() {
        this.get_attribute('scale');
        this.dx_dy_update();
        this.scale_board();
        this.scale_paths();
        this.scale_robots();
    }

    aura_changed() {
        this.get_attribute('aura');
        this.aura_robots();
    }

    value_changed() {
        this.get_all_value_attributes();

        this.excluded_points_update();
        this.dx_dy_update();

        this.board_update();
        this.draw_paths();
        this.draw_robots();

        this.scale_board();
        this.scale_paths();
        this.scale_robots();
    }

    ///////////////////////////////////////////////////////////////////////
    // update objects
    ///////////////////////////////////////////////////////////////////////

    /**
     * Update filed list with x, y coordinates, org, dest and excluded fields
     */
    fields_update() {
        this.update_org_dest();
        this.dx_dy_update();
        var fields = this.fields = [];
        for (var i = 0; i < this.width * this.height; i++) {
            var x = i % this.width;
            var y = Math.floor(i / this.width);
            fields.push({
                number: i, x: x, y: y,
                excluded: this.excluded_points.indexOf(x + ',' + y) >= 0,
                org: -1, dest: -1
            });
        }
        this.org.forEach(function (field, robot) {
            fields[field].org = robot;
        });
        this.dest.forEach(function (field, robot) {
            fields[field].dest = robot;
        });
    }

    /**
     * Create array with two dimensional x,y keys for excluded fields.
     */
    excluded_points_update(){
        this.excluded_points = [];
        for (var i in this.excluded) {
            var x = this.excluded[i] % this.width;
            var y = Math.floor(this.excluded[i] / this.width);
            this.excluded_points.push(x + ',' + y);
        }
    }

    board_update() {
        this.fields_update()
        this.draw_board();
        this.scale_board();
        this.value_update();
    }

    /**
     * Updte dx / dx properties with respect to actual scale factor
     */
    dx_dy_update() {
        this.dx = segment_width * this.scale / 100.0;
        this.dy = segment_height * this.scale / 100.0;
    }

    value_update() {
        function equals(a, b) {
            if (a === b) {
                return true;
            } else {
                if (typeof (a) == 'object' && typeof (b) == 'object') {
                    for (var attr in a) {
                        if (typeof (b[attr]) == 'undefined' || !equals(a[attr], b[attr]))
                            return false;
                    }
                    for (var attr in b) {
                        if (typeof (a[attr]) == 'undefined')
                            return false;
                    }
                    return true;
                }
                return false;
            }
        }

        // seems that new object is needed to get reliable update
        if (!this.play_mode) {
            this.excluded = [];
            for (var i = this.excluded_points.length - 1; i >= 0; i--) {
                var point = this.excluded_points[i].split(',');
                point[0] *= 1;
                point[1] *= 1;
                var field = point[0] + this.width * point[1];
                if (point[0] < this.width && point[1] < this.height)
                    this.excluded.push(field);
            }
            this.excluded.sort(); // important to avoid value changes only reordering members
            var new_value = {
                width: this.width, height: this.height, excluded: this.excluded,
                title: this.title, org: this.org, dest: this.dest, scale: this.scale, aura: this.aura
            };
            var value = this.model.get('value');
            if (!equals(new_value, value)) {
                //this.model.set('value', {});
                this.model.set('value', new_value);
                this.model.save_changes();
            }
        }
    }

    time_tick_update() {
        try {
            //console.log('time_tick: '+this.time_tick);
            this.model.set('time_tick', this.time_tick);
            this.model.save_changes();
        } catch (e) {
        }
    }

    max_time_tick_update() {
        try {
            // console.log('max_time_tick: '+this.max_time_tick);
            this.model.set('max_time_tick', this.max_time_tick);
            this.model.save_changes();
        } catch (e) {
        }
    }

    update_org_dest() {
        // set all positions within current field
        var max_field = this.width * this.height - 1;
        this.org = this.org.map(x => Math.min(x, max_field));
        this.dest = this.dest.map(x => Math.min(x, max_field));
    }

    find_next_field(x_pos, y_pos) {
        var x_index = Math.floor(x_pos / this.dx);
        var y_index = this.height - 1 - Math.floor(y_pos / this.dy);
        var field = x_index + this.width * y_index;
        return field;
    }

    find_next_valid_robot_field(robot, x_pos, y_pos) {
        var candidates = this.find_candidates(robot, x_pos, y_pos);
        var x_index = Math.floor(x_pos / this.dx);
        var y_index = this.height - 1 - Math.floor(y_pos / this.dy);
        var best_found = null;
        var min_dist = this.width + this.height;
        for (var i = 0; i < candidates.length; i++) {
            var dist = Math.sqrt((candidates[i].x - x_index) ** 2 + (candidates[i].y - y_index) ** 2);
            if (dist < min_dist) {
                min_dist = dist;
                best_found = candidates[i];
            }
        }

        return best_found
    }

    find_candidates(robot) {
        var robot_start_field = this.paths[robot][Math.max(0, this.time_tick - 1)];
        var robot_start_x = robot_start_field % this.width;
        var robot_start_y = Math.floor(robot_start_field / this.width);
        var candidates = [];
        var other_robots_auras = this.find_others_robots_auras(robot);
        for (var x = Math.max(0, robot_start_x - 1); x <= Math.min(this.width - 1, robot_start_x + 1); x++)
            for (var y = Math.max(0, robot_start_y - 1); y <= Math.min(this.height - 1, robot_start_y + 1); y++)
                if (this.excluded_points.indexOf(x + ',' + y) < 0 && other_robots_auras.indexOf(x + ',' + y) < 0) {
                    candidates.push({x: x, y: y, field: x + y * this.width});
                }
        return candidates
    }

    fill_paths_length(min_length = 1) {
        // bring all paths to same length and at least length 1
        var step, update_further = true;
        for (step = 0, update_further = true; update_further; step++) {
            update_further = (step < min_length - 1);
            for (var robot = 0; robot < this.paths.length; robot++) {
                if (step >= this.paths[robot].length)
                    this.paths[robot][step] = step == 0 ? this.org[robot] : this.paths[robot][step - 1];
                if (step < this.paths[robot].length - 1)
                    update_further = true;
            }
        }
        if(this.paths.length > 0) {
            this.max_time_tick = this.paths[0].length - 1;
            this.max_time_tick_update();
        }
    }

    prep_robot_drag(robot) {
        // robot is touched again, so we start a new step
        if (this.robot_touched[robot]) {
            // don't start a new step if there are conflicts
            if(this.any_conflict())
                return false;
            this.robot_touched = Array(this.paths.length).fill(false);
            this.fill_paths_length(this.paths[robot].length + 1);
        }
        this.robot_touched[robot] = true;
        this.time_tick = this.paths[robot].length - 1;
        this.time_tick_update();
        return true;
    }

    /**
     * figure out, if currently all robots are touched. Then remove class robot_shelved to signal select ability.
     */
    post_robot_drag() {
        var current_view = this;
        var all_touched = true;
        for(var robot in this.robot_touched)
            if(! this.robot_touched[robot])
                all_touched = false;
        if(all_touched)
            this.svg.selectAll('.robot').classed('robot_shelved', false);
        this.svg.selectAll('.robot').classed('robot_conflict', function (path, robot){return current_view.has_conflict(robot)});
    }

    /**
     * Find auras of other already touched and possibly conflicting robots. Untouched robots still have a chance
     * to move away and are not considered.
     * @param robot
     * @returns {[]}
     */
    find_others_robots_auras(robot) {
        var other_robots_auras = [];
        for (var other_robot = 0; other_robot < this.paths.length; other_robot++) {
            if (other_robot === robot || ! this.robot_touched[other_robot])
                continue;
            var robot_current_field = this.paths[other_robot][Math.max(0, this.time_tick)];
            var robot_current_x = robot_current_field % this.width;
            var robot_current_y = Math.floor(robot_current_field / this.width);
            for (var x = Math.max(0, robot_current_x - 1); x <= Math.min(this.width - 1, robot_current_x + 1); x++)
                for (var y = Math.max(0, robot_current_y - 1); y <= Math.min(this.height - 1, robot_current_y + 1); y++)
                    other_robots_auras.push(x + ',' + y);
        }
        return other_robots_auras
    }

    /**
     * Check if the robot with the given index has a conflict. Conflict means another robot, which has already moved
     * in the current step, inside the robots's aura. So this robot has to move in the current step to resolve the conflict.
     * @param robot index
     * @returns {boolean} conflict flag
     */
    has_conflict(robot){
        var conflicted_fields = this.find_others_robots_auras(robot);
        var robot_field_index = this.paths[robot][Math.max(0, this.time_tick)];
        var robot_field = +(robot_field_index % this.width) + ',' + Math.floor(robot_field_index / this.width);
        return conflicted_fields.indexOf(robot_field) >= 0;
    }

    /**
     * Check if any robot has a conflict, that needs to be resolved before entering next step.
     * @returns {boolean}
     */
    any_conflict(){
        for (var robot = 0; robot < this.paths.length; robot++) {
            if(this.has_conflict(robot))
                return true;
        }
        return false;
    }

    highlight_candidates(candidates) {
        var current_view = this;

        var field_visuals = this.svg.selectAll('rect.field', 'rect.field_excluded')
            .classed('field_high', function (field) {
                for (var candidate = 0; candidate < candidates.length; candidate++) {
                    var current_candidate = candidates[candidate];
                    if (field.number == current_candidate.field) {
                        return true;
                    }
                }
                return false;});
            /*.style("fill", function (field) {
                for (var candidate = 0; candidate < candidates.length; candidate++) {
                    var current_candidate = candidates[candidate];
                    if (field.number == current_candidate.field) {
                        return colors[0]
                    }
                }
            })*/
    }

    check_targets() {
        for (var robot = 0; robot < this.paths.length; robot++)
            if (this.paths[robot][this.paths[robot].length - 1] != this.dest[robot])
                return;
        this.level_finished = true;
        this.model.set('level_finished', this.level_finished);
        //alert('Congratulation!!!\n\nTarget reached in ' + this.max_time_tick + ' steps.');
    }

    ///////////////////////////////////////////////////////////////////////
    // draw methods
    ///////////////////////////////////////////////////////////////////////

    set_svg() {
        // remove eventually existing svg
        this.svg = d3.select(this.el).selectAll('svg').remove();
        // create new svg
        this.svg = d3.select(this.el).append('svg')
            .attr('class', 'boardchart');

        this.uid = get_unique_counter();
        var defs = this.svg.append("defs");
        // create filter with id #drop_shadow_<id>
        // height=150% so that the shadow is not clipped
        var filter = defs.append("filter")
            .attr("id", "drop_shadow_" + this.uid)
            .attr("height", "150%")
            .attr("width", "150%");

        // SourceAlpha refers to opacity of graphic that this filter will be applied to
        // convolve that with a Gaussian with standard deviation 3 and store result
        // in blur
        filter.append("feGaussianBlur")
            .attr("in", "SourceAlpha")
            .attr("stdDeviation", 5)
            .attr("result", "blur");

        // translate output of Gaussian blur to the right and downwards with 2px
        // store result in offsetBlur
        filter.append("feOffset")
            .attr("in", "blur")
            .attr("dx", 5)
            .attr("dy", 5)
            .attr("result", "offsetBlur");

        // overlay original SourceGraphic over translated blurred opacity by using
        // feMerge filter. Order of specifying inputs is important!
        var feMerge = filter.append("feMerge");

        feMerge.append("feMergeNode")
            .attr("in", "offsetBlur")
        feMerge.append("feMergeNode")
            .attr("in", "SourceGraphic");
    }

    draw_board() {
        var current_view = this;
        var field_visuals = this.svg.selectAll('rect.field, rect.field_excluded')
            .data(this.fields);
        field_visuals
            .enter().append('rect')
            .merge(field_visuals)
            .classed('field', function (field) {
                return !field.excluded;
            })
            .classed('field_excluded', function (field) {
                return field.excluded;
            })
            .on("click", function (field) {
                if (current_view.play_mode)
                    return;
                var point = field.x + ',' + field.y;
                var found_index = current_view.excluded_points.indexOf(point);
                if (found_index < 0) {
                    current_view.excluded_points.push(point);
                    field.excluded = true;
                    d3.select(this).classed('field', false);
                    d3.select(this).classed('field_excluded', true);
                } else {
                    current_view.excluded_points.splice(found_index, 1);
                    field.excluded = false;
                    d3.select(this).classed('field', true);
                    d3.select(this).classed('field_excluded', false);
                }
                current_view.value_update();
            })
            .on("mouseover",  function (field) {
                current_view.stop_replay()
            });
        field_visuals.exit().remove();


        function org_dragstarted(d) {
            d3.select(this).raise().classed("org_active", true);
        }

        function org_dragged(d) {
            d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);
        }

        function org_dragended(d) {
            var data = this.__data__;
            d3.select(this).classed("org_active", false);
            var field = current_view.find_next_field(d3.event.x, d3.event.y);
            current_view.org[data.robot] = field;
            current_view.scale_board();
            current_view.value_update();
        }

        // remove first to bring all to the top in front of new fields
        var org_visuals = this.svg.selectAll('circle.org').remove();
        org_visuals = this.svg.selectAll('circle.org')
            .data(this.org.map((f, r) => {
                return {field: f, robot: r}
            }))
            .enter().append('circle')
            .attr('class', 'org')
            .attr('fill', function (field, robot) {
                return colors[robot % colors.length]
            });
        if (!this.play_mode)
            org_visuals
                .call(d3.drag()
                    //.on("start", org_dragstarted)
                    .on("drag", org_dragged)
                    .on("end", org_dragended)
                );

        function dest_dragged(d) {
            d3.select(this).attr("x", d.x = d3.event.x - current_view.dx / 8).attr("y", d.y = d3.event.y - current_view.dy / 8);
        }

        function dest_dragended(d) {
            var data = this.__data__;
            d3.select(this).classed("dest_active", false);
            var field = current_view.find_next_field(d3.event.x, d3.event.y);
            current_view.dest[data.robot] = field;
            current_view.scale_board();
            current_view.value_update();
        }

        var dest_visuals = this.svg.selectAll('rect.dest').remove();
        dest_visuals = this.svg.selectAll('rect.dest')
            .data(this.dest.map((f, r) => {
                return {field: f, robot: r}
            }))
            .enter().append('rect')
            .attr('class', 'dest')
            .attr('fill', function (field, robot) {
                return colors[robot % colors.length]
            });

        if (!this.play_mode)
            dest_visuals
                .call(d3.drag()
                    //.on("start", dragstarted)
                    .on("drag", dest_dragged)
                    .on("end", dest_dragended));

        var title_visual = this.svg.selectAll('text.title')
            .remove();
        title_visual = this.svg.selectAll('text.title')
            .data([this.title])
        title_visual
            .enter().append('text')
            .attr('class', 'title')
            .attr("x", 0)
            .attr("y", 20)

            .text(function (d) {
                return d
            });
        title_visual
            .text(function (d) {
                return d
            });
    }

    draw_paths() {
        var line_visuals = this.svg.selectAll('path.line')
            .data(this.paths);
        line_visuals
            .exit().remove();
        line_visuals
            .enter().append('path')
            .attr('class', 'line');
    }

    draw_robots(renew=false) {
        var current_view = this;
        var paths = this.paths;

        if(renew){
            this.svg.selectAll('rect.aura').remove();

            this.svg.selectAll('.robot').remove;

        }

        var aura_visuals = this.svg.selectAll('rect.aura')
            .data(paths);
        aura_visuals
            .enter().append('rect')
            .merge(aura_visuals)
            .attr('class', 'aura')
            .attr('fill', function (path, robot) {
                return colors[robot % colors.length]
            })
            .attr('stroke', function (path, robot) {
                return colors[robot % colors.length]
            });
        aura_visuals
            .exit().remove();

        function dragended(line, robot) {
            var data = this.__data__;
            if(d3.select(this).classed("robot_active")){
                d3.select(this).classed("robot_active", false);
                d3.select(this).classed("robot_shelved", true);
                current_view.highlight_candidates([]);
                var next_valid_robot_field = current_view.find_next_valid_robot_field(robot, d3.event.x, d3.event.y);
                // positioning t.b.d.
                current_view.paths[robot][current_view.paths[robot].length - 1] = next_valid_robot_field.field;
                //current_view.paths[robot][current_view.time_tick] = next_valid_robot_field.field;
                //for(var robot_idx = 0; robot_idx < current_view.paths.length; robot_idx++)
                //    current_view.paths[robot_idx][current_view.time_tick + 1] = current_view.paths[robot_idx][current_view.time_tick];
                //current_view.draw_robots();
                current_view.post_robot_drag();
                current_view.scale_paths();
                current_view.scale_robots();
                current_view.check_targets();
                current_view.report_activity();
            }
            d3.event.returnValue = false;
            return false;
        }

        function dragstarted(line, robot) {
            if(current_view.prep_robot_drag(robot)) {
                var robot_start_field = current_view.paths[robot][Math.max(0, current_view.paths[robot].length - 1)];
                var robot_start_x = robot_start_field % current_view.width;
                var robot_start_y = Math.floor(robot_start_field / current_view.width);
                var candidates = current_view.find_candidates(robot, robot_start_x, robot_start_y);
                if(candidates.length == 0){
                    current_view.game_over = true;
                    current_view.model.set('game_over', current_view.game_over);
                    return false;
                }
                current_view.highlight_candidates(candidates);
                d3.select(this).classed("robot_active", true);
            }
            d3.event.returnValue = false;
            return false;
        }

        function dragged(line, robot) {
            var dx = current_view.dx;
            var dy = current_view.dy;
            if (d3.select(this).classed("robot_active"))
                d3.select(this).attr("cx", d3.event.x).attr("cy", d3.event.y).attr("x", d3.event.x - dx / 4).attr("y", d3.event.y - dy /  4);
            d3.event.returnValue = false;
            current_view.report_activity();
            return false;
        }

        var robot_visuals = this.svg.selectAll('.robot')
            .data(paths);
        robot_visuals
            .enter().append((this.robot_image_url)?'image':'circle')
            .merge(robot_visuals)
            .attr('class', 'robot')
            .attr((this.robot_image_url)?'href':'fill', (this.robot_image_url)?this.robot_image_url:function (path, robot) {
                return colors[robot % colors.length]
            })
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended)
            );
        robot_visuals
            .exit().remove()
        ;
    }

    ///////////////////////////////////////////////////////////////////////
    // scale and move methods
    ///////////////////////////////////////////////////////////////////////

    scale_board() {
        var dx = this.dx;
        var dy = this.dy;
        var width = this.width;
        var height = this.height;
        this.svg
            .attr('width', Math.max(1, width * dx ))
            .attr('height', Math.max(1, height * dy ));

        var field_visuals = this.svg.selectAll('rect.field, rect.field_excluded')
            .data(this.fields);
        field_visuals
            .classed('field', function (field) {
                return !field.excluded;
            })
            .classed('field_excluded', function (field) {
                return field.excluded;
            })
            .attr("x", function (d, i) {
                return dx * (i % width)
            })
            .attr("y", function (d, i) {
                return dy * (height - 1 - Math.floor(i / width))
            })
            .attr("width", dx - dist)
            .attr("height", dy - dist);

        var org_visuals = this.svg.selectAll('circle.org')
            .data(this.org.map((f, r) => {
                return {field: f, robot: r}
            }));
        org_visuals
            .attr("cx", function (d) {
                return dx * (d.field % width) + dx / 2
            })
            .attr("cy", function (d) {
                return dy * (height - 1 - Math.floor(d.field / width)) + dy / 2
            })
            .attr("r", this.dx / 8);

        var dest_visuals = this.svg.selectAll('rect.dest')
            .data(this.dest.map((f, r) => {
                return {field: f, robot: r}
            }));
        dest_visuals
            .attr("x", function (d) {
                return dx * (d.field % width) + 3 * dx / 8
            })
            .attr("y", function (d) {
                return dy * (height - 1 - Math.floor(d.field / width)) + 3 * dy / 8
            })
            .attr("width", dx / 4)
            .attr("height", dy / 4);
    }

    scale_paths() {
        var dx = this.dx;
        var dy = this.dy;
        var width = this.width;
        var height = this.height;

        var lines = this.lines = [];
        this.paths.forEach(function (path, robot) {
            var line = [];
            path.forEach(function (field, index) {
                line.push({
                    x: dx * (field % width) + dx / 2,
                    y: dy * (height - 1 - Math.floor(field / width)) + dy / 2
                })
            });
            lines.push(line);
        });

        var line_visuals = this.svg.selectAll('path.line')
            .data(this.lines);

        this.draw_breadcrumbs();

    }

    draw_breadcrumbs() {
        var time_tick = this.time_tick;

        //This is the accessor function we talked about above
        var lineFunction = d3.line()
                .x(function (d) {
                    return d.x;
                })
                .y(function (d) {
                    return d.y;
                })
            //.interpolate("linear")
            //.curve(d3.curveBundle.beta(1.0))
        ;

        var line_visuals = this.svg.selectAll('path.line')
            .transition()
            //.duration(function(d){return d.chart_index_0_concerned ? 0 : 1000;})
            .attr("d", function (line, robot) {
                return lineFunction(line.slice(0, time_tick + 1).concat(Array(line.length - time_tick - 1).fill(line[time_tick])));
            })
            .attr("stroke", function (line, robot) {
                return colors[robot % colors.length]
            })
            .attr("stroke-width", 2)
            .attr("fill", "none");
    }

    scale_robots() {
        var time_tick = this.time_tick;
        var dx = this.dx;
        var dy = this.dy;
        var aura_round = dx / 5;
        var lines = this.lines;
        var robot_visuals = this.svg.selectAll('.robot')
            .data(lines)
            .transition()
            .attr("cx", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].x;
            })
            .attr("cy", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].y;
            })
            .attr("r", dx / 4)
            .attr("x", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].x - dx / 4;
            })
            .attr("y", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].y - dx / 4;
            })
            .attr("width", dx / 2)
            .style("filter", "url(#drop_shadow_" + this.uid + ")");
        var aura_visuals = this.svg.selectAll('rect.aura')
            .data(lines)
            .transition()
            .attr("x", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].x - dx;
            })
            .attr("y", function (line, robot) {
                return line[Math.min(line.length - 1, time_tick)].y - dy;
            })
            .attr("rx", aura_round)
            .attr("ry", aura_round)
            .attr("width", 2 * dx)
            .attr("height", 2 * dy);

        this.scale_paths()
    }

    aura_robots() {
        var lines = this.lines;
        var aura_opacity = this.aura;
        var aura_visuals = this.svg.selectAll('rect.aura')
            .data(lines)
            .style('opacity', aura_opacity);
    }

    /**
     * Advance the time stamp for the last activity to the current time.
     * @returns -
     */
    report_activity(){
        this.last_activity_at = (new Date()).valueOf();
        // console.log(this.last_activity_at);
    }

    /**
     * Check if last activity is older than 30 seconds. If so, the replay is stated as animation.
     * @returns -
     */
    check_activity(){
        var idle_time = (new Date()).valueOf() - this.last_activity_at;
        // console.log(this.interval_check_activity_id + ': ' + idle_time);
        if(idle_time > 30000)
            this.start_replay();
    }

    /**
     * Create and activity check that periodically every second checks the last activity. The id of the interval
     * timer is stored for deactivation when widget is unloaded (to avoid endless events on zombie widgets in the background)
     * @returns -
     */
    create_activity_checker(){
        if(this.activity_checker_active)
            return;
        var current = this;
        this.report_activity();
        this.interval_check_activity_id = setInterval(function(){current.check_activity();}, 1000);
        d3.select(this.el).on('DOMNodeRemovedFromDocument', function(){current.remove_activity_checker()});
        this.activity_checker_active = true;
        // console.log('id: ' + this.interval_check_activity_id);
    }

    /**
     * Remove the activity check by clearing the interval timer. Also clear the time of an eventually running animation
     * (to avoid endless events on zombie widgets in the background).
     *
     * @returns -
     */
    remove_activity_checker(){
        if(!this.activity_checker_active)
            return;
        try{
            clearInterval(this.interval_check_activity_id);
            this.interval_check_activity_id = false;
        } catch (e) {

        }
        try{
            clearTimeout(this.settimeout_id)
        } catch (e) {

        }
        this.activity_checker_active = false;
    }

    /**
     * Start the replay animation. Store the current time_tick to return to that position when animation is stopped.
     *
     * @returns -
     */
    start_replay(){
        if(! this.replay_running && this.max_time_tick > 0){
            this.svg.selectAll('.robot').classed('robot_shelved', false);
            this.time_tick_on_replay_start = this.time_tick;
            this.time_tick = -1;
            this.replay_running = true;
            this.replay()
        }
    }


    /**
     * Replay the next animation step and advance the time_tick
     *
     * @returns -
     */
    replay(){
        this.time_tick += 1;
        this.scale_robots();
        if(this.replay_running){
            // console.log(this.interval_check_activity_id + ': ' + 'replay tick ' + this.time_tick);
            if(this.time_tick == this.max_time_tick)
                this.time_tick = -1;
            var current = this;
            this.settimeout_id = window.setTimeout(function(){current.replay()}, 500);
        }
    }

    /**
     * Stop the animation and return to the postion before starting animation.
     *
     * @returns -
     */
    stop_replay(tick=null){
        if(this.replay_running){
            this.time_tick = (tick == null ? this.time_tick_on_replay_start : tick) - 1;
            this.replay_running = false;
        }
        this.report_activity();
    }

}


export function render({ model, el }) {
    let view = new BoardView(model, el);
    view.render();
}



/*

export function render({ model, el }) {
  let selection = d3.select(el);
  let getCount = () => model.get("count");
  let button = document.createElement("button");
  button.classList.add("counter-button");
  button.innerHTML = `count is ${getCount()}`;
  button.addEventListener("click", () => {
    model.set("count", getCount() + 1);
    model.save_changes();
  });
  model.on("change:count", () => {
    button.innerHTML = `count is ${getCount()}`;
  });
  el.appendChild(button);
}
*/

//debugger;
