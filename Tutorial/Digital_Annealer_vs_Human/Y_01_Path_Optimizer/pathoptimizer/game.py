from .board import Board

import ipywidgets as widgets
import time
from IPython.display import clear_output, display, Javascript

import copy
import pandas as pd

class Game:
    """
    This class creates a small interactive game in a Jupyter notebook. The mission is to bring the robots
    (big colored circles) from their start position (small circle with same color) to the end position (small square).
    In each step every robot can move one field left, right, up, down or diagonal. Robots have to   maintain a security
    distance, i.e. no other robot may occupy the field next to his position. The graphics shows an aura around each robot.
    These must not overlap after each move, which is the same distance rule. The robots are moved by drag and drop.
    The goal is to finish the mission is as few steps as possible.
    """
    def __init__(self,
                 title:str='',
                 width:int=5,
                 height:int=5,
                 excluded:list=[6, 7, 8, 13, 16, 18],
                 org:list=[10, 20],
                 dest:list=[14, 0],
                 paths:list=[[], []],
                 scale:float=100.0,
                 play_levels:bool=False,
                 ranking_file_name:str='',
                 player_name:str='anonymous',
                 start_level:int=0,
                 robot_image: str = ''
                 ):
        """
        The constructor sets up the playing ground. The ground is a rectangular consisting of squares. These fields
        are numbered starting with 0 in the lower left position and then going left to right in lines and then line
        by line upwards.

        The board is displayed and the game can be started instantly.

        :param title: title displayed in the board
        :param width: width of the board (number of fields)
        :param height: height of the board (number of fields)
        :param excluded: list of excluded field, which can not be occupied by robots
        :param org: list of start positions (field indices)
        :param dest: list of destination positions (field indices)
        :param paths: solution paths as easier starting point. as list of list of field indices
        :param scale: magnification scale for the board
        :param play_levels: Boolean value to tell if player wants to play stored levels or a single game.
        :param ranking_file_name: Optional parameter for filename to store scores in case a player provides his name in the respective test widget. Name without ending.
        :param player_name: Optional parameter for player name
        """
        self.board_title = title
        self.board_size_width = width
        self.board_size_height = height
        self.excluded = excluded
        self.org = org
        self.dest = dest
        self.paths = paths
        self.scale = scale
        self.da_score = 0

        # Save initial parameters in order to reset the game after it has been finished. Deepcopy since several entries in
        # the dictionary are lists.
        self.init_parameters = copy.deepcopy(dict(title=title, width=width, height=height, excluded=excluded, org=org, dest=dest, paths=paths, scale=scale))

        # Slider widget to show the current step and for possibility to show previous steps.
        self.time_tick_slider = widgets.IntSlider(
            value=0, min=0, max=0, description='step',
            description_tooltip = 'Use slider to display previous steps',
            style={'description_width': '35px'}, layout=widgets.Layout(width="250px"))

        # Slider to change board size.
        self.scale_slider = widgets.IntSlider(value=10, min=10, max=500, description='scale')

        # Slider to adjust opacity of rober's auras.
        self.aura_slider = widgets.FloatSlider(value=0.3, min=0.0, max=1.0, description='aura')

        # Widget for the current level. Needed if attribute play_levels is True.
        self.level = widgets.Label(value='0', description='level', layout=widgets.Layout(width='auto', grid_area='level'))
        self.level.add_class('box').add_class('level')

        # Widget showing current number of steps in the game.
        self.steps = widgets.Label(value='0', description='steps', layout=widgets.Layout(width='auto', grid_area='steps'))
        self.steps.add_class('box').add_class('steps')

        # Boolean value to tell if player wants to play stored levels or a single game.
        self.play_levels = play_levels

        self.robot_image = robot_image

        # Widget with name for the file in which to store scores in case player provides his name. Name should be without ending.
        # Widget is disabled if attribute play_levels is True.
        self.ranking_file = widgets.Text(value=ranking_file_name, description='ranking file')

        # Widget for player's name.
        self.player_name = widgets.Text(value=player_name, description='player name')

        # Widget for player's mail address.
        self.player_email = widgets.Text(value='', description='mail address', label='123')

        # Next level button
        image_file = '<div id="img" style="float:left">' + \
                     '<img class="img-smiley" style="vertical-align:left;width:80px;height:120px;" src="" alt="">' + \
                     '</div>'
        self.button_next_level = widgets.Button(description="Next level")
        self.button_next_level.on_click(self.start_next_level)
        self.button_next_level.add_class('next_button')
        self.box_next_level = widgets.HBox(
            (widgets.HTML(value=image_file), self.button_next_level),
            layout=widgets.Layout(width='auto', grid_area='status')).add_class('hidden')

        # Congratulations block to show player current highscores (among previous players and by DA)
        # and for congratulation dialogue.
        self.congratulation_html = widgets.HTML(value='')
        self.congratulation = widgets.VBox(
            (self.congratulation_html, self.box_next_level),
            layout=widgets.Layout(width='auto', grid_area='status'))
        self.congratulation.add_class('box').add_class('status')
        # Picture for congratulation dialogue.


        # Widget to observe if the level is finished. On change event will initialize congratulations event.
        self.level_finished = widgets.Checkbox(value=False)

        # Widget to observe if the game is over. On change event will initialize restart.
        self.game_over = widgets.Checkbox(value=False)

        def game_status_changed(change):
            '''
            On change function for widget level_finished. Upon changing to True, Congratulation procedure is started.
            Afterwards, game is reset if play_levels is False, otherwise the next level is initialized.
            :param change: Dictionary storing new value of widget.
            '''
            if self.game_over.value == True:
                self.congratulation_html.value += '<h1>Game over</h1><p>Please try again ...</p>'
                if self.play_levels:
                    time.sleep(2)
                    self.congratulation_html.value = ''
                    self.ranking_file.disabled = True
                    self.current_level = 0
                    self.init_parameters = copy.deepcopy(self.levels[self.current_level])
                    self.init_board(**self.init_parameters)
            elif change.new and not self.result_shown:
                self.result_shown = True
                congratulation_text = f'Congratulation!\nYou finished the game in {self.time_tick_slider.value} steps.'
                self.congratulation.value = congratulation_text
                ranking = self.add_to_ranking(self.time_tick_slider.value)
                max_time_tick = self.time_tick_slider.value
                self.time_tick_slider.value = 0
                while self.time_tick_slider.value < max_time_tick:
                    time.sleep(0.5)
                    self.time_tick_slider.value += 1
                if ranking is not None:
                    self.congratulation_html.value += '<br>'+f'Your current ranking is {ranking}.'
                self.congratulation_html.value += '<br>'+congratulation_text
                self.box_next_level.remove_class('hidden')
                time.sleep(5)

        self.level_finished.observe(game_status_changed, 'value')
        self.game_over.observe(game_status_changed, 'value')

        # If play_levels, a button to show the DA score of the current level in the congratulations box is added.
        if play_levels:
            self.button_da_score = widgets.Button(description="Show DA score")
            def on_click_da_score(b):
                self.congratulation_html.value += '<br>'+ f'Digital Annealer required {self.da_score} steps to solve level {self.current_level}.'
            self.button_da_score.on_click(on_click_da_score)

        # info block
        self.info_block_html = widgets.HTML(value='<h1>Hello Annealer</h1>')
        self.info_block = widgets.HBox(
            (self.info_block_html,),
            layout=widgets.Layout(width='auto', grid_area='teaser'))
        self.info_block.add_class('box').add_class('teaser')

        # hidden menu
        self.menu = widgets.VBox((
            self.ranking_file,
            self.player_name,
            self.player_email,
            self.scale_slider,
            self.aura_slider), layout=widgets.Layout(display='none'))
        self.menu.add_class('box').add_class('setup')

        # menu button to display or hide the menu
        self.menu_button = widgets.Button(description="...", layout=widgets.Layout(width='30px'))
        self.menu_button.add_class('box_button')
        def on_click_menu_button(b):
            self.menu.layout.display = 'none' if self.menu.layout.display == 'flex' else 'flex'
            self.gui_grid.layout.grid_template_columns = '[col] 200px [col] auto [col] auto [col] 400px' if self.menu.layout.display == 'flex' else '[col] 200px [col] auto [col] auto [col] 60px'
        self.menu_button.on_click(on_click_menu_button)

        # menu control (menu plus menu button)
        self.menu_control = widgets.HBox((
            self.menu,
            self.menu_button
        ), layout=widgets.Layout(width='auto', grid_area='setup'))
        self.menu_control.add_class('box').add_class('setup')

        # control bar
        if play_levels:
            self.control = widgets.HBox((
                self.time_tick_slider,
                self.button_da_score
            ), layout=widgets.Layout(width='auto', grid_area='control'))
        else:
            self.control = widgets.HBox((
                self.time_tick_slider,
            ), layout=widgets.Layout(width='auto', grid_area='control'))
        self.control.add_class('box').add_class('control')

        # game board
        self.game = widgets.Output(layout=widgets.Layout(width='auto', grid_area='board'))
        self.game.add_class('box').add_class('board')

        # Gui containing all widgets
        self.gui_grid = widgets.GridBox(
            children=[self.level, self.steps, self.congratulation, self.control, self.info_block, self.menu_control, self.game],
            layout=widgets.Layout(
                width='100%',
                grid_template_rows='repeat(4, [row] auto  )',
                grid_template_columns='[col] 200px [col] auto [col] auto [col] 60px',
                grid_template_areas='''
                    "level status teaser setup"
                    "steps status teaser setup"
                    "control control teaser setup"
                    "board board board board"
                    ''',
                grid_gap='10px 10px',
                border='0px solid silver')
        )
        display(self.gui_grid)
        display(Javascript(
            """
            let collection = document.getElementsByClassName('img-smiley');
            for (let i = 0; i < collection.length; i++) {
                collection[i].src = window.location.pathname.replace(/\\/notebooks\\/|\\/(lab|doc)(\\/workspaces)?(\\/auto-S)?(\\/tree)?\\//, '/files/').replace(/[^/]*$/, 'pathoptimizer/browser/Smiley.png');
            }
            """))

        # Initialize board to None. Board object is initialized via method init_board.
        self.board_chart = None

        # On change event to show current ranking via method get_ranking once ranking_file's value matches a valid xlsx file
        # with rankings from previous games. Note: Widget ranking_file is disabled if play_levels is True.
        def observe_ranking_file(change):
            if change.new:
                highscore = self.get_ranking()
                if highscore is not None:
                    self.congratulation_html.value += highscore

        self.ranking_file.observe(observe_ranking_file,'value')

        self.board_chart = Board()
        # Define new links
        # self.link_steps = widgets.jslink((self.board_chart, 'max_time_tick'), (self.steps, 'value'))
        def on_new_max_time_tick(change):
            self.steps.value = str(change['new'])
        self.board_chart.observe(on_new_max_time_tick, names='max_time_tick')
        self.link_time_tick_slider_max = widgets.jslink((self.board_chart, 'max_time_tick'),
                                                        (self.time_tick_slider, 'max'))
        self.link_time_tick_slider = widgets.jslink((self.board_chart, 'time_tick'), (self.time_tick_slider, 'value'))
        self.link_aura_slider = widgets.jslink((self.board_chart, 'aura'), (self.aura_slider, 'value'))
        self.link_scale_slider = widgets.jslink((self.board_chart, 'scale'), (self.scale_slider, 'value'))
        self.link_level_finished = widgets.jslink((self.board_chart, 'level_finished'), (self.level_finished, 'value'))
        self.link_game_over = widgets.jslink((self.board_chart, 'game_over'), (self.game_over, 'value'))

        # Initialize current_level to 0. If play_levels is True, initialize board with level 0, otherwise, initialize board
        # with initial parameters.
        self.current_level = start_level
        if self.play_levels:
            self.ranking_file.disabled = True
            file_name_split = ranking_file_name.split('.')
            self.original_file_name = file_name_split[0]
            self.define_levels()
            self.init_parameters = copy.deepcopy(self.levels[self.current_level])
            self.init_board(**self.init_parameters, robot_image=self.robot_image)
        else:
            self.init_board(**self.init_parameters, robot_image=self.robot_image)


    def init_board(self,
                   title: str = 'Pathfinder Game',
                   width: int = 5,
                   height: int = 5,
                   excluded: list = None,  #[6, 7, 8, 13, 16, 18],
                   org: list = None,  #[10, 20],
                   dest: list = None,  #[14, 0],
                   paths: list = None,  #[[], []],
                   scale: float = 100.0,
                   level: str = '',
                   teaser: str = '',
                   da_score = 0,
                   robot_image: str = ''
                   ):
        '''
        This method is a helper method to initialize a Board widget object for the game.
        :param title: Title of the board.
        :param width: Number of horizontal squares of the board.
        :param height: Number of vertical squares of the board.
        :param excluded: List of excluded fields. Numbers are counted through rows from left to right, starting with the lowest row of the board.
        :param org: List of roboters' initial field positions. Numbers are counted through rows from left to right, starting with the lowest row of the board. Length should be same as dest and paths.
        :param org: List of roboters' final field positions. Numbers are counted through rows from left to right, starting with the lowest row of the board. Length should be same as org and paths.
        :param paths: List of initial path lists of robots. For each robot, list should contain an empty list. Length should be same as dest and paths.
        :param scale: Percentage value for size of the board.
        :param level: Name of the board level (key) from dictionary self.levels to initialize.
        :param teaser: Teaser text to be shown in the info block if self.play_levels is True
        '''
        self.board_title = title
        self.board_size_width = width
        self.board_size_height = height
        self.excluded = excluded
        self.org = org
        self.dest = dest
        self.paths = paths
        self.scale = scale

        self.result_shown = False

        self.time_tick_slider.value = 0
        self.time_tick_slider.max = 1
        self.level_finished.value = False
        self.game_over.value = False
        self.robot_image = robot_image

        # If not play_levels, ranking_file remains unchanged, and player can provide mail address and name in respective widgets.
        # Otherwise, teaser is shown in info block, and current level name is appended to ranking file name.
        if not self.play_levels:
            self.player_name.value = ''
            self.player_email.value = ''
        if self.play_levels:
            self.ranking_file.value = ''
            self.ranking_file.value = self.original_file_name + '_level' + level
            self.info_block_html.value = teaser or 'An introduction to Digital Annealer can be found <a href="https://www.fujitsu.com/de/themes/digitalannealer/" target="_blank">here</a>.'
            display(Javascript(
                """
                //window.setTimeout(function(){
                let collection2 = document.getElementsByClassName('img-teaser');
                for (let i = 0; i < collection2.length; i++) {
                    let new_src = collection2[i].src.replace(/\\/notebooks\\/|\\/(lab|doc)(\\/workspaces)?(\\/auto-S)?(\\/tree)?\\//, '/files/');
                    collection2[i].src = new_src;
                }
                //}, 1000);
                """))
        else:
            ranking_file_value = self.ranking_file.value
            self.ranking_file.value = ''
            self.ranking_file.value = ranking_file_value + '_level' + level

        self.da_score = da_score

        self.board_chart.title = self.board_title
        self.board_chart.width = self.board_size_width
        self.board_chart.excluded = self.excluded
        self.board_chart.height = self.board_size_height
        self.board_chart.org = self.org
        self.board_chart.dest = self.dest
        #set different value to be sure to get transmissing
        self.board_chart.paths = []
        self.board_chart.paths = self.paths
        self.board_chart.scale = self.scale
        self.board_chart.time_tick = 0
        self.board_chart.max_time_tick = 0
        self.board_chart.level_finished = False
        self.board_chart.game_over = False
        self.board_chart.robot_image = self.robot_image

        #Set level to current level and display board chart.
        self.level.value = f'Level {level}'
        with self.game:
            clear_output()
            display(self.board_chart)

    def start_next_level(self, value=None):
        self.congratulation_html.value = ''
        self.box_next_level.add_class('hidden')
        if self.play_levels:
            self.ranking_file.disabled = True
            self.current_level = (self.current_level + 1) % len(self.levels)
            self.init_parameters = copy.deepcopy(self.levels[self.current_level])
            self.init_board(**self.init_parameters, robot_image=self.robot_image)
        else:
            self.init_board(**self.init_parameters, robot_image=self.robot_image)



    def add_to_ranking(self, new_value=None):
        '''
        Helper method to add new ranking of new_value steps to the ranking file in case the name of a ranking file and a player
        have been provided in the respective widgets. Note: xlsx file must be closed to add ranking.
        :param new_value: Number of steps player required to solve the game.
        :returns: Player's ranking as given by the ranking file.
        '''
        if new_value is None or self.player_name.value == '' or self.ranking_file.value == '':
            return
        new_name = self.player_name.value or 'anonymous'
        new_email = self.player_email.value or '-'
        dest_filename = self.ranking_file.value
        # Split file name and append ending .xlsx only if file name is not already ending with .xlsx.
        split_file_name = dest_filename.split('.')
        if len(split_file_name) == 1 or dest_filename.split('.')[1] != 'xlsx':
            dest_filename += '.xlsx'

        # Try to load existing file or create new file if file is not yet existing.
        try:
            df = pd.read_excel(dest_filename)
        except:
            df = pd.DataFrame(columns=['Player Name', 'Number Steps', 'Email Address'])

        df = pd.concat([df, pd.DataFrame([{'Player Name': new_name, 'Number Steps': new_value, 'Email Address': new_email}])], ignore_index=True)
        df.sort_values('Number Steps', axis=0, ascending=True, inplace=True)
        df.reset_index(drop=True, inplace=True)
        ranking = df[(df['Player Name'] == new_name) & (df['Number Steps'] == new_value) & (
                    df['Email Address'] == new_email)].index[0] + 1
        df.to_excel(dest_filename, index=False)

        # Return ranking to display in congratulations box.
        return ranking

    def get_ranking(self):
        '''
        Helper method to retrieve current highscore from ranking file.
        :returns: String containing the current highscore for the game as given by the ranking file.
        '''
        if self.ranking_file.value == '':
            return
        dest_filename = self.ranking_file.value

        # Split file name and append ending .xlsx only if file name is not already ending with .xlsx.
        split_file_name = dest_filename.split('.')
        if len(split_file_name) == 1 or dest_filename.split('.')[1] != 'xlsx':
            dest_filename += '.xlsx'

        # Try to load existing file or create new file if file is not yet existing.
        try:
            df = pd.read_excel(dest_filename)
        except:
            df = pd.DataFrame(columns=['Player Name', 'Number Steps', 'Email Address'])

        if len(df) == 0:
            return
        else:
            highscore = df.loc[0]['Number Steps']
            player = df.loc[0]['Player Name']
            return f'The current highscore is {highscore} steps. It was achieved by {player}.'

    def define_levels(self):
        '''
        Helper method to create dictionary attribute levels, containing as keys the names of the levels and as values the respective
        dictionaries with parameters for the BoardChart objects to be created.
        For each level, the score which Digital Annealer achieved is saved in dictionary attribute da_score.
        '''
        self.levels = [
            dict(level='0', da_score=8, title='Welcome', width=7, height=5, org=[11, 25], dest=[23, 9],
                 excluded=[10, 12, 15, 16, 17, 19, 22, 24, 26, 8], paths=[[], []],
                 teaser="Hi and welcome to the Pathoptimizer game. To play this game, move the colored robots with the mouse until they reach their destination, the square dot with the same color. In each step you can move every robot at most one square left, right, up, down or diagonal. Please note that the halos of the robots must not overlap at the end of the step. In the middle of a step possibly conficting robots will flash and have to be moved away. Try to solve each level with as few steps as possible. Have fun!"),
            dict(level='1', da_score=9, title='Into the heart', width=9, height=6, org=[31, 47, 51], dest=[49, 33, 29],
                 excluded=[11, 12, 14, 15, 19, 25, 27, 30, 32, 35, 36, 37, 38, 39, 4, 41, 42, 43, 44],
                 paths=[[], [], []]),
            dict(level='2', da_score=12, title='What is that?', width=9, height=6, org=[24, 27, 0], dest=[40, 15, 2],
                 excluded=[1, 10, 14, 16, 19, 20, 21, 23, 28, 32, 37, 38, 39, 41, 5, 6, 7], paths=[[], [], []]),
            dict(level='3', da_score=6, title='Caro', width=10, height=6, org=[7, 57, 51, 3], dest=[29, 1, 25, 55],
                 excluded=[0, 2, 22, 24, 26, 28, 31, 33, 35, 37, 4, 50, 52, 54, 6, 8], paths=[[], [], [], []]),
            dict(level='4', da_score=14, title='Think first ...', width=20, height=6, org=[48, 78, 44],
                 dest=[64, 68, 77],
                 excluded=[20, 22, 23, 24, 25, 27, 30, 32, 33, 34, 35, 37, 38, 40, 41, 42, 43, 47, 50, 51, 52, 53, 55,
                           57, 58, 60, 62, 63, 67, 70, 71, 72, 73, 75, 76, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 90,
                           92, 93, 94, 95, 96, 99], paths=[[], [], []]),
            dict(level='5', da_score=8, title='Cross the river', width=13, height=5, org=[52, 1, 25], dest=[7, 27, 30],
                 excluded=[16, 17, 22, 23, 28, 31, 34, 37, 40, 45, 46, 51], paths=[[], [], []]),
            dict(level='6', da_score=7, title='Around the corner', width=6, height=5, org=[26, 0, 15], dest=[3, 16, 6],
                 excluded=[10, 13, 19, 7, 8, 9], paths=[[], [], []]),
            dict(level='7', da_score=18, title='Service offer', width=16, height=5, org=[74, 5, 39, 28],
                 dest=[20, 46, 43, 23],
                 excluded=[1, 10, 12, 13, 14, 17, 19, 2, 22, 24, 26, 29, 3, 30, 33, 35, 38, 4, 40, 42, 44, 49, 50, 51,
                           54, 56, 57, 58, 6, 60, 61, 62, 8, 9], paths=[[], [], [], []]),
            dict(level='8', da_score=10, title='Digital Annealer', width=16, height=5, org=[60, 5, 31, 32, 13, 11, 7],
                 dest=[13, 72, 31, 17, 70, 65, 7],
                 excluded=[1, 12, 18, 2, 20, 23, 25, 27, 3, 34, 36, 4, 40, 42, 49, 50, 51, 52, 57, 6],
                 paths=[[], [], [], [], [], [], []]),
            dict(level='9', da_score=11, title='Scrum', width=5, height=5, org=[0, 23, 4, 14, 15],
                 dest=[24, 22, 20, 10, 2], excluded=[12, 18, 19, 5, 6], paths=[[], [], [], [], []],
                 teaser='Find out more about Digital Annealer <a href="https://www.fujitsu.com/de/themes/digitalannealer/get-started/" target="_blank">here</a>.'),
            dict(level='R', da_score=13, title='Dr. R.', width=20, height=7,
                 org=[8, 1, 17, 127, 5, 124, 13, 134, 121, 137], dest=[122, 126, 129, 2, 132, 12, 136, 16, 9, 6],
                 excluded=[101, 104, 107, 108, 110, 111, 113, 114, 117, 119, 20, 21, 24, 27, 28, 30, 31, 33, 37, 39, 40,
                           41, 43, 45, 48, 50, 53, 57, 59, 61, 63, 65, 67, 70, 71, 74, 77, 78, 79, 81, 83, 85, 87, 90,
                           93, 95, 97, 99], paths=[[], [], [], [], [], [], [], [], [], []],
                 teaser = 'In honor of our long-time CTO, mentor, Distinguished Engineer, Fujitsu Fellow and Quantum, optimiztion, Digital Annealer etc. enthusiast <h3>Dr. Joseph Reger</h3> on the occasion of his retirement in spring 2023. <p>Your task: Please help the pensioners in Josephspark to go to their respective park bench without getting too close to each other.</p>'),
            dict(level='A', da_score=9, title='AFCEA', width=16, height=7,
                 org=[80, 37, 101, 43, 56, 14, 12, 46], dest=[37, 65, 40, 95, 69, 93, 72, 75],
                 excluded=[16, 18, 20, 23, 24, 26, 27, 29, 31, 32, 34, 36, 39, 42, 45, 47, 48, 49, 50, 52, 53, 55, 58,
                           59, 61, 62, 63, 64, 66, 68, 71, 74, 77, 79, 81, 84, 85, 87, 88, 90, 91, 94],
                 paths=[[], [], [], [], [], [], [], []],
                 teaser='<img class="img-teaser" height="300px" src="pathoptimizer/browser/AFCEA.png">')
        ]
