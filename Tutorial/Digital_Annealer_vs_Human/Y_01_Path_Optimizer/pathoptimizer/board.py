import pathlib
import anywidget
from traitlets import Unicode, Bool, Int, Float, Dict, List, validate, TraitError
from IPython.core.display import display, Javascript

import ipykernel
from json import loads
from re import compile, sub
import os


def _load_d3():
    display(
        Javascript(
            """
            // load d3, d3 script is modified to ignore possible define fuction
            var s_d3 = document.createElement('script');
            s_d3.src = window.location.pathname.replace(/\\/notebooks\\/|\\/(lab|doc)(\\/workspaces)?(\\/auto-S)?(\\/tree)?\\//, '/files/').replace(/[^/]*$/, 'pathoptimizer/browser/d3.min.js');
            s_d3.type = "text/javascript";
            s_d3.async = false;                                 
            document.getElementsByTagName('head')[0].appendChild(s_d3);
            """
        )
    )
# try to load d3 on module load to avoid timing problems on some browsers
_load_d3()

class Board(anywidget.AnyWidget):
    _esm = pathlib.Path(__file__).parent / "browser/board.js"
    _css = pathlib.Path(__file__).parent / "browser/board.css"

    # Attributes
    width = Int(default_value=5, help='Width of board').tag(sync=True)
    height = Int(default_value=5, help='Height of board').tag(sync=True)
    excluded = List(default_value=[], help='Excluded fields').tag(sync=True)
    title = Unicode(default_value='New board', help='Title displayed inside board').tag(sync=True)
    org = List(default_value=[], help='Robot origins (field indices numbered line wise starting at bottom left corner)').tag(sync=True)
    dest = List(default_value=[], help='Robot destinations (field indices numbered line wise starting at bottom left corner)').tag(sync=True)
    paths = List(default_value=[], help='Robot paths is a list of path lists per robot  (field indices numbered line wise starting at bottom left corner)').tag(sync=True)
    scale = Float(default_value=100.0, help='Scaling of board').tag(sync=True)
    aura = Float(default_value=0.5, help='Opacity of robot aura').tag(sync=True)
    time_tick = Int(default_value=0, help='Current time tick').tag(sync=True)
    max_time_tick = Int(default_value=0, help='Maximum possible time tick').tag(sync=True)
    value = Dict(default_value={'not_set': True}, help='Setting and return variable for edit mode').tag(sync=True)
    level_finished = Bool(default_value=False, help='Boolean value set to true, when a level was finished successfully.').tag(sync=True)
    game_over = Bool(default_value=False, help='Boolean value set to true when game is over. For success check level_finished.').tag(sync=True)
    robot_image = Unicode(default_value='', help='Image in browser folder to be used as robot. Empty string produces colored cicle.').tag(sync=True)

    # Basic validator for the chart dictionary
    @validate('value')
    def _validate_board(self, proposal):
        if 'not_set' not in proposal['value'] and 'title' not in proposal['value']:
            raise TraitError('Invalid board data: dictionary must contain a key "title" or "not_set".')
        return proposal['value']

    @validate('scale')
    def _validate_data(self, proposal):
        return proposal['value']

    def __init__(self, *args, **kwargs):
        _load_d3()
        super(Board, self).__init__(*args, **kwargs)


try:
    from dadk.Optimizer import WidgetGui

    WidgetGui.register_widget_class(Board)
except:
    print('failure in registration of widget')
