import math

from dadk.Optimizer import *
from pathoptimizer.board import Board
from pathoptimizer.game import Game
from dadk.JupyterTools import TimeReporter


class PathBoard:
    def __init__(self, width=5, height=5):
        self.size = width * height
        self.width = width
        self.height = height
        self.index_of_field = [[x + y * width for y in range(height)] for x in range(width)]
        self.field_of_index = [(i % width, i // width) for i in range(self.size)]

    def get_neighborhood(self, index):
        x_mid, y_mid = self.field_of_index[index]
        return [self.index_of_field[x][y]
                for y in range(max(0, y_mid - 1), min(self.height, y_mid + 2))
                for x in range(max(0, x_mid - 1), min(self.width, x_mid + 2))
                ]

    def distance(self, index_0, index_1):
        x_0, y_0 = self.field_of_index[index_0]
        x_1, y_1 = self.field_of_index[index_1]
        return math.sqrt((x_0 - x_1) ** 2 + (y_0 - y_1) ** 2)


class PathoptimizerModel(OptimizerModel):
    ##################################################################################################################
    # constructor (defines GUI and test declaration)
    ##################################################################################################################
    def __init__(self, persistent_file="Demo_Pathfinder.dao"):
        OptimizerModel.__init__(
            self,
            name='Pathoptimizer',
            persistent_file=persistent_file,

            load_parameter=[
                {'name': 'board_size_width', 'type': 'int_slider', 'value': 7, 'min': 5, 'max': 20,
                 'label': 'Board width', 'description': 'Width of playing field',
                 'on_change': 'edit_board.width = board_size_width.value'
                 },
                {'name': 'board_size_height', 'type': 'int_slider', 'value': 5, 'min': 5, 'max': 20,
                 'label': 'Board height', 'description': 'Height of playing field',
                 'on_change': 'edit_board.height = board_size_height.value'},
                {'name': 'board_title', 'type': 'test', 'value': 'New board',
                 'label': 'Board title', 'description': 'Title displayed on playing field',
                 'on_change': 'edit_board.title = board_title.value'},
                {'name': 'robots', 'type': 'int_slider', 'value': 2, 'min': 1, 'max': 20,
                 'label': 'Robots', 'description': 'Number of robots',
                 'on_change': '''
edit_board.org = list(range(0, 2 * robots.value, 2))
edit_board.dest = list(range(1,1 + 2 * robots.value, 2))
'''},
                {'name': 'robot_image', 'type': 'select', 'value': '', 'options': ['', 'drone_2.png',  'drone_3.png'],
                 'label': 'Robot image',
                 'description': 'Select image for robot visualization, empty string is displayed as colored circle.'},
                {'name': 'robot_positions', 'type': 'select', 'value': 'random', 'options': ['random', 'manual'],
                 'label': 'Robot positions',
                 'description': 'Start and end positions randomly or manually defined on board.'},
                {'type': 'begin_group', 'label': 'Board'},
                {'name': 'edit_board', 'type': 'custom', 'class': 'Board',
                 'value': {'not_set': False, 'width': 5, 'height': 5, 'title': '', 'excluded': [], 'org': [],
                           'dest': []}, 'scale': 30.0,
                 'label': 'Board settings', 'description': 'Field settings for playing field'},
                {'type': 'end_group'},
                {'name': 'time_ticks', 'type': 'int_slider', 'value': 10, 'min': 5, 'max': 50,
                 'label': 'Time', 'description': 'Number of time ticks'},
                {'name': 'random_seed', 'value': 42, 'type': 'int_slider', 'min': 0, 'max': 9999, 'step': 1,
                 'label': 'Random seed', 'description': 'Seed for random portfolio generation'}
            ],
            build_qubo_parameter=[
                {'name': 'factor_field', 'type': 'float_slider', 'value': 280, 'min': 0.0, 'max': 10 ** 4,
                 'label': 'Factor field', 'description': 'Factor for field part'},
                {'name': 'factor_single', 'type': 'float_slider', 'value': 100, 'min': 0.0, 'max': 10 ** 4,
                 'label': 'Factor single', 'description': 'Factor for single part'},
                {'name': 'factor_step', 'type': 'float_slider', 'value': 200, 'min': 0.0, 'max': 10 ** 4,
                 'label': 'Factor step', 'description': 'Factor for step part'},
                {'name': 'factor_keepmoving', 'type': 'float_slider', 'value': 100, 'min': 0.0, 'max': 10 ** 3,
                 'label': 'Factor keepmoving', 'description': 'Factor for keep moving)'},
                {'name': 'factor_destination', 'type': 'float_slider', 'value': 100, 'min': 0.0, 'max': 10 ** 3,
                 'label': 'Factor destination', 'description': 'Factor for staying at destination'},
                {'name': 'factor_dist', 'type': 'float_slider', 'value': 25, 'min': 0.0, 'max': 10 ** 3,
                 'label': 'Factor dist', 'description': 'Factor for distance part (cost)'}
            ],
            visualization_title='Board',
            visualization_toolbar_parameter=[
                {'name': 'time_tick', 'type': 'int_slider', 'value': 0, 'min': 0, 'max': 50,
                 'label': 'Time', 'description': 'Actual time tick for display',
                 'on_change': 'model.board_chart.time_tick = time_tick.value'
                 },
                {'name': 'scale', 'type': 'int_slider', 'value': 100, 'min': 5, 'max': 500,
                 'label': 'Scale', 'description': 'Scale board',
                 'on_change': 'model.board_chart.scale = scale.value'
                 },
                {'name': 'aura', 'type': 'float_slider', 'value': 0.3, 'min': 0.0, 'max': 1.0, 'step': 0.01,
                 'label': 'Aura', 'description': 'Opacity of robot aura',
                 'on_change': 'model.board_chart.aura = aura.value'
                 }

            ]
        )

    ##################################################################################################################
    # load method (defines content of tab Setup scenario)
    ##################################################################################################################
    def load(self, board_size_width=5, board_size_height=5, board_title='New board', robot_positions='random',
             edit_board={},
             robots=2, robot_image='', time_ticks=10, random_seed=42, silent=False):

        self.board_size_width = board_size_width
        self.board_size_height = board_size_height
        self.board_title = board_title
        self.fields = board_size_width * board_size_height
        self.robots = robots
        self.robot_image = robot_image
        self.time_ticks = time_ticks

        self.board = PathBoard(width=self.board_size_width, height=self.board_size_height)

        self.excluded = edit_board['excluded']
        self.included_fields = list(set(range(self.fields)).difference(self.excluded))

        random.seed(random_seed)
        np.random.seed(random_seed)

        if robot_positions == 'random':
            self.org = []
            self.dest = []
            org_choices = set(range(self.fields)).difference(self.excluded)
            dest_choices = set(range(self.fields)).difference(self.excluded)
            for r in range(self.robots):
                if len(org_choices) == 0:
                    print('Not feasible. Try again with bigger board or less robots.')
                    break
                self.org.append(random.choice(list(org_choices)))
                org_choices = org_choices.difference(self.board.get_neighborhood(self.org[-1]))
                if len(dest_choices) == 0:
                    print('Not feasible. Try again with bigger board or less robots.')
                    break
                self.dest.append(random.choice(list(dest_choices)))
                dest_choices = dest_choices.difference(self.board.get_neighborhood(self.dest[-1]))
            edit_board['org'] = self.Optimizer.gui_load.get_widget('edit_board').org = self.org
            edit_board['dest'] = self.Optimizer.gui_load.get_widget('edit_board').dest = self.dest
        else:
            self.org = edit_board['org']
            self.dest = edit_board['dest']
            """self.board_size_width = edit_board['width']
            self.board_size_height = edit_board['height']
            self.board_title = edit_board['title']
            self.fields = self.board_size_width * self.board_size_height
            self.robots = len(self.org)"""

        self.Optimizer.tab_visualization.time_tick.max = self.time_ticks - 1

        if not silent:
            print(
                f'\nTo test the game just execute the following command in an empty cell:\n\nfrom pathoptimizer.game import Game\ntest = Game(width={self.board_size_width}, height={self.board_size_height}, org={self.org}, dest={self.dest}, excluded={self.excluded}, paths={[[] for _ in range(robots)]})\n')

            #Game(width=self.board_size_width, height=self.board_size_height, org=self.org, dest=self.dest,
            #          excluded=self.excluded, paths=[[] for _ in range(robots)])

    ##################################################################################################################
    # build_qubo method (defines content of tab Build QUBO)
    ##################################################################################################################
    def build_qubo(self,
                   accelerated: bool = True,
                   factor_field=100,
                   factor_single=100,
                   factor_step=100,
                   factor_keepmoving=100,
                   factor_destination=100,
                   factor_dist=10,
                   silent=False):

        build_times = TimeReporter()
        # variable bit as default are initialized with -1
        constant_bits = np.full((self.robots, self.fields, self.time_ticks), -1, np.int8)

        # reachable is a nested dictionary containing per robot and time tick the fields reachable by the robot in the
        # respective time tick.
        reachable = {r: {0: [self.org[r]]} for r in range(self.robots)}

        is_excluded_field = [False] * self.fields

        for t in range(1, self.time_ticks):
            for r in range(self.robots):
                fields = set()
                for f_0 in reachable[r][t - 1]:
                    for f_1 in self.board.get_neighborhood(f_0):
                        if f_1 not in self.excluded:
                            fields.add(f_1)  # move to f_1
                reachable[r][t] = sorted(list(fields))

        for r in range(self.robots):
            for f in range(self.fields):
                constant_bits[r, f, 0] = 1 if self.org[r] == f else 0
                constant_bits[r, f, self.time_ticks - 1] = 1 if self.dest[r] == f else 0
                for t in range(self.time_ticks):
                    for f in range(self.fields):
                        if (constant_bits[r, f, t] == -1) and (f not in reachable[r][t]):
                            constant_bits[r, f, t] = 0

            for f in self.excluded:
                is_excluded_field[f] = True
                for t in range(self.time_ticks):
                    constant_bits[r, f, t] = 0

        self.var_shape_set = VarShapeSet(BitArrayShape(name='x',
                                                       shape=(self.robots, self.fields, self.time_ticks),
                                                       constant_bits=constant_bits,
                                                       axis_names=['Robot', 'Field', 'Time']
                                                       ))

        build_times.take_time('VarShapeSet')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_field
        self.H_field = BinPol(self.var_shape_set)
        for r in range(self.robots):
            for t in range(self.time_ticks):
                p = BinPol(self.var_shape_set).add_term(1)
                for f in range(self.fields):
                    p.add_term(-1, (('x', r, f, t),))
                p **= 2
                self.H_field += p

        build_times.take_time('H_field', f'{self.H_field.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_single
        self.H_single = BinPol(self.var_shape_set)
        for t in range(self.time_ticks):
            for r_0 in range(self.robots - 1):
                for r_1 in range(r_0 + 1, self.robots):
                    for f_0 in range(self.fields):
                        for f_1 in self.board.get_neighborhood(f_0):
                            self.H_single.add_term(1, (('x', r_0, f_0, t), ('x', r_1, f_1, t)))

        build_times.take_time('H_single', f'{self.H_single.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_step: Penalize steps outside the neighborhood
        self.H_step = BinPol(self.var_shape_set)
        if accelerated:
            for f_0 in self.included_fields:
                for f_1 in self.included_fields:
                    if f_1 not in self.board.get_neighborhood(f_0):
                        for t in range(self.time_ticks - 1):
                            for r in range(self.robots):
                                # self.H_step.add_term(1, (('x', r, f_0, t), ('x', r, f_1, t + 1)))
                                self.H_step.add_term(self.board.distance(f_0, f_1),
                                                     (('x', r, f_0, t), ('x', r, f_1, t + 1)))
        else:
            for t in range(self.time_ticks - 1):
                for r in range(self.robots):
                    for f_0 in range(self.fields):
                        for f_1 in range(self.fields):
                            if f_1 not in self.board.get_neighborhood(f_0):
                                # self.H_step.add_term(1, (('x', r, f_0, t), ('x', r, f_1, t + 1)))
                                self.H_step.add_term(self.board.distance(f_0, f_1), (('x', r, f_0, t), ('x', r, f_1, t + 1)))

        build_times.take_time('H_step', f'{self.H_step.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_keepmoving: Penalize stays in between the route
        self.H_keepmoving = BinPol(self.var_shape_set)
        for t in range(self.time_ticks - 1):
            for r in range(self.robots):
                for f in range(self.fields):
                    if f != self.dest[r]:
                        self.H_keepmoving.add_term(1, (('x', r, f, t), ('x', r, f, t + 1)))

        build_times.take_time('H_keepmoving', f'{self.H_keepmoving.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_destination
        self.H_destination = BinPol(self.var_shape_set)
        for t in range(self.time_ticks - 1):
            for r in range(self.robots):
                self.H_destination.add_term(1, (('x', r, self.dest[r], t), ))
                self.H_destination.add_term(-1, (('x', r, self.dest[r], t), ('x', r, self.dest[r], t + 1)))

        build_times.take_time('H_destination', f'{self.H_destination.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # H_dist
        self.H_dist = BinPol(self.var_shape_set)
        if accelerated:
            for f_0 in self.included_fields:
                for f_1 in self.included_fields:
                    for t in range(self.time_ticks - 1):
                        for r in range(self.robots):
                            self.H_dist.add_term(self.board.distance(f_0, f_1), (('x', r, f_0, t), ('x', r, f_1, t + 1)))
        else:
            for t in range(self.time_ticks - 1):
                for r in range(self.robots):
                    for f_0 in range(self.fields):
                        for f_1 in range(self.fields):
                            self.H_dist.add_term(self.board.distance(f_0, f_1), (('x', r, f_0, t), ('x', r, f_1, t + 1)))

        build_times.take_time('H_dist', f'{self.H_dist.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.HQ = factor_field * self.H_field + factor_single * self.H_single + \
                  factor_step * self.H_step + factor_keepmoving * self.H_keepmoving + \
                  factor_destination * self.H_destination + factor_dist * self.H_dist

        build_times.take_time('HQ', f'{self.HQ.N} bits')

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if not silent:
            print(f"QUBO generated\n  Number of bits: {self.HQ.N:5d}")

    ##################################################################################################################
    # prep_result method (evaluates result of annealing defines content of tab Solve annealing)
    ##################################################################################################################
    def prep_result(self, solution_list:SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        self.configuration = configuration
        self.energy_HQ = self.HQ.compute(configuration)
        self.energy_field = self.H_field.compute(configuration)
        self.track_table = solution_list.min_solution.extract_bit_array('x')

        if self.energy_field != 0:
            print('Solution not valid, not always unique field found.')
        self.paths = [[int(np.argmax(self.track_table.data[r, :, t])) for t in range(self.time_ticks)] for r in
                      range(self.robots)]


        if not silent:
            print(f'''
Energies
            
    HQ (weighted)  {self.energy_HQ:10.2f}
    H_field        {self.energy_field:10.2f}
    H_single       {self.H_single.compute(configuration):10.2f}
    H_step         {self.H_step.compute(configuration):10.2f}
    H_keepmoving   {self.H_keepmoving.compute(configuration):10.2f}
    H_destination  {self.H_destination.compute(configuration):10.2f}
    H_dist         {self.H_dist.compute(configuration):10.2f}
    ''')

    ##################################################################################################################
    # report method (defines content of Report tab)
    ##################################################################################################################
    def report(self):
        max_ax_per_row = int(5 + 4 / self.robots)
        self.track_table.draw(
            figsize=(12.0, self.fields / 8.0 * math.ceil(self.time_ticks / max_ax_per_row)),
            max_ax_per_row = max_ax_per_row)
        print(self.paths)

    ##################################################################################################################
    # draw method (defines content of optional "Visualization" tab)
    ##################################################################################################################
    def draw(self):
        self.board_chart = Board(
            title=self.board_title,
            width=self.board_size_width,
            excluded=self.excluded,
            height=self.board_size_height,
            org=self.org,
            dest=self.dest,
            paths=self.paths,
            scale=100.0,
            time_tick=0,
            robot_image=self.robot_image
        )
        display(self.board_chart)

    ##################################################################################################################
    # pareto method (defines records for pareto curves)
    ##################################################################################################################
    def pareto(self):
        return {'HQ': self.energy}