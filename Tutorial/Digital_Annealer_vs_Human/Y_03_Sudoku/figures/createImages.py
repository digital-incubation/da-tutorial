from PIL import Image, ImageDraw, ImageFont
import imageio
import os

SIZE = 9
BASE = 1

def create_image(fixed, highlight, filename, c1="Yellow", c2="Orange", special=None, c3=None):
    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)

    total_width = 486
    total_height = 486

    font = ImageFont.truetype("./ariblk.ttf", 16)

    img = Image.new("RGB", (total_width, total_height), color="white")

    d = ImageDraw.Draw(img)

    # d.rectangle([(4,4), (total_width - 4, total_height - 4)], outline="Red")

    width = 20
    height = 20
    gap = 8

    width2 = int(width / 2)
    height2 = int(height / 2)
    gap2 = int(gap / 2)

    min_x = 9999999
    min_y = 9999999

    max_x = 0
    max_y = 0

    v = 0
    c = 0
    row_text_points = {}
    for r in range(SIZE):
        x_base = 60 + c * (width + gap) + v * (width2 + gap2)
        y_base = 174 + (SIZE - r) * (height + gap) - v * (height2 + gap2)
        p1 = (x_base - 35, y_base - 25)
        p2 = (x_base - 10, y_base - 0)
        row_text_points[r] = (p1, p2)

    r = 0
    v = 0
    column_text_points = {}
    for c in range(SIZE):
        x_base = 60 + c * (width + gap) + v * (width2 + gap2)
        y_base = 174 + (SIZE - r) * (height + gap) - v * (height2 + gap2)
        p1 = (x_base, y_base + 10)
        p2 = (x_base + 25, y_base + 35)
        column_text_points[c] = (p1, p2)

    r = 0
    c = SIZE - 1
    value_text_points = {}
    for v in range(SIZE - 1, -1, -1):
        x_base = 60 + c * (width + gap) + v * (width2 + gap2)
        y_base = 174 + (SIZE - r) * (height + gap) - v * (height2 + gap2)
        p1 = (x_base + 30, y_base + 0)
        p2 = (x_base + 30 + 25, y_base + 0 + 25)
        value_text_points[v] = (p1, p2)

    for r in row_text_points:
        d.ellipse([row_text_points[r][0], row_text_points[r][1]], outline="Blue", fill="white")

        text = str(r + BASE)
        (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
        t_width = t_right - t_left
        t_height = t_bottom - t_top
        d.text((row_text_points[r][0][0] + 0.5 * (row_text_points[r][1][0] - row_text_points[r][0][0]) - 0.5 * t_width,
                row_text_points[r][0][1] + 0.5 * (row_text_points[r][1][1] - row_text_points[r][0][1]) - 0.8 * t_height), text, fill="black", font=font)

    for c in column_text_points:
        d.ellipse([column_text_points[c][0], column_text_points[c][1]], outline="Red", fill="white")

        text = str(c + BASE)
        (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
        t_width = t_right - t_left
        t_height = t_bottom - t_top
        d.text((column_text_points[c][0][0] + 0.5 * (column_text_points[c][1][0] - column_text_points[c][0][0]) - 0.5 * t_width,
                column_text_points[c][0][1] + 0.5 * (column_text_points[c][1][1] - column_text_points[c][0][1]) - 0.8 * t_height), text, fill="black", font=font)

    for v in value_text_points:
        d.ellipse([value_text_points[v][0], value_text_points[v][1]], outline="Green", fill="white")

        text = str(v + BASE)
        (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
        t_width = t_right - t_left
        t_height = t_bottom - t_top
        d.text((value_text_points[v][0][0] + 0.5 * (value_text_points[v][1][0] - value_text_points[v][0][0]) - 0.5 * t_width,
                value_text_points[v][0][1] + 0.5 * (value_text_points[v][1][1] - value_text_points[v][0][1]) - 0.8 * t_height), text, fill="black", font=font)

    text = 'rows'
    (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
    t_width = t_right - t_left
    t_height = t_bottom - t_top

    temp_img = Image.new("RGBA", (t_width, t_height), color=(255, 255, 255, 255))
    draw_temp = ImageDraw.Draw(temp_img)
    draw_temp.text((0, 0), text, font=font, fill='black', anchor='lt')
    temp_img = temp_img.rotate(90, expand=True)

    img.paste(temp_img, (int(row_text_points[0][0][0] + 0.5 * (row_text_points[SIZE - 1][1][0] - row_text_points[0][0][0]) - 0.5 * temp_img.width - 1 * height),
                         int(row_text_points[0][0][1] + 0.5 * (row_text_points[SIZE - 1][1][1] - row_text_points[0][0][1]) - 0.5 * temp_img.height)), temp_img)

    text = 'columns'
    (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
    t_width = t_right - t_left
    t_height = t_bottom - t_top
    d.text((column_text_points[0][0][0] + 0.5 * (column_text_points[SIZE - 1][1][0] - column_text_points[0][0][0]) - 0.5 * t_width,
            column_text_points[0][0][1] + 0.5 * (column_text_points[SIZE - 1][1][1] - column_text_points[0][0][1]) - 0.8 * t_height + 1 * height), text, fill="black", font=font)

    text = 'values'
    (t_left, t_top, t_right, t_bottom) = d.textbbox((0, 0), text, font=font)
    t_width = t_right - t_left
    t_height = t_bottom - t_top

    temp_img = Image.new("RGBA", (t_width, t_height), color=(255, 255, 255, 255))
    draw_temp = ImageDraw.Draw(temp_img)
    draw_temp.text((0, 0), text, font=font, fill='black', anchor='lt')
    temp_img = temp_img.rotate(45, expand=True)

    img.paste(temp_img, (int(value_text_points[0][0][0] + 0.5 * (value_text_points[SIZE - 1][1][0] - value_text_points[0][0][0]) - 0.5 * temp_img.width + 1 * height),
                         int(value_text_points[0][0][1] + 0.5 * (value_text_points[SIZE - 1][1][1] - value_text_points[0][0][1]) - 0.5 * temp_img.height + 1 * height)), temp_img)

    for v in range(SIZE - 1, -1, -1):
        for r in range(SIZE):
            for c in range(SIZE):

                x_base = 60 + c * (width + gap) + v * (width2 + gap2)
                y_base = 174 + (SIZE - r) * (height + gap) - v * (height2 + gap2)

                color = "Ivory"

                if (r // 3 + c // 3) % 2 == 1:
                    color = 'lightgrey'

                for index in range(len(fixed)):
                    if r in fixed[index][0] and c in fixed[index][1] and v in fixed[index][2]:
                        color = c1

                for index in range(len(highlight)):
                    if r == highlight[index][0] and c == highlight[index][1] and v == highlight[index][2]:
                        color = c2

                if special is not None:
                    if r == special[0] and c == special[1] and v == special[2]:
                        color = c3

                A = (x_base, y_base)
                B = (x_base + width, y_base)
                C = (x_base + width, y_base - height)
                D = (x_base, y_base - height)
                E = (x_base + width2 + width, y_base - height2)
                F = (x_base + width2 + width, y_base - height - height2)
                G = (x_base + width2, y_base - height - height2)

                d.rectangle([A, C], outline="black", fill=color)
                d.polygon([D, G, F, C], outline="black", fill=color)
                d.polygon([C, F, E, B], outline="black", fill=color)

                TL = (x_base, y_base - height - height2)
                BR = (x_base + width2 + width, y_base)

                min_x = min(min_x, TL[0])
                max_x = max(max_x, BR[0])
                min_y = min(min_y, TL[1])
                max_y = max(max_y, BR[1])

    img.save(filename)

# -----------------------------------------------------------

filename = "./bits-overview.png"
create_image([([], [], [])], [(-1, -1, -1)], filename)

filename = "./bits-special.png"
create_image([([8], [8], range(SIZE))], [(-1, -1, -1)], filename, c1="Ivory", special=(8, 8, 6), c3="red")

# -----------------------------------------------------------

images = []
for v in range(SIZE):
    filename = "./cell/A/%d.png" % v
    create_image([([8], [8], range(SIZE))], [(8, 8, v)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./cell/A.gif", images, duration=0.6)

images = []
counter = 0
rc_list = []
for i in range(SIZE):
    for j in range(i):
        rc_list.append((SIZE - 1 - i, SIZE - 1 - j))
    rc_list.append((SIZE - 1 - i, SIZE - 1 - i))
    for j in range(i):
        rc_list.append((SIZE - i + j, SIZE - 1 - i))

for (r, c) in rc_list:
    filename = "./cell/B/%d.png" % counter
    counter += 1
    create_image([([r], [c], range(SIZE))], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./cell/B.gif", images, duration=0.2)

# -----------------------------------------------------------

images = []
for c in range(SIZE):
    filename = "./row/A/%d.png" % c
    create_image([([0], range(SIZE), [0])], [(0, c, 0)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./row/A.gif", images, duration=0.6)

images = []
for r in range(SIZE):
    filename = "./row/B/%d.png" % r
    create_image([([r], range(SIZE), [0])], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./row/B.gif", images, duration=0.6)

images = []
for v in range(SIZE):
    filename = "./row/C/%d.png" % v
    create_image([(range(SIZE), range(SIZE), [v])], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./row/C.gif", images, duration=0.6)

# -----------------------------------------------------------

images = []
for r in range(SIZE):
    filename = "./column/A/%d.png" % r
    create_image([(range(SIZE), [0], [0])], [(r, 0, 0)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./column/A.gif", images, duration=0.6)

images = []
for c in range(SIZE):
    filename = "./column/B/%d.png" % c
    create_image([(range(SIZE), [c], [0])], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./column/B.gif", images, duration=0.6)

images = []
for v in range(SIZE):
    filename = "./column/C/%d.png" % v
    create_image([(range(SIZE), range(SIZE), [v])], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./column/C.gif", images, duration=0.6)

# -----------------------------------------------------------

images = []
for i in range(SIZE):
    filename = "./block/A/%d.png" % i
    create_image([([0, 1, 2], [0, 1, 2], [0])], [(i // 3, i % 3, 0)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./block/A.gif", images, duration=0.6)

images = []
counter = 0
for f_r in [[0, 1, 2], [3, 4, 5], [6, 7, 8]]:
    for f_c in [[0, 1, 2], [3, 4, 5], [6, 7, 8]]:
        filename = "./block/B/%d.png" % counter
        create_image([(f_r, f_c, [0])], [(-1, -1, -1)], filename)
        counter += 1
        images.append(imageio.imread(filename))
imageio.mimsave("./block/B.gif", images, duration=0.6)

images = []
for v in range(SIZE):
    filename = "./block/C/%d.png" % v
    create_image([(range(SIZE), range(SIZE), [v])], [(-1, -1, -1)], filename)
    images.append(imageio.imread(filename))
imageio.mimsave("./block/C.gif", images, duration=0.6)

# -----------------------------------------------------------

images = []
for v in range(1, SIZE):
    filename = "./hint/A/%2d.png" % len(images)
    create_image([
        ([8], range(SIZE), [0]),
        (range(SIZE), [8], [0]),
        ([8], [8], range(SIZE)),
        ([7], [6, 7, 8], [0]),
        ([6], [6, 7, 8], [0]),
    ],
        [(8, 8, v)], filename, c1="lightblue", c2="red", special=(8, 8, 0), c3="green")
    images.append(imageio.imread(filename))
for c in range(1, SIZE):
    filename = "./hint/A/%2d.png" % len(images)
    create_image([
        ([8], range(SIZE), [0]),
        (range(SIZE), [8], [0]),
        ([8], [8], range(SIZE)),
        ([7], [6, 7, 8], [0]),
        ([6], [6, 7, 8], [0]),
    ],
        [(8, SIZE - 1 - c, 0)], filename, c1="lightblue", c2="red", special=(8, 8, 0), c3="green")
    images.append(imageio.imread(filename))
for r in range(1, SIZE):
    filename = "./hint/A/%2d.png" % len(images)
    create_image([
        ([8], range(SIZE), [0]),
        (range(SIZE), [8], [0]),
        ([8], [8], range(SIZE)),
        ([7], [6, 7, 8], [0]),
        ([6], [6, 7, 8], [0]),
    ],
        [(SIZE - 1 - r, 8, 0)], filename, c1="lightblue", c2="red", special=(8, 8, 0), c3="green")
    images.append(imageio.imread(filename))
for i in range(1, SIZE):
    filename = "./hint/A/%2d.png" % len(images)
    create_image([
        ([8], range(SIZE), [0]),
        (range(SIZE), [8], [0]),
        ([8], [8], range(SIZE)),
        ([7], [6, 7, 8], [0]),
        ([6], [6, 7, 8], [0]),
    ],
        [(6 + (SIZE - 1 - i) // 3, SIZE - 1 - i % 3, 0)], filename, c1="lightblue", c2="red", special=(8, 8, 0), c3="green")
    images.append(imageio.imread(filename))
imageio.mimsave("./hint/a.gif", images, duration=0.6)

# -----------------------------------------------------------

print("ready")
