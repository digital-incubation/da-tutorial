# Orginal
# https://medium.com/@gorjanz/data-analysis-in-python-interactive-scatterplot-with-matplotlib-6bb8ad2f1f18
# import the random module since we will use it to generate the data
import random as rnd
import numpy as np

# import the main drawing library
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
from matplotlib.text import Annotation

# import the seaborn module which is based on matplotlib to make our visualization more presentable
import seaborn as sns

from sources import config  # needed for global variable

# set the default style
sns.set()
# define two colors, just to enrich the example
labels_color_map = {0: 'yellow', 1: 'orange'}

# generate the data needed for the scatterplot
generated_data = config.seats_walls

font1 = {'family': 'serif', 'color': 'blue', 'size': 14, 'weight': 'bold'}
generated_labels = ["Seat {0} out!".format(i) if i <= 42 else "Wall {0} out!".format(i - 42) for i in range(1, len(generated_data) + 1)]
# print(generated_labels)
config.data_points_choosen = np.zeros(np.array(generated_data).shape)
config.data_points_choosen_click_again = np.zeros(np.array(generated_data).shape)

# add the values one by one to the scatterplot
instances_colors = []
axis_values_x = []
axis_values_y = []
for index, instance in enumerate(generated_data):
    coordinate_x, coordinate_y = instance
    if index < 42:
        color = labels_color_map[0]
    else:
        color = labels_color_map[1]

    instances_colors.append(color)
    axis_values_x.append(coordinate_x)
    axis_values_y.append(coordinate_y)

# draw a scatter-plot of the generated values
fig = plt.figure(figsize=(8, 8))
ax = plt.subplot()

# extract the scatterplot drawing in a separate function so we ca re-use the code
def draw_scatterplot():
    img = plt.imread("sources/SeatExample1_step2_with_walls.png")
    ax.imshow(img, extent=[0, 8, 0, 8])
    ax.scatter(
        axis_values_x,
        axis_values_y,
        c=instances_colors,
        picker=True,
        s=230
    )

# draw the initial scatterplot
draw_scatterplot()

# create and add an annotation object (a text label)
def annotate(axis, text, x, y, ind):
    text_annotation = Annotation(text, xy=(x, y), xycoords='data')
    # axis.add_artist(text_annotation)
    config.WHAT_IS_IT = axis.add_artist(text_annotation)

# define the behaviour -> what happens when you pick a dot on the scatterplot by clicking close to it
def onpick(event):
    # step 1: take the index of the dot which was picked
    ind = event.ind
    config.data_points_choosen[ind] = 1
    config.data_points_choosen_click_again[ind] += 1

    # step 2: save the actual coordinates of the click, so we can position the text label properly
    label_pos_x = event.mouseevent.xdata
    label_pos_y = event.mouseevent.ydata

    # just in case two dots are very close, this offset will help the labels not appear one on top of each other
    offset = 0.02

    # if the dots are to close one to another, a list of dots clicked is returned by the matplotlib library

    for i in ind:
        # step 3: take the label for the corresponding instance of the data
        label = generated_labels[i]

        # draw_scatterplot()

        # step 4: log it for debugging purposes
        # print( "index", i, label)

        # step 5: create and add the text annotation to the scatterplot
        if config.data_points_choosen_click_again[ind][0][0] < 2:
            # annotate(
            #     ax,
            #     label,
            #     label_pos_x + offset,
            #     label_pos_y + offset,
            #     ind
            # )
            ax.text(label_pos_x + offset, label_pos_y + offset, label, fontdict=font1, bbox=dict(facecolor='white', alpha=0.8))

            # Change color
            instances_colors[i] = 'grey'
            ax.scatter(
                axis_values_x,
                axis_values_y,
                c=instances_colors,
                picker=True,
                s=230
            )

            # step 6: force re-draw
            ax.figure.canvas.draw_idle()

            # alter the offset just in case there are more than one dots affected by the click
            offset += 0.01

# connect the click handler function to the scatterplot
fig.canvas.mpl_connect('pick_event', onpick)

# create the "clear all" button, and place it somewhere on the screen
ax_clear_all = plt.axes([0.05, 0.9, 0.1, 0.05])
button_clear_all = Button(ax_clear_all, 'Clear all')

def restart_data(generated_data):
    return np.zeros(np.array(generated_data).shape)

# define the "clear all" behaviour
def onclick(event):
    global generated_data

    # Get back all colors
    for index, instance in enumerate(generated_data):
        if index < 42:
            color = labels_color_map[0]
        else:
            color = labels_color_map[1]

        instances_colors[index] = color

    # restart plot
    config.data_points_choosen = restart_data(generated_data)
    config.data_points_choosen_click_again = restart_data(generated_data)

    # step 1: we clear all artist object of the scatter plot
    ax.cla()

    # step 2: we re-populate the scatterplot only with the dots not the labels
    draw_scatterplot()
    ax.grid(False)
    ax.axis('off')

    # step 3: we force re-draw
    ax.figure.canvas.draw_idle()

# link the event handler function to the click event on the button
button_clear_all.on_clicked(onclick)

# initial drawing of the scatterplot
fig.canvas.toolbar_visible = False
fig.canvas.header_visible = False
fig.canvas.footer_visible = False
plt.plot()
# print( "scatterplot done")

# present the scatterplot
ax.grid(False)
ax.axis('off')
display(fig.canvas)
