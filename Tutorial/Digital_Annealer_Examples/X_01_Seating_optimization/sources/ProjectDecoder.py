from typing import Dict, Any, Callable

class ProjectDecoder:
    """
    ``ProjectDecoder`` provides methods to convert a JSON document back into objects.
    """

    _functions = {}

    @classmethod
    def register_function(cls, registered_name: str, registered_function: Callable):
        """
        The method registers a custom decoder-function ``registered_function`` under the name ``registered_name``.

        :param str registered_name: name of decoder in registry.
        :param Callable registered_function: Function to be registered
        """
        cls._functions[registered_name] = registered_function

    @classmethod
    def object_hook(cls, obj: Dict[str, Any]) -> Any:
        """
        Main method that is called by json for each entry in the parsed object-tree to convert the entry into the corresponding registered object.

        :param Dict[str, Any] obj: the JSON-structure to convert

        :return: new object
        :rtype: Any
        """

        if obj is None:
            return obj

        class_qualname = obj.get('__class__', None)
        if (class_qualname is not None) and (class_qualname in cls._functions):
            result = cls._functions[class_qualname](obj)
            if result is not None:
                return result

        return obj

    @classmethod
    def decode(cls, obj: Dict[str, Any]) -> Any:
        return cls.object_hook(obj)
