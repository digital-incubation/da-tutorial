import numpy as np

def default_distance_matrix(seats=5):
    seat_distance_matrix = np.empty((seats, seats))
    for position_row in range(seats):
        for position_column in range(seats):
            if position_column < position_row:
                seat_distance_matrix[position_row, position_column] = position_row - position_column
            else:
                seat_distance_matrix[position_row, position_column] = position_column - position_row
    return seat_distance_matrix
