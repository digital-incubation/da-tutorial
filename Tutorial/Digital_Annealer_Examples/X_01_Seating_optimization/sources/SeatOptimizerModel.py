from dadk.Optimizer import *
from ipywidgets import widgets
from .default_distance_matrix import default_distance_matrix
from .find_seat_size import find_seat_size
from .hq_binpol_function import hq_binpol_function
from .solution_seat_plot import solution_seat_plot
from .seat_plot_after_click_event import seat_plot_after_click_event
import colorama
from colorama import Fore, Style

from .Room import Room

import numpy as np
import matplotlib.pyplot as plt

MAX_SEAT_HISTOGRAM_SIZE = 20

MAX_SEAT_SIZE = 1000
MAX_SAFTEY_DISTANCE = 20
DEFAULT_SOLVER_PARAMETERS = {
    "graphics": True,
    "number_iterations_CPU": 10000,
    "number_runs": 2,
    "offset_increase_rate": 0.1,
    "processor": "CPU",
    "solution_mode": "QUICK",
}

class SeatOptimizerModel(OptimizerModel):

    def __init__(
            self,
            persistent_file="X_01_Seating_optimizaton_tutorial.dao",
            hq_binpol_function=hq_binpol_function,
            seat_plan_matrix=(),
            row_key_to_coordinate_matrix=(),
            clicked_wall=(),
            bound_seats=MAX_SEAT_SIZE):

        self.hq_binpol_function = hq_binpol_function
        self.seat_plan_matrix = seat_plan_matrix
        self.row_key_to_coordinate_matrix = row_key_to_coordinate_matrix
        self.clicked_wall = clicked_wall
        self.old_seats = ()

        name_project = 'Seat Optimization'
        load_parameter_project = []
        if seat_plan_matrix == ():
            load_parameter_project += [{'name': 'seats',
                                        'value': 10,
                                        'type': 'int_slider',
                                        'min': 1,
                                        'max': bound_seats,
                                        'label': 'Number of Seats',
                                        'description': 'Number of seats in the current Workspace.'}
                                       ]
        load_parameter_project += [{'name': 'percentual_occupancy',
                                    'value': 0.5,
                                    'type': 'int_slider',
                                    'min': 0,
                                    'max': 100,
                                    'label': 'percentual seat occupancy',
                                    'description': 'Aimed percentual occupancy of seats.'},
                                   {'name': 'safety_distance',
                                    'value': 2,
                                    'type': 'float_slider',
                                    'min': 0.0,
                                    'max': MAX_SAFTEY_DISTANCE,
                                    'label': 'safety distance',
                                    'description': 'Safety distance between seats.'}
                                   ]

        OptimizerModel.__init__(
            self,
            name=name_project,
            load_parameter=load_parameter_project,
            build_qubo_parameter=[{'name': 'A_1', 'value': 1.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': '(H1) weight a_1', 'description': '%-Seat occupancy scaling factor for QUBO (H1)'},
                                  {'name': 'A_2', 'value': 1.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': '(H2) weight a_2', 'description': 'Max. distance scaling factor for QUBO (H2)'},
                                  {'name': 'A_3', 'value': 2.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': '(H3) weight a_3', 'description': 'Safety distance scaling factor for constraint (H3)'},

                                  {'name': 'temp_start_f', 'type': 'float_bounded', 'visible': False,
                                   'value': 100.0, 'min': 10 ** -10, 'max': 10 ** 10,
                                   'label': 'Temperature start', 'description': 'Scaling factor for temperature_start.'},
                                  {'name': 'temp_end_f', 'type': 'float_bounded', 'visible': False,
                                   'value': 0.0001, 'min': 10 ** -10, 'max': 10 ** 10,
                                   'label': 'Temperature end', 'description': 'Scaling factor for temperature_end.'},
                                  {'name': 'offset_increase_f', 'type': 'float_bounded', 'visible': False,
                                   'value': 0.005, 'min': 10 ** -10, 'max': 10 ** 10,
                                   'label': 'Offset increase rate', 'description': 'Scaling factor for offset_increase_rate.'},
                                  ],
            calculated_solver_parameter={'temperature_start': 3300000.0,
                                         'temperature_end': 3.0,
                                         'offset_increase_rate': 163.0
                                         },
            default_solver_parameter=DEFAULT_SOLVER_PARAMETERS,
            persistent_file=persistent_file
        )

    def load(self, seats=(), percentual_occupancy=0.5, safety_distance=2):

        # room = Room.load('sources/office.json')
        # room.show()

        # -store load parameters as attributes -#################################################################################################################

        # At first we construct a default matrix if no self.seat_plan_matrix is given by the user
        # Besser als funktion
        if self.seat_plan_matrix == () or (self.old_seats != seats and seats != ()):
            print('self.old_seats != seats', self.old_seats != seats)
            print('self.old_seats', self.old_seats)
            print('seats', seats)
            self.seats = seats
            self.old_seats = seats
            self.seat_plan_matrix = default_distance_matrix(seats)
        if self.seat_plan_matrix != ():
            self.seats = find_seat_size(self.seat_plan_matrix, MAX_SEAT_SIZE)
            self.old_seats = self.seats
        # print('Default distance matrix is ...')
        # print(self.seat_plan_matrix)

        # Now we set all other variables
        self.percentual_occupancy = percentual_occupancy / 100
        self.safety_distance = safety_distance
        print('Selected percentage occupancy = ', percentual_occupancy, ' %')
        print('Selected safety distance = ', safety_distance)
        print('')
        print('Seating plan optimization:')
        print(Style.DIM + Fore.BLACK + 'Grey seat = Seat is taken out')
        print(Fore.BLACK + 'Grey dot on wall = Wall is taken out')
        print(Fore.YELLOW + 'Yellow seat = Seat to be optimized')
        print(Style.DIM + Fore.RED + 'Orange wall = Wall remains')
        seat_plot_after_click_event(self.row_key_to_coordinate_matrix, self.clicked_wall)

    def build_qubo(self, A_1, A_2, A_3,
                   temp_start_f=100.0, temp_end_f=0.0001, offset_increase_f=0.005, silent=False):

        # Scaling factors ----------------------------------------------------------------------------------------
        print('%-Seat occupancy scaling factor for QUBO (H1):')
        print(A_1)
        print('Max. distance scaling factor for QUBO (H2):')
        print(A_2)
        print('Safety distance scaling factor for constraint (H3):')
        print(A_3)

        self.A_1 = A_1
        self.A_2 = A_2
        self.A_3 = A_3

        # ----------------------------------------------------------------------------- Preparation of variable sets -
        self.H1, self.H2, self.H3, self.HQ, self.qubo_vs = \
            self.hq_binpol_function(self.seats, self.percentual_occupancy, self.seat_plan_matrix,
                                    self.safety_distance, self.A_1, self.A_2, self.A_3)

        # DA works only with int values hance we have to raise all coefficients below 0
        self.HQ.scale(bit_precision= 64 if (self.HQ.N < 4096) else 16)
        temp_scale = max([abs(self.HQ.p[keys]) for keys in self.HQ.p])

        # ----------------------------------------------------------------------------- Set optimization parameter
        self.Optimizer.set_calculated_solver_parameter(
            temperature_start=round(temp_scale * temp_start_f, 3),
            temperature_end=round(temp_scale * temp_end_f, 3),
            offset_increase_rate=round(temp_scale * offset_increase_f, 3))

        print('Number of Bits = ', self.HQ.N)

    def prep_result(self, solution_list:SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        self.x_data = solution_list.min_solution.extract_bit_array('x').data
        constraint3 = self.H3.compute(configuration)  # Social distance

        constraint1 = self.H1.compute(configuration)  # Social distance
        constraint2 = self.H2.compute(configuration)  # Social distance
        print('%-Seat occupancy QUBO (H1) value = ', constraint1)
        print('Max. distance QUBO (H2) value = ', constraint2)
        print('Safety distance QUBO (H3) value = ', constraint3)

        # Check if hard constraints are fulfilled
        if constraint3 != 0:
            print('Social distance is not fulfilled. Try again!')

    def report(self, silent=False):

        seat_plan_matrix = self.seat_plan_matrix

        # How many seats are available in office
        print('###############################################################')

        print('Available seats = ', np.sum(self.x_data))

        print('###############################################################')

        print('Seat occupancy target is ', self.percentual_occupancy * 100, ' %')
        print('Current seat occupancy is ', round(np.sum(self.x_data) / self.seats, 2) * 100, ' %')

        print('###############################################################')
        employee_seat = np.nonzero(self.x_data)
        employee_seat = employee_seat[0]

        min_distance = np.max(seat_plan_matrix)

        employees_dist = {}
        employees_dist_all = {}
        for key1 in employee_seat:
            for key2 in employee_seat:
                employees_dist_all[(int(key1), int(key2))] = seat_plan_matrix[int(key1), int(key2)]
                if key1 < key2:
                    min_distance = min([min_distance, seat_plan_matrix[int(key1), int(key2)]])
                    employees_dist[(int(key1), int(key2))] = seat_plan_matrix[int(key1), int(key2)]
        if not silent:
            print('smallest distance between seated employees = ', min_distance)

        dist_table = np.empty((self.seats + 1, self.seats + 1))
        dist_table2 = np.zeros((self.seats + 1, self.seats + 1))
        if not silent:
            print('Histogram is in progress, please wait!')
        for key1 in employee_seat:
            for key2 in employee_seat:
                dist_table[key1, key2] = employees_dist_all[(key1, key2)]
                if key1 > key2:
                    dist_table2[key1, key2] = np.max(seat_plan_matrix) / (employees_dist_all[key1, key2] + 1)
                else:
                    dist_table2[key1, key2] = 0
        if not silent:
            fig, ax = plt.subplots()
            im = ax.imshow(dist_table2)

            # We want to show all ticks...
            if self.seats < MAX_SEAT_HISTOGRAM_SIZE:
                ax.set_xticks(np.arange(self.seats))
                ax.set_yticks(np.arange(self.seats))
                # ... and label them with the respective list entries
                ax.set_xticklabels(np.arange(self.seats))
                ax.set_yticklabels(np.arange(self.seats))

                # Loop over data dimensions and create text annotations.
                for i in employee_seat:
                    for j in employee_seat:
                        if i != j:
                            if dist_table[i, j] <= 1.4 * min_distance:
                                text = ax.text(j, i,
                                               dist_table[i, j],
                                               ha="center", va="center", color="b")
                            else:
                                text = ax.text(j, i,
                                               dist_table[i, j],
                                               ha="center", va="center", color="w")

            ax.set_title("Distances between employees in office")
            fig.tight_layout()
            ax.set_xlabel('seat no.')
            ax.set_ylabel('seat no.')
            plt.show()

        if self.seats < MAX_SEAT_HISTOGRAM_SIZE:
            print('employee distances:')
            for emp_to_emp in employees_dist:
                print('Seat ', emp_to_emp[0], ' distance to seat', emp_to_emp[1], ' is ',
                      employees_dist[emp_to_emp])
        print('###############################################################')

    def draw(self):

        print('Seating plan optimization:')
        print(Fore.BLUE + 'Blue seat = Usable seat')
        print(Style.DIM + Fore.BLACK + 'Grey seat = Not usable seat')
        print(Style.DIM + Fore.BLACK + 'Grey dot on wall = Wall is taken out')
        print(Style.DIM + Fore.RED + 'Orange dot on wall = Wall remains')
        solution_seat_plot(self.x_data, self.row_key_to_coordinate_matrix, self.clicked_wall, inside_optimizer=True)
