# import the main drawing library
import matplotlib.pyplot as plt
from . import config  # needed for global variable
# import the seaborn module which is based on matplotlib to make our visualization more presentable
import seaborn as sns

def solution_seat_plot(scenario, row_key_to_coordinate_matrix, clicked_wall, inside_optimizer=False, title="Solution"):
    if inside_optimizer:
        seats_solution = scenario
    else:
        seats_solution = scenario.model.x_data
    # set the default style
    sns.set()

    # define two colors, just to enrich the example
    labels_color_map = {0: 'grey', 1: 'orange', 2: 'blue'}

    # generate the data needed for the scatterplot
    generated_data = config.seats_walls

    # add the values one by one to the scatterplot
    instances_colors = []
    axis_values_x = []
    axis_values_y = []
    for index, instance in enumerate(generated_data):
        coordinate_x, coordinate_y = instance
        if index < 42:
            color = labels_color_map[0]
        else:
            if (coordinate_x, coordinate_y) in clicked_wall:
                color = labels_color_map[0]
            else:
                color = labels_color_map[1]

        instances_colors.append(color)
        axis_values_x.append(coordinate_x)
        axis_values_y.append(coordinate_y)

    for index in range(len(seats_solution)):
        if seats_solution[index]:
            coordinate_x = row_key_to_coordinate_matrix[index][0]
            coordinate_y = row_key_to_coordinate_matrix[index][1]
            color = labels_color_map[2]
            instances_colors.append(color)
            axis_values_x.append(coordinate_x)
            axis_values_y.append(coordinate_y)

    # draw a scatter-plot of the generated values
    fig = plt.figure(figsize=(8, 8))
    ax = plt.subplot()

    # extract the scatterplot drawing in a separate function so we ca re-use the code
    # def draw_scatterplot():
    img = plt.imread("sources/SeatExample1_step2_with_walls.png")
    ax.imshow(img, extent=[0, 8, 0, 8])
    ax.scatter(
        axis_values_x,
        axis_values_y,
        c=instances_colors,
        picker=True,
        s=210
    )

    # initial drawing of the scatterplot
    fig.canvas.toolbar_visible = False
    fig.canvas.header_visible = False
    fig.canvas.footer_visible = False
    plt.plot()
    plt.title(title)

    # present the scatterplot
    ax.grid(False)
    ax.axis('off')
    plt.show()
