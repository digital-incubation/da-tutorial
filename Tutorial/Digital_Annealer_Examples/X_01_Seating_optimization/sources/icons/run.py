import glob

from PIL import Image

for filename in glob.glob('*.png'):

    if '-24' in filename:
        continue

    print(filename)

    tgt = Image.new("RGBA", (32, 24), color="white")
    print(tgt.width, tgt.height)

    src = Image.open(filename)
    print(src.width, src.height)

    tgt.paste(src, ((tgt.width - src.width) // 2, (tgt.height - src.height) // 2))
    tgt.save(filename.replace('.png', '.png'))

    print()
