import numpy as np

def find_seat_size(seat_plan_matrix, max_seat_size):
    if max_seat_size is None:
        raise TypeError('No maximal seat size give!')

    seat_mat_shape = seat_plan_matrix.shape
    if len(seat_mat_shape) != 2 or seat_mat_shape[0] != seat_mat_shape[1]:
        raise TypeError('A symmetric 2D numpy matrix with positive float values as components is necessary!')

    for components in seat_plan_matrix:
        if isinstance(components, (int, float)) and components >= 0:
            raise IndexError('seat matrix components have to be int or float and >=0.')

    if np.sum(seat_plan_matrix - seat_plan_matrix.transpose()) > 0:
        raise TypeError('Given distance matrix is not symmetric')

    for index in range(seat_mat_shape[0]):
        if seat_plan_matrix[index, index]:
            raise TypeError('Given distance matrix diagonal elements should be 0')

    seats = seat_mat_shape[0]
    if seats > max_seat_size:
        raise IndexError('Seat size for this tutorial is to high. Please consider at most' + str(max_seat_size) + ' number of seats.')
    return seats
