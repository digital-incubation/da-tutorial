import numpy as np
from . import config

NUMBER_COORDINATES_SEATS = 42
MIN_DIST_CORRECTION = 1.7  # Needed to get the somehow meter in demo

def click_event_distance_matrix():
    # At first distance table between seats

    seats_coordinates = config.seats_walls[0:NUMBER_COORDINATES_SEATS]  # all seat coordinates
    min_dist = 10 ** 10
    seat_to_seat_distance = {}
    for seat_coord1 in seats_coordinates:
        for seat_coord2 in seats_coordinates:
            if seat_coord1 != seat_coord2:
                distance_seats = np.linalg.norm(np.array(seat_coord1) - np.array(seat_coord2))
                seat_to_seat_distance[(seat_coord1, seat_coord2)] = distance_seats
                min_dist = min([distance_seats, min_dist])
            else:
                seat_to_seat_distance[(seat_coord1, seat_coord2)] = 0

    min_dist = MIN_DIST_CORRECTION * min_dist

    seat_to_seat_distance_normed = {seat_pairs: seat_to_seat_distance[seat_pairs] / min_dist for seat_pairs in seat_to_seat_distance}  # Normalize to 1 meter
    max_dist_after_norming = max([seat_to_seat_distance_normed[seat_pairs] for seat_pairs in seat_to_seat_distance_normed])

    # Next check walls active
    wall_coordinates = config.seats_walls[NUMBER_COORDINATES_SEATS:]
    clicked_wall = [wall_coordinates[clicked] for clicked in range(len(wall_coordinates)) if config.data_points_choosen[clicked + NUMBER_COORDINATES_SEATS][0] == 1]

    for seats_coord in seat_to_seat_distance_normed:
        for wall_coord in config.wall_seat_dependence:
            if not wall_coord in clicked_wall:
                if seats_coord[0] in config.wall_seat_dependence[wall_coord][0] and seats_coord[1] in config.wall_seat_dependence[wall_coord][1]:
                    seat_to_seat_distance_normed[seats_coord] = max_dist_after_norming
                if seats_coord[1] in config.wall_seat_dependence[wall_coord][0] and seats_coord[0] in config.wall_seat_dependence[wall_coord][1]:
                    seat_to_seat_distance_normed[seats_coord] = max_dist_after_norming

    # Next take out clicked seats
    clicked_seats = [seats_coordinates[clicked] for clicked in range(len(seats_coordinates)) if config.data_points_choosen[clicked][0] == 1]
    seat_to_seat_distance_normed_copy = seat_to_seat_distance_normed.copy()
    for coord in clicked_seats:
        for coord_pairs in seat_to_seat_distance_normed_copy:
            if coord == coord_pairs[0] or coord == coord_pairs[1]:
                seat_to_seat_distance_normed.pop(coord_pairs, None)

    # Create Matrix:
    matrix_coord_for_2d_coord = list(set([pair_coord[0] for pair_coord in seat_to_seat_distance_normed]
                                         + [pair_coord[1] for pair_coord in seat_to_seat_distance_normed]))

    distance_matrix_seats = np.zeros((NUMBER_COORDINATES_SEATS - len(clicked_seats), NUMBER_COORDINATES_SEATS - len(clicked_seats)))

    row = 0
    column = 0
    max_distance = 0
    row_key_to_coordinate_matrix = {}
    for seats1 in matrix_coord_for_2d_coord:
        row_key_to_coordinate_matrix[row] = seats1
        for seats2 in matrix_coord_for_2d_coord:
            distance_matrix_seats[row, column] = seat_to_seat_distance_normed[(seats1, seats2)]
            max_distance = max([max_distance, seat_to_seat_distance_normed[(seats1, seats2)]])
            column += 1
        column = 0
        row += 1

    return distance_matrix_seats, max_distance, row_key_to_coordinate_matrix, clicked_wall
