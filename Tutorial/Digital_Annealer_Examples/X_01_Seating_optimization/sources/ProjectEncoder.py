from typing import Dict, Any, Callable
import json

class ProjectEncoder(json.JSONEncoder):
    """
    ``ProjectEncoder`` provides methods to convert objects into json document.
    """

    _functions = {}

    @classmethod
    def register_function(cls, registered_name: str, registered_function: Callable):
        """
        The method registers a custom encoder-function ``registered_function`` under the name ``registered_name``.

        :param str registered_name: name of function in registry.
        :param Callable registered_function: Function to be registered
        """

        cls._functions[registered_name] = registered_function

    def default(self, obj: Any) -> Dict[str, Any]:
        """
        Method for converting an object to JSON.

        :param Any obj: obj to convert to JSON

        :return: dictionary representation of this object
        :rtype: Dict[str, Any]
        """

        class_qualname = obj.__class__.__qualname__

        if class_qualname in self._functions:
            result = self._functions[class_qualname](obj)
            if result is not None:
                return result

        return super().default(obj)

    @classmethod
    def encode(cls, obj: Any) -> Dict[str, Any]:
        class_qualname = obj.__class__.__qualname__

        if class_qualname in cls._functions:
            result = cls._functions[class_qualname](obj)
            if result is not None:
                return result

        return obj
