from dadk.Optimizer import *

def hq_binpol_function(seats, percentual_occupancy, seat_plan_matrix, safety_distance, A_1, A_2, A_3):
    """
    :seats: Number of seats, counting from 0 to ´´seats´´-1
    :percentual_occupancy: wished percentual occupancy of seats as float between 0 and 1
    :seat_plan_matrix: given symmetric seat plan matrix with positive entries and diagonal equal to 0
    :safety_distance: given minimal distance between seats
    :A_1: given weight factor for H1
    :A_2: given weight factor for H2
    :A_3: given weight factor for H3
    """

    var_shape_set = VarShapeSet(BitArrayShape('x', (seats,)))

    # Seat Occupancy Percentage
    H1 = BinPol(var_shape_set)
    for seat in range(seats):
        H1.add_term(1, ('x', seat))
    H1.multiply_scalar(1 / seats)
    H1.add_term(-percentual_occupancy)
    H1.power(2)
    H1.normalize()
    H1.multiply_scalar(A_1)

    # Max Distance Optimization Target
    H2 = BinPol(var_shape_set)
    for seat1 in range(seats):
        for seat2 in range(seats):
            H2.add_term(-seat_plan_matrix[seat1, seat2], ('x', seat1), ('x', seat2))
    H2.normalize()
    H2.multiply_scalar(A_2 / len(H2.p))

    # Safety Distance Constraint
    H3 = BinPol(var_shape_set)
    for seat1 in range(seats):
        for seat2 in range(seats):
            if (seat1 != seat2) and (seat_plan_matrix[seat1, seat2] < safety_distance):
                H3.add_term(1, ('x', seat1), ('x', seat2))
    H3.normalize()
    H3.multiply_scalar(A_3 * len(H3.p))

    # Here we sum up all constraints and optimization targets
    HQ = H1 + H2 + H3

    return H1, H2, H3, HQ, var_shape_set
