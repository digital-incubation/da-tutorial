import os.path
from typing import List, Tuple, Dict, Any
import math
import json
import io
import glob

from PIL import ImageFont, Image, ImageDraw
import ipywidgets.widgets as widgets
from ipywidgets import Layout
from ipyevents import Event
from IPython.display import display

try:
    from .ProjectDecoder import ProjectDecoder
    from .ProjectEncoder import ProjectEncoder
    from .Desk import Desk, Status
except:
    from ProjectDecoder import ProjectDecoder
    from ProjectEncoder import ProjectEncoder
    from Desk import Desk, Status

SEAT_RADIUS = 0.4
BORDER_WIDTH = 0.5
MOVE_STEP_SIZE = 0.4
GRID_SIZE = 2.0

SOURCES_DIR = os.path.abspath(os.path.dirname(__file__))
FONTS_DIR = os.path.abspath(os.path.join(SOURCES_DIR, 'fonts'))
ICONS_DIR = os.path.abspath(os.path.join(SOURCES_DIR, 'icons'))

class Room:

    def __init__(
            self,
            ID: str,
            size: Tuple[float, float],
            description: str = None):
        self._ID = str(ID)
        self._size = (float(size[0]), float(size[1]))
        self.desks: List[Desk] = list()
        self._description = str(description) if (description is not None) else None

    @property
    def ID(self) -> str:
        return self._ID

    @property
    def size(self) -> Tuple[float, float]:
        return self._size

    @size.setter
    def size(self, value: Tuple[float, float]):
        self._size = value

    @property
    def description(self) -> str:
        return self._description

    def _create_image(self, safety_distance: float = None) -> Image:

        factor = 40 * self._zoom / 100
        font_size = int(12 * self._zoom / 100)

        font = ImageFont.truetype(os.path.join(FONTS_DIR, 'ariblk.ttf'), font_size)
        img = Image.new("RGB", (int((self.size[0] + 2 * BORDER_WIDTH) * factor), int((self.size[1] + 2 * BORDER_WIDTH) * factor)), color="white")
        draw = ImageDraw.Draw(img)

        def x(value: float) -> float:
            return (BORDER_WIDTH + value) * factor

        def y(value: float) -> float:
            return (BORDER_WIDTH + value) * factor

        draw.rectangle((
            (x(0), y(0)),
            (x(self.size[0]), y(self.size[1]))
        ), outline="black", fill="WhiteSmoke")

        def draw_desk_corona(desk: Desk):
            if hasattr(desk, 'available'):
                if desk.available:
                    draw.ellipse([
                        (x(desk.position[0] - safety_distance), y(desk.position[1] - safety_distance)),
                        (x(desk.position[0] + safety_distance), y(desk.position[1] + safety_distance))
                    ], fill='#90ee90cc')

        def draw_desk_corona_outline(desk: Desk):
            if hasattr(desk, 'available'):
                if desk.available:
                    draw.ellipse([
                        (x(desk.position[0] - safety_distance), y(desk.position[1] - safety_distance)),
                        (x(desk.position[0] + safety_distance), y(desk.position[1] + safety_distance))
                    ], outline='green', fill=None)

        if safety_distance is not None:
            for desk in self.desks:
                draw_desk_corona(desk)
            for desk in self.desks:
                draw_desk_corona_outline(desk)

        if self._grid > 0:
            d = self._grid
            while d < self.size[0]:
                draw.line((
                    (x(d), y(0)),
                    (x(d), y(self.size[1]))
                ), fill='grey')
                d += self._grid
            d = self._grid
            while d < self.size[1]:
                draw.line((
                    (x(0), y(d)),
                    (x(self.size[0]), y(d))
                ), fill='grey')
                d += self._grid

        desk_color = 'lightgrey'
        seat_color = {
            Status.free: 'yellow',
            Status.blocked: 'red',
            Status.occupied: 'blue'
        }

        angles = [
            math.degrees(math.asin(-2 / math.sqrt(5))),
            math.degrees(math.asin(-2 / math.sqrt(13))),
            math.degrees(math.asin(+2 / math.sqrt(13))),
            math.degrees(math.asin(+2 / math.sqrt(5)))
        ]

        distances = [
            SEAT_RADIUS * math.sqrt(5),
            SEAT_RADIUS * math.sqrt(13),
            SEAT_RADIUS * math.sqrt(13),
            SEAT_RADIUS * math.sqrt(5)
        ]

        def draw_desk(desk: Desk):

            selected = True if (self._selected_desk is not None and self._selected_desk == desk) else False
            outline = 'red' if selected else 'black'
            width = 3 if selected else 1

            if hasattr(desk, 'available'):
                outline = 'green' if desk.available else 'black'
                width = 5 if desk.available else 1

            draw.ellipse([
                (x(desk.position[0] - SEAT_RADIUS), y(desk.position[1] - SEAT_RADIUS)),
                (x(desk.position[0] + SEAT_RADIUS), y(desk.position[1] + SEAT_RADIUS))
            ], outline=outline, width=width, fill=seat_color.get(desk.status, 'yellow'))

            (t_left, t_top, t_right, t_bottom) = draw.textbbox((0, 0), desk.ID, font=font)
            t_width = t_right - t_left
            t_height = t_bottom - t_top
            draw.text((x(desk.position[0]) - 0.5 * t_width, y(desk.position[1]) - 0.8 * t_height), desk.ID, fill='black', font=font)

            if desk.orientation is not None:
                points = [(
                    x(desk.position[0] + distances[i] * math.sin(math.radians(desk.orientation + angles[i]))),
                    y(desk.position[1] - distances[i] * math.cos(math.radians(desk.orientation + angles[i])))
                ) for i in range(len(angles))]
                if desk.orientation % 90 == 0:
                    points = [(0.1 * round(point[0] / 0.1), 0.1 * round(point[1] / 0.1)) for point in points]
                draw.polygon(tuple(points), outline=outline, width=width, fill=desk_color)

        for desk in self.desks:
            draw_desk(desk)

        return img

    def show(self, zoom: float = 100, grid: float = 2.0, safety_distance: float = None):
        self._display(zoom=zoom, grid=grid, editable=False, safety_distance=safety_distance)

    def edit(self, zoom: float = 100, grid: float = 2.0, safety_distance: float = None):
        self._display(zoom=zoom, grid=grid, editable=True, safety_distance=safety_distance)

    def _display(self, zoom: float, grid: float, editable: bool, safety_distance: float = None):

        self._zoom = zoom
        self._grid = grid
        self._editable = editable
        self._selected_desk: Desk = None

        img = self._create_image(safety_distance=safety_distance)
        buffer = io.BytesIO()
        img.save(buffer, format='png')
        image = widgets.Image(value=buffer.getvalue(), format='png', height=img.height, width=img.width)

        def repaint():
            img = self._create_image(safety_distance=safety_distance)
            buffer = io.BytesIO()
            img.save(buffer, format='png')
            image.width = img.width
            image.height = img.height
            image.value = buffer.getvalue()

            self._wide_slider.value = self.size[0]
            self._long_slider.value = self.size[1]

        info = widgets.HTML('')

        # ------------------------------------------------------------

        def on_click(event):

            factor = 40 * self._zoom / 100

            x = event['dataX'] / factor - BORDER_WIDTH
            y = event['dataY'] / factor - BORDER_WIDTH

            info.value = f'<h4>Click ({x:.2f}, {y:.2f}).</h4>'

            nearest_desk = None
            nearest_desk_distance = math.inf

            for desk in self.desks:
                dx = x - desk.position[0]
                if abs(dx) > SEAT_RADIUS:
                    continue
                dy = y - desk.position[1]
                if abs(dy) > SEAT_RADIUS:
                    continue
                distance = math.sqrt(dx ** 2 + dy ** 2)
                if distance < nearest_desk_distance:
                    nearest_desk = desk
                    nearest_desk_distance = distance

            if nearest_desk_distance > SEAT_RADIUS:
                nearest_desk = None

            do_repaint = False

            if nearest_desk is None:
                info.value = f'<h4>No seat near position ({x:.2f}, {y:.2f}) found.</h4>'
                if self._selected_desk is not None:
                    do_repaint = True
                    self._selected_desk = None
            else:
                if (self._selected_desk is not None) and (self._selected_desk == nearest_desk):
                    pass
                else:
                    do_repaint = True
                    self._selected_desk = nearest_desk
                info.value = f'<h4>Selected {"desk" if (self._selected_desk.orientation is not None) else "seat"} = {self._selected_desk.ID} {self._selected_desk.status.name} {self._selected_desk.description if (self._selected_desk.description is not None) else ""}</h4>'

            if do_repaint:
                repaint()

        click_event = Event(source=image, watched_events=['click'])
        click_event.on_dom_event(on_click)

        # ------------------------------------------------------------

        def on_keydown(event):

            do_repaint = False

            key = str(event['key']).strip()
            if self._selected_desk is not None:
                if key == 'b':
                    self._selected_desk.status = Status.free if (self._selected_desk.status == Status.blocked) else Status.blocked
                    do_repaint = True
                elif key == 'o':
                    self._selected_desk.status = Status.free if (self._selected_desk.status == Status.occupied) else Status.occupied
                    do_repaint = True
                elif self._editable:
                    if key == 'r':
                        self._selected_desk.orientation += 90
                        do_repaint = True
                    elif key == 'R':
                        self._selected_desk.orientation += 15
                        do_repaint = True
                    elif key == 'l':
                        self._selected_desk.orientation -= 90
                        do_repaint = True
                    elif key == 'L':
                        self._selected_desk.orientation -= 15
                        do_repaint = True
                    elif key == 'ArrowUp':
                        self._selected_desk.position = (self._selected_desk.position[0], self._selected_desk.position[1] - MOVE_STEP_SIZE)
                        do_repaint = True
                    elif key == 'ArrowRight':
                        self._selected_desk.position = (self._selected_desk.position[0] + MOVE_STEP_SIZE, self._selected_desk.position[1])
                        do_repaint = True
                    elif key == 'ArrowDown':
                        self._selected_desk.position = (self._selected_desk.position[0], self._selected_desk.position[1] + MOVE_STEP_SIZE)
                        do_repaint = True
                    elif key == 'ArrowLeft':
                        self._selected_desk.position = (self._selected_desk.position[0] - MOVE_STEP_SIZE, self._selected_desk.position[1])
                        do_repaint = True
                    elif key == 'Delete':
                        self.desks.remove(self._selected_desk)
                        self._selected_desk = None
                        do_repaint = True

            if do_repaint:
                repaint()
                if self._selected_desk is not None:
                    info.value = f'<h4>Selected {"desk" if (self._selected_desk.orientation is not None) else "seat"} = {self._selected_desk.ID} {self._selected_desk.status.name} {self._selected_desk.description if (self._selected_desk.description is not None) else ""}</h4>'
                else:
                    info.value = f'<h4></h4>'

        keydown_event = Event(source=image, watched_events=['keydown'])
        keydown_event.on_dom_event(on_keydown)

        # ------------------------------------------------------------

        def create_icon(filename: str) -> widgets.Image:
            src = Image.open(filename)
            buffer = io.BytesIO()
            src.save(buffer, format='png')
            return widgets.Image(value=buffer.getvalue(), format='png',
                                 height=src.height, width=src.width,
                                 layout=Layout(width=f'{src.width}px', height=f'{src.height}px'))

        # ------------------------------------------------------------

        self._load_select = widgets.Dropdown(
            description='open',
            disabled=not editable,
            value=self._filename if hasattr(self, '_filename') else None,
            options=sorted(glob.glob(os.path.join('data', '*.json'))))

        def load_select_changed(change):

            new_room = Room.load(change['new'])

            self._ID = new_room.ID
            self._size = new_room.size
            self._description = new_room.description
            self.desks = new_room.desks
            self._filename = new_room._filename
            self._save_text.value = self._filename
            repaint()

        self._load_select.observe(load_select_changed, names='value')

        # ------------------------------------------------------------

        if editable:
            self._save_text = widgets.Text(
                description='save',
                disabled=not editable,
                value=self._filename)

            self._save_button = widgets.Button(
                description='save',
                disabled=not editable)

            def save_button_clicked(b):
                self.save(self._save_text.value)
                self._load_select.options = sorted(glob.glob(os.path.join('data', '*.json')))

            self._save_button.on_click(save_button_clicked)

        # ------------------------------------------------------------

        self._wide_slider = widgets.FloatSlider(
            description='wide [m]',
            disabled=not editable,
            value=self.size[0],
            min=2.0, max=50.0, step=0.2)

        def wide_slider_changed(change):
            self.size = (change['new'], self.size[1])
            repaint()

        self._wide_slider.observe(wide_slider_changed, names='value')

        # ------------------------------------------------------------

        self._long_slider = widgets.FloatSlider(
            description='long [m]',
            disabled=not editable,
            value=self.size[1],
            min=2.0, max=50.0, step=0.2)

        def long_slider_changed(change):
            self.size = (self.size[0], change['new'])
            repaint()

        self._long_slider.observe(long_slider_changed, names='value')

        # ------------------------------------------------------------

        self._zoom_slider = widgets.IntSlider(
            description='zoom [%]',
            value=self._zoom,
            min=10, max=200, step=10)

        def zoom_slider_changed(change):
            self._zoom = change['new']
            repaint()

        self._zoom_slider.observe(zoom_slider_changed, names='value')

        # ------------------------------------------------------------

        self._grid_slider = widgets.FloatSlider(
            description='grid [m]',
            value=self._grid,
            min=0.0,
            max=5.0,
            step=0.2)

        def grid_slider_changed(change):
            self._grid = change['new']
            repaint()

        self._grid_slider.observe(grid_slider_changed, names='value')

        # ------------------------------------------------------------

        if editable:

            self._add_select = widgets.Dropdown(
                description='desk',
                disabled=not editable,
                value='desk 80x160',
                options=['desk 80x160'])

            self._add_button = widgets.Button(
                description='add',
                disabled=not editable)

            def add_button_clicked(b):
                centre_x = MOVE_STEP_SIZE * math.floor(self.size[0] / (2 * MOVE_STEP_SIZE))
                centre_y = MOVE_STEP_SIZE * math.floor(self.size[1] / (2 * MOVE_STEP_SIZE))

                r = 0
                while r < 100:
                    for dx in range(-r, +r + 1, +1):
                        for dy in range(-r, +r + 1, +1):
                            x = centre_x + dx * 2 * MOVE_STEP_SIZE
                            y = centre_y + dy * 2 * MOVE_STEP_SIZE
                            if 0 < x < self.size[0] and 0 < y < self.size[1]:
                                if len(self.desks) == 0 or min([math.sqrt((x - desk.position[0]) ** 2 + (y - desk.position[1]) ** 2) for desk in self.desks]) > 3.05 * MOVE_STEP_SIZE:
                                    self.desks.append(Desk(ID=str(1 + len(self.desks)), position=(x, y), orientation=0))
                                    self._selected_desk = self.desks[-1]
                                    repaint()
                                    return
                    r += 1

            self._add_button.on_click(add_button_clicked)

        # ------------------------------------------------------------

        vbox = widgets.VBox(tuple(
            ([widgets.HBox((
                create_icon(os.path.join(ICONS_DIR, "folder-open-solid.png")),
                self._load_select))] if editable else []) +
            ([widgets.HBox((
                create_icon(os.path.join(ICONS_DIR, "floppy-disk-solid.png")),
                self._save_text,
                self._save_button))] if editable else []) +
            ([widgets.HBox((
                create_icon(os.path.join(ICONS_DIR, "left-right-solid.png")),
                self._wide_slider,
                create_icon(os.path.join(ICONS_DIR, "up-down-solid.png")),
                self._long_slider))] if editable else []) +
            [widgets.HBox((
                create_icon(os.path.join(ICONS_DIR, "magnifying-glass-solid.png")),
                self._zoom_slider,
                create_icon(os.path.join(ICONS_DIR, "table-cells-solid.png")),
                self._grid_slider))] +
            ([widgets.HBox((
                create_icon(os.path.join(ICONS_DIR, "circle-plus-solid.png")),
                self._add_select,
                self._add_button))] if editable else []) +
            [image] +
            [info]))

        display(vbox)

    def save(self, filename: str = None):
        if filename is None:
            filename = f'{self.ID}.json'
        with open(filename, 'w') as fp:
            json.dump(self, fp, indent=4, cls=ProjectEncoder)

    @classmethod
    def load(cls, filename: str) -> 'Room':

        filename = os.path.join(*filename.split('\\'))
        filename = os.path.join(*filename.split('/'))

        with open(filename, 'r') as fp:
            room = json.load(fp, object_hook=ProjectDecoder.object_hook)
            room._filename = filename
            return room

def _encode_Room(room: Room) -> Dict[str, Any]:
    """
    convert Room to json

    :param Room room: desk to convert to JSON

    :return: dictionary representation of this object
    :rtype: Dict[str, Any]
    """
    value = {'__class__': Room.__qualname__}

    value['ID'] = room.ID
    value['size'] = room.size

    if room.desks is not None:
        value['desks'] = [ProjectEncoder.encode(desk) for desk in room.desks]

    if room.description is not None:
        value['description'] = room.description

    return value

ProjectEncoder.register_function(Room.__qualname__, _encode_Room)

def _decode_Room(data: Dict[str, Any]) -> Room:
    """
    convert json to Room

    :param Dict[str,Any] data: the JSON-structure to convert

    :return: new Room object
    :rtype: Room
    """

    room = Room(
        ID=data['ID'],
        size=data['size'],
        description=data['description'] if ('description' in data) else None)

    room.desks = data['desks'] if ('desks' in data) else []

    return room

ProjectDecoder.register_function(Room.__qualname__, _decode_Room)
