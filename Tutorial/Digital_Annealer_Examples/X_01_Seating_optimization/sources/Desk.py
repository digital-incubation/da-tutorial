from typing import Tuple, Dict, Any
from enum import Enum

try:
    from .ProjectDecoder import ProjectDecoder
    from .ProjectEncoder import ProjectEncoder
except:
    from ProjectDecoder import ProjectDecoder
    from ProjectEncoder import ProjectEncoder

class Status(Enum):
    free = -1
    blocked = 0
    occupied = 1

class Desk:

    def __init__(
            self,
            ID: str,
            position: Tuple[float, float],
            orientation: float = None,
            description: str = None,
            status: Status = Status.free):
        self._ID = str(ID)
        self._position = (float(position[0]), float(position[1]))
        self._orientation = float(orientation) if (orientation is not None) else None
        self._description = str(description) if (description is not None) else None
        self._status = status

    def __str__(self):
        parts = [f'{self.ID} {self.position}']
        if self.orientation is not None:
            parts.append(f'{self.orientation}°')
        if self.description is not None:
            parts.append(f'"{self.description}"')
        return ' '.join(parts)

    @property
    def ID(self) -> str:
        return self._ID

    @property
    def position(self) -> Tuple[float, float]:
        return self._position

    @position.setter
    def position(self, position: Tuple[float, float]):
        self._position = position

    @property
    def orientation(self) -> float:
        return self._orientation

    @orientation.setter
    def orientation(self, orientation: float):
        self._orientation = orientation % 360

    @property
    def description(self) -> str:
        return self._description

    @property
    def status(self) -> Status:
        return self._status

    @status.setter
    def status(self, status: Status):
        self._status = status

def _encode_Desk(desk: Desk) -> Dict[str, Any]:
    """
    convert Desk to json

    :param Desk desk: desk to convert to JSON

    :return: dictionary representation of this object
    :rtype: Dict[str, Any]
    """
    value = {'__class__': Desk.__qualname__}

    value['ID'] = desk.ID
    value['position'] = desk.position

    if desk.orientation is not None:
        value['orientation'] = desk.orientation

    if desk.description is not None:
        value['description'] = desk.description

    return value

ProjectEncoder.register_function(Desk.__qualname__, _encode_Desk)

def _decode_Desk(data: Dict[str, Any]) -> Desk:
    """
    convert json to Desk

    :param Dict[str,Any] data: the JSON-structure to convert

    :return: new Desk object
    :rtype: Desk
    """

    return Desk(
        ID=data['ID'],
        position=data['position'],
        orientation=data['orientation'] if ('orientation' in data) else None,
        description=data['description'] if ('description' in data) else None)

ProjectDecoder.register_function(Desk.__qualname__, _decode_Desk)
