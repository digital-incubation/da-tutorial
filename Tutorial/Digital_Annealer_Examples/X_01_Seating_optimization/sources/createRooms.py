from Room import Room
from Desk import Desk, Status
import math

room = Room(ID='office', size=(14.8, 21.6))
room.desks = [
    Desk(ID='1', position=(2.4, 1.6), orientation=180),
    Desk(ID='2', position=(4.4, 2.8), orientation=270),
    Desk(ID='3', position=(2.4, 4.0), orientation=0),
    Desk(ID='4', position=(6.8, 1.6), orientation=180),
    Desk(ID='5', position=(8.8, 2.8), orientation=270),
    Desk(ID='6', position=(6.8, 4.0), orientation=0),
    Desk(ID='7', position=(11.2, 1.6), orientation=180),
    Desk(ID='8', position=(13.2, 2.8), orientation=270),
    Desk(ID='9', position=(11.2, 4.0), orientation=0),
    Desk(ID='10', position=(2.0, 7.6), orientation=90),
    Desk(ID='11', position=(4.0, 6.4), orientation=180),
    Desk(ID='12', position=(4.0, 8.8), orientation=0),
    Desk(ID='13', position=(6.4, 7.6), orientation=90),
    Desk(ID='14', position=(8.4, 6.4), orientation=180),
    Desk(ID='15', position=(8.4, 8.8), orientation=0),
    Desk(ID='16', position=(10.8, 7.6), orientation=90),
    Desk(ID='17', position=(12.8, 6.4), orientation=180),
    Desk(ID='18', position=(12.8, 8.8), orientation=0),
    Desk(ID='19', position=(2.0, 11.6), orientation=270),
    Desk(ID='20', position=(4.8, 11.6), orientation=270),
    Desk(ID='21', position=(7.6, 11.6), orientation=270),
    Desk(ID='22', position=(2.0, 14.0), orientation=270),
    Desk(ID='23', position=(4.8, 14.0), orientation=270),
    Desk(ID='24', position=(7.6, 14.0), orientation=270),
    Desk(ID='25', position=(11.2, 11.2), orientation=180),
    Desk(ID='26', position=(13.2, 12.4), orientation=270),
    Desk(ID='27', position=(12.0, 14.4), orientation=0),
    Desk(ID='28', position=(10.0, 13.2), orientation=90),
    Desk(ID='29', position=(3.2, 16.8), orientation=180),
    Desk(ID='30', position=(5.2, 18.0), orientation=270),
    Desk(ID='31', position=(4.0, 20.0), orientation=0),
    Desk(ID='32', position=(2.0, 18.8), orientation=90),
    Desk(ID='33', position=(6.4, 16.4), orientation=90),
    Desk(ID='34', position=(8.8, 16.4), orientation=270),
    Desk(ID='35', position=(6.4, 20.0), orientation=90),
    Desk(ID='36', position=(8.8, 20.0), orientation=270),
    Desk(ID='37', position=(12.0, 16.8), orientation=180),
    Desk(ID='38', position=(13.2, 18.8), orientation=270),
    Desk(ID='39', position=(11.2, 20.0), orientation=0),
    Desk(ID='40', position=(10.0, 18.0), orientation=90)
]

print(f'room with {len(room.desks)} desks')
room.save(f'../data/{room.ID}.json')

# --------------------------------------------------------------------------------

room = Room(ID='theatre', size=(25, 14))
row = 0
for n in range(11, 48, 5):
    row += 1
    r = (n - 1) * 0.802 / math.pi
    room.desks += [Desk(ID=f'{row}/{i + 1}',
                        position=(
                            room.size[0] / 2 + r * math.cos(i * (math.pi / (n - 1))),
                            1.5 + r * math.sin(i * (math.pi / (n - 1)))),
                        description=f'Row: {row}, Seat: {i + 1}',
                        status=Status.free)
                   for i in range(n)]

print(f'room with {len(room.desks)} seats')
room.save(f'../data/{room.ID}.json')

# --------------------------------------------------------------------------------

room = Room(ID='croud', size=(35, 35))
row = 0
for n in range(6, 121, 6):
    row += 1
    r = n * 0.802 / (2 * math.pi)
    room.desks += [Desk(ID=f'{row}/{i + 1}',
                        position=(
                            room.size[0] / 2 + r * math.cos(i * ((2 * math.pi) / n)),
                            room.size[1] / 2 + r * math.sin(i * ((2 * math.pi) / n))),
                        description=f'Row: {row}, Seat: {i + 1}',
                        status=Status.free)
                   for i in range(n)]

print(f'room with {len(room.desks)} seats')
room.save(f'../data/{room.ID}.json')
