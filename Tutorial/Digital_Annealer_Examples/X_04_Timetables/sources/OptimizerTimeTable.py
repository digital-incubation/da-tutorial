import numpy as np
import pandas as pd
import random
from dadk.Optimizer import *
from termcolor import colored
from IPython.display import display
from sources.DataGeneration import DataGeneration


class OptimizerTimeTable(OptimizerModel):

    ##################################################################################################################
    def __init__(self, persistent_file="X_04_Timetables.dao", create_complete_qubo=None):
        ##################################################################################################################
        self.create_complete_qubo = create_complete_qubo

        OptimizerModel.__init__(
            self,
            name='Time_Table_Assignment',
            load_parameter=[
                {'type': 'begin_group', 'label': 'General', 'tags': ["normal"]},
                {'name': 'source', 'type': 'select', 'value': 'file', 'options': ['file', 'generator'],
                 'label': 'Source', 'description': 'Source for conflict graph.', 'tags': ['normal'],
                 'on_change': 'accordion1.selected_index = 0 if new_value=="file" else 1'},
                {'type': 'end_group'},
                {'type': 'begin_group', 'label': 'Files', 'tags': ["normal"]},
                {'name': 'filename_requirements',
                 'value': 'data/hdtt4req.txt',
                 'type': 'file_selector',
                 'suffix': 'txt',
                 'label': 'Requirement Data',
                 'description': 'Requirement Data File Name',
                 'tags': ['demo']},
                {'type': 'begin_group', 'label': 'Generator', 'tags': ["normal"]},
                {'name': "CLASSES",
                 'type': 'int_bounded',
                 'value': 2,
                 'min': 2,
                 'max': 6,
                 'label': 'CLASSES',
                 'description': 'CLASSES'},
                {'name': "TEACHERS",
                 'type': 'int_bounded',
                 'value': 2,
                 'min': 2,
                 'max': 6,
                 'label': 'TEACHERS',
                 'description': 'TEACHERS'},
                {'name': "ROOMS",
                 'type': 'int_bounded',
                 'value': 2,
                 'min': 2,
                 'max': 6,
                 'label': 'ROOMS',
                 'description': 'ROOMS'},
                {'name': "PERIODS",
                 'type': 'int_bounded',
                 'value': 3,
                 'min': 2,
                 'max': 6,
                 'label': 'PERIODS',
                 'description': 'PERIODS'},
                {'name': "DAYS",
                 'type': 'int_bounded',
                 'value': 2,
                 'min': 2,
                 'max': 5,
                 'label': 'DAYS',
                 'description': 'DAYS'},
                {'name': "seed_number",
                 'type': 'int_bounded',
                 'value': 1,
                 'min': 0,
                 'max': 100000,
                 'label': 'Random Seed',
                 'description': 'Random seed'},
                {'name': "iterations",
                 'type': 'int_bounded',
                 'value': 5000,
                 'min': 500,
                 'max': 50000,
                 'label': 'Iterations for Data Generations',
                 'description': 'Iterations for Data Generations'},
                {'type': 'end_group'}],
            build_qubo_parameter=[
                {'name': "penalty_coefficient",
                 'type': 'int_bounded',
                 'value': 5,
                 'min': 1,
                 'max': 1000,
                 'label': 'Penalty Coefficient',
                 'description': 'Coefficient multiplied to Penalty QUBO terms',
                 },
                {'name': "objective_coefficient",
                 'type': 'int_bounded',
                 'value': 5,
                 'min': 1,
                 'max': 1000,
                 'label': 'Objective Coefficient',
                 'description': 'Coefficient multiplied to the objective',
                 },
                {'name': "qubo_weight",
                 'type': 'int_bounded',
                 'value': 500,
                 'min': 10,
                 'max': 100000,
                 'label': 'Qubo Weight',
                 'description': 'Coefficient multiplied to final QUBO',
                 },
                {'type': 'end_group'}],
            persistent_file=persistent_file)


    ##################################################################################################################
    def load(self,
             source='file',
             filename_requirements='data/hdtt4req.txt',
             CLASSES=None,
             TEACHERS=None,
             ROOMS=None,
             PERIODS=None,
             DAYS=None,
             iterations=None,
             seed_number=None,
             silent=False):
        '''
        The method takes the metadata file and the requirement data file, and loads the data in parameters `CLASSES`, `ROOM`, `TEACHERS`, `REQUIREMENTS`.
        The requirement data is stored in a dictionary named `requirement_data`.
        :param `filename_requirements`: Filename for requirement data
        :return: -
        '''
        if source == 'file':
            self.filename_requirements = filename_requirements
            file_number = int(self.filename_requirements.split('hdtt')[1][0])

            self.PERIODS = 6
            self.DAYS = 5
            self.CLASSES = file_number
            self.ROOMS = file_number
            self.TEACHERS = file_number

            file = open(filename_requirements).readlines()
            self.requirement_data = dict()
            counter = -1
            for line in file:
                counter += 1
                for teacher_num in range(self.TEACHERS):
                    key = (1 + counter % self.CLASSES, 1 + int(np.floor(counter / self.ROOMS)), teacher_num + 1)  # (c,r,t)
                    self.requirement_data[key] = int(line.split(' ')[teacher_num])

        elif source == 'generator':
            self.PERIODS = PERIODS
            self.DAYS = DAYS
            self.CLASSES = CLASSES
            self.ROOMS = ROOMS
            self.TEACHERS = TEACHERS
            self.iterations = iterations
            self.seed_number = seed_number

            self.requirement_bit_size = len(bin(self.PERIODS)[2::])
            self.weekly_lesson_numbers = self.PERIODS * self.DAYS
            total_bits = self.requirement_bit_size * self.CLASSES * self.ROOMS * self.TEACHERS
            if total_bits > 1024:
                raise Exception('Total bits greater than 1024, please reduce the selected values, and try again!.')
            else:
                self.data_object = DataGeneration(self.CLASSES, self.TEACHERS, self.ROOMS, self.PERIODS, self.DAYS, self.iterations, self.seed_number)
                self.requirement_data = self.data_object.requirement_data

        print(f'Total Teachers   : {self.TEACHERS}')
        print(f'Total Classes    : {self.CLASSES}')
        print(f'Total Rooms      : {self.ROOMS}')
        print(f'Periods/Day      : {self.PERIODS}')
        print(f'School Days/Week : {self.DAYS}')

    ##################################################################################################################

    def build_qubo(self, penalty_coefficient, objective_coefficient, qubo_weight, silent=False):
        '''
        The `build_qubo` method is used to create the QUBO described in the description of the notebook.
        The method takes different penalty terms and saves the QUBO terms in the class attribute.
        :param `penalty_coefficient`: Penalty term multiplied to QUBO terms corresponding to constraints
        :param `objective_penalty`: Penalty term multiplied to QUBO terms corresponding to objective
        :param `qubo_weight`: Overall weight multiplied to the complete QUBO for better convergence of the solution
        :return: -
        '''
        if self.create_complete_qubo is not None:
            self.HQ, self.H1, self.H2, self.H3, self.H_obj = self.create_complete_qubo(self.CLASSES,
                                                                                       self.ROOMS,
                                                                                       self.TEACHERS,
                                                                                       self.PERIODS,
                                                                                       self.DAYS,
                                                                                       self.requirement_data,
                                                                                       penalty_coefficient,
                                                                                       objective_coefficient,
                                                                                       qubo_weight)
        else:
            var_shape_set = VarShapeSet(BitArrayShape('x', (self.CLASSES, self.ROOMS, self.TEACHERS, self.PERIODS, self.DAYS)))

            H1 = BinPol(var_shape_set)
            for p in range(self.PERIODS):
                for t in range(self.TEACHERS):
                    for d in range(self.DAYS):
                        temp_linear = BinPol(var_shape_set)
                        for c in range(self.CLASSES):
                            for r in range(self.ROOMS):
                                temp_linear.add_term(1, ('x', c, r, t, p, d))
                        temp_qubo = (temp_linear - 1).multiply(temp_linear)
                        H1.add(temp_qubo)
            H1.multiply_scalar(penalty_coefficient)

            H2 = BinPol(var_shape_set)
            for r in range(self.ROOMS):
                for p in range(self.PERIODS):
                    for d in range(self.DAYS):
                        temp_linear = BinPol(var_shape_set)
                        for c in range(self.CLASSES):
                            for t in range(self.TEACHERS):
                                temp_linear.add_term(1, ('x', c, r, t, p, d))
                        temp_qubo = (temp_linear - 1).multiply(temp_linear)
                        H2.add(temp_qubo)
            H2.multiply_scalar(penalty_coefficient)

            H3 = BinPol(var_shape_set)
            for c in range(self.CLASSES):
                for p in range(self.PERIODS):
                    for d in range(self.DAYS):
                        temp_linear = BinPol(var_shape_set)
                        for t in range(self.TEACHERS):
                            for r in range(self.ROOMS):
                                temp_linear.add_term(1, ('x', c, r, t, p, d))
                        temp_qubo = (temp_linear - 1).multiply(temp_linear)
                        H3.add(temp_qubo)
            H3.multiply_scalar(penalty_coefficient)

            H_obj = BinPol(var_shape_set)
            for c in range(self.CLASSES):
                for r in range(self.ROOMS):
                    for t in range(self.TEACHERS):
                        req_key = (c + 1, r + 1, t + 1)
                        periods = self.requirement_data[req_key]
                        temp_qubo = BinPol(var_shape_set)
                        temp_qubo.add_term(periods)
                        for p in range(self.PERIODS):
                            for d in range(self.DAYS):
                                temp_qubo.add_term(-1, ('x', c, r, t, p, d))
                        temp_qubo.multiply(temp_qubo)
                        H_obj.add(temp_qubo)
            H_obj.multiply_scalar(objective_coefficient)

            HQ = H1.clone().add(H2).add(H3).add(H_obj).multiply_scalar(qubo_weight)

            self.HQ = HQ
            self.H1 = H1
            self.H2 = H2
            self.H3 = H3
            self.H_obj = H_obj

        print(f'Total QUBO Bits: {self.HQ.N}\n')
        print(f'QUBO Build Done.')

    ##################################################################################################################

    def prep_result(self, solution_list:SolutionList, silent=False):
        '''
        The `prep_result` method is used to read the solution after the optimization. 
        :param `solution_list`: Solution list attribute of the Optimizer Class
        :return: -
        '''
        solution = solution_list.min_solution
        values = solution.extract_bit_array('x').data
        time_table_solution = dict()
        indices = zip(*np.where(values == 1))
        for index in indices:
            (c, r, t, p, d) = index
            key = (int(c + 1), int(r + 1), int(t + 1))
            if key in time_table_solution:
                time_table_solution[key].append((int(p + 1), int(d + 1)))
            else:
                time_table_solution[key] = []
                time_table_solution[key].append((int(p + 1), int(d + 1)))
        self.time_table_solution = time_table_solution

        HQ_value = self.HQ.compute(solution.configuration)
        H1_value = self.H1.compute(solution.configuration)
        H2_value = self.H2.compute(solution.configuration)
        H3_value = self.H3.compute(solution.configuration)
        H_obj_value = self.H_obj.compute(solution.configuration)

        print(f'HQ value    : {HQ_value}')
        print('--------------------------')
        print(f'H1 value    : {H1_value}')
        print(f'H2 value    : {H2_value}')
        print(f'H3 value    : {H3_value}')
        print(f'H_obj value : {H_obj_value}\n')

        if round(HQ_value, 6) == 0.0:
            print(colored('An Optimal Solution Obtained \n', 'green'))
        else:
            print(colored('Infeasible Solution, Parameters Need Tuning!  \n', 'red'))

    ##################################################################################################################

    def report(self, silent=False):
        '''
        The `report` method generates a solution table consisting of the final assignments obtained after optimization
        :param None:
        :return: -
        '''

        column_names = []
        for p in range(self.PERIODS):
            column_names.append(f'P{p + 1}')
        Class = dict()
        for c in range(1, self.CLASSES + 1):
            Class[f'C{c}'] = pd.DataFrame(np.nan, index=list(range(self.DAYS)), columns=column_names, dtype = 'object')
        for key in self.time_table_solution:
            (c, r, t) = key
            pd_tuples = self.time_table_solution[key]
            for (p, d) in pd_tuples:
                Class[f'C{c}'].loc[d - 1, f'P{p}'] = f'R{r}, T{t}'
        for c in range(1, self.CLASSES + 1):
            print(f'\n\033[1m Room and Teacher Assignment for Class {c} \033[0m ')
            Class[f'C{c}'] = Class[f'C{c}'].rename(index={0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday', 4: 'Friday'})
            d = dict(selector="th", props=[('text-align', 'center')])
            display(Class[f'C{c}'].style.set_properties(**{'width': '10em', 'text-align': 'center'}).set_table_styles([d]))

        data = dict()
        data['(C, R, T)'] = []
        data['Requirements'] = []
        data['(Periods, Days)'] = []
        for key in self.time_table_solution:
            data['(C, R, T)'].append(key)
            data['Requirements'].append(self.requirement_data[key])
            data['(Periods, Days)'].append(self.time_table_solution[key])

        self.dataframe = pd.DataFrame(data=data)
        d = dict(selector="th", props=[('text-align', 'center')])
        display(self.dataframe.style.set_properties(**{'width': '15em', 'text-align': 'center'}).set_table_styles([d]))
