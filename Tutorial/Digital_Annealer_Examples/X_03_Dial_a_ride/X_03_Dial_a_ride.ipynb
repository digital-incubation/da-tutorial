{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dial a Ride Model Tutorial\n",
    "\n",
    "<img src=\"figures/josh-connor-H3cQClU6JC0-unsplash.jpg\" alt=\"Drawing\" style=\"width: 640px;\"/>\n",
    "<font><center><a href=\"https://unsplash.com/photos/H3cQClU6JC0\">https://unsplash.com/photos/H3cQClU6JC0</a></center></font>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "This notebook shows a solution methods for a static dial a ride problem by a binary optimization approach.\n",
    "\n",
    "In particular, the scenario is given by a map, along with a number of requests with pickup and dropoff locations and desired pickup times. Moreover, a fleet of cars with initial locations and start times is spread over the map. \n",
    "The solution should assign the most possible passengers - requests - with the least possible waiting times to available cars, such that the overall win as computed from individual rides is maximal.\n",
    "\n",
    "<img src=\"figures/Static_ride_hailing_pre_optimization.png\" alt=\"Drawing\" style=\"width: 700px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dial a Ride Model: Problem description\n",
    "\n",
    "In this notebook, cars will drive with constant speed. Each driven kilometer by a car has a certain cost.\n",
    "At the same time, each kilometer that a car drives with a request on board has a price. Moreover, a base price per delivered request can be set.\n",
    "One optimization target is the maximization of win for all cars, which is price reduced by cost.\n",
    "\n",
    "<img src=\"figures/Costs.png\" alt=\"Drawing\" style=\"width: 300px;\"/>\n",
    "\n",
    "For example, let us assume a car drives $7$ kilometers in total and the costs per kilometer are $0.1\\$$. For this car, this means a total cost of $0.7\\$$. Further assume, the car carries one passenger for a distance of $5$ kilometers and the price per kilometer for carrying a passenger is $1.5\\$$. Then the total price for the car is $7.5\\$$.\n",
    "Therefore, the win of the car is $6.8\\$$.\n",
    "\n",
    "Each request wants to be picked up as early as possible after his desired pickup time. With delayed pickup, the request is assigned a dissatisfaction which grows with waiting time according to a dissatisfaction measure, until the request is rejected after some time span.\n",
    "\n",
    "<img src=\"figures/Dissatisfaction.png\" alt=\"Drawing\" style=\"width: 300px;\"/>\n",
    "\n",
    "In the example above, let us assume two requests want to be picked up as early as possible, but the car has to drive $2$ kilometers to the pickup location of the closest request in a big traffic jam, so that it arrives after $6$ minutes. Further assume, the total time span until rejection is $10$ minutes, and the rejection cost after this is $10$. The car could reach the second request only after $11$ minutes, and hence the request must be rejected. According to the dissatisfaction measure above, this implies a dissatisfaction of $2$ plus a rejection cost of $10$, since only one passenger could be assigned to a car, but it can still be reached in time.\n",
    "The overall dissatisfaction is therefore $12$.\n",
    "\n",
    "The problem to solve is to assign as many requests as possible to cars, such that the overall win (i.e. price reduced by cost for each car), reduced by the overall dissatisfaction is maximal.\n",
    "Since both optimization targets are contradictive, they must be weighted accordingly with constants $C_{cars},C_{requests}>0$.\n",
    "\n",
    "<img src=\"figures/optimization_target.png\" alt=\"Drawing\" style=\"width: 800px;\"/>\n",
    "\n",
    "If $C_{cars} >> C_{requests}$, maximization of win is favored over minimization of waiting time, while for $C_{cars} << C_{requests}$, lower wins are rather tolerated to reduce request waiting time.\n",
    "In the example above, let us assume for simplicity that we have one car and two passengers to be carried. However, as explained above, the car can only carry one passenger, since it can not reach the second request in time. For $C_{cars}=2, C_{requests}=1$, this implies an optimization target value of\n",
    "$$\n",
    "2 * 6.8 - 12 = 1.6. \n",
    "$$\n",
    "Let us assume, the car would have chosen the other request, which would have taken $3$ kilometers to reach plus a traveling distance of $6$ kilometers. Then the cost would have been $0.3\\$$, and the price $9\\$$. This implies a win of $8.7\\$$.\n",
    "In the scenario, assume the request could have been reached only after $8$ minutes, which implies a dissatisfaction of $6$, and that now the first request would have to be rejected at a rejection cost of $10$.\n",
    "Then the overall dissatisfaction would be $16$.\n",
    "The optimization target value would be\n",
    "$$\n",
    "2 * 8.7 - 16 = 1.4. \n",
    "$$\n",
    "The optimal assignment for the example is therefore the first one, since carrying both passengers is not possible, and carrying none of them would imply an optimization value of $-20$ due to two rejections.\n",
    "\n",
    "The solution of a problem consists of an assignment sequence of requests to cars. As mentioned, requests which could not be assigned have to be rejected.\n",
    "\n",
    "<img src=\"figures/Static_ride_hailing_post_optimization.png\" alt=\"Drawing\" style=\"width: 700px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Result presentation\n",
    "\n",
    "In the implementation below, results are presented in a tabular, and visualized with an interactive graphic.\n",
    "\n",
    "<table>\n",
    "  <tr>\n",
    "    <td> <img src=\"figures/Result_example_report.PNG\" alt=\"Drawing\" style=\"width: 900px;\"/>\n",
    "    <td> <img src=\"figures/Result_example_picture.PNG\" alt=\"Drawing\" style=\"width: 500px;\"/>\n",
    "  </tr>\n",
    "</table>\n",
    "\n",
    "Desired and actual pickup times are listed for all passengers as `Pickup request` and `Pickup time`. Moreover, in the column `Car`, the tabular shows the assigned cars for all requests, or if a request was rejected. Also dropoff times as computed from ride distance and driving speed are presented as `Dropoff time`. \n",
    "Moreover, the tabular contains `Price`, `Cost`, `Win` and `Dissatisfaction` for each request. \n",
    "In the `Rejection` column, a one means that a request has been rejected, while a zero means the request could be assigned. The corresponding `Rejection costs` are shown in a separate column. \n",
    "Finally, the `Contribution` from each request is computed as `Win - Dissatisfaction`.\n",
    "\n",
    "In the visual presentation, the axis unit is kilometers. The route of each car is plotted in a different color as an alternating sequence of dashed and solid lines. A dashed line means, that the car is free and on its way to a pickup location. Conversely, a solid lines means, that the car is currently occupied with a request, which it delivers at the corresponding dropoff location.\n",
    "Each request is shown at its pickup location, and annotated with a window of possible pickup times until rejection. Each car is shown at its starting location, and annotated with its win in $\\$$, its transport distance in $km$, its overall driving distance in $km$, and the list of requests assigned to the car. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Format\n",
    "The data for requests and car positions is extracted from a CSV file located in the `data` sub folder. A test scenario based on New York taxi data is already provided in the file `test_data_NY.csv`. A screenshot of the required data format is given below.\n",
    "\n",
    "<img src=\"figures/data_format.png\" alt=\"Drawing\" style=\"width: 700px;\"/>\n",
    "\n",
    "The first six columns $A-F$ contain request information. In particular, the requests are numbered starting with zero as can be seen in the column `requests`. This information is not read by the code, but the column is only a helper column for the reader. The column `PU time` contains desired pickup times for each request in seconds counted from initialization of the scenario upon click of the `Call` button in the `Setup scenario` tab as described in the next section. In particular, the current time when clicking the `Call` button is considered as start time, the desired pickup time of a request is computed as start time plus the respective `PU time`.\n",
    "The columns `PU x-coord` and `PU y-coord` contain x and y coordinates of all requests pickup locations on the map. These values are interpreted in the unit [$10$ meters] and converted to kilometers. For example, an x coordinate value of $261$ will be converted to $2.61$ kilometers. Similarly, the columns `DO x-coord` and `DO y-coord` contain x and y coordinates of all requests dropoff locations on the map in the unit [$10$ meters].\n",
    "\n",
    "The columns $H-J$ contain car specific information. As for column `requests`, column `cars` is only a helper column and not read by the code. Cars are numbered starting with zero. Columns `x-coord` and `y-coord` contain x and y coordinates of the start locations for each car in the unit [$10$ meters].\n",
    "The start time for each car is assumed as the time when the scenario is initialized upon click of the `Call` button in the `Setup scenario` tab as described in the next section.\n",
    "\n",
    "Note that cars in this implementation are only able to drive on vertical and horizontal lines. In particular, cars move to locations on the concatenation of a vertical and a horizontal line. Note that this implies movements of cars on a grid of $10*10$ meter cells. The implementation is based on the typical architecture of American cities, although probably with a different scaling.\n",
    "\n",
    "You can also test the optimization algorithm on your own data files, provided in the explained format.\n",
    "The waiting time of passengers is counted in full minutes. The maximal waiting time is determined by the parameter `tolerance_interval` in the `Setup scenario` tab of the dashboard below. In particular, the maximal waiting time is twice the number chosen in `tolerance_interval`. The approach requires $C*R^2$ bits, which implies $3*6^2 = 108$ bits for the scenario. Hence, bigger scenarios can be executed as well."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Model usage\n",
    "In the following code block the model is implemented and loaded in a dashboard. Classes for requests and cars are implemented as `Request` and `Car`, and allow to model single customer requests and the car properties and activities including individual tour planning. The main part is implemented in the class `Darp` inheriting form `OptimizerModel`. Provided as argument to an `Optimizer` object upon initialization, the latter creates a dashboard for testing and tuning the model.\n",
    "\n",
    "The following tabs are included in the dashboard:\n",
    "### Setup scenario\n",
    "In this tab, the whole scenario for optimization is loaded upon press of the `Call` button. The data is loaded in from a CSV file, which can be selected under `Data File`. `number_cars` determines the number of cars to be extracted from the file. The extracted number of requests can be chosen proportional to the number of cars via `requests_per_car`. The constant speed that each car can drive is chosen via `speed`. Costs and prices for driven kilometers can be determined by `costs_per_km`, `price_per_km` and the base price for a delivered requests is defined by `base_price`.\n",
    "The maximal dissatisfaction costs of a request is determined by `dissatisfaction_costs`. Three dissatisfaction measures are implemented and can be chosen via `tolerance_model`. The option `no_tolerance` adds the value of `dissatisfaction_costs` for each request which is not picked up in time but faster than twice the `tolerance_interval` Otherwise, the value of `rejection_costs` is added. The measure corresponding to option `half_linear` implies no dissatisfaction cost for waiting times less than `tolerance_interval`, and then increases in a linear way up to half the `dissatisfaction_costs`. For waiting times more than twice the `tolerance_interval`, the the value of `rejection_costs` is added. \n",
    "Finally, the measure for option `flat_linear` implies no dissatisfaction cost for waiting times less than `tolerance_interval`, then increases in a linear way up to the `dissatisfaction_costs`, and for waiting times more than twice the `tolerance_interval`, the value of `rejection_costs` is added. \n",
    "\n",
    "### Build QUBO\n",
    "In the tab, weights for the different QUBO terms of the approach described below can be set.\n",
    "\n",
    "### Solve annealing\n",
    "Annealing parameters for solving the optimization can be defined. If a solution is not optimal after pressing the `Call` button, QUBO parameters can be changed in the `Build QUBO` tab and the `Call` button can be pressed to invoke generation of a new QUBO. Then, annealing parameters can be adapted in the respective `Solve annealing` sub-tab. Pressing the `Call` button, invokes a new annealing process for the respective approach. \n",
    "\n",
    "### Anneal tracker\n",
    "In this tab, a detailed overview for times of the different optimization approaches can be seen.\n",
    "\n",
    "### Report\n",
    "This tab shows the result which was achieved by the optimization approach.\n",
    "\n",
    "### Visualization\n",
    "This tab visualizes the routes of all assigned cars from the result of the optimization approach. Each request is plotted with its desired pickup time and the latest possible pickup time before rejection. Each car is annotated at its starting location along with its win, the distance which it traveled with requests on board, the overall distance it traveled, and the sequence of requests it serviced. The plot is interactive, meaning that you can zoom into subsections of the figure. To do this, click on the white square in the bottom left of the tab. A black cross is shown upon moving the mouse above the figure. Holding the left mouse button clicked, you can select the desired area in which you want to zoom. The zoom is applied by releasing the left mouse button. To obtain the original plot, click on the house in the lower left of the tab.\n",
    "The figure can also be resized. To to this, click on the triangle in the lower right of the tab, and drag the the figure to the desired size. Again, the original plot is obtained by clicking the house in the lower left of the tab.\n",
    "\n",
    "### Remark\n",
    "The default parameters upon execution of the cell below are already tuned for the subproblem of the file `test_data_NY.csv` consisting of the first $8$ requests and the first $4$ cars. Different parameters in the `Setup scenario` cell might require different settings in the `Build QUBO` and `Solve annealing` tabs. To solve your own scenario optimal, you might first need to learn from the respective exercises in the rest of this tutorial. With the default settings, annealing with CPU of the approach can take up to 16 seconds."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A replay for an example scenario is started upon execution of the following cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sources.Darp import *\n",
    "\n",
    "scenario1 = Optimizer(Darp(persistent_file = \"X_03_Dial_a_ride_example.dao\"), auto_load=True, read_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell initializes a dashboard in which you can experiment with your own scenarios. Initially, all settings are as in the scenario above. Your changes are stored, such that you can resume with your last executed scenario in the next session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scenario2 = Optimizer(Darp(persistent_file = \"X_03_Dial_a_ride.dao\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Formulation as Quadratic Unconstrained Binary Optimization Problem (QUBO)\n",
    "In order to formulate the assignment problem as a Quadratic Unconstrained Binary Optimization Problem (QUBO), we have to formalize it first.\n",
    "\n",
    "Let $L$ be a set of locations and $Dist:L \\times L \\to \\mathbb{R}$ a distance function. Further, let $R \\in \\mathbb{N}$ be the number of requests and $C \\in \\mathbb{N}$ the number of cars. For $r=0,...R-1$ let $p_r \\in L$ and $d_r \\in L$ be the pick-up and drop-off locations of the requests and for $c=0,...C-1$ let $s_c \\in L$ be the starting positions of cars. \n",
    "\n",
    "## QUBO approach\n",
    "It is assumed, that a car can fulfill zero, one or two requests in the planned horizon. \n",
    "\n",
    "For $c \\in \\{0, 1, ... C-1 \\}$ and $r, s \\in \\{0, 1, ... R-1\\}$ with $r \\ne s$ let $x_{c, r,s} = 1$ iff the car with index $c$ first fulfills request $r$ and then $s$. For two identical indices $r \\in \\{0, 1, ... R-1\\}$ set $x_{c,r,r}=1$ iff car $c$ only fulfills request $r$.\n",
    "The following constraints have to be satisfied in order to obtain feasible configurations.\n",
    "\n",
    "### Constraint 1:\n",
    "Every request can be fulfilled by at most one car either in single service or double service operation:\n",
    "$$\n",
    "H_1 = \\sum_{r=0}^{R-1}\n",
    "        \\left(\n",
    "          \\left( \n",
    "            \\left( \n",
    "              \\sum_{c=0}^{C-1}\n",
    "                \\left( \n",
    "                  \\sum_{s=0}^{R-1} x_{c, r, s} +\n",
    "                  \\sum_{\\substack {s=0 \\\\ s \\ne r}}^{R-1} x_{c, s, r} \n",
    "                \\right) \n",
    "            \\right) \n",
    "            - 1 \n",
    "          \\right) * \n",
    "          \\left( \n",
    "            \\sum_{c=0}^{C-1}\n",
    "              \\left( \n",
    "                \\sum_{s=0}^{R-1} x_{c, r, s} +\n",
    "                \\sum_{\\substack {s=0 \\\\ s \\ne r}}^{R-1} x_{c, s, r}  \n",
    "              \\right) \n",
    "          \\right)\n",
    "        \\right) \\stackrel ! = 0\n",
    "$$\n",
    "\n",
    "### Constraint 2:\n",
    "Every car can do none, one single or double transportation: \n",
    "\n",
    "$$\n",
    "H_2 = \\sum_{c=0}^{C-1}\n",
    "        \\left(\n",
    "          \\left(\n",
    "            \\sum_{r=0}^{R-1}\\sum_{s=0}^{R-1} x_{c, r, s}\n",
    "            - 1\n",
    "          \\right) *\n",
    "          \\left(\n",
    "            \\sum_{r=0}^{R-1}\\sum_{s=0}^{R-1} x_{c, r, s}\n",
    "          \\right) \n",
    "        \\right)\n",
    "        \\stackrel ! = 0\n",
    "$$\n",
    "\n",
    "\n",
    "The target function to be maximized is the following.\n",
    "### Maximize:\n",
    "Let $w_{c,r,r}$ be the win (possibly negative), when car $c$ fulfills exactly request $r$ and $w_{c,r,s}$ the win (possibly negative), when car $c$ fulfills first request $r$ and then request $s$. Note that each win is computed as the price minus the cost for the driven distance of the car $c$, weighted by $C_{cars}$.\n",
    "In the implementation below, $C_{cars}=1$.\n",
    "similarly, let $d_{c,r,r}$ be the dissatisfaction of request $r$, when car $c$ fulfills exactly request $r$, and $d_{c,r,s}$ the dissatisfaction, when car $c$ fulfills first request $r$ and then request $s$. Note that each dissatisfaction is computed as the sum of dissatisfactions of each passenger in case the car can reach every request in time, or the corresponding rejection cost for requests which can not be reached in time. The values $d_{c,r,r}, d_{c,r,s}$ are already multiplied by $C_{requests}$, but in the implementation below, $C_{requests}=1$.\n",
    "The total target function to be maximized is:\n",
    "$$\n",
    "H_3 = \\sum_{c=0}^{C-1}\n",
    "        \\sum_{r=0}^{R-1}\n",
    "          \\sum_{s=0}^{r-1} (w_{c, r, s} - d_{c, r, s}) x_{c, r, s} \n",
    "$$\n",
    "\n",
    "### QUBO:\n",
    "Let $A$, $B$ and $C$ be positive real numbers with A, B > C (better estimation of against maximum possible win t.b.d.) then the QUBO is\n",
    "\n",
    "$$\n",
    "H = A H_1 + B H_2 - C H_3\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Code\n",
    "\n",
    "The code is divided in two separate Python files.\n",
    "\n",
    "You can find the code for the helper classes `Car` and `Request` by clicking\n",
    "\n",
    "[Car, Request](./sources/cars_requests.py)\n",
    "\n",
    "The Optimizer model including the QUBO is implemented under\n",
    "\n",
    "[Darp](./sources/Darp.py)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
