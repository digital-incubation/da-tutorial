# general settings
# sizing of map
plan_size_x = 5.0  # size in km
plan_size_y = 15.0  # size in km

from random import seed as rd_seed
import pandas as pd
from dadk.Optimizer import *
from sources.cars_requests import *

from random import random

class Darp(OptimizerModel):
    def __init__(self, persistent_file="DARP_ASYMETRIC_NY_0.pps"):
        self.requests = []
        self.cars = []
        self.plan_default()

        OptimizerModel.__init__(self,
            name='Dial a Ride',
            load_parameter=[
               {'name': 'filename',
                'value': '',
                'type': 'file_selector',
                'suffix': 'csv',
                'label': 'Data File',
                'description': 'Filename of Car csv data',
                'tags': ['normal']},
               {'name': 'number_cars', 'value': 3, 'type': 'int_slider', 'min': 1, 'max': 12,
                'description': 'Number of cars'},
               {'name': 'requests_per_car', 'value': 2, 'type': 'int_slider', 'min': 1,
                'max': 10, 'description': 'Maximum requests per cars'},
               {'name': 'speed', 'value': 36.0, 'type': 'float_slider', 'min': 1.0,
                'max': 100.0, 'step': 0.5, 'description': 'Speed of cars [km/h]'},
               {'name': 'costs_per_km', 'value': 1.0, 'type': 'float_slider', 'min': 0.0,
                'max': 10.0, 'step': 0.01, 'description': 'Costs per distance [$/km]'},
               # {'name': 'test_planning', 'value': 'default', 'type': 'select',
               # 'options': ['default', 'nearest_car']},
               {'name': 'price_per_km', 'value': 2.0, 'type': 'float_slider', 'min': 0.0,
                'max': 20.0, 'step': 0.01, 'description': 'Price per distance [$/km]'},
               {'name': 'base_price', 'value': 10.0, 'type': 'float_slider', 'min': 0.0,
                'max': 50.0, 'step': 0.1, 'description': 'Base price [$]'},
               {'name': 'dissatisfaction_costs', 'value': 10.0, 'type': 'float_slider',
                'min': 0.0, 'max': 50.0, 'step': 0.1,
                'description': 'Maximum dissatifaction costs [$]'},
               {'name': 'tolerance_model', 'value': 'no_tolerance', 'type': 'select',
                'options': ['no_tolerance', 'flat_linear', 'flat_halflinear'],
                'description': 'Tolerance phase model'},
               {'name': 'tolerance_interval', 'value': 5, 'type': 'int_slider',
                'min': 0, 'max': 30, 'step': 1,
                'description': 'Duration of phases  [min]'},
               {'name': 'rejection_costs', 'value': 10.0, 'type': 'float_slider',
                'min': 0.0, 'max': 100.0, 'step': 0.1,
                'description': 'Cost for rejection [$]'},
            ],
            build_qubo_parameter=[
                {'name': 'A', 'value': 500.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10, 'description': 'Constraint 1 (A)'},
                {'name': 'B', 'value': 500.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10, 'description': 'Constraint 2 (B)'},
                {'name': 'C', 'value': 10.0, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10, 'description': 'Minimize (C)'},
                ],
            persistent_file=persistent_file
            )

    def load(self, number_cars=3, requests_per_car=2,
             speed=36.0, costs_per_km=1.0, price_per_km=2.0, base_price=5.0,
             dissatisfaction_costs=50.0,
             tolerance_model='no_tolerance',
             tolerance_interval=4.0,
             rejection_costs=100.0,
             filename='',
             silent=False
             ):
        if filename == '':
            print('Please provide a data file.')
            return
        df = pd.read_csv(filename, sep=';')
        requests = df[['PU time', 'PU x-coord', 'PU y-coord', 'DO x-coord', 'DO y-coord']]
        cars = df[['x-coord', 'y-coord']].loc[0:number_cars - 1]

        self.requests = []
        self.cars = []
        start_time = datetime.now()
        self.start_time = start_time
        for index, row in cars.loc[0:number_cars - 1].iterrows():
            self.add_car(
                Car(start_location=(float(row['x-coord']) / 100.0,
                                    float(row['y-coord']) / 100.0),
                    speed=speed, base_price=base_price, price_per_km=price_per_km, costs_per_km=costs_per_km,
                    dissatisfaction_costs=dissatisfaction_costs, tolerance_model=tolerance_model,
                    tolerance_interval=tolerance_interval,
                    rejection_costs=rejection_costs,
                    start_time=start_time
                    )
            )

        for index, row in requests.loc[0:requests_per_car * number_cars - 1].iterrows():
            self.add_request(
                Request(pickup_earliest=start_time + timedelta(seconds=int(row['PU time'])),
                        pickup_location=(float(row['PU x-coord']) / 100.0, float(row['PU y-coord']) / 100.0),
                        dropoff_location=(float(row['DO x-coord']) / 100.0, float(row['DO y-coord']) / 100.0),
                        rejection_costs=rejection_costs
                        )
            )

        if not silent:
            print('Model was initialized for %d car(s) and %d requests.' % (number_cars, requests_per_car * number_cars))

    def clear_planning(self):
        for car in self.cars:
            car.clear_requests()
        for request in self.requests:
            request.set_planned_trip()

    def plan_default(self):
        self.clear_planning()
        for car in self.cars:
            car.assign_request(self.get_request(2 * car.get_car_number()))
            car.assign_request(self.get_request(2 * car.get_car_number() + 1))
            car.evaluate_tour()

    def plan_nearest_car(self):
        self.clear_planning()
        for request in self.requests:
            nearest_car = None
            best_arrival_time = None
            for car in self.cars:
                if len(car.requests) < 2:
                    arrival_time = car.estimated_arrival(request.pickup_location)
                    if nearest_car is None or arrival_time < best_arrival_time:
                        nearest_car = car
                        best_arrival_time = arrival_time
            if nearest_car is not None:
                nearest_car.assign_request(request)
                nearest_car.evaluate_tour()

    def add_request(self, request=None):
        self.requests.append(request if request is not None else Request())
        self.requests[-1].set_request_number(len(self.requests) - 1)

    def get_request(self, index):
        return self.requests[index] if index < len(self.requests) else None

    def add_car(self, car=None):
        self.cars.append(car if car is not None else Car())
        self.cars[-1].set_car_number(len(self.cars) - 1)

    def build_qubo(self, A=500.0, B=500.0, C=10.0, silent=False):
        self.w = []
        for c in self.cars:
            self.w.append([])
            for r in self.requests:
                self.w[-1].append([])
                for s in self.requests:
                    self.w[-1][-1].append(c.assign_evaluate_request_pair(r, s))

        self.var_shape_set = VarShapeSet(
            BitArrayShape('x', (len(self.cars), len(self.requests), len(self.requests)))
        )

        if not hasattr(self, 'H_1') or self.qubo_dims != (len(self.cars), len(self.requests)):
            self.qubo_dims = (len(self.cars), len(self.requests))
            self.H_1 = BinPol(self.var_shape_set)
            for r in range(len(self.requests)):
                p = BinPol(self.var_shape_set)
                for c in range(len(self.cars)):
                    for s in range(len(self.requests)):
                        if r != s:
                            p.add_term(1, ('x', c, r, s)).add_term(1, ('x', c, s, r))
                        else:
                            p.add_term(1, ('x', c, r, s))
                q = p.clone().add_term(-1)
                p.multiply(q)
                self.H_1.add(p)

            self.H_2 = BinPol(self.var_shape_set)
            for c in range(len(self.cars)):
                p = BinPol(self.var_shape_set)
                for r in range(len(self.requests)):
                    for s in range(len(self.requests)):
                        p.add_term(1, ('x', c, r, s))
                q = p.clone().add_term(-1)
                p.multiply(q)
                self.H_2.add(p)

        self.H_3 = BinPol(self.var_shape_set)
        for c in range(len(self.cars)):
            for r in range(len(self.requests)):
                for s in range(len(self.requests)):
                    self.H_3.add_term(self.w[c][r][s], ('x', c, r, s))

        self.H = self.H_1.clone().multiply_scalar(A). \
            add(self.H_2.clone().multiply_scalar(B)). \
            add(self.H_3.clone().multiply_scalar(-C))

        self.HQ = self.H
        if not silent:
            print('Number of bits: %d' % self.HQ.N)

    def prep_result(self, solution_list: SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        self.clear_planning()
        x_data = solution_list.min_solution.extract_bit_array('x').data
        for ci, c in enumerate(self.cars):
            for ri, r in enumerate(self.requests):
                for si, s in enumerate(self.requests):
                    # if configuration[self.var_shape_set.flat_index((('x', ci, ri, si),))[0]]:
                    if x_data[ci, ri, si]:
                        c.assign_evaluate_request_pair(r, s)

    def report(self, silent=False):
        def format_time(t):
            return t.strftime("%H:%M:%S") if t is not None else '-'

        def plus_avg_sum(lst):
            return list(lst) + [sum(lst) / len(lst), sum(lst)]

        if len(self.requests) == 0:
            return

        results = zip(
            list(map(lambda x: x.request_number, self.requests)) + ['Avg', 'Sum'],
            list(map(lambda x: format_time(x.pickup_earliest), self.requests)) + ['-', '-'],
            list(map(lambda x: str(x.car.get_car_number()) if x.car is not None else 'rejected', self.requests)) + ['-',
                                                                                                                    '-'],
            list(map(lambda x: format_time(x.pickup_time), self.requests)) + ['-', '-'],
            list(map(lambda x: format_time(x.dropoff_time), self.requests)) + ['-', '-'],
            plus_avg_sum(list(map(lambda x: (x.price or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.costs or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.win or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.dissatisfaction or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.rejection or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.rejection_costs or 0.0), self.requests))),
            plus_avg_sum(list(map(lambda x: (x.contribution or 0.0), self.requests))),
        )
        headers = ['#', 'Pickup request', 'Car', 'Pickup time', 'Dropoff time',
                   'Price', 'Costs', 'Win', 'Dissatisfaction', 'Rejection', 'Rejection costs', 'Contribution']
        if not silent:
            display(HTML(tabulate(results, headers=headers, tablefmt='html', floatfmt=".2f")))
        else:
            summary_list = list(results)[-2:]
            summary_object = {}
            for c in range(5, len(headers), 1):
                col_obj = {}
                summary_object[headers[c]] = col_obj
                for i, name in enumerate(['average', 'sum']):
                    col_obj[name] = summary_list[i][c]
            return summary_object

    def draw(self, case=''):
        import matplotlib.pyplot as plt
        fig = plt.figure('Ride Planning (created %s)' % datetime.now(), figsize=(9, 7))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_aspect('equal')
        # ax.set_xlim([0.0, plan_size_x])
        # ax.set_ylim([0.0, plan_size_y])

        for c in self.cars:
            plt.plot(c.start_location[0], c.start_location[1], 'o', color=c.color, linewidth=2, zorder=1)
            plt.annotate(c.get_tour_label(),
                         (c.start_location[0], c.start_location[1]),
                         xytext=(5, 0),
                         textcoords='offset points',
                         color=c.color
                         )
            tour = c.get_tour()
            p_x = [point["location"][0] for point in tour]
            p_y = [point["location"][1] for point in tour]
            for p_i in range(len(p_x) // 4):
                plt.plot(p_x[4 * p_i: 4 * p_i + 3], p_y[4 * p_i: 4 * p_i + 3], '--', color=c.color, linewidth=2,
                         zorder=1)
                plt.plot(p_x[4 * p_i + 2: 4 * p_i + 5], p_y[4 * p_i + 2: 4 * p_i + 5], '-', color=c.color, linewidth=2,
                         zorder=1)
            for point in tour:
                if point["time"] is not None:
                    plt.annotate(point["time"].strftime('%H:%M'), point["location"],
                                 xytext=(-40, 0),
                                 textcoords='offset points',
                                 color='white',
                                 backgroundcolor=c.color
                                 )
        for r in self.requests:
            plt.plot([r.pickup_location[0], r.dropoff_location[0]],
                     [r.pickup_location[1], r.dropoff_location[1]],
                     ':', color='grey', linewidth=3, zorder=0)
            plt.annotate('$request_{%d}$: %s-%s' % (r.get_request_number(),
                                                    r.pickup_earliest.strftime('%H:%M'),
                                                    r.pickup_latest.strftime('%H:%M')),
                         (r.pickup_location[0], r.pickup_location[1]),
                         xytext=(5, 0),
                         textcoords='offset points',
                         color='black'
                         )
        plt.show()
