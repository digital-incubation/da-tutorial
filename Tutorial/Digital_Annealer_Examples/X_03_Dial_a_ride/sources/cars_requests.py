# general settings
# sizing of map
plan_size_x = 5.0  # size in km
plan_size_y = 15.0  # size in km

from dadk.BinPol import *
from datetime import timedelta

class Request:
    def __init__(self, pickup_location=None,
                 dropoff_location=None,
                 pickup_earliest=None, pickup_latest=None,
                 rejection_costs=10.0
                 ):
        self.pickup_location = pickup_location if pickup_location is not None else \
            (plan_size_x * random.random(), plan_size_y * random.random())
        self.dropoff_location = dropoff_location if dropoff_location is not None else \
            (plan_size_x * random.random(), plan_size_y * random.random())
        self.pickup_earliest = pickup_earliest if pickup_earliest is not None else \
            datetime.now() + timedelta(minutes=int(10.0 + random.random() * 10.0))
        self.pickup_latest = pickup_latest if pickup_latest is not None else \
            self.pickup_earliest + timedelta(minutes=10)
        self.request_number = None
        self.assumed_rejection_costs = rejection_costs

    def set_request_number(self, request_number):
        self.request_number = request_number

    def get_request_number(self):
        return self.request_number

    def set_planned_trip(self, car=None, pickup_time=None, dropoff_time=None,
                         price=0.0, costs=0.0, win=0.0, dissatisfaction=0.0):
        self.car = car
        self.pickup_time = pickup_time
        self.dropoff_time = dropoff_time
        self.price = price
        self.costs = costs
        self.win = win
        self.dissatisfaction = dissatisfaction
        self.rejection = 1 if car is None else 0
        self.rejection_costs = self.assumed_rejection_costs if car is None else 0.0
        self.contribution = self.win - self.dissatisfaction - self.rejection * self.rejection_costs

class Car:
    def __init__(self, start_location=None, start_time=None, speed=36.0,
                 base_price=10.0, price_per_km=2.0, costs_per_km=1.0,
                 dissatisfaction_costs=10.0, tolerance_model='flat_halflinear', tolerance_interval=5.0,
                 rejection_costs=10.0
                 ):
        self.start_location = start_location if start_location is not None else \
            (plan_size_x * random.random(), plan_size_y * random.random())
        self.start_time = start_time if start_time is not None else datetime.now()
        self.speed = speed
        self.base_price = base_price
        self.price_per_km = price_per_km
        self.costs_per_km = costs_per_km
        self.dissatisfaction_costs = dissatisfaction_costs
        self.tolerance_model = tolerance_model
        self.tolerance_interval = tolerance_interval
        self.rejection_costs = rejection_costs
        self.color = (random.random(), random.random(), random.random())
        self.clear_requests()
        self.car_number = None

    def set_car_number(self, car_number):
        self.car_number = car_number

    def get_car_number(self):
        return self.car_number

    def clear_requests(self):
        self.requests = []
        self.tour = [{"location": self.start_location, "time": self.start_time}]
        self.distance = 0
        self.transport = 0
        self.costs = 0
        self.price = 0
        self.win = 0

    def assign_request(self, request=None):
        if request is not None:
            self.requests.append(request)

    @staticmethod
    def distance_metric(point_1, point_2):
        return sum([abs(point_1[d] - point_2[d]) for d in range(2)])

    def dissatisfaction_measure(self, requested_time, delivered_time):
        max_dissatisfaction_after = timedelta(minutes=2 * self.tolerance_interval)
        if self.tolerance_model == 'no_tolerance':
            return self.rejection_costs if (delivered_time > requested_time + max_dissatisfaction_after) else \
                self.dissatisfaction_costs if delivered_time > requested_time else 0
        elif self.tolerance_model in ['flat_linear', 'flat_halflinear']:
            tolerated_delay = timedelta(minutes=self.tolerance_interval)
            return self.rejection_costs if (delivered_time > requested_time + max_dissatisfaction_after) else \
                max(0.0,
                    (delivered_time - requested_time - tolerated_delay).total_seconds() /
                    (max_dissatisfaction_after - tolerated_delay).total_seconds() *
                    self.dissatisfaction_costs * (1.0 if self.tolerance_model == 'flat_linear' else 0.5)
                    )
        else:
            return 42

    def evaluate_tour(self):
        prev_point = self.start_location
        prev_time = self.start_time
        self.distance = 0.0
        self.transport = 0.0
        self.price = 0.0
        self.costs = 0.0
        self.win = 0.0
        self.dissatisfaction = 0.0
        self.tour = [{"location": prev_point, "time": prev_time}]
        for request in self.requests:
            request_distance = 0.0
            request_transport = 0.0
            for i, point in enumerate([request.pickup_location, request.dropoff_location]):
                distance = Car.distance_metric(point, prev_point)
                elapsed_time = float(distance) / self.speed
                request_distance += distance
                prev_time = prev_time + timedelta(hours=elapsed_time)
                self.distance += distance
                if i == 0 and prev_time < request.pickup_earliest:
                    prev_time = request.pickup_earliest
                elif i == 1:
                    self.transport += distance
                    request_transport += distance
                self.tour.append({"location": (prev_point[0], point[1]), "time": None})
                self.tour.append({"location": point, "time": prev_time})
                prev_point = point
            request_price = self.base_price + request_transport * self.price_per_km
            request_costs = request_distance * self.costs_per_km
            request_win = request_price - request_costs
            dissatisfaction = self.dissatisfaction_measure(request.pickup_earliest, self.tour[-3]['time'])
            self.price += request_price
            self.costs += request_costs
            self.win += request_win
            self.dissatisfaction += dissatisfaction
            request.set_planned_trip(car=self,
                                     pickup_time=self.tour[-3]['time'],
                                     dropoff_time=self.tour[-1]['time'],
                                     price=request_price,
                                     costs=request_costs,
                                     win=request_win,
                                     dissatisfaction=dissatisfaction
                                     )
        self.contribution = self.win - self.dissatisfaction
        return self.contribution

    def estimated_arrival(self, destination):
        self.evaluate_tour()
        distance = Car.distance_metric(self.tour[-1]['location'], destination)
        elapsed_time = float(distance) / self.speed
        return self.tour[-1]['time'] + timedelta(hours=elapsed_time)

    def assign_evaluate_request_pair(self, request_1=None, request_2=None):
        self.clear_requests()
        self.assign_request(request_1)
        if request_1 != request_2:
            self.assign_request(request_2)
        return self.evaluate_tour()

    def get_tour(self):
        return self.tour

    def get_tour_label(self):
        label = '$car_{%d}$: %.2f \\$, %.2f (%.2f) km' % (self.car_number, self.win, self.transport, self.distance)
        for r in self.requests:
            label += ', $request_{%d}$' % r.get_request_number()
        return label
