# Imports section

import random as random
from dadk.Optimizer import *
import warnings
import pandas as pd
from IPython.display import IFrame

#                                                    _       _       __            _ _
#                                                   | |     | |     / _|          | | |
#  _ __   __ _ _ __ ___   ___  ___    __ _ _ __   __| |   __| | ___| |_ __ _ _   _| | |_ ___
# | '_ \ / _` | '_ ` _ \ / _ \/ __|  / _` | '_ \ / _` |  / _` |/ _ \  _/ _` | | | | | __/ __|
# | | | | (_| | | | | | |  __/\__ \ \ (_| | | | | (_| | | (_| |  __/ || (_| | |_| | | |_\__ \
# |_| |_|\__,_|_| |_| |_|\___||___/  \__,_|_| |_|\__,_|  \__,_|\___|_| \__,_|\__,_|_|\__|___/
#


# Please define number of workers in the problem, WORKERS
NAME_WORKERS = 'Workers'
DEFAULT_WORKERS = 5
# Please define number of days in a week
NAME_DAYS = 'Days'
DEFAULT_DAYS = 7
# Define number of shifts in a day
NAME_SHIFTS = 'Shifts'
DEFAULT_SHIFTS = 3
# Define free days in a week for a worker
NAME_DEFAULT_FREE_DAYS = 'Free_days'
DEFAULT_FREE_DAYS = 2

NAME_A = 'A'
DEFAULT_A = 150
NAME_B = 'B'
DEFAULT_B = 50
NAME_C = 'C'
DEFAULT_C = 50
NAME_D = 'D'
DEFAULT_D = 105
NAME_E = 'E'
DEFAULT_E = 100

DEFAULT_SOLVER_PARAMETERS = {
    "graphics": True,
    "number_iterations": 5000,
    "number_runs": 16,
    "offset_increase_rate": 0.1,
    "processor": "CPU",
    "solution_mode": "QUICK",
    "temperature_end": 0.1,
    "temperature_interval": 1,
    "temperature_mode": 0,
    "temperature_start": 50.0
}


class WorkforceOptimization(OptimizerModel):
    def __init__(self, persistent_file="X_02_Workforce_scheduling.dao"):
        OptimizerModel.__init__(
            self,
            name='Workforce Optimization',
            load_parameter=[{'name': NAME_WORKERS, 'value': DEFAULT_WORKERS, 'type': 'int_slider', 'min': 1, 'max': 50,
                             'label': 'Workers (W)', 'description': 'Number of Workers considered for planning'},
                            {'name': NAME_DAYS, 'value': DEFAULT_DAYS, 'type': 'int_slider', 'min': 1, 'max': 10,
                             'label': 'Days (D)', 'description': 'Planning period in days',
                             'on_change': 'shift_table.rows = new_value'
                             },
                            {'name': NAME_SHIFTS, 'value': DEFAULT_SHIFTS, 'type': 'int_slider', 'min': 1, 'max': 5,
                             'label': 'Shifts (S)', 'description': 'Number of shifts per day',
                             'on_change': 'shift_table.columns = new_value'
                             },
                            {'name': NAME_DEFAULT_FREE_DAYS, 'value': DEFAULT_FREE_DAYS, 'type': 'int_slider', 'min': 1,
                             'max': 10,
                             'label': 'Vacation Days (V)',
                             'description': 'Number of free days per worker within the planning time'},
                            {'name': 'input_method', 'value': 'Default', 'type': 'select',
                             'options': ['Default', 'Random', 'Manual'],
                             'label': 'Demand parameters',
                             'description': 'Determine the required manpower for each shift on any given day',
                             'on_change': 'random_seed.visible = new_value == "Random"\nshift_table.visible = new_value == "Manual"\n'
                             },
                            {'name': 'random_seed', 'value': 42, 'type': 'int_slider', 'min': 0, 'max': 9999, 'step': 1,
                             'label': 'Random table input', 'description': 'Seed for random demand table generation'},
                            {'name': 'shift_table', 'type': 'int_array_bounded', 'rows': DEFAULT_DAYS,
                             'columns': DEFAULT_SHIFTS, 'value': [[0] * DEFAULT_SHIFTS] * DEFAULT_DAYS,
                             'min': 0, 'max': 9, 'step': 1, 'label': 'Demand Table',
                             'description': 'Required number of workers per shift (columns) and day (rows) '
                             },
                            ],
            build_qubo_parameter=[
                {'name': NAME_A, 'value': DEFAULT_A, 'type': 'float_bounded', 'min': 0, 'max': 10 ** 10,
                 'label': '$A$', 'description': 'Penalty A for H_0 '},
                {'name': NAME_B, 'value': DEFAULT_B, 'type': 'float_bounded', 'min': 0, 'max': 10 ** 10,
                 'label': '$B$', 'description': 'Penalty B for H_1 '},
                {'name': NAME_C, 'value': DEFAULT_C, 'type': 'float_bounded', 'min': 0, 'max': 10 ** 10,
                 'label': '$C$', 'description': 'Penalty C for H_2 '},
                {'name': NAME_D, 'value': DEFAULT_D, 'type': 'float_bounded', 'min': 0, 'max': 10 ** 10,
                 'label': '$D$', 'description': 'Penalty D for H_3 '},
                {'name': NAME_E, 'value': DEFAULT_E, 'type': 'float_bounded', 'min': 0, 'max': 10 ** 10,
                 'label': '$E$', 'description': 'Penalty E for H_4 '}
            ],
            default_solver_parameter=DEFAULT_SOLVER_PARAMETERS,

            persistent_file=persistent_file
        )

    ##################################################################################################################
    def load(self, Workers=DEFAULT_WORKERS, Days=DEFAULT_DAYS, Shifts=DEFAULT_SHIFTS, Free_days=DEFAULT_FREE_DAYS,
             input_method='Default', random_seed=42, shift_table=[[1] * DEFAULT_SHIFTS] * DEFAULT_DAYS, silent=False):
        ##################################################################################################################
        self.workers = Workers
        self.days = Days
        self.shifts = Shifts
        self.free_days = Free_days
        self.schedule = None
        self.input_method = input_method

        print('Number of workers    %3.d' % (self.workers))
        print('Number of days       %3.d' % (self.days))
        print('Number of shifts     %3.d' % (self.shifts))
        print('Free days per worker %3.d' % (self.free_days))

        if self.free_days > self.days:
            warnings.warn('Free days cannot be greater than total days in a week.')

        if self.input_method == 'Default':
            self.A_DS = [[self.workers // (self.shifts * (2 if d % 7 in [5, 6] else 1)) for s in range(self.shifts)] for
                         d in range(self.days)]
        elif self.input_method == 'Random':
            random.seed(random_seed)
            self.A_DS = [[random.randint(0, self.workers // (self.shifts * 0.6)) for s in range(self.shifts)] for d in
                         range(self.days)]
            random.seed()
        elif self.input_method == 'Manual':
            self.A_DS = shift_table

        self.output_table = tabulate(zip(range(self.days), *zip(*self.A_DS)),
                                     ['Day'] + [f'Shift {i}' for i in range(self.shifts)], tablefmt="html")
        print("\nDemand table")
        display(HTML(self.output_table))

    ##################################################################################################################
    def build_qubo(self,
                   A=DEFAULT_A,
                   B=DEFAULT_B,
                   C=DEFAULT_C,
                   D=DEFAULT_D,
                   E=DEFAULT_E,
                   silent=False):
        ##################################################################################################################
        self.constant_bits = np.full((self.workers, self.days, self.shifts), -1, np.int8)

        for d in range(self.days):
            for s in range(self.shifts):
                if self.A_DS[d][s] == 0:
                    for w in range(self.workers):
                        self.constant_bits[w, d, s] = 0

        var_shape_set = VarShapeSet(*(
                [BitArrayShape('x', (self.workers, self.days, self.shifts), constant_bits=self.constant_bits,
                               axis_names=["Workers", "Days", "Shifts"])] +
                [BitArrayShape('b', (self.workers,), axis_names=["Workers_Duty"])] +
                [VarSlack('slk%s' % str(w), slack_type=SlackType.binary, start=self.free_days, stop=self.days + 1,
                          step=1) for w in range(self.workers)]))

        self.HQ = BinPol(var_shape_set)
        self.Hs = {}

        H_0 = BinPol(var_shape_set)
        for d in range(self.days):
            for s in range(self.shifts):
                H_0_ds = BinPol(var_shape_set)
                for w in range(self.workers):
                    H_0_ds += Term(1, (('x', w, d, s),), var_shape_set)
                H_0_ds -= self.A_DS[d][s]
                H_0_ds **= 2
                H_0_ds *= A
                H_0 += H_0_ds
        self.Hs['H_0'] = H_0
        self.HQ += H_0

        H_1 = BinPol(var_shape_set)
        for w in range(self.workers):
            H_1_w = BinPol(var_shape_set)
            for d in range(self.days):
                for s in range(self.shifts):
                    H_1_w += Term(1, (('x', w, d, s),), var_shape_set)
            H_1_w -= self.days
            H_1_w.add_variable('slk%s' % str(w))
            H_1_w **= 2
            H_1_w *= B
            H_1 += H_1_w
        self.Hs['H_1'] = H_1
        self.HQ += H_1

        H_2 = BinPol(var_shape_set)
        for w in range(self.workers):
            for d in range(self.days):
                H_2_wd_one = BinPol(var_shape_set)
                H_2_wd_two = BinPol(var_shape_set)
                for s in range(self.shifts):
                    H_2_wd_one += Term(1, (('x', w, d, s),), var_shape_set)
                H_2_wd_two += -1
                for s in range(self.shifts):
                    H_2_wd_two += Term(1, (('x', w, d, s),), var_shape_set)
                H_2 += H_2_wd_one * H_2_wd_two * C
        self.Hs['H_2'] = H_2
        self.HQ += H_2

        H_3 = BinPol(var_shape_set)
        for w in range(self.workers):
            H_3_w_a = BinPol(var_shape_set)
            H_3_w_a += 1
            H_3_w_a += Term(-1, (('b', w),), var_shape_set)
            H_3_w_b = BinPol(var_shape_set)
            for d in range(self.days):
                for s in range(self.shifts):
                    H_3_w_b += Term(1, (('x', w, d, s),), var_shape_set)
            H_3 += H_3_w_a * H_3_w_b * D
        self.Hs['H_3'] = H_3
        self.HQ += H_3

        H_4 = BinPol(var_shape_set)
        H_4 += -self.workers
        for w in range(self.workers):
            H_4 += Term(1, (('b', w),), var_shape_set)
        H_4 *= E
        self.Hs['H_4'] = H_4
        self.HQ += H_4

        self.Hs['HQ'] = self.HQ
        print(f'Number of bits {self.HQ.N}')

    def prep_result(self, solution_list:SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        if not silent:
            for ID in self.Hs:
                print("%s: %10.6f" % (ID, self.Hs[ID].compute(configuration)))
        self.solution = configuration

        self.schedule = solution_list.min_solution.extract_bit_array('x')
        self.worker_on_off = solution_list.min_solution.extract_bit_array('b')

        A_DS_solution = [[0 for s in range(self.shifts)] for d in range(self.days)]
        for d in range(self.days):
            for s in range(self.shifts):
                for w in range(self.workers):
                    if self.schedule.data[w][d][s]:
                        A_DS_solution[d][s] += 1
        if A_DS_solution == self.A_DS:
            print('Solution validated')
        else:
            warnings.warn('Invalid solution')

    ##################################################################################################################
    def report(self, silent=False):
        ##################################################################################################################
        html = f"""
        <style>
            th {{
                padding: 5px;
                text-align: center;
                background-color: #aaa;
                color: #333;
            }}
            td {{
                min-width: 25px;
                text-align: center;
                padding: 4px;
            }}
            td.booked {{
                background-color: #faa;
            }}
            tr:nth-child(odd) {{
                background-color: #eee;
            }}
            tr:nth-child(even) {{
                background-color: #ddd;
            }}
            tr:hover  {{ 
                background-color: lightsteelblue; 
            }}
        </style>
        <table style="font-family:Arial;border-spacing: 2px;border-collapse: separate;">
            <tr>
                <th>Day</th>
                {' '.join([f'<th colspan="3" style="text-align:center">{day}</th>' for day in range(self.days)])}
            </tr>
            <tr>
                <th>Shift</th>
                {' '.join([f'<th style="text-align:center">{shift}</th>' for day in range(self.days) for shift in range(self.shifts)])}
            </tr>
            {' '.join([f'''<tr>
                <td>Worker&nbsp;{worker}</td>
                {' '.join([f'<td style="text-align:center" {"class=booked>x" if self.schedule.data[worker][day][shift] > 0 else ">"}</td>' for day in range(self.days) for shift in range(self.shifts)])}
            </tr>''' for worker in range(self.workers)])}
        </table>
        """
        display(HTML(html))

    ##################################################################################################################
    def draw(self):
        ##################################################################################################################
        self.schedule.draw(figsize=(15.0, 10.0), max_ax_per_row=5, order=[2, 1, 0])
        self.worker_on_off.draw(figsize=(5.0, 1.0), max_ax_per_row=1, order=[0])
