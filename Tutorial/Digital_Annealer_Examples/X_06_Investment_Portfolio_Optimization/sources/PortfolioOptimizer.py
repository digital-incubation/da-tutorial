import random

from IPython.display import display, HTML

from dadk.Optimizer import *
from dadk.SolverFactory import *
from dadk.Solution_SolutionList import *
from dadk.BinPol import *
from dadk.JupyterTools import TimeReporter

from random import uniform
from tabulate import tabulate
from numpy import argmax

class Mode(Enum):
    CLASSIC = 0
    BOTH = 1
    NUMPY = 2

class PortfolioOptimizer(OptimizerModel):
    ##################################################################################################################
    # constructor (defines GUI and test declaration)
    ##################################################################################################################
    def __init__(self, persistent_file="PortfolioOptimizer.dao", mode: 'Mode' = Mode.CLASSIC):
        OptimizerModel.__init__(
            self,
            name='Demo_UIFunctions',
            load_parameter=[
                {'name': 'random_seed', 'label': 'random seed',
                 'description': 'Seed for reproducible random correlation matrix',
                 'type': 'int_slider', 'value': 42, 'min': 0, 'max': 10 ** 3},
                {'name': "number_sectors", 'label': 'number sectors',
                 'description': 'Please select an integer value larger than 2',
                 'type': 'int_slider', 'value': 3, 'min': 2, 'max': 200, 'step': 1,
                 'on_change': [
                     "sector_fund_numbers.rows = new_value",
                     "sector_fund_numbers.row_labels = [f's{number}' for number in range(new_value)]"
                 ]},
                {'name': "autofill_sectors", 'label': 'autofill sectors',
                 'description': 'Select range to fill sectors randomly between limits',
                 'type': 'int_range_slider', 'value': (3, 3), 'min': 1, 'max': 200, 'step': 1,
                 'on_change': [
                     "import random",
                     "sector_fund_numbers.value = [[random.randint(new_value[0], new_value[1])] for s in range(len(sector_fund_numbers.value))]"
                 ]},
                {'name': 'sector_fund_numbers', 'label': 'Sector fund numbers',
                 'description': 'Please select an integer value between 1 and 99, step = 1 for all fields',
                 'type': 'int_array_bounded', 'rows': 3, 'columns': 1, 'headers': ['Number funds'],
                 'row_labels': ['s1', 's2', 's3'],
                 'value': [[3],
                           [3],
                           [3], ],
                 'min': 1, 'max': 99, 'step': 1}
            ],
            build_qubo_parameter=[
                {'name': 'factor_penalty', 'label': '$\\alpha$',
                 'description': 'Choose factor sufficiently large to avoid invalid solutions.',
                 'type': 'float_slider', 'value': 30.0, 'min': 0.0, 'max': 40},
                {'name': 'upscaling_factor', 'label': 'scaling factor',
                 'type': 'float_log_slider', 'value': 10_000_000.0, 'base': 10, 'min': 0, 'max': 8, 'format': '4.3e'
                 }
            ],
            #
            # Define default values for the "Solve annealing" tab.
            # The user can modify these fields, or press the button
            # "Default" and get these values again.
            #
            default_solver_parameter={
                'number_iterations': 200,
                'temperature_start': 110000000.0,
                'temperature_end': 1900000.0,
                'offset_increase_rate': 20000000.0
            },
            pareto_setting={
                'cartesian_product': False,
                'var_object': ['load_parameter', 'load_parameter', 'build_qubo_parameter'],
                'var_attribute': ['number_sectors', 'sector_fund_numbers', 'factor_penalty'],
                'var_values': [
                    [10, 10, 10, 10, 20, 20, 20, 20, 30, 30, 30, 30, 40, 40, 40, 40],
                    [[[20]] * 10, [[20]] * 10, [[20]] * 10, [[20]] * 10,
                     [[20]] * 20, [[20]] * 20, [[20]] * 20, [[20]] * 20,
                     [[20]] * 30, [[20]] * 30, [[20]] * 30, [[20]] * 30,
                     [[20]] * 40, [[20]] * 40, [[20]] * 40, [[20]] * 40],
                    [2, 5, 8, 10, 5, 10, 15, 20, 8, 15, 23, 30, 10, 20, 30, 40]
                ]
            },

            persistent_file=persistent_file
        )

        self.mode = mode

    ##################################################################################################################
    # load method (defines content of tab Setup scenario)
    ##################################################################################################################
    def load(self, random_seed, number_sectors, sector_fund_numbers, silent=False):
        self.number_sectors = number_sectors
        self.sector_fund_numbers = sector_fund_numbers

        self.sector_fund_numbers = sum(self.sector_fund_numbers, [])
        random.seed(random_seed)
        np.random.seed(random_seed)

        self.sectors = [f's{idx}' for idx in range(self.number_sectors)]

        self.sector_funds = {self.sectors[sec_idx]: [f'{self.sectors[sec_idx]}_f{fund}' for fund in range(self.sector_fund_numbers[sec_idx])]
                             for sec_idx in range(self.number_sectors)}

        self.total_funds = sum(self.sector_fund_numbers)
        print('total_funds', self.total_funds)

        self.start_bit_nos = []
        start_bit_no = 0
        for sec_idx in range(self.number_sectors):
            funds_in_sector = self.sector_fund_numbers[sec_idx]
            self.start_bit_nos.append(start_bit_no)
            start_bit_no += funds_in_sector

        time_reporter = TimeReporter()

        if self.mode in [Mode.CLASSIC, Mode.BOTH]:
            self.correlation_coefficients = {}
            for index1 in range(self.number_sectors):
                sector1 = self.sectors[index1]
                for index2 in range(index1 + 1, self.number_sectors):
                    sector2 = self.sectors[index2]
                    sector_pair = (sector1, sector2)
                    self.correlation_coefficients[sector_pair] = {}
                    for fund1 in self.sector_funds[sector1]:
                        for fund2 in self.sector_funds[sector2]:
                            fund_pair = (fund1, fund2)
                            self.correlation_coefficients[sector_pair][fund_pair] = uniform(-1.0, 1.0)
            time_reporter.take_time(f'correlation_coefficients ({Mode.CLASSIC.name})')

        if self.mode == Mode.BOTH:
            self.F_matrix = np.zeros((self.total_funds, self.total_funds), dtype=np.float64)
            for index1 in range(self.number_sectors):
                sector1 = self.sectors[index1]
                for index2 in range(index1 + 1, self.number_sectors):
                    sector2 = self.sectors[index2]
                    sector_pair = (sector1, sector2)
                    for fund_index1, fund1 in enumerate(self.sector_funds[sector1]):
                        for fund_index2, fund2 in enumerate(self.sector_funds[sector2]):
                            fund_pair = (fund1, fund2)
                            self.F_matrix[self.start_bit_nos[index1] + fund_index1, self.start_bit_nos[index2] + fund_index2] = self.correlation_coefficients[sector_pair][fund_pair]
            time_reporter.take_time(f'correlation_coefficients ({self.mode.name})')

        if self.mode == Mode.NUMPY:
            self.F_matrix = np.triu(np.random.uniform(-1.0, 1.0, size=(self.total_funds, self.total_funds)))
            for sec_idx in range(self.number_sectors):
                start_bit_no = self.start_bit_nos[sec_idx]
                funds_in_sector = self.sector_fund_numbers[sec_idx]
                blank = np.zeros((funds_in_sector, funds_in_sector))
                self.F_matrix[start_bit_no:start_bit_no + funds_in_sector, start_bit_no:start_bit_no + funds_in_sector] = blank
            time_reporter.take_time(f'correlation_coefficients ({self.mode.name})')

        if not silent:
            tab = OutputTab('Funds', 'Correlations', height='600px')
            tab.display()

            with tab.tab_out['Funds']:
                print(f'Total number of funds: {len(sum(self.sector_funds.values(), []))}')
                display(HTML(tabulate([[sector + ':'] + list(self.sector_funds[sector]) for sector in self.sectors], tablefmt="html")))

            with tab.tab_out['Correlations']:
                if (self.number_sectors <= 10):
                    summary = []
                    for index0 in range(self.number_sectors):
                        sector1 = self.sectors[index0]
                        row = [sector1]
                        for index2 in range(self.number_sectors):
                            if index2 > index0:
                                sector2 = self.sectors[index2]
                                sector_pair = (sector1, sector2) if (sector1, sector2) in self.correlation_coefficients else (sector2, sector1)
                                row.append(tabulate(
                                    [[fund0] + [
                                        self.correlation_coefficients[sector_pair][(fund0, fund1)]
                                        if (fund0, fund1) in self.correlation_coefficients[sector_pair] else
                                        self.correlation_coefficients[sector_pair][(fund1, fund0)]
                                        for fund1 in self.sector_funds[sector2]
                                    ] for fund0 in self.sector_funds[sector1]],
                                    headers=[''] + self.sector_funds[sector2],
                                    tablefmt="html", floatfmt=".2f"))
                            else:
                                row.append('x')
                        summary.append(row)

                    display(HTML(tabulate(summary, headers=['Correlation'] + self.sectors, tablefmt="unsafehtml", floatfmt=".2f")))
                else:
                    print(f'Display suppressed for {self.number_sectors} sectors. Maximum is 10.')

    ##################################################################################################################
    # build_qubo method (defines content of tab Build QUBO)
    ##################################################################################################################
    def build_qubo(self, factor_penalty, upscaling_factor, silent=False):

        BinPol.freeze_var_shape_set(VarShapeSet(*[Category(name=sector, values=self.sector_funds[sector])
                                                  for sector in self.sectors]))

        time_reporter = TimeReporter()

        # --------------------------------------------------------------------------------

        if self.mode in [Mode.CLASSIC, Mode.BOTH]:
            self.P = BinPol()
            for sector in self.sectors:
                self.P.add(BinPol.exactly_1_bit_on(bits=[(sector, fund) for fund in self.sector_funds[sector]]))
            self.P.multiply_scalar(factor_penalty)
            time_reporter.take_time(f'P created ({Mode.CLASSIC.name})')

        if self.mode in [Mode.BOTH, Mode.NUMPY]:
            self.P_matrix = np.zeros((self.total_funds, self.total_funds), dtype=np.float64)
            for sec_idx in range(self.number_sectors):
                start_bit_no = self.start_bit_nos[sec_idx]
                funds_in_sector = self.sector_fund_numbers[sec_idx]
                one_hot = np.triu(np.full((funds_in_sector, funds_in_sector), 2.0))
                np.fill_diagonal(one_hot, -1)
                self.P_matrix[start_bit_no:start_bit_no + funds_in_sector, start_bit_no:start_bit_no + funds_in_sector] = factor_penalty * one_hot
            self.P_numpy = BinPol(qubo_matrix_array=self.P_matrix, constant=factor_penalty * self.number_sectors)
            time_reporter.take_time(f'P created ({self.mode.name})')

        if self.mode == Mode.BOTH:
            if self.P != self.P_numpy:
                raise (ValueError('ERROR in P!'))
            time_reporter.take_time(f'P created (check)')

        # --------------------------------------------------------------------------------

        if self.mode in [Mode.CLASSIC, Mode.BOTH]:
            self.F = BinPol()
            for sector1, sector2 in self.correlation_coefficients:
                sector_pair = (sector1, sector2)
                for fund1 in self.sector_funds[sector1]:
                    for fund2 in self.sector_funds[sector2]:
                        fund_pair = (fund1, fund2)
                        self.F.add_term(self.correlation_coefficients[sector_pair][fund_pair],
                                        (sector1, fund1),
                                        (sector2, fund2))
            time_reporter.take_time(f'F created ({Mode.CLASSIC.name})')

        if self.mode in [Mode.BOTH, Mode.NUMPY]:
            self.F_numpy = BinPol(qubo_matrix_array=self.F_matrix)
            time_reporter.take_time(f'F created created ({self.mode.name})')

        if self.mode == Mode.BOTH:
            if self.F != self.F_numpy:
                raise (ValueError('ERROR in F!'))
            time_reporter.take_time(f'F created (check)')

        # --------------------------------------------------------------------------------

        if self.mode in [Mode.CLASSIC, Mode.BOTH]:
            self.HQ = upscaling_factor * (self.F + self.P)
            time_reporter.take_time(f'HQ created ({Mode.CLASSIC.name})')

        if self.mode in [Mode.BOTH, Mode.NUMPY]:
            self.HQ_numpy = upscaling_factor * (self.F_numpy + self.P_numpy)
            time_reporter.take_time(f'HQ created ({self.mode.name})')

        if self.mode == Mode.BOTH:
            if self.HQ != self.HQ_numpy:
                raise (ValueError('ERROR in HQ!'))
            time_reporter.take_time(f'HQ created (check)')

        # --------------------------------------------------------------------------------

        if self.mode == Mode.NUMPY:
            self.P = self.P_numpy
            self.F = self.F_numpy
            self.HQ = self.HQ_numpy

        if not silent:
            print(f'Qubo created with {self.HQ.N} bits.')

    ##################################################################################################################
    # prep_result method (evaluates result of annealing defines content of tab Solve annealing)
    ##################################################################################################################
    def prep_result(self, solution_list:SolutionList, silent=False):

        self.solution = solution_list.min_solution
        self.penalty_value = self.P.compute(self.solution.configuration)
        self.objective_value = self.F.compute(self.solution.configuration)
        self.result = {'penalty_value': self.penalty_value, 'invalid': 0 if self.penalty_value < 0.01 else 1, 'objective_value': self.objective_value}
        if not silent:
            if self.penalty_value == 0.0:
                print(f'Penalty value: {self.penalty_value}')
                print(f'Objective value: {self.objective_value}')
                print(f'QUBO value: {self.HQ.compute(self.solution.configuration)}')
            else:
                print("invalid solution, try again!")

    ##################################################################################################################
    # report method (defines content of Report tab)
    ##################################################################################################################
    def report(self, silent=False):
        if not silent:
            if self.penalty_value == 0.0:
                print('Chosen portfolio:')
                display(tabulate(
                    [[sector, self.sector_funds[sector][argmax(self.solution[sector].data)]] for sector in self.sectors],
                    headers=['Sector', 'Fund'], tablefmt="html"
                ))

                if self.mode in [Mode.CLASSIC, Mode.BOTH]:
                    correlation_sum = sum([
                        self.correlation_coefficients[(self.sectors[index1], self.sectors[index2])][
                            (self.sector_funds[self.sectors[index1]][argmax(self.solution[self.sectors[index1]].data)],
                             self.sector_funds[self.sectors[index2]][argmax(self.solution[self.sectors[index2]].data)])]
                        for index1 in range(self.number_sectors) for index2 in range(index1 + 1, self.number_sectors)
                    ])

                if self.mode in [Mode.BOTH, Mode.NUMPY]:
                    correlation_sum_numpy = sum([
                        self.F_matrix[self.start_bit_nos[index1] + argmax(self.solution[self.sectors[index1]].data), self.start_bit_nos[index2] + argmax(self.solution[self.sectors[index2]].data)]
                        for index1 in range(self.number_sectors) for index2 in range(index1 + 1, self.number_sectors)
                    ])

                if self.mode == Mode.BOTH:
                    if correlation_sum != correlation_sum_numpy:
                        raise (ValueError('ERROR in correlation_sum!'))

                if self.mode == Mode.NUMPY:
                    correlation_sum = correlation_sum_numpy

                print(f'Summed correlation values: {"{:.2f}".format(correlation_sum)}')
            else:
                print("invalid solution, please solve again!")

    ##################################################################################################################
    # pareto method (returns data for measurement)
    ##################################################################################################################
    def pareto(self):
        return self.result
