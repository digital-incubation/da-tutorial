from dadk.Optimizer import *
import random as random
import pandas as pd
import warnings
import math

DEFAULT_SOLVER_PARAMETERS = {
    "graphics": True,
    "number_iterations": 1000,
    "number_runs": 40,
    "offset_increase_rate": 1,
    "processor": "CPU",
    "solution_mode": "QUICK",
    "temperature_end": 0.01,
    "temperature_interval": 1,
    "temperature_mode": 0,
    "temperature_start": 10000
}
DEFAULT_cpu_SERVER_CAP = {0: 2500, 1: 4000, 2: 500, 3: 1000}
DEFAULT_MEM_SERVER_CAP = {0: 500, 1: 3500, 2: 1500, 3: 2000}
DEFAULT_CPU_REQ = {0: [500], 1: [1000], 2: [900], 3: [600]}
DEFAULT_MEM_REQ = {0: [350], 1: [100], 2: [1500], 3: [800]}


class ResourcePlanning(OptimizerModel):
    ##################################################################################################################
    def __init__(self, persistent_file="X_05_Datacenter_resource_planning.dao"):
        ##################################################################################################################
        OptimizerModel.__init__(
            self,
            name='Datacenter resource planning',
            load_parameter=[{'name': 'input_method', 'value': 'Manual', 'type': 'select',
                             'options': ['Manual', 'Select_data', 'Random'],
                             'label': 'Input method', 'description': 'User preferred input method',
                             'on_change': '''
import random as random
import os
import pandas as pd

random_seed.visible = (new_value == 'Random')
server_range_cpu.visible = (new_value == 'Random')
server_range_mem.visible = (new_value == 'Random')
no_servers.visible = (new_value == 'Manual')
no_services.visible = (new_value == 'Manual')
directory.visible = (new_value == 'Select_data')
server_cap.visible = (new_value == 'Manual')
service_req.visible = (new_value == 'Manual')  
service_range.visible = (new_value == 'Random')     
server_range.visible = (new_value == 'Random')  
service_range_cpu.visible = (new_value == 'Random')        
service_range_mem.visible = (new_value == 'Random')         

                             '''},
                            {'name': 'directory',
                             'type': 'directory_selector',
                             'value': 'data/Default',
                             'label': 'Input data directory',
                             'description': 'Please select a directory',
                             },

                            {'name': "random_seed",
                             'type': 'int_slider',
                             'value': 42,
                             'min': 0,
                             'max': 100,
                             'step': 1,
                             'label': 'Random seed',
                             'description': 'Please select a random seed'},

                            {'name': "no_servers",
                             'type': 'int_slider',
                             'value': 4,
                             'min': 0,
                             'max': 10,
                             'step': 1,
                             'label': 'Number of Servers',
                             'description': 'Please select number of servers, step = 1',
                             'on_change': 'server_cap.rows = new_value'},

                            {'name': "server_cap",
                             'type': 'int_array_bounded',
                             'rows': 4,
                             'columns': 2,
                             'headers': ['avail. CPU', 'avail. Memory'],
                             'value': [[2500, 500],
                                       [4000, 3500],
                                       [500, 1500],
                                       [1000, 2000]],
                             'min': 0,
                             'max': 100000000,
                             'step': 1,
                             'label': 'Servers',
                             'description': 'Select associated resources for servers'},

                            {'name': "no_services",
                             'type': 'int_slider',
                             'value': 4,
                             'min': 0,
                             'max': 10,
                             'step': 1,
                             'label': 'Number of Services',
                             'description': 'Please select number of Services, step = 1',
                             'on_change': 'service_req.rows = new_value'},

                            {'name': "service_req",
                             'type': 'int_array_bounded',
                             'rows': 4,
                             'columns': 2,
                             'headers': ['req. CPU', 'req. Memory'],
                             'value': [[500, 350],
                                       [1000, 100],
                                       [900, 1500],
                                       [700, 800]],
                             'min': 0,
                             'max': 100000000,
                             'step': 1,
                             'label': 'Services',
                             'description': 'Select associated resources requirements for services'},

                            {'name': "server_range_cpu",
                             'type': 'int_range_slider',
                             'value': (500, 4000),
                             'min': 0,
                             'max': 10000,
                             'step': 5,
                             'label': 'Server CPU capacity range for random mode',
                             'description': 'Please select an integer range, for Server cpu capacity range for random mode'},

                            {'name': "server_range_mem",
                             'type': 'int_range_slider',
                             'value': (500, 4000),
                             'min': 0,
                             'max': 10000,
                             'step': 5,
                             'label': 'Server memory capacity range for random mode',
                             'description': 'Please select an integer range, for server memory capacity range for random mode'},

                            {'name': "service_range_cpu",
                             'type': 'int_range_slider',
                             'value': (50, 1000),
                             'min': 0,
                             'max': 10000,
                             'step': 5,
                             'label': 'Service CPU requirement range for random mode',
                             'description': 'Please select an integer range, for service cpu requirement range for random mode'},

                            {'name': "service_range_mem",
                             'type': 'int_range_slider',
                             'value': (50, 1000),
                             'min': 0,
                             'max': 10000,
                             'step': 5,
                             'label': 'Service memory requirement range for random mode',
                             'description': 'Please select an integer range, for services memory requirement range for random mode'},

                            {'name': "server_range",
                             'type': 'int_range_slider',
                             'value': (2, 6),
                             'min': 0,
                             'max': 1000,
                             'step': 1,
                             'label': 'Server number range',
                             'description': 'Please select an integer range, for number of servers'},

                            {'name': "service_range",
                             'type': 'int_range_slider',
                             'value': (2, 6),
                             'min': 0,
                             'max': 1000,
                             'step': 1,
                             'label': 'Service number range',
                             'description': 'Please select an integer range, for number of servers'},
                            ],

            build_qubo_parameter=[{'name': 'A', 'value': 400000, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 0 (A)', 'description': 'Scaling factor for constraint 0 (A)'},
                                  {'name': 'B', 'value': 10, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 1 (B)', 'description': 'Scaling factor for constraint 1 (B)'},
                                  {'name': 'C', 'value': 10, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 2 (C)', 'description': 'Scaling factor for constraint 2 (C)'},
                                  {'name': 'D', 'value': 24900, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 3 (D)', 'description': 'Scaling factor for constraint 3 (D)'},
                                  {'name': 'E', 'value': 55000, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 4 (E)', 'description': 'Scaling factor for constraint 4 (E)'},
                                  {'name': 'F', 'value': 100, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Constraint 5 (F)', 'description': 'Scaling factor for constraint 5 (F)'},
                                  {'name': 'Z', 'value': 1, 'type': 'float_bounded', 'min': 0.0, 'max': 10 ** 10,
                                   'label': 'Scaling factor', 'description': 'Scaling factor for all constraint'}
                                  ],

            default_solver_parameter=DEFAULT_SOLVER_PARAMETERS,

            persistent_file=persistent_file
        )

    ##################################################################################################################
    def load(self, input_method,
             no_servers,
             server_cap,
             no_services,
             service_req,
             server_range_cpu,
             server_range_mem,
             random_seed,
             directory,
             server_range,
             service_range,
             service_range_cpu,
             service_range_mem,
             silent=False):
        ##################################################################################################################

        random.seed(random_seed)
        self.CPU_server_capacity = {}
        self.mem_server_capacity = {}
        self.time = 1
        self.CPU_req = {}
        self.mem_req = {}
        self.input_method = input_method

        if self.input_method == 'Random':
            new_servers_value = random.randint(server_range[0], server_range[1])
            new_services_value = random.randint(service_range[0], service_range[1])
            self.servers = [i for i in range(new_servers_value)]
            self.services = [i for i in range(new_services_value)]
            server_cap = []
            for s in range(new_servers_value):
                server_cap.append([random.randint(server_range_cpu[0], server_range_cpu[1]),
                                   random.randint(server_range_mem[0], server_range_mem[1])])

            service_req = []
            for n in range(new_services_value):
                service_req.append([random.randint(service_range_cpu[0], service_range_cpu[1]),
                                    random.randint(service_range_mem[0], service_range_mem[1])])
            print('Number of random servers:%d' % new_servers_value)
            print('Number of random services:%d' % new_services_value)
            print('Server CPU resources will range between %d to %d' % (server_range_cpu[0], server_range_cpu[1]))
            print('Server memory resources will range between %d to %d' % (server_range_mem[0], server_range_mem[1]))
            print('Services CPU requirement will range between %d to %d' % (service_range_cpu[0], service_range_cpu[1]))
            print('Services memory requirement will range between %d to %d' % (
            service_range_mem[0], service_range_mem[1]))


        elif self.input_method == 'Select_data':
            self.directory = directory
            print(self.directory + '/last_status_server_cap.xlsx')
            if os.path.exists(self.directory + '/last_status_server_cap.xlsx'):
                pd_server_cap = pd.read_excel(self.directory + '/last_status_server_cap.xlsx', index_col=False)
                self.servers = [i for i in range(len(pd_server_cap.columns) - 1)]
                server_cap = []
                for s in range(len(pd_server_cap.columns) - 1):
                    server_cap.append(list(pd_server_cap['Server_cap' + str(s)]))
            else:
                print('No last file available')
                empty_df = pd.DataFrame()
                with pd.ExcelWriter(self.directory + '/last_status_server_cap.xlsx', engine='xlsxwriter') as writer:
                    empty_df.to_excel(writer, 'Sheet1')
                server_cap = []

            if os.path.exists(self.directory + '/last_status_service_requirements.xlsx'):
                pd_service_req = pd.read_excel(self.directory + '/last_status_service_requirements.xlsx',
                                               index_col=False)
                self.services = [i for i in range(len(pd_service_req.columns) - 1)]
                service_req = []
                for n in range(len(pd_service_req.columns) - 1):
                    service_req.append(list(pd_service_req['Service_req' + str(n)]))
            else:
                print('No last file available')
                empty_df = pd.DataFrame()
                with pd.ExcelWriter(self.directory + '/last_status_service_requirements.xlsx', engine='xlsxwriter') as writer:
                    empty_df.to_excel(writer, 'Sheet1')
                service_req = []
        else:
            self.servers = [i for i in range(no_servers)]
            self.services = [i for i in range(no_services)]

        for s in self.servers:
            self.CPU_server_capacity[s] = server_cap[s][0]
            self.mem_server_capacity[s] = server_cap[s][1]
        for n in self.services:
            self.CPU_req[n] = [service_req[n][0]]
            self.mem_req[n] = [service_req[n][1]]
        server_capacity = {}
        for s in self.servers:
            server_capacity['Server_cap' + str(s)] = [self.CPU_server_capacity[s], self.mem_server_capacity[s]]
        pd_shift_req = pd.DataFrame.from_dict(server_capacity)
        with pd.ExcelWriter('./data/Actual/last_status_server_cap.xlsx', engine='xlsxwriter') as writer:
            pd_shift_req.to_excel(writer, sheet_name='Sheet1')

        service_requirements = {}
        for n in self.services:
            for t in range(self.time):
                service_requirements['Service_req' + str(n)] = [self.CPU_req[n][t], self.mem_req[n][t]]
        pd_shift_req = pd.DataFrame.from_dict(service_requirements)
        with pd.ExcelWriter('./data/Actual/last_status_service_requirements.xlsx', engine='xlsxwriter') as writer:
            pd_shift_req.to_excel(writer, sheet_name='Sheet1')

        print('Servers:  ', self.servers)
        print('Services:  ', self.services)
        print('Server CPU resources:', self.CPU_server_capacity)
        print('Server memory resources', self.mem_server_capacity)
        print('Services CPU requirements', self.CPU_req)
        print('Services memory requirements', self.mem_req)

        print('---------------------------- Load Complete ----------------------------')

    def fraction_resource(self, service, server, time, cpu=False, mem=False):
        '''
        This function calculates fraction of resource used by service of server of type cpu or mem
        :param service: int: service number
        :param server: int: server number
        :param time: int: timer of the day
        :param cpu: boolean: True calculate CPU resource utilization %
        :param mem: boolean: True calculate mem resource utilization %
        :return: float: fraction resource use
        '''
        if cpu:
            return self.CPU_req[service][time] / self.CPU_server_capacity[server]
        elif mem:
            return self.mem_req[service][time] / self.mem_server_capacity[server]

    ##################################################################################################################
    def build_qubo(self, A=400000, B=10, C=10, D=24900, E=55000, F=100, Z=1, silent=False):
        ##################################################################################################################
        print('QUBO formation started')
        S = len(self.servers)  # total server number
        N = len(self.services)  # total services number
        Y_1 = 7  # total slack bits for MAXCPU
        Y_2 = 7  # total slack bits for MAXMEM
        T = self.time
        var_shape_set = VarShapeSet(*(
                [BitArrayShape('s_n', (S, N), axis_names=["Servers", "Services"])] +
                [BitArrayShape('s', (S,), axis_names=["Used_servers"])] +
                [VarSlack('slk%s_%s_Y1' % (str(s), str(t)), slack_type=SlackType.binary, start=0, stop=101, step=1)
                 for t in range(T) for s in range(S)] +
                [VarSlack('slk%s_%s_Y2' % (str(s), str(t)), slack_type=SlackType.binary, start=0, stop=101, step=1)
                 for t in range(T) for s in range(S)]))

        ########### Constarint 0 Each service must be assigned exactly to 1 server #########
        result_qubo = BinPol(var_shape_set)
        self.Hs = {}
        if A != 0:
            self.HQ0 = BinPol(var_shape_set)
            for n in self.services:
                service_poly_temp = BinPol(var_shape_set)
                service_poly_temp += Term(1, ())
                for s in self.servers:
                    service_poly_temp += Term(-1, (('s_n', s, n),))
                service_poly_temp **= 2
                self.HQ0 += service_poly_temp
            self.HQ0 *= Z * A
            result_qubo += self.HQ0
            self.Hs['H0'] = self.HQ0

        ############# Constraint 1 cpu resource with slack bits #######################################################

        if B != 0:
            self.HQ1 = BinPol(var_shape_set)
            for t in range(T):
                for s in self.servers:
                    cpu_resource_poly_temp = BinPol(var_shape_set)
                    cpu_resource_poly_temp += Term(-100, ())
                    for n in self.services:
                        cpu_resource_poly_temp += Term(math.ceil(self.fraction_resource(n, s, t, cpu=True) * 100), (
                            ('s_n', s, n),))
                    cpu_resource_poly_temp.add_variable('slk%s_%s_Y1' % (str(s), str(t)))
                    cpu_resource_poly_temp **= 2
                    self.HQ1 += cpu_resource_poly_temp
            self.HQ1 *= Z * B
            result_qubo += self.HQ1
            self.Hs['H1'] = self.HQ1

        if C != 0:
            self.HQ2 = BinPol(var_shape_set)
            for t in range(T):
                for s in self.servers:
                    mem_resource_poly_temp = BinPol(var_shape_set)
                    mem_resource_poly_temp += Term(-100, ())
                    for n in self.services:
                        mem_resource_poly_temp += (self.fraction_resource(n, s, t, mem=True) * 100, (
                            ('s_n', s, n),))
                    mem_resource_poly_temp.add_variable('slk%s_%s_Y2' % (str(s), str(t)))
                    mem_resource_poly_temp **= 2
                    self.HQ2 += mem_resource_poly_temp
            self.HQ2 *= Z * C
            result_qubo += self.HQ2
            self.Hs['H2'] = self.HQ2

        if D != 0:
            self.HQ3 = BinPol(var_shape_set)
            for s in self.servers:
                HQ_5_temp = BinPol(var_shape_set)
                HQ_5_temp += Term(-1, ())
                HQ_5_temp += Term(1, (('s', s),))
                HQ_5_temp **= 2
                self.HQ3 += HQ_5_temp
            self.HQ3 *= -D * Z
            result_qubo += self.HQ3
            self.Hs['H3'] = self.HQ3

        if E != 0:
            self.HQ4 = BinPol(var_shape_set)
            for s in self.servers:
                c_s_poly_first = BinPol(var_shape_set)
                c_s_poly_first += Term(1, ())
                c_s_poly_first += Term(-1, (('s', s),))
                c_s_poly_second = BinPol(var_shape_set)
                for n in self.services:
                    c_s_poly_second += (1, (('s_n', s, n),))
                c_s_poly_first.multiply(c_s_poly_second)
                self.HQ4 += c_s_poly_first
            self.HQ4 *= Z * E
            result_qubo += self.HQ4
            self.Hs['H4'] = self.HQ4

        if F != 0:
            self.HQ5 = BinPol(var_shape_set)
            for s in self.servers:
                temp_poly_one = BinPol(var_shape_set)
                c_s = BinPol(var_shape_set)
                for t in range(T):
                    temp_poly_one.add_variable('slk%s_%s_Y1' % (str(s), str(t)))
                temp_poly_one.multiply(c_s.add_term(1, ('s', s)))
                self.HQ5 += temp_poly_one
            self.HQ5 *= Z * F
            result_qubo += self.HQ5
            self.Hs['H5'] = self.HQ5

            self.HQ6 = BinPol(var_shape_set)
            for s in self.servers:
                temp_poly_one = BinPol(var_shape_set)
                c_s = BinPol(var_shape_set)
                for t in range(T):
                    temp_poly_one.add_variable('slk%s_%s_Y2' % (str(s), str(t)))
                temp_poly_one.multiply(c_s.add_term(1, ('s', s)))
                self.HQ6 += temp_poly_one
            self.HQ6 *= Z * F
            result_qubo += self.HQ6
            self.Hs['H6'] = self.HQ6

        result_qubo = result_qubo.make_qubo(bit_precision=64)
        self.Hs['result_qubo'] = result_qubo

        if not silent:
            print('Generated QUBO HQ has {} bits.'.format(result_qubo.N))
        self.HQ = result_qubo
        print('QUBO generation complete')

    def prep_result(self, solution_list:SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        validation = self.HQ0.compute(configuration)
        if not silent:
            for poly in self.Hs:
                print("%s               = %10.6f" % (poly, self.Hs[poly].compute(configuration)))
        if validation != 0:
            warnings.warn(' Allocation is wrong, service extra allocated or not at all')
        self.solution = configuration
        self.schedule = solution_list.min_solution.extract_bit_array('s_n')
        self.servers_used = solution_list.min_solution.extract_bit_array('s')
        print('Scheduled services are as follows \n', self.schedule.data)

        self.schedule_services = {}  # dictionary with key as server and values are services running on it
        for s in self.servers:
            for n in self.services:
                if self.schedule.data[s][n]:
                    if s in self.schedule_services.keys():
                        self.schedule_services[s].append(n)
                    else:
                        self.schedule_services.update({s: [n]})

        print('Scheduled services are as follows \n', self.schedule_services)

    ##################################################################################################################
    def report(self, silent=False):
        ##################################################################################################################

        table_header = ['Servers/Services'] + [f'Service {n}' for n in self.services] + ['CPU usage [%]',
                                                                                         'Memory usage [%]']

        table_body = []
        for s in self.servers:
            cpu_resource_used = 0
            mem_resource_used = 0
            server_schedule = []
            server_schedule.append('Server %d' % s)
            if s in self.schedule_services.keys():
                for n in self.services:
                    if n in self.schedule_services[s]:
                        server_schedule.append(1)
                        cpu_resource_used += math.ceil(self.fraction_resource(n, s, 0, cpu=True) * 100)
                        mem_resource_used += math.ceil(self.fraction_resource(n, s, 0, mem=True) * 100)
                    else:
                        server_schedule.append(0)
            else:
                for n in self.services:
                    server_schedule.append(0)
            server_schedule.append(cpu_resource_used)
            server_schedule.append(mem_resource_used)
            table_body.append((server_schedule))

        self.output_table = tabulate(table_body, table_header, tablefmt="html")

        print(f'Total number of servers used: {sum(self.servers_used.data)} ')
        print("\nScheduling table")
        display(HTML(self.output_table))

    ##################################################################################################################
    def draw(self):
        ##################################################################################################################
        self.schedule.draw(figsize=(5.0, 10.0), max_ax_per_row=5, order=[1, 0])
        self.servers_used.draw(figsize=(3.0, 1.0), max_ax_per_row=1, order=[0])

