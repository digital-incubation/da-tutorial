{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parallel Tempering\n",
    "\n",
    "Beside simulated annealing the digital annealer supports parallel tempering as a second optimization algorithm. Parallel tempering executes the optimization on multiple temperatures in parallel on several replicas to find the best solution.\n",
    "Each replica can exchange the temperature assigned to it with a temperature of a neighboring replica based on a Metropolis criterion. This method thus enables a global search for an optimum.\n",
    "In addition, the digital annealer is able to automatically calibrate algorithmic convergence parameters while using the parallel tempering approach. This is useful, for example, when no calibration of convergence parameters of a chosen approach is desired for a particular QUBO problem, as for example in simulated annealing.\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pics_titel2.png\" width=\"1000\"></b></center>\n",
    "\n",
    "In this notebook, we will first explain the idea of the parallel tempering algorithm.\n",
    "\n",
    "Later we will present how the parallel tempering solver can be found in the dashboard.\n",
    "\n",
    "Except for determining the number of replicas, whose meaning we will explain in more detail later, no further input is needed for the parallel tempering algorithm in the digital annealer. \n",
    "\n",
    "In last section we will conclude in a hands-on chapter. Our CPU emulator version of the parallel tempering algoritm can be applied with different strategies to determine the replica temperatures and replica exchange pairs. We will explain the different options and you can experiment and observe the outcome."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Parallel Tempering Algorithm\n",
    "\n",
    "Like simulated annealing, parallel tempering (or replica exchange MCMC sampling) is an algorithm from the family of Markov chain Monte Carlo algorithms. The original version of parallel tempering was introduced by [Swendsen RH and Wang JS](https://doi.org/10.1103/PhysRevLett.57.2607), who introduced this method for a simulation of quenched random interactions in 1986. Practical experiences show that this method approximates \"acceptable\" solutions of optimization problems with less computing time than other standard Monte Carlo methods, especially those Monte Carlo methods where a considered Boltzmann-distribution based Metropolis criterion runs on small temperature differences, which are characterized by long correlation times. Long correlation times mean that these methods require many steps to produce samples to reach a \"good enough\" solution for the targeted optimization problem due to a possibly high difference to a presumed distribution or prior distribution in the initialization of the random walk.\n",
    "\n",
    "Since our focus is on optimization problems for the digital annealer, we consider in the following a QUBO model $H(x):=\\sum_{k,j=1}^K W_{k,j} \\cdot x_{k} \\cdot x_{j} + b_{k} \\cdot x_{k}$ to be minimized, for some $K\\in \\mathbb{N}_{>0}$, with binary decision variables $x_{k}\\in \\lbrace 0,1\\rbrace$ to be optimized. On the basis of this energy/cost function to be minimized, the parallel tempering approach will be explained in the lower section along the use of the digital annealer.\n",
    "\n",
    "The parallel tempering method presented in the following can basically be made up of two steps. In the first step a 'Constant - Temperature Monte Carlo' algorithm is used to develop certain stochastic processes (or replicas) under given temperatures. Each process develops under its own temperature and performs a temperature specific random walk. In a second step the developed random walk of one replica is compared with another replica, which is a temperature neighbour. A possible exchange of both replicas is carried out according to a Metropolis-Hastings criterion. A replica exchange means that the neighbouring replicas change their assigned temperatures. After the second step, we return to the first step ('Constant - Temperature Monte Carlo') and develop all replicas with their new assigned temperatures. Finally, it will be iterated between these two steps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "### First Step: Constant - Temperature Monte Carlo\n",
    "\n",
    "In parallel tempering, basically $N$-many stochastic processes/simulations (replicas) are considered, each of which corresponds to a certain temperature in $\\lbrace T_1, \\cdots, T_N \\rbrace$ with $\\infty>T_k > T_j>0$ for $k<j$. Let us denote these replicas with $S_i$, $i=1,\\cdots,N$, which each take their values in $\\lbrace 0,1\\rbrace^K$.\n",
    "\n",
    "Basically a replica $S_i$ is initialized randomly according to a given stochastic (prior) distribution in $\\lbrace 0,1\\rbrace^K$. Then each individual replica $S_i$ is developing a random walk by a \"Constant - Temperature Monte Carlo\" method with respect to the underlying QUBO model $H(S_i)$.\n",
    "A \"Constant - Temperature Monte Carlo\" method is actually a simulated annealing method based on the given QUBO model $H$ with equal start and end temperature (constant temperature) and a given number of iterations. In particular, replica $S_i$ is annealed with constant temperature by its assigned temperature, e.g. if $S_i$ is assigned to $T_i$ then $T_i$ is the start and end temperature of the annealing. For a better understanding consider the mentioned example in the figure below:\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pic_ovens2.PNG\" width=\"700\"></b></center>\n",
    "<font size=\"2\"><center>\n",
    "Each oven represents a random walk development of the corresponding replica $S_i$ under the constant temperature $T_i$,\n",
    "</center></font>\n",
    "<font size=\"2\"><center>\n",
    "where the process development takes place by a \"Constant - Temperature Monte Carlo\" method.\n",
    "</center></font>\n",
    "<font size=\"2\"><center>\n",
    "The oven color stands for the level of temperature, i.e. blue = cold and red = hot with increasing temperature order from left to right.\n",
    "</center></font>\n",
    "\n",
    "It should be immediately recognizable that the stochastic development of a replica $S_i$ does not depend on other replicas $S_j$, $i\\neq j$. This fact also leads to the name \"parallel tempering\".\n",
    "\n",
    "As we already learned from the simulated annealing demo notebook, the digital annealer is a highly specialized sample hardware unit, which is able to perform a quantum-inspired simulated annealing. With parallel tempering, each replica develops its random walk on a DA unit under a fixed temperature. In particular, each DA unit uses its highly specialized hardware environment to develop replicas of random walks, also known as a parallel trial scheme, see [DA-Paper](https://pdfs.semanticscholar.org/fd79/4c47edf63576c08dee95e901017c2b6d8f49.pdf). \n",
    "\n",
    "In the figure below we see what possible random walks of replicates can look like. Since the temperatures are chosen in a descending order, i.e. $T_k>T_j$ for $k<j$, replicates with high temperatures will perform a more extensive random walk (\"global walk\") than replicates with low temperatures, which move in smaller steps (\"local walk\").\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pic_random_walk_2.PNG\" width=\"700\"></b></center>\n",
    "\n",
    "<font size=\"2\"><center>\n",
    "This graphic shows possible random walks of replicas on a simplified 2D state map.\n",
    "</center></font>\n",
    "<font size=\"2\"><center>\n",
    "The replicas $S_i$ continue to develop under the temperatures $T_i$ assigned to them.\n",
    "</center></font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Second Step: Replica Exchange\n",
    "\n",
    "After step one ('Constant - Temperature Monte Carlo') a replica exchange step with temperature neighbouring replicas is performed. Each replica $S_i$ has developed a state in $\\lbrace 0,1 \\rbrace^K$. Now temperature-neighbouring replicas are compared with each other and exchanged according to a metropolis criterion. Exchanged here means that the replicas swap the adjacent temperatures used in the 'Constant - Temperature Monte Carlo' method. The process is illustrated again with the oven metaphor in the figure below.\n",
    "\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pic_exchange2.PNG\" width=\"700\"></b></center>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step One and Two: Parallel Tempering\n",
    "\n",
    "By coupling step one and two, the parallel tempering method is now established. More precisely, this means that after each step 1 the exchange step 2 takes place and then one returns to the first step. The advantage of parallel tempering is that replicas can process a global random walk on temperatures with higher values (e.g. $T_1$) and can therefore attain a wide range of possibilities at very different states. At the same time, by changing replicas in low temperatures (e.g. $T_N$), it is possible to perform a local search. This is demonstrated by the following figure:\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pic_random_walk2.PNG\" width=\"700\"></b></center>\n",
    "\n",
    "\n",
    "The following picture also shows how a random walk of replicas through the temperature landscape can look like.\n",
    "\n",
    "<center><b><img src=\"figures/parallel_temp_pic_temp_exchange.PNG\" width=\"500\"></b></center>\n",
    "<font size=\"1\"><center>http://www.inference.org.uk/tcs27/talks/sampling.html#49</center></font>\n",
    "\n",
    "\n",
    "The Digital Annealer itself only needs a specification of the number of replicas and then uses further heuristics for automatic temperature selection, see for example [DA parallel Tempering](https://www.frontiersin.org/articles/10.3389/fphy.2019.00048/full). In general, a higher number of replicas is correlated with a higher quality of the solutions. However, this can imply a 10 times longer runtime compared to the simulated annealing mode in the DA unit (under comparable settings), and therefore a separate break-even point for solution quality compared to solution run time must be found."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Remark on the parallel tempering mode in the DA unit\n",
    "\n",
    "The parallel tempering tool is particularly well suited for the initial study of QUBO issues. Especially if the user is not yet sure how to set the individual convergence parameters, e.g. temperatures and offset parameters in simulated annealing mode. The parallel tempering mode in the digital annealer takes over the parameter calibration. A disadvantage of this approach is that the digital annealer in parallel tempering mode is about 10 times slower than in simulated annealing mode.  \n",
    "\n",
    "However, one should have in mind the mentioned advantages of a parallel tempering approach we mentioned in the beginning of section **'Parallel Tempering Algorithm'**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ## Dashboard Parallel Tempering\n",
    "Now we can explore the **'Solve annealing'** dashboard, which allows us to switch to almost fully automatic solver **'parallel tempering'** in the Digital Annealer. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Solve annealing\n",
    " \n",
    "In the **'Solve annealing'** environment it is possible to set all necessary parameters to use parallel tempering as a solver.\n",
    "To use parallel tempering in the digital annealer, you must first set the **'parallel_tempering'** mode in the **'optimization_method'** window. For **'number_replicas'** one can choose between 26 and 128. For **'number_iterations'** one can choose up to 2 billion iterations. The higher the settings of **'number_replicas'** or **'number_iterations'** are chosen, the better the solution can be, but you have to expect longer computing times.\n",
    "\n",
    "<img src=\"figures/parallel_temp_pic_AnnealTab.PNG\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hands On\n",
    "\n",
    "Now that you have learned all the theory about parallel tempering and interpretation of the graphics in the `Anneal tracker`, it is time to experiment with different parameters.\n",
    "Below, you find the same example as in [M_01_Annealing](../M_01_Annealing/M_01_Annealing.ipynb). In particular, given a number of classes, teachers, rooms, periods per day and days, a random generator produces a time schedule problem. Each class obtains a required number of lessons. The optimization problem consists of finding a conflict free assignment of teachers, days, periods and rooms to classes such that each class gets its required lesson number.\n",
    "\n",
    "In the following cell we load the sources for the `OptimizerModel`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.Optimizer import *\n",
    "from sources.OptimizerAnnealing import OptimizerAnnealing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you execute the cell below, you can take a look at an example which was solved with one version of the parallel tempering solver as described above. Please take a look at the different tabs to get familiar with the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scenario = Optimizer(OptimizerAnnealing('M_02_Parallel_tempering_example.dao'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, in the `Solve annealing` tab you have the opportunity to switch between `parallel_tempering` and `annealing` as `optimization_mode`. In `parallel_tempering` mode, as described above, you can choose the temperature of the hottest and coldest furnace as `temperature_start` and `temperature_end`. Moreover, the number of furnaces can be chosen as parameter `number_replicas`.\n",
    "\n",
    "You can also switch between different `TemperatureModel`s and `ReplicaExchangeModel`s.\n",
    "\n",
    "In particular, the `Exponential` (`Linear`) temperature model creates a temperature list for the furnaces such that temperatures are exponentially (linearly) decreasing between `temperature_start` and `temperature_end`.\n",
    "The `Hukushima` temperature model samples a random walk for each furnace of an `Exponential` temperature model to estimate the energy of replicas in each furnace. Afterwards it determines the temperature of each furnace (except the first and last one) by a fixed point algorithm, updating estimated energies of replicas in the furnaces and furnace temperatures by interpolation. The algorithm stops when the estimated exchange probability between each pair of neighboring furnaces is (almost) equal.\n",
    "The exact algorithm can be studied in [Hukushima](https://doi.org/10.1103/PhysRevE.60.3606).\n",
    "\n",
    "For the `ReplicaExchangeModel`s, you can choose between the options `Neighbours` and `Far jump`. \n",
    "The former applies replica exchanges based on the Metropolis criterium mentioned above between any two pairs of furnaces in a shuffled list of transpositions between adjacent furnaces. The latter randomly chooses a partner furnace for each furnace and adds the corresponding pair plus the furnace and its direct neighbor to the list of transpositions. Replica exchanges are the executed based on the shuffled list of transpositions.\n",
    "\n",
    "Below you can see pictures of the `Anneal tracker` tab for optimization runs of the problem above with replica exchange model `Neighbours` and all temperature models. As you can see in the replica journey figures,  the temperature model `Hukushima` allows each replica to visit more furnaces than the `Exponential` and `Linear` one, albeit taking more time due to the sampling and fixed point operations to determine furnace temperatures. Moreover, the energy curve (red plot in waiting cycles figure) is more stable towards the end of the optimization in the `Hukushima` and `Exponential` model than in the `Linear` model. Therefore, the `Hukushima` and `Exponential` seem to be the best choices for the current problem.\n",
    "\n",
    "<center><b><img src=\"figures/Exponential_Neighbor.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Neighbours replica exchange model, Exponential temperature model</center></font>\n",
    "\n",
    "<p>\n",
    "    \n",
    "<center><b><img src=\"figures/Linear_Neighbor.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Neighbours replica exchange model, Linear temperature model</center></font>\n",
    "\n",
    "<p>\n",
    "\n",
    "<center><b><img src=\"figures/Hukushima_Neighbor.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Neighbours replica exchange model, Hukushima temperature model</center></font>\n",
    "\n",
    "For the replica exchange model `Far jump`, as you can see below, all temperature models seem to have a somewhat random replica-furnace history.\n",
    "At the same time, the energy curves are very unstable at all times. Therefore, the `Neighbors` replica exchange model seems to be the best choice.\n",
    "\n",
    "<center><b><img src=\"figures/Exponential_Jump.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Far jump replica exchange model, Exponential temperature model</center></font>\n",
    "\n",
    "<p>\n",
    "\n",
    "<center><b><img src=\"figures/Linear_Jump.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Far jump replica exchange model, Linear temperature model</center></font>\n",
    "\n",
    "<p>\n",
    "\n",
    "<center><b><img src=\"figures/Hukushima_Jump.png\" width=\"700\"></b></center>\n",
    "<font size=\"1\"><center>Far jump replica exchange model, Hukushima temperature model</center></font>\n",
    "\n",
    "After all the theory, it is now your turn to try the different parallel tempering options and solve the problem yourself. \n",
    "As a start please use `Default` settings in the tabs `Setup Scenario` and `Build QUBO`. In the `Solve annealing` tab please select 'All' in the `graphics` drop-down menu. \n",
    "Create your own scheduling problem executing the `Setup Scenario` and `Build QUBO` tab.\n",
    "Once the `Solve annealing` tab has finished execution, you may proceed to the `Anneal tracker` tab to obtain the current *anneal tracker* for the given parameters. Please try out different parameters for the parallel tempering process until you obtain a valid solution and compare the behavior using the anneal tracker plots. \n",
    "You can also switch to normal `annealing` mode and observe the differences.\n",
    "If you are struggling with the setting for `temperature_start` and `temperature_end` you can also try the automatic `Temperature determination and scaling` functionality as `tuning_method` in the `Tuning` accordion. \n",
    "For further reading on automatic tuning please also have a look at [M_01_Annealing](../M_06_Tuning/M_06_Tuning.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scenario1 = Optimizer(OptimizerAnnealing('M_02_Parallel_tempering.dao'))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
