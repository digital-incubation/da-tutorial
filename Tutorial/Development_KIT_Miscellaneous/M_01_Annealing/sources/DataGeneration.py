from dadk.BinPol import *
from dadk.QUBOSolverCPU import QUBOSolverCPU
from termcolor import colored
import random


class DataGeneration:

    def __init__(self,
                 CLASSES=None,
                 TEACHERS=None,
                 ROOMS=None,
                 PERIODS=None,
                 DAYS=None,
                 iterations=None,
                 seed_number=None):
        random.seed(seed_number)
        self.PERIODS = PERIODS
        self.DAYS = DAYS
        self.CLASSES = CLASSES
        self.ROOMS = ROOMS
        self.TEACHERS = TEACHERS
        self.iterations = iterations

        self.requirement_bit_size = len(bin(self.PERIODS)[2::])
        self.weekly_lesson_numbers = self.PERIODS * self.DAYS
        total_bits = self.requirement_bit_size * self.CLASSES * self.ROOMS * self.TEACHERS

        if total_bits > 1024:
            raise Exception('Total bits greater than 1024, please reduce the selected values, and try again!.')

        print('Artificial Data Generation: In progress!')
        print('This may take approximately 30-40 seconds, depending on the host machine.')
        print('Please note that we are creating a dense time table schedule,')
        print('where no room, teacher or class is free over the whole week.')
        print('Such a dataset corresponds to a Magic Cube, (with an exception to diagonal sum)!')
        print('We create such a dataset using the QUBO strategy.')

        self.HQ = self._create_qubo_for_data_generation()

        self.solution_list = self._create_solve_qubo()

        self.requirement_data = self._prepare_result()

    def _create_qubo_for_data_generation(self, qubo_weight=500):
        """
        The `build_qubo` method is used to create the QUBO described in the description of the notebook.
        The method takes different penalty terms and saves the QUBO terms in the class attribute.
        :param `qubo_weight`: Overall weight multiplied to the complete QUBO for better convergence of the solution
        :return: -
        """

        var_shape_set = VarShapeSet(BitArrayShape('x', (self.requirement_bit_size, self.CLASSES, self.ROOMS, self.TEACHERS)))

        H_class = BinPol(var_shape_set)
        for c in range(self.CLASSES):
            temp_qubo = BinPol(var_shape_set)
            temp_qubo.add_term(self.weekly_lesson_numbers, ())
            for r in range(self.ROOMS):
                for t in range(self.TEACHERS):
                    for bit_size in range(self.requirement_bit_size):
                        temp_qubo.add_term(-2 ** bit_size, (('x', bit_size, c, r, t),))
            temp_qubo.power(2)
            H_class.add(temp_qubo)

        H_room = BinPol(var_shape_set)
        for r in range(self.ROOMS):
            temp_qubo = BinPol(var_shape_set)
            temp_qubo.add_term(self.weekly_lesson_numbers, ())
            for c in range(self.CLASSES):
                for t in range(self.TEACHERS):
                    for bit_size in range(self.requirement_bit_size):
                        temp_qubo.add_term(-2 ** bit_size, (('x', bit_size, c, r, t),))
            temp_qubo.power(2)
            H_room.add(temp_qubo)

        H_teacher = BinPol(var_shape_set)
        for t in range(self.TEACHERS):
            temp_qubo = BinPol(var_shape_set)
            temp_qubo.add_term(self.weekly_lesson_numbers, ())
            for r in range(self.ROOMS):
                for c in range(self.CLASSES):
                    for bit_size in range(self.requirement_bit_size):
                        temp_qubo.add_term(-2 ** bit_size, (('x', bit_size, c, r, t),))
            temp_qubo.power(2)
            H_teacher.add(temp_qubo)

        HQ = H_class.clone().add(H_room).add(H_teacher).multiply_scalar(qubo_weight)

        return HQ

    def _create_solve_qubo(self):
        solver = QUBOSolverCPU(
            number_iterations=self.iterations,
            number_runs=1,
            temperature_start=10000,
            temperature_end=10,
            temperature_mode=0,
            temperature_interval=10,
            offset_increase_rate=100,
            solution_mode='QUICK')

        solution_list = solver.minimize(self.HQ)
        return solution_list

    def _prepare_result(self):
        """
        The `prep_result` method is used to read the solution after the optimization. 
        :param `solution_list`: Solution list attribute of the Optimizer Class
        :param `objective_penalty`: Penalty term multiplied to QUBO terms corresponding to objective
        :return: -
        """
        solution = self.solution_list.min_solution
        values = solution.extract_bit_array('x').data
        requirement_data = dict()
        for c in range(self.CLASSES):
            for r in range(self.ROOMS):
                for t in range(self.TEACHERS):
                    key = (c + 1, r + 1, t + 1)
                    if key not in requirement_data:
                        requirement_data[key] = 0
                    for bit_size in range(self.requirement_bit_size):
                        if values[bit_size, c, r, t]:
                            requirement_data[key] += 2 ** bit_size

        HQ_value = self.HQ.compute(solution.configuration)
        if round(HQ_value, 6) == 0.0:
            print(colored('Data Generation Successful \n', 'green'))
        else:
            print(colored('Re-run the load tab!  \n', 'red'))

        return requirement_data
