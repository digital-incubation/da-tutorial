{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Annealing Tutorial\n",
    "\n",
    "In this notebook we explain the **Simulated Annealing** (SA) Algorithm which is a general purpose metaheuristic algorithm to solve NP-Hard optimization problems. In addition to the standard SA, we also explain some additional features of the specialized Simulated Annealing incorporated with the **Fujitsu's Digital Annealer Unit** (DAU). The DAU is accessed by using a dedicated python framework called the **Digital Annealer Development Kit** (DADK). In this notebook, the user will be provided with the opportunity to work the DADK and explore all the features, along with a deeper understanding of the core optimization techniques adopted and developed for the DAU. \n",
    "\n",
    "To understand SA, one needs to understand the core idea behind the algorithm and the parameters involved in the algorithm, which play a vital role in obtaining an optimal/best solution for an **NP-hard** problem. Towards the end of this notebook, we also provide a *playground* for the reader to change and tune several parameters to better understand the complete algorithm. Note that throughout this notebook, we assume that the optimization task is to minimize some objective function.\n",
    "\n",
    "\n",
    "\n",
    "## Simulated Annealing\n",
    "\n",
    "Simulated Annealing is conceptually based on metallurgical annealing where a crystalline solid is heated and then allowed to cool very slowly until it achieves its most stable lattice energy state. If the cooling schedule is sufficiently slow, the final configuration results in a solid with best structural stability. The figure below shows the stages of annealing process in metallurgy. Simulated Annealing establishes the connection between this type of thermodynamic behavior and the search for a global minimum for a discrete optimization problem. Furthermore, it provides an algorithmic means for exploiting such a connection [[2]](https://link.springer.com/chapter/10.1007/0-306-48056-5_10). \n",
    "\n",
    "\n",
    "\n",
    "\n",
    "<center><b><img src=\"figures/annealing.png\" alt=\"Drawing\" style=\"width: 800px;\"/> </b></center>\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "At each iteration of a SA, the algorithm retains the current solution candidate, and generates a new solution. The new solution is obtained by selecting a local neighbor of the current solution. If the new solution gives a lower energy it is accepted. However, if the new solution yields a higher energy it is not discarded, instead, it is accepted with a certain probability. The probability of accepting a non-improving solution depends on a temperature parameter, which is typically non-increasing with each iteration of the algorithm. The key algorithmic feature of SA is that it provides a means to escape local optima by allowing hill-climbing moves (i.e., moves which worsen the objective function value). As the temperature is reduced, hill-climbing moves occur less frequently, and the solution converges to a global-optimum (or near-optimum)  [[3]](https://link.springer.com/chapter/10.1007/0-306-48056-5_10).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Energy Function\n",
    "\n",
    "The energy function is assumed to be a Quadratic Unconstrained Binary Optimization (QUBO) expression, defined on binary variables $\\mathbf{x}_i \\in \\{0,1\\}^n$, where $n$ is assumed to be the total number of variables. Formally, we refer to the energy function as $f(\\mathbf{x})$ which is a QUBO, defined as\n",
    "\n",
    "$$\n",
    "f(\\mathbf{x}) = \\mathbf{x}^T W  \\mathbf{x} \\;,\n",
    "$$\n",
    "such that $W$ is a square matrix containing the coefficients of the QUBO terms. \n",
    "**_Note_**: The matrix $W$ in the above equation is assumed to be an upper triangular matrix, such that linear-term coefficients are in the diagonal and the second order of type $x_ix_j$ (s.t. $i\\neq j$) are present in the upper triangle.\n",
    "\n",
    "\n",
    "## Acceptance Probability\n",
    "\n",
    "Suppose we have an optimization problem where the binary decision variables at some SA iteration $i$ are defined by a vector $\\mathbf{x}_i \\in \\{0,1\\}^n$, where $n$ is the total number of decision variables. Additionally there is an associated temperature $T_i$ which is initialized (for $i=0$) to some high value. Let $f(\\cdot)$ be the discrete objective function such that $f(\\mathbf{x})$ is the energy of the configuration $\\mathbf{x}$. The optimization task is to find the vector $\\mathbf{x_\\text{min}}$ such that $f(\\mathbf{x_\\text{min}})$ is the minimum value of $f(\\mathbf{x})$. Formally, \n",
    "\n",
    "$$\n",
    "\\mathbf{x_\\text{min}} = \\underset{\\mathbf{x}}{\\mathrm{argmin}}  f(\\mathbf{x}) \\;.\n",
    "$$\n",
    "\n",
    "**Note**: _A QUBO always has a global minimum, due to its inherent finite structure._\n",
    "\n",
    "\n",
    "The SA starts with some initial configuration $\\mathbf{x}$ and computes the current energy as $f(\\mathbf{x})$. The configuration $\\mathbf{x}$ then goes through a perturbation, where certain bits in the vector $\\mathbf{x}$ are chosen and flipped from $0$ to $1$ (and vice-versa). The perturbation step allows us to generate a new solution, say $\\mathbf{x'}$, with the energy $f(\\mathbf{x'})$. The next step involves the decision to accept this new configuration for the next step or to discard it. \n",
    "\n",
    "If the new configuration offers a better solution (*i.e.* a lower energy) it is accepted as the current solution and the state $\\mathbf{x'}$ is taken as the new $\\mathbf{x}$. However, if $f(\\mathbf{x'}) > f(\\mathbf{x})$, the new configuration goes through a Metropolis acceptance criterion. With this criterion the new higher energy solution $\\mathbf{x'}$ is accepted with a certain probability $P(\\mathbf{x},\\mathbf{x'})$ which depends on the current temperature $T_i$, such that\n",
    "\n",
    "\\begin{equation*}\n",
    "P(\\mathbf{x},\\mathbf{x'}) = \\begin{cases}\n",
    "1 &\\text{$f(\\mathbf{x'}) \\leq f(\\mathbf{x})$}\\;,\\\\\n",
    "\\exp \\left( \\frac{-\\left( f(\\mathbf{x'}) - f(\\mathbf{x}) \\right) }{T_i} \\right) & \\text{$f(\\mathbf{x'}) > f(\\mathbf{x})$} \\;.\n",
    "\\end{cases}\n",
    "\\end{equation*}\n",
    "\n",
    "This acceptance probability is the basic element of the search mechanism in SA. This so called 'Metropolis Acceptance Criterion' helps the algorithm to avoid falling into a local minimum. The figure below (adapted from [[4]](https://beta.vu.nl/nl/Images/werkstuk-boxelaar_tcm235-839827.pdf)) shows the acceptance of a bad solution (during hill climbing) and the overall convergence of SA.\n",
    "\n",
    "<center><b><img src=\"figures/simulated_annealing.png\" alt=\"Drawing\" style=\"width: 700px;\"/> </b></center>\n",
    "\n",
    "The figure above shows that the SA algorithm starts with a high temperature, at which stage the algorithm accepts both kinds of solutions: _better_ and _worse_. The solutions which are better are anyway accepted, while the solutions which are _worse_ also get accepted since at high temperatures the metropolis acceptance probability is very high. As the temperature is decreased, the acceptance of worse solution is gradually decreased (but still $\\gg 0$). In this phase the solutions which perform hill climbing by perturbation, are accepted probabilistically depending on the Metropolis acceptance. In this phase (unlike the initial phase), not _all_ the worse solutions are accepted. Since the temperature is reduced throughout the annealing process, towards the end of the annealing process, the algorithm is expected to start converging towards an optimal/near-optimal solution since the acceptance probability of _worse_ solution becomes very close to $0$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Annealing temperatures\n",
    "\n",
    "Appropriate annealing temperatures are of vital importance to obtaining an optimal/good solution through simulated Annealing. To understand the role of temperature in SA, we study the acceptance criterion once again, given by equation below.\n",
    "\\begin{equation*}\n",
    "P(\\mathbf{x},\\mathbf{x'}) = \\begin{cases}\n",
    "1 &\\text{$f(\\mathbf{x'}) \\leq f(\\mathbf{x})$}\\;,\\\\\n",
    "\\exp \\left( \\frac{-\\left( f(\\mathbf{x'}) - f(\\mathbf{x}) \\right) }{T_i} \\right) & \\text{$f(\\mathbf{x'}) > f(\\mathbf{x})$} \\;.\n",
    "\\end{cases}\n",
    "\\end{equation*}\n",
    "\n",
    "Recall that an improved solution is always accepted in the SA, while a *worse* solution is accepted with a probability of $P(\\mathbf{x},\\mathbf{x'})$ which depends on the current state of the solutions and the current temperature $T_i$. We only need to focus on the case when $f(\\mathbf{x'}) > f(\\mathbf{x})$. For further analysis we will denote the term $ \\left( f(\\mathbf{x'}) - f(\\mathbf{x}) \\right) $ in the exponential as $\\Delta E$ for brevity.\n",
    "\n",
    "\n",
    "### Case 1: Temperature too high ($T_i \\longrightarrow \\infty$)\n",
    "If the current temperature is too high, _i.e._ $T_i \\longrightarrow \\infty$, and $\\Delta E \\neq 0$, then $\\frac{-\\Delta E}{T_i} \\rightarrow 0$, implying $\\exp\\left(\\frac{-\\Delta E}{T_i}\\right) \\rightarrow 1$. Hence, the implication of a very high temperature is that no matter what the solution is, a new solution is always accepted, and the algorithm turns to a random search.\n",
    "\n",
    "### Case 2: Temperature too low ($T_i \\longrightarrow 0$)\n",
    "If the current temperature is too low, _i.e._ $T_i \\longrightarrow 0$, and $\\Delta E \\neq 0$, then $\\frac{-\\Delta E}{T_i} \\rightarrow - \\infty$, implying $\\exp\\left(\\frac{-\\Delta E}{T_i}\\right) \\rightarrow 0$. Hence, the implication of a very low temperature is that SA will not accept any *worse* solutions, and make only greedy steps.\n",
    "\n",
    "\n",
    "\n",
    "Hence, the temperature for SA is chosen high enough in the beginning to leave room for exploration of the search space. The temperature is then gradually reduced at each (or certain iterations) to a lower value. The end temperature must be small enough for convergence of the annealing algorithm, but it must not be too small so that it gets stuck in some local minimum for a lot or iterations towards the end.\n",
    "\n",
    "**Remark: In Simulated Annealing the temperature at any point in time (iterations) must be greater than or equal to $0$. This is important to keep the bound of $P(\\mathbf{x},\\mathbf{x'})$ with in $[0,1]$.**\n",
    "\n",
    "The reduction of the temperature is done with the help of an *anneal schedule*, which defines how the temperature is reduced over SA iterations. Most common anneal schedules are,\n",
    "- **Exponential schedule**, $T_{i+1} = \\alpha \\cdot T_i$, where $\\alpha$ is called the cooling rate of SA algorithm,\n",
    "\n",
    "\n",
    "- **Linear schedule**, $T_{i+1} = T_i - \\beta$, where $\\beta$ is the linear cooling decrement value. When using a linear schedule, care must be taken to avoid negative temperatures,\n",
    "\n",
    "\n",
    "- **Custom schedule**, $\\gamma = \\frac{T_{\\text{initial}} - T_{\\text{final}} }{\\text{Number of Iterations}}$, where the cooling rate is automatically computed from the initial and final temperature. In this cooling schedule, the cooling rate $\\gamma$ is computed based on the initial temperature $T_{\\text{initial}}$, the final temperature $T_{\\text{final}}$, and the number of iterations for the annealing process. Initial Temperature should be high enough, while the final temperature should be low enough for the Metropolis acceptance to take effect. Note that the decrease in temperature is made using a linear schedule, _i.e._ , \n",
    "$$\n",
    "T_{i+1} = T_i - \\gamma \\;.\n",
    "$$\n",
    "\n",
    "\n",
    "The DAU also carries out the above mentioned _custom_ cooling schedule. The cooling rate $\\alpha$ $\\left(\\text{or $\\beta$ or $\\gamma$, depending on which cooling schedule is chosen}\\right)$ must be kept as close as possible to $1$, to avoid rapid decrease in temperature. As a matter of fact, it has been theoretically proven that the probability of the SA algorithm terminating to a global optimal approaches $1$, as the annealing schedule is extended [[5]](https://ieeexplore.ieee.org/document/295910). \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Temperature Interval\n",
    "Temperature Interval is another parameter incorporated with the annealing algorithm in the DAU. Usually in SA the temperature is reduced in every iteration. However, if the cooling rate is quite high the probability of acceptance can decrease very rapidly, as the number of annealing iterations increase. To avoid a rapid decrease in temperature, the _temperature_interval_ parameter ensures that the temperature does not change for a given number of iterations (these given number of iterations is actually the _temperature_interval_ parameter). As per the cooling schedule, a temperature is changed from $T_i$ to $T_{i+1}$ only after every _temperature_interval_ iterations. Note that if the _temperature_interval_ is greater than zero, then the computation of cooling rate is changed to,\n",
    "\n",
    "$$\n",
    "\\gamma = \\frac{T_{\\text{initial}} - T_{\\text{final}}}{\\text{Number of Iterations}} \\cdot \\text{temperature\\_interval} \\;.\n",
    "$$\n",
    "\n",
    "The temperature is changed as per the linear scheduled after every _temperature_interval_ steps.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Stopping Criteria\n",
    "Stopping criteria are important to decide when to stop the annealing process. One of the standard way is to define a number of iterations that must take place in the annealing process. Note that, in the initial iterations SA consists both greedy steps as well as explorative steps wherein high energy solutions are also accepted. Towards the end, however, SA relies more on convergence and less on the explorative aspect. Hence, it is important to have the number of iterations to be high enough, so that, both the convergence and explorative aspects of the SA are utilized. At the same time, if the number of iterations are too high, one may not gain much, as towards the end the temperature is too low and the solution would predominantly perform a greedy local search in the global minimum. Another stopping criterion is the stalling energy. One may record some number of previous iterations' energies, and the algorithm may be halted if the energies are stalled.\n",
    "\n",
    "In the DADK, the stopping criterion is the number of iterations. In this criterion, the annealing process explained above is executed for the given number of iterations. After the execution of all the iterations, the final solution (solution candidate) is provided as the output of the optimization problem. Note that the cooling rate for the DAU (as mentioned above) depends on the total number of iterations, initial temperature and the final temperature.\n",
    "\n",
    "\n",
    "## DAU Parallel Implementation\n",
    "In the previous sections, we have learned about the individual technical aspects involved with the DAU's optimization algorithm. We now present a short overview of the implementation of the above mentioned annealing process and its parallelization. \n",
    "\n",
    "The operational process of the DA is divided into the following phases: a _trial_ phase in which the bit flip satisfying the acceptance criterion is selected, and an _update_ phase in which the selected bit is flipped.\n",
    "\n",
    "Specifically, in the _trial_ phase, a _single_ bit is chosen from the current state $\\mathbf{x}$, and the energy change ($\\Delta E$) for flipping the bit is computed. This energy change is used to determine whether to accept the bit flip in accordance with the Metropolis-Hastings criterion. \n",
    "\n",
    "_Remark_ : A single bit flip is one of the major aspects of the DA, which offers tremendous speed-up, when clubbed with a parallel implementation of the trial phase. The Metropolis acceptance of all possible bit flips up to 8192 can be calculated in parallel. Even more the DA offers $16$ independent _trial-update_ processes on different base states so that $16$ independent annealing processes can be executed in parallel.\n",
    "\n",
    "In the _update_ phase, the flip-bit selector selects one bit (say, $x_i$, $i$ being some bit-index) to be flipped and updates the value of that bit ($x_i \\rightarrow 1 - x_i$). In the DA, it is the _trial_ phase which is performed in parallel, so the probability of transition to a new state increases at most $N$ ($N$ is the total number of bits) times as compared to a serial search. The figure below shows the schematic diagram of the parallel search technique [[6]](https://ieeexplore.ieee.org/abstract/document/9045100).\n",
    "<center><b><img src=\"figures/parallel_technique.png\" alt=\"Drawing\" style=\"width: 500px;\"/> </b></center>\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "### Offset\n",
    "\n",
    "Apart from the standard SA features explained above, the DAU and its emulator also offer a parameter called _offset_ energy. Imagine your random walk is in a local minimum and the temperature is so low that $\\exp\\left(\\frac{-\\Delta E}{T_i}\\right) << 1$. With such a low acceptance probability the annealing process is very unlikely to make a step. One can introduce a positive offset in the energy of the current state, which in turn increases the probability of a candidate to jump out of a local minimum. The high temperature of SA does ensure the exploration of the search space given a high temperature, but towards the end of the iterations when the temperature is too low, the offset parameter helps avoiding local minimum, as shown in the figure below [[6]](https://ieeexplore.ieee.org/abstract/document/9045100). The _offset_ energy is depicted by $E_\\text{off}$, in the figure. \n",
    "\n",
    "<center><b><img src=\"figures/offset_escape.png\" alt=\"Drawing\" style=\"width: 600px;\"/> </b></center>\n",
    "\n",
    "\n",
    "More formally, one can understand the effect of $E_\\text{off}$ by considering the Metropolis acceptance probability. If in the trial phase all bit flips create worse solutions and all the Metropolis acceptance experiments fail, then we get no candidate for the update phase. We can't go in any direction and hence name this trial-update-process a waiting cycle. In order to avoid too many waiting cycles now the Metropolis criterion for all bit flips in the next trial is modified with a higher acceptance probability of $\\exp\\left(\\frac{-\\Delta E + E_\\text{off}}{T_i}\\right)$. This can be understood as lifting the energy from the local minimum energy by $E_{\\text{off}}$ so that the energy difference between our current state and the tested state is reduced and therefore the acceptance probability is increased. Nevertheless, it can happen that again no candidate is produced in the next trial phase. After the second and more generally after the $k^{th}$ waiting cycle the Metropolis acceptance probability is changed to $\\exp\\left(\\frac{-\\Delta E + k\\cdot E_\\text{off}}{T_i}\\right)$. After a finite number of steps the lifted energy will reach the energy of trial states and latest then the probability is 1 and a candidate is produced. Once this is the case and we can go forward, the complete dynamic energy is switched off and the process continues with the normal Metropolis criterion.\n",
    "\n",
    "As one can imagine, the offset value must not be too low, as this would not allow the solution to come out of local minimum in few steps. Additionally, a very high offset value might totally loose the result of the annealing process, as a very high _offset_ energy would allow for any new solution to be accepted regardless of how worse the new solution is. Hence, an offset energy value is chosen such that in a few number of offset increments the solution can jump out of its barrier."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Various Parameter Settings**\n",
    "\n",
    "\n",
    "The size of the problem in all our illustration is $3750$ bits, *i.e.* there are $3750$ decision variables in our QUBO. We now present different test scenarios, by taking various combinations of parameters. \n",
    "\n",
    "**Case 1:**\n",
    "\n",
    "\n",
    "- Number of Iterations: 20000\n",
    "- Initial Temperature : 5000\n",
    "- Final Temperature: 100\n",
    "- Temperature Interval: 20\n",
    "- Offset Increase Rate: 10\n",
    "\n",
    "\n",
    "<center><b><img src=\"figures/suboptimal_annealing.png\" alt=\"Drawing\" style=\"width: 1000px;\"/> </b></center>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The anneal tracking of the above setting are shown in the above figure. The aim of the anneal tracker is to identify good combinations of SA parameters to run on the DA. A good analysis of the anneal tracker gives us enough information to better tune the parameters.\n",
    "\n",
    "\n",
    "For the above mentioned parameters, the first graph (on top left side) shows the plot of *Waiting cycle vs. iterations*, along with the log of temperature over iterations as the red line. The waiting cycle is defined as the number of offset increments a solution needs to jump out of the current local minimum barrier. The first graph on the upper left corner, shows the annealing process taking place until approximately 10K iterations. After that point the offset increments start to occur, to jump off the local minimum. In this setting however, one can see that the offset increment of even 175 times the _offset_ energy does not allow the solution to jump out of the local minimum. The plot of (log of energy) vs iterations shows that the annealing algorithm is basically behaving as a greedy algorithm until 2500 iterations. After 2500 iterations the fluctuations in the graph suggest that the Metropolis acceptance criterion is showing its effect by accepting worse solutions and jumping to a new local minimum. The best solution is actually obtained approximately towards the end of the annealing process near iteration number 12500, however some offset increments also played a role in reaching that solution. Nonetheless, the solution obtained is not optimal, which tells us that the temperature and offset increase parameters need some tuning. \n",
    "\n",
    "The upper graph on the right side shows the bit flips over the iterations. In this particular case, we initially see the blue dots, which suggest us that initially several bits are switched off as this leads to an improvement of the solution, until approximately 2500 iteration. Note the corresponding drop of the curve in the plot of log of energy vs. iterations. This initial stage is the greedy stage. In the upper right graph the red dots correspond to the annealing phase between 2500 iterations to around 12500 iterations, where the bits are flipped and the Metropolis acceptance criterion is taking effect. Towards the end of this graph, the green squares (which appears like a continuous line here) depict that no bits are flipped. This is due to the offset increase, when the energy of the candidate state is increased, but the new solution is not accepted.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "**Case 2:**\n",
    "\n",
    "- Number of Iterations: 80000\n",
    "- Initial Temperature : 10000\n",
    "- Final Temperature: 50\n",
    "- Temperature Interval: 50\n",
    "- Offset Increase Rate: 1000\n",
    "\n",
    "<center><b><img src=\"figures/annealing_optimal.png\" alt=\"Drawing\" style=\"width: 1000px;\"/> </b></center>\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this particular case, we have a better annealing process, along with the offset increments. In the top left plot, we can now see the waiting cycles for the offset increment. On average an offset increase of around 3 times allows the solution to jump out of the barrier. The top right graph shows that in the initial phase many bits are switched off by greedy steps, but after around 500 to 1000 iterations the bit flip is occurring for almost all the bits. The phase between 1000 iterations to around 40000 iterations is the thermal annealing phase, where the bits are flipped and the Metropolis acceptance criterion is taking effect. The offset increment is also causing the bits to flip back and forth, which is evident from the bit plot after the 40000 iterations. The green squares show that no bit flips took place. This is the phase when the offset increment is taking place and the new solution is not accepted. These green points are at the same iteration positions, where we see the waiting cycles in the first graph.\n",
    "\n",
    "The log-energy curve shows the trend of the energy values of the solution over time. The initial greedy phase is very evident from the curve between 0 to ~1000 iterations. The sharp increase and decrease in the energies between 1K to 40K iterations happens due to the SA acceptance of worse solutions, and jumping to another local minimum. The hills and valleys towards the end show the effect of the offset increase rate with a crossing of the solution to some other local minimum. The optimal solution is obtained near 60K iterations. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hands On\n",
    "In the next section, we present several scenarios of the above features and parameters, to illustrate their effect on the final solution quality as well as the convergence process of SA adopted by the DAU. The user is asked to understand the effects, and to try out parameters of their choice for a better understanding of the SA algorithm running on the Digital Annealer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from dadk.Optimizer import *\n",
    "from sources.OptimizerAnnealing import OptimizerAnnealing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scenario\n",
    "\n",
    "In this part of the notebook we ask the user to test the annealing parameters and see the changes that occur in the anneal tracker. As a start we ask the user to use `Default` settings in all the tabs: `Setup Scenario`, `Build QUBO`, `Solve annealing` tabs, except for one change. In the `Solve annealing` tab please select 'All' for graphics drop-down menu. Other parameters need not be changed. \n",
    "\n",
    "Once the `Solve annealing` tab has finished execution, then you may proceed to the `Anneal tracker` tab to obtain the current *anneal tracker* for the given parameters. We ask the user to try out different parameters for the annealing process and understand the behaviour using the anneal tracker plots.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scenario = Optimizer(OptimizerAnnealing('M_01_Annealing.dao'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
