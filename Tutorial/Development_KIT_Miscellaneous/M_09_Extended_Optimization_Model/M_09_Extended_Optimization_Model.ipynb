{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "from IPython.display import display, HTML\n",
    "display(HTML(\"<style>.container{width:100% !important;}</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Extended Optimization Model: Digital Annealer System (Version 3 and Higher)\n",
    "\n",
    "Version 3 of Digital Annealer introduced the Digital Annealer System (DAS), which integrates one or multiple Digital Annealer Units (DAU). This system automatically decomposes big QUBO problems into smaller subproblems, which can be solved on the Digital Annealer Unit. The results of the subproblems are used to create solutions for the full problem. This concept allows to overcome limitation in the number of bits that can be supported by a hardware unit like DAU. The Digital Annealer System supports QUBO's with up to 100,000 bits.\n",
    "\n",
    "Beside the tremendous extension in QUBO size the Digital Annealer System supports new modeling features:\n",
    "- 1-hot bit groups\n",
    "- separate penalty polynomial\n",
    "- linear inequality constraints\n",
    "\n",
    "This tutorial explains these features and their usage. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimization Process\n",
    "\n",
    "Digital Annealer Version 2 executes an energy minimization for a QUBO directly and returns the results of the parallel runs. Digital Annealer Version 3 and later has a more differentiated optimization process flow as shown in the image below:\n",
    "1. The input can be a QUBO or the input can be enriched by a dedicated penalty QUBO, 1-hot definitions or linear inequalities. The problem definition as input is transferred to different optimization pipelines. The global search is a tabu search. In the first round it uses a random start state or a start state specified by the user. It executes a global search as a random walk with \"wide steps\" and writes the results to a solution buffer. The local search reduces big problems by setting many bit variables constant and only variating a subset of the bits during an optimization. The local search in the first step is based on a random start state or a start state specified by the user. The result is written to the solution buffer. The Solution buffer has a fixed size and when the size is exceeded, then only the best solutions are preserved and the weakest solutions are removed from the buffer. \n",
    "2. After an optimization pipeline has terminated it can start again. The start state for the new optimization is created from the solution states currently in the solution buffer. This is done by combining and variating states to be sure, not to repeat previous optimizations. On the other hand the search is started in the neighborhood of promising states. This couples the two pipelines and e.g. can start the local search influenced by good solution states found in the global search.\n",
    "3. The optimization pipelines are executed in loop until a given time limit is exceeded or a given target energy is undercut by a found solution. Once the stoppage criterion is reached, no more optimization pipelines are executed and the best solutions from the buffer are given  back to the client.\n",
    "\n",
    "![Figure 1: Digital Annealer Optimization Process](./figures/DA_optimization_process.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solver parameter\n",
    "The optimization process is controlled by parameters:\n",
    "- Overall execution process: The break-off criteria execution time and target energy can be specified.\n",
    "- Optimization pipeline: Only very few like number of iterations are required\n",
    "- Penalty constraint weighting: Specify the progress of the automatic coefficient scaling between penalty polynomial and optimization polynomial.\n",
    "\n",
    "An overview of the available solver parameter can be generated with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dadk.JupyterTools import ShowSolverParameter\n",
    "ShowSolverParameter()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Traveling Salesman Problem (TSP) Revisited\n",
    "\n",
    "This combinatorial optimization problem can be formulated as a quadratic unconstrained binary optimization problem (QUBO).\n",
    "All constraints and the optimization target are formulated with $N\\cdot N$ binary variables $x_{t,c}\\in \\{0,1\\}$, where $t\\in \\{0,\\ldots, N-1\\}$ stands for time and $c\\in \\{0,\\ldots, N-1\\}$ stands for city, and \n",
    "$$ \n",
    "x_{t,c} = \\left\\{ \\begin{matrix}\n",
    "                1, &&\\text{if the salesman visits city }c\\text{ at time }t,\\\\\n",
    "                0 &&\\text{else.}\n",
    "          \\end{matrix}\\right.\n",
    "$$\n",
    "\n",
    "Based on this bit model the TSP model has two hard constraints and an optimization target.\n",
    "\n",
    "The first constraint is that at each time $t$ exactly one city $c$ must be visited. This condition can be forced by the penalty term \n",
    "$$\n",
    "P_{time} = \\sum_{t=0}^{N - 1} \\left(1 - \\sum_{c=0}^{N - 1} x_{t,c} \\right)^2.\n",
    "$$\n",
    "\n",
    "The second constraint is that each city $c$ must be visited exactly once within the time steps $t\\in \\{0,\\ldots,N-1\\}$. This condition can be forced by the penalty term \n",
    "$$\n",
    "P_{city} = \\sum_{c=0}^{N - 1} \\left(1 - \\sum_{t=0}^{N - 1} x_{t,c} \\right)^2.\n",
    "$$\n",
    "\n",
    "Finally, the optimization target for this problem is to find a roundtrip which minimizes the overall traveled distance, i.e. to minimize\n",
    "$$\n",
    "P_{distance} = \\sum_{t=0}^{N - 1} \n",
    "                \\sum_{c_0=0}^{N - 1} \n",
    "                 \\sum_{c_1=0 \\\\c_0 \\ne c_1}^{N - 1} \n",
    "                  \\Delta_{c_0, c_1} x_{t,c_0} x_{(t+1) \\% N, c_1}.\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Single bit selection: 1-hot\n",
    "The traveling salesman problem searches for best round trips. A round trip is a journey that visits every city exactly once. The bit model above describes the attendance at a certain point of time $t$ in a city $c$ by a true bit $x_{t,c}$. At a certain point of time $t$ the salesman has to be in exactly one city, i.e. exactly one of the bits $C_t := \\{x_{t,c} | c=0,1,...N-1\\}$ has to be switched on. Such a collection of bits is called a 1-hot group, more precisely a 1-way-1-hot group.\n",
    "\n",
    "### 1-way-1-hot\n",
    "A 1-way-1-hot group can be any collection of bits in the bit vector; it defines the constraint, that in a valid solution exactly one bit of this collection is switched on. This can also be expressed as a QUBO term, which is the square of the difference of 1 and the sum of all bits contained in the 1-way-1-hot group: $\\left ( 1 - \\sum_{x \\in C} x \\right ) ^2$. The definition of the 1-way-1-hot group is done in addition to the polynomial term and it guarantees, that the solution is valid w.r.t. the 1-hot constraint regardless the weighting of the polynomial term. It also speeds up the search process for minimum solutions, since bit states with a number of true bits in a 1-way-1-hot group different from 1 are not assumed; the random walk moves from a valid state to another valid state by switching one bit off and another bit on in one step.\n",
    "\n",
    "There are different ways to define 1-way-1-hot groups. It is always recommended to use the symbolic bit names of a ``BitArrayShape``. A ``BitArrayShape`` variable can be declared a 1-way-1-hot group. The decision bits for a city at a certain point in time can be stored in variables ``time_0``, ``time_1``,... ``time_N-1`` declared as 1-way-1-hot groups: ``BitArrayShape(name='time_0', shape=[self.N], one_hot=OneHot.one_way)``. This makes the declaration a little more complicated. But there is a comfortable behavior applicable to the 2-dimensional variable ``x``: for multi-dimensional\n",
    "arrays the variation in the last dimension defines the 1-hot groups, i.e. all collections of bits identical in all but the last dimension form a 1-way-1-hot group. So we can define all rows in our matrix as 1-way-1-hot groups in the following compact form:\n",
    "\n",
    "```python\n",
    "self.qubo_vs = VarShapeSet(\n",
    "    BitArrayShape(\n",
    "        name='x', shape=(self.N, self.N),\n",
    "        one_hot=OneHot.one_way\n",
    "    )\n",
    "```\n",
    "\n",
    "If we want to declare the time decisions for the cities as 1-way-1-hot groups, we can use the ``OneHotGroup`` class. It allows a symbolic declaration of any subset of our bit vector just as a list of symbolic selections. To define the first column as 1-hot group one can write:\n",
    "\n",
    "```python\n",
    "first_column = OneHotGroup(('x', 0, 0), ('x', 1, 0), ... ('x', N-1, 0))\n",
    "```\n",
    "\n",
    "Using a generated list for the arguments the same can be defined by:\n",
    "```python\n",
    "first_column = OneHotGroup(*[('x', t, 0) for t in range(self.N)]) \n",
    "```\n",
    "\n",
    "It is also allowed to write lists or ranges to the coordinate places of the selection tuples, which creates the cartesian product of the specified coordinate sets as one hot group. Here we could write:\n",
    "```python\n",
    "first_column = OneHotGroup(('x', range(self.N), 0)) \n",
    "```\n",
    "\n",
    "If you want to specify the complete range of a coordinate, then None can be used to say no restriction in this coordinate:\n",
    "```python\n",
    "first_column = OneHotGroup(('x', None, 0)) \n",
    "```\n",
    "\n",
    "So we can use the latest short formulation to define the ``VarShapeSet`` with columns as one hot groups:\n",
    "```python\n",
    "self.qubo_vs = VarShapeSet(\n",
    "    BitArrayShape(\n",
    "        name='x', shape=(self.N, self.N)\n",
    "    ),\n",
    "    one_hot_groups = [OneHotGroup(('x', None, c)) for c in range(self.N)]\n",
    ")\n",
    "```\n",
    "\n",
    "This demonstrates the usage of 1-way-1-hot groups either to define constraints for the city decision in each point of time or the time decision for each city. Since the two partitionings overlap only one can be used. For traveling salesman we want both constraints simultaneously which is possible by a 2-way-1-hot declaration.\n",
    "\n",
    "### 2-way-1-hot\n",
    "A quadratic 2-dimensional ``BitArrayShape`` can be declared as ``OneHot.two_way``. In that case in every column and row of the array exactly one bit of this variable is ``True`` in a valid solution. This is the needed for the traveling salesman problem. The ``VarShapeSet`` can then be defined by:\n",
    "```python\n",
    "self.qubo_vs = VarShapeSet(\n",
    "    BitArrayShape(\n",
    "        name='x', shape=(self.N, self.N),\n",
    "        one_hot=OneHot.two_way\n",
    "    )\n",
    "```\n",
    "\n",
    "With the following Optimizer block you can try the 1-hot settings. If you use the 2-way-1-hot you can enlarge the weight for the distance and still get always valid solutions. This delivers much faster optimization, e.g. with weight for distance of 10 you can get optimum result for a 10 city TSP in 20 steps.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from src.Optimizer_Demo_TSP_Model import *\n",
    "optimizer = Optimizer(Optimizer_Demo_TSP_Model(persistent_file='M_09_Extended_Optimization_Model_TSP_0'), \n",
    "                      read_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Automatic Weighting: Separate Penalty Polynomial\n",
    "In many optimization problems formulated as QUBO a penalty polynomial is part of the model. This is typically a quadratic polynomial that should result to zero for valid solutions. The weight factor in front of the penalty polynomial has to be chosen sufficiently large, to guarantee the fulfillment of constraints with high probability. Balancing the weight factors of the cost and the penalty part of the QUBO can be automated with Digital Annealer version 3.\n",
    "\n",
    "The penalty polynomial can be specified as a parameter of the ``minimize`` method. In the traveling salesman problem we have the distance polynomial as cost qubo and the time and city polynomials as non-negative penalty polynomials, that have to return zero for valid solutions. :\n",
    "\n",
    "``` python\n",
    "my_solver = QUBOSolverDAv3()\n",
    "my_solver.minimize(qubo=self.P_distance, penalty_qubo=self.P_time + self.P_city)\n",
    "```\n",
    "\n",
    "To use this feature from Optimizer the ``build_qubo`` method has to return a dictionary with keys ``qubo`` and ``penalty_qubo``. Therefore the ``build_qubo`` has to look like this:\n",
    "\n",
    "``` python\n",
    "def build_qubo(self, ...):\n",
    "    ...\n",
    "    return {'qubo': self.P_distance, 'penalty_qubo': self.P_time + self.P_city}\n",
    "```\n",
    "\n",
    "Using the following block you can inspect a process for running TSP on 100 cities. In the Anneal Tracker tab you can find a visualization of the progress in the optimization. First the penalty energy (red curve) is reduced before the distance curve (blue curve) comes down for distance minimization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from src.Optimizer_Demo_TSP_Model import *\n",
    "optimizer = Optimizer(Optimizer_Demo_TSP_Model(persistent_file='M_09_Extended_Optimization_Model_TSP_1'), \n",
    "                      immediate_load=True, read_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Inequalities\n",
    "Constraints in optimization problems are often described as equalities or inequalities over the decision variables:\n",
    "\n",
    "$$\n",
    "\\sum_{n=0}^{N-1} a_n x_n = b\\\\\n",
    "\\sum_{n=0}^{N-1} a_n x_n \\le b\n",
    "$$\n",
    "\n",
    "In QUBO models linear equality constraints are realized as penalty polynomials by squaring the equivalent equation to 0:\n",
    "\n",
    "$$\n",
    "H_{eq}(\\mathbf x) = \\left(b-\\sum_{n=0}^{N-1} a_n x_n \\right)^2 .\n",
    "$$\n",
    "\n",
    "Linear inequalities are transformed to equations by introducing a slack variable s, that fills the difference between the left hand side and limit on the right hand side. This derived equation is then handled as in the first case:\n",
    "\n",
    "$$\n",
    "H_{ineq}(\\mathbf x) = \\left(b-s-\\sum_{n=0}^{N-1} a_n x_n \\right)^2 .\n",
    "$$\n",
    "\n",
    "This is a valid mathematical representation for equalities and inequalities. In practical work especially the inequalities become a challenge when the model size grows. The slack variable has to be discretized. This requires additional bits. Depending on the representation of the slack variable it needs often many bit flips to accommodate the slack variable to the required value. Longer optimization processes are necessary. To overcome these issues the Digital Annealer V3 offers a direct formulation for linear inequalities.\n",
    "\n",
    "The dadk library supports the construction of linear inequalities. First the inequality has to be transformed to the form $ p(\\mathbf x) \\le 0$ for a linear polynomial $p$. In our case that results in:\n",
    "$$\n",
    "\\sum_{n=0}^{N-1} a_n x_n - b \\le 0 .\n",
    "$$\n",
    "\n",
    "\n",
    "Now the ``BinPol`` polynomial of the left hand side is used to define an ``Inequality`` object. The inequality above can be defined by the following Python code:\n",
    "```python\n",
    "inequality_0 = Inequality(BinPol.sum(Term(a[n], ('x', n)) for n in range(self.N)) - b)\n",
    "```\n",
    "For better readability the same inequality can be expressed by the operators ``<=`` or ``>=``:\n",
    "```python\n",
    "inequality_0 = BinPol.sum(Term(a[n], ('x', n)) for n in range(self.N)) <= b\n",
    "```\n",
    "\n",
    "The list of inequalities can be specified as a parameter of the ``minimize`` method:\n",
    "\n",
    "``` python\n",
    "my_solver = QUBOSolverDAv3()\n",
    "my_solver.minimize(qubo=self.HQ, inequalities=[inequality_0])\n",
    "```\n",
    "\n",
    "To use this feature from Optimizer the ``build_qubo`` method has to return a dictionary with keys ``qubo`` and ``inequalities``. Therefore the ``build_qubo`` has to look like this:\n",
    "\n",
    "``` python\n",
    "def build_qubo(self, ...):\n",
    "    ...\n",
    "    return {'qubo': self.HQ, 'inequalities': [inequality_0]}\n",
    "```\n",
    "In the following block a simple optimization problem is recorded. Bits from 0 to 11 are created and it is searched for solution which have two bits switched on and for which the sum $\\sum_{n=0}^{11} n x_n$ lies between 8 and 12."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from src.Optimizer_Inequality_Model import *\n",
    "optimizer = Optimizer(Demo_DA_inequalities(persistent_file='M_09_Extended_Optimization_Model_Inequalities_0.dao'), \n",
    "                      immediate_load=True, read_only=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
