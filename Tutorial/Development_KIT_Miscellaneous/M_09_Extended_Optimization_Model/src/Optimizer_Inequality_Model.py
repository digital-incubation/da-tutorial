from dadk.Optimizer import *

class Demo_DA_inequalities(OptimizerModel):

    def __init__(self, persistent_file="Demo_DA_inequalities.dao"):
        super().__init__(
            name='Demo_DA_inequalities',
            persistent_file=persistent_file,
            load_parameter=[
                {'name': 'N', 'type': 'int_slider', 'value': 12, 'min': 2, 'max': 200,
                 'label': 'Size', 'description': 'problem size'}
            ]
        )

    def load(self, N=16, silent=False):
        self.N = N

    def build_qubo(self, silent=False):

        qubo = BinPol.exactly_n_bits_on(n=2, bits=list(range(self.N)))
        print('\n# exactly 2 bit on')
        print(qubo)

        # sum <= 12
        inequality1 = Inequality(BinPol.sum(Term(i, i) for i in range(self.N)) - 12)
        # inequality1 = BinPol.sum(Term(i, (i,)) for i in range(self.N)) <= 12
        print('\n# sum <= 12')
        print(inequality1)

        # sum >= 8
        inequality2 = BinPol.sum(Term(i, i) for i in range(self.N)) >= 8
        print('\n# sum >= 8')
        print(inequality2)

        return {'qubo': qubo, 'penalty_qubo': qubo, 'inequalities': [inequality1, inequality2]}

    def prep_result(self, solution_list:SolutionList, silent=False):
        for solution in solution_list.get_sorted_solution_list():
            configuration = solution.configuration
            solution_sum = sum([i for i in range(self.N) if configuration[i]])
            valid = 'valid' if 8 <= solution_sum <= 12 else 'not valid'
            print(f'{solution.frequency:4d} x {solution.energy:4d} (energy)', configuration,
                  f"{' + '.join([f'{i}*{configuration[i]}' for i in range(self.N)])} = {solution_sum:4d} is {valid}")
