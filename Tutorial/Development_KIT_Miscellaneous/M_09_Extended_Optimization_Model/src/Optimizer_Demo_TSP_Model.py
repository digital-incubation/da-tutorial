import matplotlib.pyplot as plt

from dadk.Optimizer import *
from dadk.utils.TSPUtils import *

from random import random, seed

from tabulate import tabulate

class Optimizer_Demo_TSP_Model(OptimizerModel):
    ##################################################################################################################
    # constructor (defines GUI and test declaration)
    ##################################################################################################################
    def __init__(self, persistent_file="Optimizer_Demo_TSP.dao"):

        super().__init__(
            name='Demo_UIFunctions',
            load_parameter=[
                {'name': 'N', 'label': 'Size',
                 'description': 'Set the problem size by number of cities to be vsited.',
                 'type': 'int_slider', 'value': 10, 'min': 3, 'max': 200,
                 'on_change': "fixed_city.max = fixed_time.max = new_value - 1"
                 },
                {'name': 'shape', 'label': 'Shape',
                 'description': 'Select a shape to arrange the generated cities on a map.',
                 'type': 'select', 'value': 'circle', 'options': ['circle', 'triangle', 'random'],
                 'on_change': 'random_seed.visible = new_value == "random"'
                 },
                {'name': 'random_seed', 'label': 'Random seed',
                 'description': 'Random see allows reproducible creation of random maps.',
                 'type': 'float_bounded', 'value': 42.0, 'min': 0.0, 'max': 10000.0
                 },
                {'name': 'fixed_event', 'label': 'Fixed event',
                 'description': 'Define one city and time step to be used in roundtrip.',
                 'type': 'check', 'value': False,
                 'on_change': 'fixed_city.visible = fixed_time.visible = new_value'
                 },
                {'name': 'fixed_city', 'label': 'Fixed city',
                 'description': 'Select the city to be fixed as start point.',
                 'type': 'int_slider', 'value': 0, 'min': 0, 'max': 90,
                 },
                {'name': 'fixed_time', 'label': 'Fixed time',
                 'description': 'Select the time step, whe the fixed city has to be visited.',
                 'type': 'int_slider', 'value': 0, 'min': 0, 'max': 90,
                 }
            ],
            build_qubo_parameter=[
                {'type': 'begin_group', 'label': 'Features'},
                {'name': 'one_hot', 'label': '1-hot',
                 'description': 'Select the 1-hot feature to be tested.',
                 'type': 'radio', 'value': 'None', 'options': [('No 1-hot', 'None'),
                                                               ('1-way-1-hot for cities', '1-way-cities'),
                                                               ('1-way-1-hot for times', '1-way-times'),
                                                               ('2-way-1-hot', '2-way')]
                 },
                {'name': 'penalty_polynomial', 'label': 'Penalty polynomial',
                 'description': 'Check box to use separate penalty polynomial.',
                 'type': 'check', 'value': False,
                 },
                {'type': 'begin_group', 'label': 'Norming'},
                {'name': 'norm_time', 'label': 'Norm $P_{time}$',
                 'description': 'Check box to norm time polynomial to maximum absolute coefficient value of 1',
                 'type': 'check', 'value': True,
                 },
                {'name': 'norm_city', 'label': 'Norm $P_{city}$',
                 'description': 'Check box to norm city polynomial to maximum absolute coefficient value of 1',
                 'type': 'check', 'value': True
                 },
                {'name': 'norm_distance', 'label': 'Norm $P_{distance}$',
                 'description': 'Check box to norm distance polynomial to maximum absolute coefficient value of 1',
                 'type': 'check', 'value': True
                 },
                {'type': 'begin_group', 'label': 'Scaling'},
                {'name': 'factor_A', 'label': 'A (factor for $P_{time}$)',
                 'description': 'Scaling factor A for time polynomial',
                 'type': 'float_slider', 'value': 2.1, 'min': 0.0, 'max': 10 ** 2, 'step': 0.01
                 },
                {'name': 'factor_B', 'label': 'B (factor for $P_{city}$)',
                 'description': 'Scaling factor B for city polynomial',
                 'type': 'float_slider', 'value': 2.1, 'min': 0.0, 'max': 10 ** 2, 'step': 0.01
                 },
                {'name': 'factor_C', 'label': 'C (factor for $P_{distance}$)',
                 'description': 'Scaling factor C for distance polynomial',
                 'type': 'float_slider', 'value': 1.0, 'min': 0.0, 'max': 10 ** 2, 'step': 0.01
                 },
                {'name': 'factor_Z', 'label': 'Z (global factor)',
                 'description': 'Scaling factor for combined polynomial',
                 'type': 'float_log_slider', 'value': 1000.0, 'min': -3, 'max': 20, 'base': 10.0, 'step': 0.1,
                 'format': '.3e'
                 },
                {'type': 'begin_group', 'label': 'Show'},
                {'name': 'show_polynomials', 'label': 'Show polynomials',
                 'description': 'Show polynomials. Not recommended for bigger scenarios.',
                 'type': 'check', 'value': False
                 },
                {'type': 'end_group'}
            ],
            #
            # Define default values for the "Solve annealing" tab.
            # The user can modify these fields, or press the button
            # "Default" and get these values again.
            #
            default_solver_parameter={
                'number_iterations': 2000,
                'temperature_start': 5000,
                'temperature_end': 5,
                'offset_increase_rate': 150.0},
            #
            # Define calculated values for the "Solve annealing" tab.
            # The user CAN'T modify these fields any more. The value
            # shown here is a default, which is will be overwritten with
            # the methode self.Optimizer.set_calculated_solver_parameter(...)
            # see end of build_qubo for an example.
            #
            # calculated_solver_parameter={'temperature_start': 10000},
            demo_solver_parameter=[{'name': 'number_iterations', 'tags': ['demo1', 'demo2']},
                                   {'name': 'number_runs', 'tags': ['demo1']},
                                   'number_replicas'
                                   ],
            persistent_file=persistent_file,
            solvers=["CPU", "DAv3", "DAv4"]
        )

    ##################################################################################################################
    # load method (defines content of tab Setup scenario)
    ##################################################################################################################
    def load(self, N=10, shape='circle', random_seed=42, fixed_event=False, fixed_city=0, fixed_time=0, silent=False):

        # store paramters in model attributes
        self.N = N
        self.fixed_event = fixed_event
        self.fixed_city = min(fixed_city, N - 1)
        self.fixed_time = min(fixed_time, N - 1)

        # calculate distance table
        if shape == 'circle':
            self.cities = [[100 * math.sin(c * 2 * math.pi / N), 100 * math.cos(c * 2 * math.pi / N)] for c in range(N)]

        elif shape == 'triangle':
            self.cities = [[100 * c / (N - 2), 0] for c in range(N - 1)] + [[50, 100]]

        elif shape == 'random':
            seed(random_seed)
            self.cities = [[100 * random(), 100 * random()] for c in range(N)]

        self.distance_table = [[math.sqrt(
            (self.cities[c_1][0] - self.cities[c_0][0]) ** 2 + (self.cities[c_1][1] - self.cities[c_0][1]) ** 2)
            for c_0 in range(N)] for c_1 in range(N)]

        # report actions to tab
        self.LOGGER.info(f'Model for circular map with {self.N} cities created.')

        if not silent:
            print(f"""
Model for map created:
    Number of cities: {self.N:3d}
    Shape:            {shape}
    Random seed:      {random_seed:7.3f}
    Fixed event:      {self.fixed_event}
    Fixed city:       {self.fixed_city:3d}
    Fixed time:       {self.fixed_time:3d}
""")

    ##################################################################################################################
    # build_qubo method (defines content of tab Build QUBO)
    ##################################################################################################################
    def build_qubo(
            self,
            one_hot: str = 'None', penalty_polynomial: bool = False,
            norm_time: bool = False, norm_city: bool = False, norm_distance: bool = False,
            factor_A: float = 1.0, factor_B: float = 1.0, factor_C: float = 1.0, factor_Z: float = 1.0,
            show_polynomials: bool = False,
            silent: bool = False
    ):

        # variable bit as default are initialized with -1
        constant_bits = np.full((self.N, self.N), -1, np.int8)
        if self.fixed_event:
            # all bits for fixed_time are set to constant 0
            constant_bits[self.fixed_time, :] = 0
            # all bits for fixed city are set to constant 0
            constant_bits[:, self.fixed_city] = 0
            # bit for fixed_time in fixed_city is set to constant 1
            constant_bits[self.fixed_time, self.fixed_city] = +1
        # square array x with constant bits is created as VarShapeSet
        one_hot_groups = []
        if one_hot == '1-way-cities':
            var_shape_set = VarShapeSet(
                BitArrayShape(
                    name='x', shape=(self.N, self.N),
                    constant_bits=constant_bits,
                    one_hot=OneHot.one_way,
                    axis_names=['Time', 'City']
                )
            )
        elif one_hot == '1-way-times':
            var_shape_set = VarShapeSet(
                BitArrayShape(
                    name='x', shape=(self.N, self.N),
                    constant_bits=constant_bits,
                    axis_names=['Time', 'City']
                ),
                # one_hot_groups = [OneHotGroup(*[('x', t, c) for t in range(self.N)]) for c in range(self.N)]
                # one_hot_groups = [OneHotGroup(('x', range(self.N), c)) for c in range(self.N)]
                one_hot_groups=[OneHotGroup(('x', None, c)) for c in range(self.N)]
            )
        elif one_hot == '2-way':
            var_shape_set = VarShapeSet(
                BitArrayShape(
                    name='x', shape=(self.N, self.N),
                    constant_bits=constant_bits,
                    one_hot=OneHot.two_way,
                    axis_names=['Time', 'City']
                )
            )
        else:
            var_shape_set = VarShapeSet(
                BitArrayShape(
                    name='x', shape=(self.N, self.N),
                    constant_bits=constant_bits,
                    one_hot=OneHot.no_way,
                    axis_names=['Time', 'City']
                )
            )

        BinPol.freeze_var_shape_set(var_shape_set)

        # -P_time-------------------------------------------------------------------------------

        self.P_time = BinPol.sum(
            BinPol.exactly_1_bit_on(bits=[('x', t, c) for c in range(self.N)])
            for t in range(self.N))
        if norm_time:
            self.P_time *= 0.5
        self.P_time *= factor_A

        # -P_city-------------------------------------------------------------------------------

        self.P_city = BinPol.sum(
            BinPol.exactly_1_bit_on(bits=[('x', t, c) for t in range(self.N)])
            for c in range(self.N))
        if norm_city:
            self.P_city.normalize()
        self.P_city *= factor_B

        # -P_distance---------------------------------------------------------------------------

        self.P_distance = BinPol.sum(
            Term(self.distance_table[c_0][c_1], (('x', t, c_0), ('x', (t + 1) % self.N, c_1)))
            for c_0 in range(self.N) for c_1 in range(self.N) if c_0 != c_1 for t in range(self.N))
        if norm_distance:
            self.P_distance.normalize()
        self.P_distance *= factor_C

        # -HQ-----------------------------------------------------------------------------------

        self.HQ = factor_Z * (self.P_time + self.P_city + self.P_distance)

        self.recommended_scale_factor_16 = self.HQ.get_scale_factor(bit_precision=16) * factor_Z
        self.recommended_scale_factor_32 = self.HQ.get_scale_factor(bit_precision=32) * factor_Z
        self.recommended_scale_factor_64 = self.HQ.get_scale_factor(bit_precision=64) * factor_Z

        if not silent:
            print(f"""
    Polynomial created 
    Number of bits:               {self.HQ.N: 5d}
    One Hot:                      {one_hot}
    Scale factor Z:               {factor_Z: .3e}
    Recommended scale factor 16:  {self.recommended_scale_factor_16: .3e}
    Recommended scale factor 32:  {self.recommended_scale_factor_32: .3e}
    Recommended scale factor 64:  {self.recommended_scale_factor_64: .3e}
    """)
            if show_polynomials:
                poly_tabs = OutputTab('P_time', 'P_city', 'P_distance', 'HQ')
                with poly_tabs.tab_out['P_time']:
                    print('A * P_time: ', self.P_time, '\n')
                with poly_tabs.tab_out['P_city']:
                    print('B * P_city: ', self.P_city, '\n')
                with poly_tabs.tab_out['P_distance']:
                    print('C * P_distance: ', self.P_distance, '\n')
                with poly_tabs.tab_out['HQ']:
                    print('Z * HQ: ', self.HQ, '\n')
                poly_tabs.display()

        if penalty_polynomial:
            return {'qubo': factor_Z * self.P_distance, 'penalty_qubo': factor_Z * (self.P_time + self.P_city)}

    ##################################################################################################################
    # prep_result method (evaluates result of annealing defines content of tab Solve annealing)
    ##################################################################################################################
    def prep_result(self, solution_list:SolutionList, silent=False):

        self.solution = solution_list.min_solution # <-- best solution
        self.route = None

        e_HQ = self.HQ.compute(self.solution.configuration)
        e_time = self.P_time.compute(self.solution.configuration)
        e_city = self.P_city.compute(self.solution.configuration)
        e_distance = self.P_distance.compute(self.solution.configuration)

        print('Energy of QUBO / parts in best solution (P_time and P_city should be zero)')
        print("HQ         = %10.2F" % e_HQ)
        print("P_time     = %10.2F" % e_time)
        print("P_city     = %10.2F" % e_city)
        print("P_distance = %10.2F" % e_distance)
        print("")

        if e_time < 0.01 and e_city < 0.01:

            self.route = [min(c if self.solution['x'][t, c] else self.N for c in range(self.N)) for t in range(self.N)]

            distances = [self.distance_table[self.route[t]][self.route[(t + 1) % self.N]] for t in range(self.N)]
            self.output_table = tabulate(zip(range(self.N),
                                             self.route,
                                             self.route[1:] + self.route[:1],
                                             distances,
                                             np.cumsum(distances)
                                             ),
                                         ['t', 'from', 'to', 'distance', 'total'], tablefmt="html")
            display(HTML(self.output_table))
        else:
            print("invalid solution, try again!")

    ##################################################################################################################
    # report method (defines content of Report tab)
    ##################################################################################################################
    def report(self, silent=False):

        if self.route is not None:
            self.solution['x'].draw(figsize=(6.0, 6.0))

    def report_title(self):
        return "Bit map report"

    ##################################################################################################################
    # draw method (defines content of visualization tab)
    ##################################################################################################################
    def draw(self):

        if self.route is not None:
            TSPUtils.plot_route(coordinates=self.cities, route=self.route)
