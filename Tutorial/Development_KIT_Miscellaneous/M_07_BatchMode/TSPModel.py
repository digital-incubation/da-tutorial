from dadk.Optimizer import *
from dadk.utils.TSPUtils import *
from dadk.utils.CoordinatesUtils import *

SIZE = 100


class TSPModel(OptimizerModel):


    def __init__(self, persistent_file="TSPModel.dao"):
        OptimizerModel.__init__(
            self,
            name='TSPModel',
            persistent_file=persistent_file,
            load_parameter=[
                {'name': 'N', 'type': 'int_slider', 'value': 12, 'min': 0, 'max': 90,
                 'label': 'Size', 'description': 'problem size'},
                {'name': 'special_case', 'type': 'check', 'value': False,
                 'label': 'cities on a circle',
                 'on_change': 'random_seed.visible = not special_case.value'},
                {'name': 'random_seed', 'value': 42, 'type': 'int_slider', 'min': 0, 'max': 9999, 'step': 1,
                 'label': 'Random seed', 'description': 'Seed for random number generation'}],
            build_qubo_parameter=[
                {'name': 'factor_rules', 'type': 'float_slider', 'value': 5000.0, 'min': 0.0, 'max': 10 ** 4,
                 'label': 'factor rules'},
                {'name': 'factor_distance', 'type': 'float_slider', 'value': 20.0, 'min': 0.0, 'max': 10 ** 3,
                 'label': 'factor distance'}])


    def load(self, N=16, special_case: bool = False, random_seed=42, silent=False):

        self.N = N
        self.special_case = special_case
        self.random_seed = random_seed

        random.seed(random_seed)

        self.coordinates = CoordinatesUtils.generate_coordinates(quantity=self.N, size=SIZE, special_case=special_case)

        if not silent:
            CoordinatesUtils.plot_coordinates(
                self.coordinates,
                xlim=(-SIZE, +SIZE),
                ylim=(-SIZE, +SIZE),
                filename=None if not self.Optimizer.batch_mode else self.persistent_file.replace('.dao', '_map.png'))


    def build_qubo(self, factor_rules: float, factor_distance: float, silent: bool = False):

        self.HQ, self.P_time, self.P_city, self.P_distance = TSPUtils.create_QUBOS(
            coordinates=self.coordinates, factor_rules=factor_rules, factor_distance=factor_distance)

        if not silent:
            print('HQ has %d bits' % self.HQ.N)

        self.Optimizer.set_solver_parameter(number_iterations_CPU=self.HQ.N, number_runs_CPU=1, temperature_interval=self.N)


    def prep_result(self, solution_list:SolutionList, silent=False):

        configuration = solution_list.min_solution.configuration

        if self.P_time.compute(configuration) == 0 and self.P_city.compute(configuration) == 0:
            if not silent:
                x_data = solution_list.min_solution.extract_bit_array('x').data
                TSPUtils.plot_route(
                    coordinates=self.coordinates,
                    route=[min(c if x_data[t][c] else self.N for c in range(self.N)) for t in range(self.N)],
                    xlim=(-SIZE, +SIZE),
                    ylim=(-SIZE, +SIZE),
                    filename=None if not self.Optimizer.batch_mode else self.persistent_file.replace('.dao', '_route.png'))
